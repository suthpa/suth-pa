﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmAssesseeList : DevExpress.XtraEditors.XtraForm
    {
        public frmAssesseeList()
        {
            InitializeComponent();
        }
        DataTable dtAssessor = new DataTable();
        DataTable dtAssessee = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();
        int RNDNO;
        private void frmAssesseeList_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            GlobalVariables.FlagRefresh = false;
            ConfigurationController ctlCf = new ConfigurationController();
            RNDNO = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO));
            //dtAssessee.Columns.Clear();

            dtAssessee.Columns.Add("EmployeeID");
            dtAssessee.Columns.Add("EmployeeName");
            dtAssessee.Columns.Add("PositionName");
            dtAssessee.Columns.Add("DepartmentName");
            dtAssessee.Columns.Add("DivisionName");
            dtAssessee.Columns.Add("EmployeeLevelID");
            dtAssessee.Columns.Add("DepartmentUID");
            dtAssessee.Columns.Add("DivisionUID");
            dtAssessee.Columns.Add("LevelName");
            dtAssessee.Columns.Add("isAssessment");
            dtAssessee.Columns.Add("Assm");
            dtAssessee.Columns.Add("GroupName");
            dtAssessee.Columns.Add("NetScore1");
            dtAssessee.Columns.Add("NetScore2");
            dtAssessee.Columns.Add("SortOrder",typeof(int));
            dtAssessee.Columns.Add("Remark");
            dtAssessee.Columns.Add("AssessmentLevelTypeID");
            dtAssessee.Columns.Add("AssessmentLevelTypeName");
            
          
            LoadAssessees();
        }

        private bool OpenAssesseeListForm()
        {

            bool AppAlive;
            AppAlive = true;

            int Bdate, Edate, sToday;
            ConfigurationController ctlCf = new ConfigurationController();

            Bdate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_STARTDATE));
            Edate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_ENDDATE));
            sToday = Convert.ToInt32(BaseClass.ConvertStrDate2YYYYMMDDString(ctlCf.GET_DATE_SERVER().ToString()));
            if (sToday >= Bdate && sToday <= Edate)
            {
                AppAlive = true;
            }
            else
            {
                EmployeeController ctlU = new EmployeeController();

                if (ctlU.User_GetAllowPermission(GlobalVariables.username))
                {
                    AppAlive = true;
                }
                else
                {

                    if (sToday < Bdate)
                    {
                        MessageBox.Show("ยังไม่ถึงกำหนดเวลาประเมิน ท่านสามารถเริ่มทำแบบประเมินได้ตั้งแต่วันที่ 12 - 23 กรกฎาคม 2564", "Notice",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                        AppAlive=false;
                    }

                    if (sToday > Edate)
                    {
                        if (!GlobalVariables.IsAdmin)
                        {
                            MessageBox.Show("หมดเขตระยะเวลาที่ให้ประเมินแล้ว", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        AppAlive=false;
                    }
                }

            }

            //if (GlobalVariables.IsSuperAdmin)
            //{
            //    AppAlive = true;
            //}
            
            return AppAlive;
        }



        private void LoadAssessees()
        {

            //dtAssessor = ctlEmp.Employee_GetAssessorStatus(GlobalVariables.username);

            //if (string.Concat(dtAssessor.Rows[0][0])=="N")
            //{
            //    return;
            //}

            int i,n;
            string iDir,iDiv,iDept;
            DataRow row;
            DataRow rowA;
            DataRow dr;
            DataTable dtA = new DataTable();
            dtAssessee.Rows.Clear();

            //string sAssessorLevelName;
            if (GlobalVariables.AssessmentClass=="C") //เพื่อนร่วมงานจะเอาตำแหน่งสูงสุดเท่านั้น
            {
                dtAssessor = ctlEmp.Employee_GetAssessorTOPDetail(GlobalVariables.BYear, GlobalVariables.username);
            }
            else
            {
                dtAssessor = ctlEmp.Employee_GetAssessorDetail(GlobalVariables.BYear, GlobalVariables.username);
            }
                       

            //ถ้าประเมิน level เดียว ให้เปิด for
            for (i = 0; i < dtAssessor.Rows.Count; i++)
            {
                row = dtAssessor.Rows[i];
                iDir = "";
                iDiv = "";
                iDept = "";

                iDir = row["DirectionUID"].ToString();
                iDiv = row["DivisionUID"].ToString();
                iDept = row["DepartmentUID"].ToString();
                #region "pa 2020"

                //if (row["isInDivision"].ToString() == "Y") iDiv =  row["DivisionUID"].ToString();
                //if (row["isInDepartment"].ToString() == "Y") iDept =row["DepartmentUID"].ToString();


                ////กรณีประเมิน level เดียว เหมือนปี  2019------------------------------
                //switch (row["EmployeeLevelID"].ToString())
                //{
                //    case "1": //ผอ.
                //        dtA = ctlEmp.Employee_GetAssesseeManager(GlobalVariables.BYear,GlobalVariables.username);
                //        break;
                //    case "2": //หัวหน้าฝ่าย
                //        dtA = ctlEmp.Employee_GetAssesseeHeader(GlobalVariables.BYear,iDiv, GlobalVariables.username);
                //        break;
                //    case "3": //หัวหน้าแผนก
                //        dtA = ctlEmp.Employee_GetAssesseesByDepartment(GlobalVariables.BYear,iDept, GlobalVariables.username);
                //        break;
                //}
                ////-----------End 2019---------------------------

                ////กรณีประเมิน 2 level ปี  2020------------------------------
                //switch (row["EmployeeLevelID"].ToString())
                //{
                //    case "1": //ผอ.
                //        dtA = ctlEmp.Employee_GetAssessees2LevelByTOP(GlobalVariables.BYear, GlobalVariables.username);
                //        break;
                //    case "9": //หน.กลุ่มงาน
                //        dtA = ctlEmp.Employee_GetAssessees2LevelByDirector(GlobalVariables.BYear,iDir, GlobalVariables.username);
                //        break;
                //    case "2": //หัวหน้าฝ่าย
                //        dtA = ctlEmp.Employee_GetAssessees2LevelByManager(GlobalVariables.BYear, iDiv, GlobalVariables.username);
                //        break;
                //    case "3": //หัวหน้าแผนก
                //        dtA = ctlEmp.Employee_GetAssessees2LevelByHeader(GlobalVariables.BYear, iDept, GlobalVariables.username);
                //        break;
                //}
                ////-----------End 2020---------------------------
                ///
#endregion
                //กรณีประเมิน หลาย level ปี  2021------------------------------
                switch (row["EmployeeLevelID"].ToString())
                {
                    case "0": //ผอ.
                        dtA = ctlEmp.Employee_GetAssesseesByTOP2021(GlobalVariables.BYear, GlobalVariables.username);
                        break;
                    case "1": //หน.กลุ่มงาน
                        dtA = ctlEmp.Employee_GetAssesseesByDirector2021(GlobalVariables.BYear, iDir, GlobalVariables.username);
                        break;
                    case "2": //หัวหน้าฝ่าย
                        dtA = ctlEmp.Employee_GetAssesseesByManager2021(GlobalVariables.BYear, iDiv, GlobalVariables.username,2, GlobalVariables.AssessmentClass);
                        break;
                    case "3": //หัวหน้าแผนก
                    case "4": //รองหัวหน้า
                        dtA = ctlEmp.Employee_GetAssesseesByHeader2021(GlobalVariables.BYear,iDiv, iDept, GlobalVariables.username,3,GlobalVariables.AssessmentClass);
                        break;
                    default: //ระดับปฏิบัติการ
                        dtA = ctlEmp.Employee_GetAssessees(GlobalVariables.BYear, row["EmployeeLevelID"].ToString(), iDiv, iDept, GlobalVariables.username, GlobalVariables.AssessmentClass);
                        break;
                }
                //-----------End 2021---------------------------



                // dtA = ctlEmp.Employee_GetAssessees(row["LevelAssessee"].ToString(), iDiv, iDept, GlobalVariables.username);

                if (dtA.Rows.Count>0)
                {
                    for (n = 0; n < dtA.Rows.Count; n++)
                    {
                        rowA = dtA.Rows[n];
                        dr = dtAssessee.NewRow();
                        dr[0] = rowA["EmployeeID"];
                        dr[1] = rowA["EmployeeName"];
                        dr[2] = rowA["PositionName"];
                        dr[3] = rowA["DepartmentName"];
                        dr[4] = rowA["DivisionName"];
                        dr[5] = rowA["EmployeeLevelID"];
                        dr[6] = rowA["DepartmentUID"];
                        dr[7] = rowA["DivisionUID"];
                        dr[8] = rowA["LevelName"];
                        dr[9] = rowA["isAssessment"];
                        dr[10] = "Y";
                        dr[11] = rowA["GroupName"];
                        dr[12] = rowA["NetScore1"];
                        dr[13] = rowA["NetScore2"];
                        dr[14] = rowA["SortOrder"];
                        dr[15] = rowA["Remark"];
                        dr[16] = rowA["AssessmentLevelTypeID"];
                        dr[17] = rowA["AssessmentLevelTypeName"];

                        dtAssessee.Rows.Add(dr);
                    }              

                }
            }

            //ctlEmp.xAssessorAssessment_Delete(GlobalVariables.BYear, GlobalVariables.username);

            //int k;
            //for (k = 0; k < dtAssessee.Rows.Count; k++)
            //{
            //    ctlEmp.xAssessorAssessment_Add(GlobalVariables.BYear, GlobalVariables.username, dtAssessee.Rows[k].Field<string>("EmployeeID"));
            //}

          

            if (dtAssessee.Rows.Count > 0)
            {
                dtAssessee = dtAssessee.AsEnumerable().GroupBy(r => new { Col1 = r["AssessmentLevelTypeID"], Col2 = r["EmployeeID"] }).Select(g => g.First()).CopyToDataTable();
            }
            grdEmployee.DataSource = null;
            grdEmployee.DataSource = dtAssessee;


            lblAssessor.Text = "รายชื่อพนักงานที่ต้องถูกประเมิน โดย " + GlobalVariables.UserEmployeeName + " ทั้งหมด " + dtAssessee.Rows.Count.ToString() + " คน"; //  (" + GlobalVariables.UserEmployeeLevelName + ")";


            //grdViewEmployee.Columns[3].Width = 300;
            //grdViewEmployee.Columns[4].BestFit();
            //grdViewEmployee.Columns[5].BestFit();
            //grdViewEmployee.Columns[6].BestFit();
            //grdViewEmployee.Columns[9].Width = 30;
            ////grdViewEmployee.Columns[1].Caption = " ";
            //grdViewEmployee.Columns[10].Width=300;
            //grdViewEmployee.Columns[10].AppearanceCell.ForeColor = Color.Red;
            grdViewEmployee.Columns[3].AppearanceCell.ForeColor = Color.Blue;

            if (OpenAssesseeListForm() == true)
            {
                grdViewEmployee.Columns[9].Visible = true;
            }
            else
            {
                grdViewEmployee.Columns[9].Visible = false;
            }

            grdViewEmployee.BestFitColumns();
        }

        private void grdViewEmployee_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            try
            {
                GridHitInfo hi = view.CalcHitInfo(e.X, e.Y);
                if (hi.Column != null)
                { 
                    if (hi.Column.FieldName == "Assm") //&& view.GetRowCellValue(hi.RowHandle, view.Columns["Assm"]).ToString() == "Y"
                    {
                        if (e.X >= 1010 && e.X <= 1050)
                        {
                            if (hi.InGroupRow == false)
                                view.GridControl.Cursor = Cursors.Hand;
                        }
                        else
                        {
                            view.GridControl.Cursor = Cursors.Default;
                        }
                    }
                    else
                    {
                        view.GridControl.Cursor = Cursors.Default;
                    }

                }
                //if (hi.Column.Caption == "PACS")
                //{
                //    view.GridControl.Cursor = Cursors.Hand;
                //}
            }
            catch
            {
                view.GridControl.Cursor = Cursors.Default;
            }
        }

        private void grdViewEmployee_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) != Keys.Control)
            {
                GridView view = sender as GridView;
                GridHitInfo hi = view.CalcHitInfo(e.Location);
                try
                {
                    if (hi.Column.Name == "colAssessment")
                    {
                        view.FocusedRowHandle = hi.RowHandle;
                        view.FocusedColumn = hi.Column;
                        view.ShowEditor();
                        if (grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString() != "0" && grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString() != "")
                        {
                            DataTable dtC = new DataTable();
                            CompetencyController ctlCom = new CompetencyController();

                            GlobalVariables.AssesseeLevelUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID"));

                            dtC = ctlCom.Evaluation_GetByEmployee(GlobalVariables.BYear, grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString(),  grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID").ToString(),GlobalVariables.username, GlobalVariables.AssessmentClass, RNDNO, Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("AssessmentLevelTypeID")));

                            if (dtC.Rows.Count > 1)
                            {
                                frmCompetencyEmployee frmComp = new frmCompetencyEmployee();
                                frmComp.EmployeeID = grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString();
                                frmComp.EmployeeLevelUID =Convert.ToInt32( grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID"));

                                frmComp.AssessorLevelUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("AssessmentLevelTypeID"));
                                                              
                                frmComp.EmployeeName = grdViewEmployee.GetFocusedRowCellValue("EmployeeName").ToString();
                                frmComp.ShowDialog(this);
                               
                            }
                            else
                            {
                                //เปิดฟอร์ม PA
                                frmAssessment fPA = new frmAssessment();
                                //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                                fPA.EmployeeID = grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString();

                                fPA.AssessorLevelUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("AssessmentLevelTypeID"));

                                fPA.EvaluationUID = dtC.Rows[0].Field<int>("EvaluationUID");
                                if (dtC.Rows[0]["isAssessment"].ToString() == "Y")
                                    fPA.flagEdit = true;

                                fPA.EmployeeName = grdViewEmployee.GetFocusedRowCellValue("EmployeeName").ToString();

                                ////this.Close();
                                ////fPA.MdiParent = this.MdiParent;
                                //fPA.TopMost = true;
                                fPA.ShowDialog();
                               
                            }

                            LoadAssessees();

                            //frmEmployeeRelation2_Load(sender, e);
                        }

                    }
                }
                catch { }
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdViewEmployee_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {

                if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[12]) != null)
                {
                    if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[12]).ToString() == "0")
                    {
                        e.Appearance.BackColor = Color.MintCream;
                    }
                    else if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[12]).ToString() == "2")
                    {
                        e.Appearance.BackColor = Color.AliceBlue;
                    }
                    else if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[12]).ToString() == "3")
                    {
                        e.Appearance.BackColor = Color.LavenderBlush;
                    }
                    else
                    {

                        //e.Appearance.BackColor = Color.White;
                    }
                }
            }
        }
    }
}