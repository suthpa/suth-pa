﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using DevExpress.XtraTabbedMdi;
using DevExpress.XtraEditors;
using System.Net;

namespace SUTHPA
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        Thread th;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            this.Text = "SUTH Performance Appraisal : Suranaree University Of Technology Hospital";
            this.ribbonControl.ApplicationCaption = "SUTH Performance Appraisal : Suranaree University Of Technology Hospital";

            ConfigurationController ctlCf = new ConfigurationController();
            GlobalVariables.BYear = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_PAYEAR));
            
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                Version ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
                this.barVersion.Caption = "v." + string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision)   + " ® 17.11.2021";
            }
            else
            {
                this.barVersion.Caption = "v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + " ® 17.11.2021";
            }

          if ( System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
            {
                var host= Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily ==  System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        this.barIP.Caption = "IP : " +  ip.ToString();
                    }
                }

            }



            //this.barContact.Caption = "<color=#006600>แผนกทรัพยากรมนุษย์ โทร. 6670</color><color=#FF0099> แผนกสารสนเทศ โทร. 3437 </color>";

            barHearderUser.Caption = GlobalVariables.LoginUser +" : "+ GlobalVariables.UserEmployeeName;         
            lblInfoDB.Caption = GlobalVariables.UseDatabase;

            ribbonControl.Pages.GetPageByName("pageAdmin").Visible = false;
            ribbonControl.Pages.GetPageByName("pageReport").Visible = false;

            ribbonControl.GetGroupByName("GroupGrade").Visible = false;
            ribbonControl.GetGroupByName("GroupEmployee").Visible = false;
            ribbonControl.GetGroupByName("GroupManager").Visible = false;
            ribbonControl.GetGroupByName("GroupReport").Visible = false;
            ribbonControl.GetGroupByName("GroupConfig").Visible = false;

            if (GlobalVariables.IsReporter)
            {
                ribbonControl.Pages.GetPageByName("pageReport").Visible = true;
                ribbonControl.GetGroupByName("GroupReport").Visible = true;
            }

            if (GlobalVariables.AssessorLevelUID >=1 && GlobalVariables.AssessorLevelUID <=3)
            {
                ribbonControl.Pages.GetPageByName("pageReport").Visible = true;
                ribbonControl.GetGroupByName("GroupManager").Visible = true;
            }
                     
            
             if (GlobalVariables.IsAdmin)
            {
                ribbonControl.Pages.GetPageByName("pageReport").Visible = true;
                ribbonControl.Pages.GetPageByName("pageAdmin").Visible = true;
                ribbonControl.GetGroupByName("GroupEmployee").Visible = true;
                ribbonControl.GetGroupByName("GroupManager").Visible = true;
                ribbonControl.GetGroupByName("GroupReport").Visible = true;

                ribbonControl.Items["mnuRPTManagerScore"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonControl.Items["mnuRPTManagerFinalScore"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonControl.GetGroupByName("GroupConfig").Visible = true;

            }
            if (GlobalVariables.IsSuperAdmin)
            {
                ribbonControl.Pages.GetPageByName("pageReport").Visible = true;
                ribbonControl.GetGroupByName("GroupGrade").Visible = true;
                ribbonControl.Pages.GetPageByName("pageAdmin").Visible = true;
                ribbonControl.GetGroupByName("GroupManager").Visible = true;
                ribbonControl.Items["mnuRPTManagerScore"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonControl.Items["mnuRPTManagerFinalScore"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonControl.GetGroupByName("GroupConfig").Visible = true;

            }
            else
            {
                ribbonControl.Items["mnuPrintAsm"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }

            if ( GlobalVariables.AssessorLevelUID >= 3)
            {
                                ribbonControl.Items["mnuRPTManagerScore"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonControl.Items["mnuRPTManagerFinalScore"].Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }



            frmNote fN = new frmNote();
            fN.MdiParent = this;
            fN.Show();


            //OpenAssesseeListForm();
            //frmAssesseeList frmEmp = new frmAssesseeList();
            //frmEmp.MdiParent = this;
            //frmEmp.Show();


        }

        private void Close_ChildForm(string fname)
        {
            //Form f = default(Form);
            foreach (Form f in this.MdiChildren)
            {
                if (f.Name==fname)
                {
                    f.Close();
                }                
            }
   //         try
   //         {         
   //         foreach (DevExpress.XtraEditors.XtraForm f2 in this.MdiChildren)
   //         {
   //             if (f2.Name == fname)
   //             {
   //                 f2.Close();
   //             }
   //         }
   //}
   //         finally { }
        }

        private void mnuEmployee_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             
            frmEmployeeInfo frmEmp = new frmEmployeeInfo();
Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void menuAssesseeList_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //frmAssesseeList frmEmp = new frmAssesseeList();

            frmAsmMain frmEmp = new frmAsmMain();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void OpenAssesseeListForm()
        {
            //Close_ChildForm(frmEmp.Name);

            int Bdate, Edate, sToday;
            ConfigurationController ctlCf = new ConfigurationController();

            Bdate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_STARTDATE));
            Edate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_ENDDATE));
            sToday = Convert.ToInt32(BaseClass.ConvertStrDate2YYYYMMDDString(ctlCf.GET_DATE_SERVER().ToString()));
            if (sToday >= Bdate && sToday <= Edate)
            {
                frmAssesseeList frmEmp = new frmAssesseeList();
                Close_ChildForm(frmEmp.Name);
                frmEmp.MdiParent = this;
                frmEmp.Show();
            }
            else
            {
                EmployeeController ctlU = new EmployeeController();

                if (ctlU.User_GetAllowPermission(GlobalVariables.username))
                {
                    frmAssesseeList frmEmp = new frmAssesseeList();
                    Close_ChildForm(frmEmp.Name);
                    frmEmp.MdiParent = this;
                    frmEmp.Show();
                }
                else
                { 

                if (sToday < Bdate)
                {
                    MessageBox.Show("ยังไม่ถึงกำหนดเวลาประเมิน ท่านสามารถเริ่มทำแบบประเมินได้ตั้งแต่วันที่ 12 - 23 กรกฎาคม 2564", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                }

                if (sToday > Edate)
                {
                    if (!GlobalVariables.IsAdmin)
                    {
                        MessageBox.Show("หมดเขตระยะเวลาที่ให้ประเมินแล้ว", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    return;
                }
            }

            }
        }

        private void mnuEmpNotAssessment_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
            frmAssessmentStatus frmEmp = new frmAssessmentStatus();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuRptScore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมิน (คะแนนดิบ)";
            GlobalVariables.FagRPT = "SCORE";
            frmReportCondition frmEmp = new frmReportCondition();
            Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuReportAvg_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            GlobalVariables.ReportTitle = "รายงานคะแนนสรุป (Step by Step)";
            //GlobalVariables.FagRPT = "AVG";
            GlobalVariables.FagRPT = "FINALEX";
            frmReportCondition frmEmp = new frmReportCondition();
 Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuRPTFinalScore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
    
            GlobalVariables.ReportTitle = "รายงานคะแนนสุทธิ";
            GlobalVariables.FagRPT = "FINAL";
            frmReportCondition frmEmp = new frmReportCondition();
        Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuRPTManagerScore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
       
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินสำหรับหัวหน้ากลุ่ม/ฝ่าย";
            if (GlobalVariables.IsDirector)
            {
                GlobalVariables.FagRPT = "DIRSCORE";
            }
            else {
                GlobalVariables.FagRPT = "MANSCORE";
            }

            frmReportCondition frmEmp = new frmReportCondition();
     Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuRPTManagerFinalScore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
         
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินสุทธิ (Final) สำหรับหัวหน้ากลุ่ม/ฝ่าย";
            if (GlobalVariables.IsDirector)
            {
                GlobalVariables.FagRPT = "DIRFINAL";
            }
            else
            {
                GlobalVariables.FagRPT = "MANFINAL";
            }
                         
            frmReportCondition frmEmp = new frmReportCondition();
   Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuEmpWeight_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
         
            GlobalVariables.ReportTitle = "% น้ำหนักคะแนนรวม";
            GlobalVariables.FagRPT = "EMPWEIGHT";
            frmReportCondition frmEmp = new frmReportCondition();
   Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuAssessmentGrade_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            frmAssessmentGrade frmEmp = new frmAssessmentGrade();
 Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuNote_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmNote frmN = new frmNote();
            Close_ChildForm(frmN.Name);
            frmN.MdiParent = this;
            frmN.Show();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            GC.Collect();
            this.Dispose();
            this.Close();
            //Environment.Exit(1);
            th = new Thread(openLoginFrom);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
        private void openLoginFrom(object obj)
        {
            Application.Run(new frmLogin());
        }

        private void mnuRPTScoreSubstandard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินที่ต่ำกว่าค่าคาดหวัง";
            GlobalVariables.FagRPT = "SUBSTANDARD";
            frmReportCondition frmEmp = new frmReportCondition();
 Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuRPTAssessmentGrade_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินรวมทั้งหมด (ตัดเกรด)";
            GlobalVariables.FagRPT = "GRADE";
            frmReportCondition frmEmp = new frmReportCondition();
    Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void btnChangePassword_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            frmChangePassword frmEmp = new frmChangePassword();
 Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void barLog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void mnuDownload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("http://172.100.50.210/checkup/pa/PA2021.zip");
        }

        private void mnuRPTOverStandard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินที่สูงกว่าค่าคาดหวัง";
            GlobalVariables.FagRPT = "OVERSTANDARD";
            frmReportCondition frmEmp = new frmReportCondition();
    Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();
        }

        private void mnuRPTComment_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
            GlobalVariables.ReportTitle = "รายงานข้อเสนอแนะจากหัวหน้า";
            GlobalVariables.FagRPT = "COMMENT";
            frmReportViewer frmEmp = new frmReportViewer();
  Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuEmployeeRelation_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
       
            frmEmployeeRelation2 frmEmp = new frmEmployeeRelation2();
     Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuLogout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void mnuAssesseeNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmAssesseeNewList frmEmp = new frmAssesseeNewList();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuEmpImport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
            frmEmployeeImport frmEmp = new frmEmployeeImport();
  Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuDivision_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Close_ChildForm(frmEmp.Name);
            frmDivision frmEmp = new frmDivision();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuDepartment_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Close_ChildForm(frmEmp.Name);
            frmDepartment frmEmp = new frmDepartment();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuPosition_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Close_ChildForm(frmEmp.Name);
            frmPosition frmEmp = new frmPosition();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuImportImage_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmPictureImport frmEmp = new frmPictureImport();
            Close_ChildForm(frmEmp.Name);
            //frmEmp.MdiParent = this;
            //frmEmp.Show();             
            frmEmp.ShowDialog(this);
        }

        private void xtraTabbedMdiManager_PageAdded(object sender, MdiTabPageEventArgs e)
        {
            Image.GetThumbnailImageAbort callback = new Image.GetThumbnailImageAbort(ThumbnailCallBack);
            Bitmap img = new Bitmap(this.ribbonControl.ApplicationIcon);
            Image thumbnail;
            thumbnail = img.GetThumbnailImage(16, 16, callback, IntPtr.Zero);
            e.Page.Image = thumbnail;
        }
        public static bool ThumbnailCallBack()
        {
            return false;
        }

        private void mnuRptSelf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินตนเอง";         
            GlobalVariables.FagRPT = "SELFSCORE";           
            frmReportCondition frmEmp = new frmReportCondition();
            Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
        }

        private void mnuRptCoWork_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GlobalVariables.ReportTitle = "รายงานคะแนนประเมินเพื่อนร่วมงาน";
            GlobalVariables.FagRPT = "COSCORE";
            frmReportCondition frmEmp = new frmReportCondition();
            Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
        }

        private void mnuPrintAsm_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GlobalVariables.ReportTitle = "รายงานสรุปผลการประเมิน";
            GlobalVariables.FagRPT = "ASMPRNT";
            frmPrintCondition frmEmp = new frmPrintCondition();
            Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
        }

        private void mnuHOD_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmHODRelation frmEmp = new frmHODRelation();
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuSystemConfig_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmConfig frmEmp = new frmConfig();
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuFinalGrade_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmAssessmentGrade2 frmEmp = new frmAssessmentGrade2();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuCheckScore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmAssessmentScore frmEmp = new frmAssessmentScore();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }

        private void mnuPrintFormEmpNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GlobalVariables.ReportTitle = "พิมพ์แบบฟอร์ม";
            GlobalVariables.FagRPT = "NEWFORM";
            frmPrintForm frmEmp = new frmPrintForm();
            Close_ChildForm(frmEmp.Name);
            frmEmp.ShowDialog(this);
        }

        private void mnuAsmRelation_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmEmployeeRelation frmEmp = new frmEmployeeRelation();
            Close_ChildForm(frmEmp.Name);
            frmEmp.MdiParent = this;
            frmEmp.Show();
        }
    }
}
