﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using System.Collections;

namespace SUTHPA
{
    public partial class frmAssessmentGrade2 : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtPA = new DataTable();
        AssessmentController ctlPA = new AssessmentController();
        public string EmployeeID;
        public int EvaluationUID;
        public string EmployeeName;
        public bool flagEdit = false;
        public int row;
        public frmAssessmentGrade2()
        {
            InitializeComponent();
        }

        private void frmAssessment_Load(object sender, EventArgs e)
        {
            cboYear.Properties.DataSource = ctlPA.Assessment_GetYear();
            cboYear.Properties.DisplayMember = "BYear";
            cboYear.Properties.ValueMember = "BYear";

            cboYear.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            cboYear.Properties.DropDownRows = 3;
            cboYear.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BYear", 100, "");
            cboYear.Properties.Columns.Add(col1);

            cboYear.EditValue = GlobalVariables.BYear;
            txtRN.Value = 1;
                      
            try
            {

               ctlPA.AssessmentGrade_Update(GlobalVariables.BYear);
               LoadScore();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }     

        private void cmdView_Click(object sender, EventArgs e)
        {
            LoadScore();
        }
        private void LoadScore()
        {

            try
            {        

                dtPA = ctlPA.AssessmentGrade_GetView(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero(txtRN.Value.ToString()));

                row = dtPA.Rows.Count;
                grdAssessment.DataSource = dtPA;
                grdViewAssessment.BestFitColumns();
                //viewPAAll.Columns[3].AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                //viewPAAll.Columns[3].Width = 500;


                for (int i = 6; i < 18; i++)
                {
                    grdViewAssessment.Columns[i].Width = 50;
                }
                               
            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }
    }
}