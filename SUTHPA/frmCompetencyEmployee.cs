﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmCompetencyEmployee : DevExpress.XtraEditors.XtraForm
    {
        public frmCompetencyEmployee()
        {
            InitializeComponent();
        }
        DataTable dtC = new DataTable(); 
        CompetencyController ctlCom = new CompetencyController();

        public string EmployeeID;
        public string EmployeeName;
        public int EmployeeLevelUID;
        public int AssessorLevelUID;
        int RNDNO;
        private void frmCompetencyEmployee_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            ConfigurationController ctlCf = new ConfigurationController();
            RNDNO = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO));
            LoadCompetencyType();
        }

        private void LoadCompetencyType()
        {
            dtC.Rows.Clear();

            if (this.EmployeeID == GlobalVariables.username && this.AssessorLevelUID == 0)
            { this.AssessorLevelUID = GlobalVariables.AssessorLevelUID; }

            dtC = ctlCom.Evaluation_GetByEmployee(GlobalVariables.BYear, this.EmployeeID , this.EmployeeLevelUID.ToString(),GlobalVariables.username, GlobalVariables.AssessmentClass,RNDNO,this.AssessorLevelUID);
            if (dtC.Rows.Count > 1)
            {
                grdEmployee.DataSource = dtC;
            }
            else //ถ้าเจอหัวข้อเดียวก็ให้ไปเปิดฟอร์มประเมินเลย
            {             
                if (dtC.Rows[0].Field<int>("CompetencyLevel")==3 || dtC.Rows[0].Field<int>("CompetencyLevel") == 4)
                {
                    //เปิดฟอร์ม PA
                    frmAssessment fPA = new frmAssessment();
                    //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                    fPA.EmployeeID = this.EmployeeID;
                    fPA.EvaluationUID = dtC.Rows[0].Field<int>("EvaluationUID");
                    fPA.AssessorLevelUID = this.AssessorLevelUID;

                    if (dtC.Rows[0]["isAssessment"].ToString() == "Y")
                        fPA.flagEdit = true;

                    fPA.EmployeeName = EmployeeName;

                    //this.Close();
                    fPA.MdiParent = this.MdiParent;
                    //fPA.TopMost = true;
                    fPA.ShowDialog();
                }
                else
                {
                    //เปิดฟอร์ม PA
                    frmAssessment2 fPA = new frmAssessment2();
                    //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                    fPA.EmployeeID = this.EmployeeID;
                    fPA.EvaluationUID = dtC.Rows[0].Field<int>("EvaluationUID");
                    
                    fPA.AssessorLevelUID = this.AssessorLevelUID;

                    if (dtC.Rows[0]["isAssessment"].ToString() == "Y")
                        fPA.flagEdit = true;

                    fPA.EmployeeName = EmployeeName;

                    //this.Close();
                    fPA.MdiParent = this.MdiParent;
                    //fPA.TopMost = true;
                    fPA.ShowDialog();
                }
                    
              

                if (GlobalVariables.FlagRefresh == true)
                {
                    this.Close();
                    //frmAssesseeList frmA = new frmAssesseeList();
                    //frmA.MdiParent = this.MdiParent;
                    //frmA.Show();
                }
               
            }
        }
        private void grdViewEmployee_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            try
            {
                GridHitInfo hi = view.CalcHitInfo(e.X, e.Y);

                if (hi.Column.FieldName == "Assm" && view.GetRowCellValue(hi.RowHandle, view.Columns["Assm"]).ToString() == "Y")
                {
                    if (e.X >= 556 && e.X <= 600)
                    {
                        if (hi.InGroupRow == false)
                            view.GridControl.Cursor = Cursors.Hand;
                    }
                    else
                    {
                        view.GridControl.Cursor = Cursors.Default;
                    }
                }
                else
                {
                    view.GridControl.Cursor = Cursors.Default;
                }
               
            }
            catch
            {
                view.GridControl.Cursor = Cursors.Default;
            }
        }

        private void grdViewEmployee_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) != Keys.Control)
            {
                GridView view = sender as GridView;
                GridHitInfo hi = view.CalcHitInfo(e.Location);
                try
                {
                    if (hi.Column.Name == "colAssessment")
                    {
                        view.FocusedRowHandle = hi.RowHandle;
                        view.FocusedColumn = hi.Column;
                        view.ShowEditor();
                        if (grdViewEmployee.GetFocusedRowCellValue("EvaluationUID").ToString() != "0" && grdViewEmployee.GetFocusedRowCellValue("EvaluationUID").ToString() != "")
                        {
                            if (grdViewEmployee.GetFocusedRowCellValue("CompetencyLevel").ToString() == "3" || grdViewEmployee.GetFocusedRowCellValue("CompetencyLevel").ToString() == "4")
                            {
                                //เปิดฟอร์มแบบประเมิน พร้อมส่งพารามิเตอร์ 
                                frmAssessment fPA = new frmAssessment();
                                //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                                fPA.EmployeeID = this.EmployeeID;
                                fPA.AssessorLevelUID = this.AssessorLevelUID;
                                fPA.EvaluationUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("EvaluationUID"));
                                if (grdViewEmployee.GetFocusedRowCellValue("isAssessment").ToString() == "Y")
                                    fPA.flagEdit = true;

                                fPA.EmployeeName = EmployeeName;
                                fPA.MdiParent = this.MdiParent;
                                //fPA.TopMost = true;
                                fPA.ShowDialog();
                            }
                            else
                            {
                                //เปิดฟอร์มแบบประเมิน พร้อมส่งพารามิเตอร์ 
                                frmAssessment2 fPA = new frmAssessment2();
                                //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                                fPA.EmployeeID = this.EmployeeID;
                                fPA.AssessorLevelUID = this.AssessorLevelUID;
                                fPA.EvaluationUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("EvaluationUID"));
                                if (grdViewEmployee.GetFocusedRowCellValue("isAssessment").ToString() == "Y")
                                    fPA.flagEdit = true;

                                fPA.EmployeeName = EmployeeName;
                                fPA.MdiParent = this.MdiParent;
                                //fPA.TopMost = true;
                                fPA.ShowDialog();
                            }

                            if (GlobalVariables.FlagRefresh == true)
                            {
                             LoadCompetencyType();
                                //frmAssesseeList frmA = new frmAssesseeList();
                                //frmA.MdiParent = this.MdiParent;
                                //frmA.Show();
                            }
                           
                        }
                        
                    }
                }
                catch (Exception ex)                
                    {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}