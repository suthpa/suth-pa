﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using System.Collections;

namespace SUTHPA
{
    public partial class frmAssessmentGrade : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtPA = new DataTable();
        AssessmentController ctlPA = new AssessmentController();
        public string EmployeeID;
        public int EvaluationUID;
        public string EmployeeName;
        public bool flagEdit = false;
        public int row;
        public frmAssessmentGrade()
        {
            InitializeComponent();
        }

        private void frmAssessment_Load(object sender, EventArgs e)
        {
            lblTitlePA.Text = "Performance Evaluation " + GlobalVariables.BYear;
            try
            {

               ctlPA.AssessmentGrade_Update(GlobalVariables.BYear);

               dtPA = ctlPA.AssessmentGrade_Get(GlobalVariables.BYear);

                row = dtPA.Rows.Count;
                grdAssessment.DataSource = dtPA;
                grdViewAssessment.BestFitColumns();
                //viewPAAll.Columns[3].AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                //viewPAAll.Columns[3].Width = 500;


                for (int i = 6; i < 18; i++)
                {
                         grdViewAssessment.Columns[i].Width = 50;
                }

                 

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCoreComp_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtExR_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtCQI_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtQA_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtICRTeam_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtICR_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtDiscipline_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtTraining_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtCheckup_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
        private void txtActivity_Leave(object sender, EventArgs e)
        {
            SaveScore();
        }
               
        private void SaveScore()
        {
            try
            {
                double CoreComp = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colCoreComp).ToString());
                double LeaderComp = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colLeaderComp).ToString());
                double FuncComp = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colFuncComp).ToString());
                //double CoreCompTest = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colCoreComTest).ToString()); // ปี 2019 ไม่มี
                double ExR = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colExR).ToString());
                double CQI = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colCQI).ToString());
                double QA = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colQA).ToString());
                double ICR_Team = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colICRTeam).ToString());
                double ICR = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colICR).ToString());
                double Discipline = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colDiscipline).ToString());
                double Training = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colTraining).ToString());
                double Checkup = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colCheckup).ToString());
                double Activity = BaseClass.StrNull2Dbl(grdViewAssessment.GetFocusedRowCellValue(colActivity).ToString());

                double Total = (CoreComp+LeaderComp+FuncComp + ExR + CQI + QA + ICR + ICR_Team + Discipline + Training + Checkup + Activity);

                string EmpID = grdViewAssessment.GetFocusedRowCellValue(colEmployeeID).ToString();
                if (Total>100)
                {
                    MessageBox.Show("กรุณาตรวจสอบตัวเลข ", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //grdViewAssessment.SetFocusedRowCellValue(colCoreComTest, "");
                    grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;
                }
                else
                {
                    grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colTotalScore, Total);
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                     
                }
                if (Total > 0)
                {
                    ctlPA.AssessmentGrade_Save(GlobalVariables.BYear,EmpID, CQI , QA, Discipline , Training , Checkup ,Total,GlobalVariables.username);
                     
                }
                                              
            }
            catch { }
        }             
    }
}