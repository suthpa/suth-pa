using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlClient;
using System.Drawing.Printing;


namespace  SUTHPA
{
    public  class BaseClass
    {
        
	#region "Private Members"

	private const string ProviderType = "data";
	private const string ModuleQualifier = "";
	private string _providerPath;
	private string _objectQualifier;

	private string _databaseOwner;
	#endregion
    public DataTable dt = new DataTable();

	internal SqlConnection Connection;
	public bool pointDup = false;
	public bool f_debug;
	public int MAX_ENG_YEAR = 2018;
	public string STR_LANGUAGE = "th";
	public string SQLwhere = "";

    public int num;
  
	public bool has_data = false;
	public  string sqlServer;
	public  string sqlDatabase;
	public  string sqlUsername;
	public  string sqlPassword;
	public  string sqlReport;
	public  string ReportURL;
	public  string ConnectionString = "";

    public string HOsqlServer;
    public string HOsqlDatabase;
    public string HOsqlUsername;
    public string HOsqlPassword;
    public string HOsqlReport;
    public string HOReportURL;
    public string ConnectionStringHO = "";



	public  string PassPhase;

	public  int iData = 0;
	public  string sData = "";
	public  string sCode = "";
	public  string sName = "";

	public  System.DateTime sDate;
	public  bool isAll = false;
	public  bool isSearch = false;
	public  bool isAdd = false;

	public  bool isSubAdd = false;
	public string strTableName;
	public string strKeyGen;
	//Public Shared strTableName As String
	public stcField[] tblField;

	public string FieldSort;

	internal SqlTransaction Transaction;

	public int intTotalField = 0;


    



	//******************* ตัวแปรติดต่อฐานข้อมูล  ***********************
	public SqlConnection Conn = new SqlConnection();
	public DateTimeFormatInfo dtfInfo;

	public string SQL;
	// Public Shared strConn As String

	public string ServerPath;

	public string DBPath;
	 
	 public static  int _UserID;
     public static string _Username;
	 public static  string _UserPwd;
	 public static string _NameOfUser;
	 public static  int _UserRole;


		public static int UserID {
			get { return _UserID; }
			set { _UserID = value; }
		}

        public static string UserPwd
        {
			get { return _UserPwd; }
			set { _UserPwd = value; }
		}

        public static string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public static string NameOfUser
        {
			get { return _NameOfUser; }
			set { _NameOfUser = value; }
		}

        public static int UserRole
        {
			get { return _UserRole; }
			set { _UserRole = value; }
		}

 


	public struct stcField
	{
		private string _fldName;
		private string _fldValue;
		private string _fldType;
		private bool _fldAffect;

		private int _fldLength;
		public string fldName {
			get { return _fldName; }
			set { _fldName = value; }
		}
		public string fldValue {
			get { return _fldValue; }
			set { _fldValue = value; }
		}
		public string fldType {
			get { return _fldType; }
			set { _fldType = value; }
		}
		public bool fldAffect {
			get { return _fldAffect; }
			set { _fldAffect = value; }
		}
		public int fldLength {
			get { return _fldLength; }
			set { _fldLength = value; }
		}

        //public stcField(string fValue)
        //{
        //    _fldValue = fValue;
        //}
	}


	//Public Structure stcDocNoField
	//    Dim strInitialField As String
	//    Dim strYearField As String
	//    Dim strMonthField As String
	//    Dim strRunningField As String
	//    Dim strDefaultDept As String
	//    Dim strDefaultStore As String
	//    Dim strDocNoFieldName As String
	//End Structure

	#region "Connect Database"

	public string GetFullyQualifiedName(string name)
	{
		return DatabaseOwner + ObjectQualifier + ModuleQualifier + name;
	}

	public bool OpenConnection(string constr = "", bool f_ShowMsg = true)
	{

		try {
			Connection = new SqlConnection();
			//If Connection.State = ConnectionState.Open Then Connection.Close()
			//Connection = New SqlConnection(ConString)
			//Connection.Open()
			//Return True
			if (Connection.State == ConnectionState.Closed) {
				Connection = new SqlConnection(ConnectionString);
				Connection.Open();
			}
			return true;
		} catch (Exception ex) {
			if (f_ShowMsg)
				 MessageBox.Show(ex.Message,"Error!!",MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
		}
	}

	#region "Connection Properties"

	public string ProviderPath {
		get { return _providerPath; }
	}

	public string ObjectQualifier {
		get { return _objectQualifier; }
	}

	public string DatabaseOwner {
		get { return _databaseOwner; }
	}

	#endregion


	public void getConnectionString()
	{
		StreamReader sr = default(StreamReader);
		DataSet ds = new DataSet();
        sr = new StreamReader(@"C:\ProgramData\SUTHPA\DataConfig2021.xml");
		ds.ReadXml(sr);
		sr.Close();
		sqlServer = ds.Tables[0].Rows[0].Field<string>("ServerPath");
		sqlDatabase = ds.Tables[0].Rows[0].Field<string>("DatabaseName");
		sqlUsername = ds.Tables[0].Rows[0].Field<string>("Username");
		sqlPassword = ds.Tables[0].Rows[0].Field<string>("Password");
		//sqlReport = ds.Tables[0].Rows[0].Field<string>("ReportPath"))
		//ReportURL = ds.Tables[0].Rows[0].Field<string>("ReportURL"))
		PassPhase = ds.Tables[0].Rows[0].Field<string>("PassPhase");           
            
		ConnectionString = "Data Source=" + sqlServer + ";Database=" + sqlDatabase + ";User Id=" + sqlUsername + ";Password=" + sqlPassword + ";";            

		ds = null;

	}

	#endregion



	public BaseClass(SqlConnection conn = null)
	{
		getConnectionString();

		if ((conn != null)) {
			this.Conn = conn;
		}

	}

	public SqlTransaction Trans {
		get { return Transaction; }
		set { Transaction = value; }
	}



	public SqlConnection getConn {
		get { return Connection; }
	}
	//Set(ByVal Value As SqlConnection)
	//    Connection = Value
	//End Set


    public bool beginTrans()
    {
        bool f_return = false;
        try
        {
            Transaction = Connection.BeginTransaction();
            f_return = true;
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message, "Error!!", MessageBoxButtons.OK,MessageBoxIcon.Error);
            f_return = false;
        }
        return f_return;
    }

	public bool commitTrans()
	{
		bool f_return = false;
		try {
			Transaction.Commit();
			f_return = true;
		} catch (Exception ex) {
			MessageBox.Show(ex.Message,"Error!!" , MessageBoxButtons.OK,MessageBoxIcon.Error);
			f_return = false;
		}
		return f_return;
	}

	public bool rollbackTrans()
	{
		bool f_return = false;
		try {
			Transaction.Rollback();
			f_return = true;
		} catch (Exception ex) {
			MessageBox.Show(ex.Message,"Error!!" , MessageBoxButtons.OK);
			f_return = false;
		}
		return f_return;
	}

	public void CloseConnection()
	{
		try {
			if ((Connection != null)) {
				if (Connection.State != ConnectionState.Closed) {
					Connection.Close();
				}
				Connection.Dispose();
				Connection = null;
			}
		} catch (Exception ex) {
		}
	}

	public System.DateTime GET_DATE_SERVER()
	{
		string sqlD = null;
		sqlD = "select getdate() as dateServer";
		dt = ExecuteQuery(sqlD);
		return dt.Rows[0].Field<DateTime>(0);
	}
	public DateTime GET_DATETIME_SERVER()
	{
		string sqlD = null;
		sqlD = "select getdate() as dateServer";
		dt = ExecuteQuery(sqlD);
        return dt.Rows[0].Field<DateTime>(0);
	}

	public string GET_TIME_SERVER()
	{
		string sqlD = null;
		sqlD = "select getdate() as dateServer";
		dt = ExecuteQuery(sqlD);
        return   dt.Rows[0].Field<DateTime>(0).ToShortTimeString();
	}



	//Public Function ExecuteNonQuery(ByVal strSQL As String) As Boolean
	//    Dim excCommand As SqlCommand
	//    Dim retValue As Boolean = False
	//    Try
	//        excCommand = New SqlCommand(strSQL, Connection)
	//        If Not IsNothing(Transaction) Then excCommand.Transaction = Transaction
	//        excCommand.ExecuteNonQuery()
	//        retValue = True
	//    Catch ex As Exception

	//        MsgBox(ex.Message & excCommand.CommandText, MsgBoxStyle.OkOnly, "Error")
	//        retValue = False
	//    End Try
	//    If Not excCommand Is Nothing Then
	//        excCommand.Dispose()
	//        excCommand = Nothing
	//    End If
	//    Return retValue
	//End Function


	//Public Function ExecuteSQL(ByVal strSQL As String) As Boolean
	//    Dim excCommand As SqlCommand
	//    Dim retValue As Boolean = False
	//    Try
	//        excCommand = New SqlCommand(strSQL, Connection)
	//        If Not IsNothing(Transaction) Then excCommand.Transaction = Transaction
	//        excCommand.ExecuteNonQuery()
	//        retValue = True
	//    Catch ex As Exception

	//        MsgBox(ex.Message & excCommand.CommandText, MsgBoxStyle.OKOnly, "Error")
	//        retValue = False
	//    End Try
	//    If Not excCommand Is Nothing Then
	//        excCommand.Dispose()
	//        excCommand = Nothing
	//    End If
	//    Return retValue
	//End Function

	//Public Function ExecuteQuery(ByVal strSQL As String) As DataTable
	//    Dim da As SqlDataAdapter
	//    Dim ds As DataSet
	//    Dim dt As DataTable
	//    Try
	//        da = New SqlDataAdapter(strSQL, Connection)
	//        ds = New DataSet
	//        If Not IsNothing(Transaction) Then da.SelectCommand.Transaction = Transaction
	//        da.Fill(ds, "dataname")
	//        dt = ds.Tables("dataname")
	//    Catch ex As Exception
	//        MsgBox(ex.Message, MsgBoxStyle.OkOnly, "Error")
	//        dt = New DataTable
	//    End Try
	//    If Not da Is Nothing Then
	//        da.Dispose()
	//        da = Nothing
	//    End If
	//    ds = Nothing
	//    Return dt
	//End Function


	//Public Function delData(Optional ByVal strWhere As String = "", Optional ByVal f_debug As Boolean = False) As Boolean
	//    Dim strSql As String
	//    strSql = "delete from " & strTableName
	//    If strWhere.Length > 0 Then strSql &= " where " & strWhere
	//    If f_debug Then showSQLString(strSql)
	//    delData = ExecuteSQL(strSql)
	//End Function

	//Public Sub showSQLString(ByVal strSql As String)
	//    If f_debug Then InputBox("sql command :", "DBClass debug", strSql)
	//End Sub

	//Public Function selData(Optional ByVal strSQL As String = "", Optional ByVal strWhere As String = "", Optional ByVal strOrder As String = "", Optional ByVal f_debug As Boolean = False) As DataTable
	//    If strSQL.Length = 0 Then strSQL = "select * from " & strTableName
	//    If strWhere.Length > 0 And strSQL.IndexOf("where") <= 0 Then strSQL &= " where " & strWhere
	//    If strOrder.Length > 0 And strSQL.IndexOf("order by") <= 0 Then strSQL &= " order by " & strOrder
	//    If f_debug Then showSQLString(strSQL)

	//    selData = ExecuteQuery(strSQL)
	//End Function

	//Public Function selTopData(Optional ByVal topVal As Integer = 0, Optional ByVal strSQL As String = "", Optional ByVal strWhere As String = "", Optional ByVal strOrder As String = "", Optional ByVal f_debug As Boolean = False) As DataTable
	//    If topVal > 0 Then
	//        If strSQL.Length > 0 Then
	//            strSQL = strSQL.Remove(strSQL.ToUpper.IndexOf("select".ToUpper), 6)
	//            strSQL = "select top " & topVal.ToString & strSQL
	//        Else
	//            strSQL = "select top " & topVal.ToString & " * from " & strTableName
	//        End If
	//    End If
	//    selTopData = selData(strSQL, strWhere, strOrder, f_debug)
	//End Function

	public void setSearchField(int index, string type = "normal")
	{
		//tblField(index).fldSearchType = type
        
		tblField[index].fldAffect = true;
	}

	public string getTableName()
	{
		return strTableName;
	}

	public void setFieldAffectValue(bool blnFieldAffect)
	{
		int i = 0;
		for (i = 0; i <= this.intTotalField; i++) {
            this.tblField[i].fldAffect = blnFieldAffect;
		}
	}

	public string getStrWhere(stcField[] aField)
	{
		int i = 0;
		string strWhere = "";
		for (i = 0; i <= aField.Length - 1; i++) {
			if (strWhere.Length > 0)
				strWhere += " and ";
			switch (aField[i].fldType) {
				case "number":
				case "integer":
				case "double":
					strWhere += aField[i].fldName + " = " + aField[i].fldValue;
					break;
				default:
					strWhere += aField[i].fldName + " = '" + aField[i].fldValue + "'";
					break;
			}
		}
		return strWhere;
	}


	public DataTable ExecuteQuery(string sql)
	{
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		SqlDataAdapter da = new SqlDataAdapter();
		SqlCommand cmd = new SqlCommand();

		try {
			if (OpenConnection()) {
				cmd = new SqlCommand();
				cmd.Connection = Connection;
				cmd.CommandText = sql;
				da = new SqlDataAdapter();
				da.SelectCommand = cmd;
				da.TableMappings.Add("Table", "Table");
				da.Fill(ds);
				dt = ds.Tables["Table"];
			}


		} catch (Exception ex) {
			MessageBox.Show("Could not Execute Query : " + ex.Message);
		} finally {
			this.CloseConnection();
		}

		return dt;

	}


	public bool ExecuteNonQuery(string sql)
	{

		bool rowsAffect = false;
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		SqlDataAdapter da = new SqlDataAdapter();
		SqlCommand cmd = new SqlCommand();

		try {
			if (OpenConnection()) {
				this.beginTrans();
				cmd = Connection.CreateCommand();
				cmd.Connection = Connection;
				cmd.Transaction = Transaction;
				cmd.CommandText = sql;
				rowsAffect = Convert.ToBoolean(cmd.ExecuteNonQuery());
				this.commitTrans();
			}


		} catch (Exception ex) {
			MessageBox.Show(ex.Message);
		} finally {
			this.CloseConnection();
		}

		return rowsAffect;

	}

	public string getSqlInsert(stcField[] field)
	{
		int i = 0;
		string sql = null;
		bool f_FirstField = true;
		sql = " Insert Into " + strTableName + "(";

		//วนลูปเก็บ field ที่ต้องการ insert
		//change from i = 0 to i = 1 by oh in 04/01/2007
		//change back from i = 1 to i = 0 because some table doesn't have id as field#0

		for (i = 0; i <= field.Length - 1; i++) {
			if (field[i].fldAffect == true) {
				if (f_FirstField) {
					sql += " " + field[i].fldName + "";
					f_FirstField = false;
				} else {
					sql += "," + field[i].fldName + "";
				}
			}
		}

		sql += ") Values(";
		f_FirstField = true;
		//วนลูปเก็บค่า insert
		//change from i = 0 to i = 1 by oh in 04/01/2007
		//change back from i = 1 to i = 0 because some table doesn't have id as field#0
		for (i = 0; i <= field.Length - 1; i++) {
			if (field[i].fldAffect == true) {
				if (f_FirstField) {
					switch (setValue(field[i].fldType).Trim().ToLower()) {
						case "money":
							sql += " " + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + " ";
							break;
						case "datetime":
							if (setValue(field[i].fldValue).Trim().Length == 0) {
								sql += " NULL ";
							} else {
								sql += " '" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "' ";
							}
							break;
						case "varchar":
						case "varchar2":
						case "text":
						case "string":
						case "nvarchar":
							sql += " '" + PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) + "' ";
							break;
						default:
							sql += " '" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "' ";
							break;
					}
					//If setValue(field[i].fldType).Trim.ToLower = "money" Then
					//    sql += " " & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & ""
					//ElseIf setValue(field[i].fldType).Trim.ToLower = "datetime" Then
					//    If setValue(field[i].fldValue).Trim.Length = 0 Then
					//        sql += " NULL "
					//    Else
					//        sql += "'" & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & "'"
					//    End If
					//Else
					//    sql += " '" & PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) & "'"
					//End If
					f_FirstField = false;
				} else {
					switch (setValue(field[i].fldType).Trim().ToLower()) {
						case "money":
							sql += "," + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "";
							break;
						case "datetime":
							if (setValue(field[i].fldValue).Trim().Length == 0) {
								sql += ",NULL ";
							} else {
								sql += ",'" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "'";
							}
							break;
						case "varchar":
						case "varchar2":
						case "text":
						case "string":
						case "nvarchar":
						case "ntext":
							sql += ",'" + PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) + "'";
							break;
						default:
							sql += ",'" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "'";
							break;
					}
					//If setValue(field[i].fldType).Trim.ToLower = "money" Then
					//    sql += "," & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & ""
					//ElseIf setValue(field[i].fldType).Trim.ToLower = "datetime" Then
					//    If setValue(field[i].fldValue).Trim.Length = 0 Then
					//        sql += ",NULL "
					//    Else
					//        sql += ",'" & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & "'"
					//    End If
					//Else
					//    sql += ",'" & PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) & "'"
					//End If
				}
			}
		}

		sql += ") ";
		return sql;
	}
	public bool updData(string strWhere = "", bool f_debug = false)
	{
		string strSql = null;
		bool f_return = false;

		strSql = getSqlUpdate(tblField);
		if (strSql.Length > 0) {
			if (strWhere.Length > 0)
				strSql += " where " + strWhere;
			if (f_debug)
				showSQLString(strSql);
			f_return = ExecuteNonQuery(strSql);
		} else {
			f_return = false;
		}
		return f_return;
	}

	public bool delData(string strWhere = "", bool f_debug = false)
	{
		string strSql = null;
		strSql = "delete from " + strTableName;
		if (strWhere.Length > 0)
			strSql += " where " + strWhere;
		if (f_debug)
			showSQLString(strSql);
		return ExecuteNonQuery(strSql);
	}

	public decimal insData(bool f_debug = false, bool f_with_running_id = true)
	{
		string strSql = null;
		DataTable dtData = default(DataTable);
		decimal Result = default(decimal);
		strSql = getSqlInsert(tblField);
		if (f_debug)
			showSQLString(strSql);
		if (ExecuteNonQuery(strSql)) {
			if (f_with_running_id) {
				Result = Convert.ToDecimal(ExecuteQuery("select ident_current('" + strTableName + "') as ident").Rows[0].Field<int>("ident").ToString());
			} else {
				Result = 0;
			}
		} else {
			Result = -1;
		}
		return Result;
		//ถ้า Result >= 0 เป็น true ,แต่ถ้า  Result < 0 เป็น false
	}

	public void showSQLString(string strSql)
	{
		if (f_debug)
			MessageBox.Show("sql command :" + strSql , "baseclass debug",MessageBoxButtons.OK );
	}

	public DataTable selData(string strSQL = "", string strWhere = "", string strOrder = "", bool f_debug = false)
	{
		if (strSQL.Length == 0)
			strSQL = "select * from " + strTableName;
		if (strWhere.Length > 0 & strSQL.IndexOf("where") <= 0)
			strSQL += " where " + strWhere;
		if (strOrder.Length > 0 & strSQL.IndexOf("order by") <= 0)
			strSQL += " order by " + strOrder;
		if (f_debug)
			showSQLString(strSQL);

		return ExecuteQuery(strSQL);
	}
	public string getSqlUpdate(stcField[] field)
	{
		int i = 0;
		string sql = null;
		string sqlWhere = null;
		bool f_FirstField = true;
		sql = " UPDATE " + strTableName + " SET ";
		sqlWhere = "";
		//change back from i = 1 to i = 0 because some table doesn't have id as field#0


		for (i = 0; i <= field.Length - 1; i++) {
			if (field[i].fldAffect == true) {
				if (f_FirstField) {
					switch (setValue(field[i].fldType).Trim().ToLower()) {
						case "money":
							sqlWhere += " " + field[i].fldName + " = " + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "";
							break;
						case "datetime":
							if (setValue(field[i].fldValue).Length == 0) {
								sqlWhere += " " + field[i].fldName + " =NULL";
							} else {
								sqlWhere += " " + field[i].fldName + " = " + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "";
							}
							break;
						case "varchar":
						case "varchar2":
						case "text":
						case "string":
						case "nvarchar":
						case "ntext":
							sqlWhere += " " + field[i].fldName + " = '" + PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) + "'";
							break;
						default:
							sqlWhere += " " + field[i].fldName + " = '" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "'";
							break;
					}
					//If setValue(field[i].fldType).Trim.ToLower = "money" Then
					//    sqlWhere += " " & field[i].fldName & " = " & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & ""
					//ElseIf setValue(field[i].fldType).Trim.ToLower = "datetime" Then
					//    If setValue(field[i].fldValue).Length = 0 Then
					//        sqlWhere += " " & field[i].fldName & " =NULL"
					//    Else
					//        sqlWhere += " " & field[i].fldName & " = " & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & ""
					//    End If
					//Else
					//    sqlWhere += " " & field[i].fldName & " = '" & PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) & "'"
					//End If
					f_FirstField = false;
				} else {
					switch (setValue(field[i].fldType).Trim().ToLower()) {
						case "money":
							sqlWhere += ", " + field[i].fldName + " = " + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "";
							break;
						case "datetime":
							if (setValue(field[i].fldValue).Length == 0) {
								sqlWhere += "," + field[i].fldName + " =NULL";
							} else {
								sqlWhere += "," + field[i].fldName + " = '" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "'";
							}
							break;
						case "varchar":
						case "varchar2":
						case "text":
						case "string":
						case "nvarchar":
						case "ntext":
							sqlWhere += "," + field[i].fldName + " = '" + PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) + "'";
							break;
						default:
							sqlWhere += "," + field[i].fldName + " = '" + PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) + "'";
							break;
					}
					//If setValue(field[i].fldType).Trim.ToLower = "money" Then
					//    sqlWhere += ", " & field[i].fldName & " = " & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & ""
					//ElseIf setValue(field[i].fldType).Trim.ToLower = "datetime" Then
					//    If setValue(field[i].fldValue).Length = 0 Then
					//        sqlWhere += "," & field[i].fldName & " =NULL"
					//    Else
					//        sqlWhere += "," & field[i].fldName & " = '" & PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)) & "'"
					//    End If
					//Else
					//    sqlWhere += "," & field[i].fldName & " = '" & PrepareFieldLenght(PrepareSqlValue(setValue(field[i].fldValue, field[i].fldType)), field[i].fldLength) & "'"
					//End If
				}
			}
		}
		if (sqlWhere.Length > 0) {
			return sql + sqlWhere;
		} else {
			return "";
		}
	}


	#region "General Functions"
	#region "Auto Serial"


	private void RunningNumber_InsertNewYear(string tCode, string yCode)
	{
		SQL = "Insert Into KCB_Running(Code,YearCode,LastRunning)";
		SQL += " Values('" + tCode;
		SQL += "','" + yCode;
		SQL += "',0)";

		ExecuteNonQuery(SQL);
	}

    //public void RunningNumber_Update(string tCode)
    //{
    //    int iY = GET_DATE_SERVER().Date.Year;
    //    string strY = "";

    //    if (iY < 2500) {
    //        strY = BaseGlobalClase.BaseGlobalClase.Mid((iY + 543).ToString(), 3, 2).Trim();
    //    } else {
    //        strY = BaseGlobalClase.Mid(iY.ToString(), 3, 2).Trim();
    //    }

    //    SQL = "Update  KCB_Running set  LastRunning=LastRunning+1 Where ";
    //    SQL += "  Code='" + tCode;
    //    SQL += "'  and YearCode='" + strY;
    //    SQL += "'";

    //    ExecuteNonQuery(SQL);

    //}
    //public long RunningNumber_GetLast(string code, string yCode)
    //{
    //    string sqlRun = null;

    //    sqlRun = "select  LastRunning  from  KCB_Running  where Code='" + code + "'  And YearCode ='" + yCode + "'";

    //    dt = ExecuteQuery(sqlRun);

    //    if (dt.Rows.Count > 0) {
    //        return Convert.ToInt64(dt.Rows(0).Item(0));
    //    } else {
    //        RunningNumber_InsertNewYear(code, yCode);
    //        return 0;
    //    }

    //    dt = null;

    //}

    //public string RunningNumber_New(string code)
    //{

    //    int iY = GET_DATE_SERVER().Date.Year;
    //    string strY = "";

    //    if (iY < 2500) {
    //        strY = (BaseGlobalClase.Mid(iY + 543, 3, 2)).ToString.Trim();
    //    } else {
    //        strY = (BaseGlobalClase.Mid(iY, 3, 2)).ToString.Trim();
    //    }

    //    //genRunningNumber = code + yCode
    //    //genRunningNumber = genRunningNumber + Date.Today.Month.ToString("0#")
    //    //genRunningNumber = genRunningNumber + Date.Today.Day.ToString("0#")

    //    string str = "";
    //    int i = 0;
    //    string strZero = "";


    //    str = Convert.ToString(RunningNumber_GetLast(code, strY) + 1);


    //    for (i = 1; i <= 4 - Strings.Len(str); i++) {
    //        strZero = strZero + "0";
    //    }

    //    return strY + "8" + strZero + str;


    //}

	

	#endregion


	//Public Sub BindDataToCombo(ByVal cbo As ComboBox, ByVal dt As DataSet, ByVal displayFld As String, ByVal valueFld As String, Optional ByVal currRow As Integer = 0)
	//    With cbo
	//        .DataSource = dt
	//        .DisplayMember = displayFld
	//        .ValueMember = valueFld
	//        .SelectedIndex = currRow
	//    End With
	//End Sub
	public void StrYNsetCheckBox(string Str_Flag, ref CheckBox objChk)
	{
		if (Str_Flag.Trim().ToUpper() == "Y") {
			objChk.Checked = true;
		} else if (Str_Flag.Trim().ToUpper() == "N") {
			objChk.Checked = false;
		}
	}

	public bool CheckDigitOnlyInteger(int index)
	{
		bool functionReturnValue = false;
		switch (index) {
			case 47:
			case 48:
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:
			case 56:
			case 57:
			case 58:
				// �Ţ 0 - 9
				functionReturnValue = false;
				break;
			case 8:
			case 13:
				// Backspace = 8, Enter = 13, Delete/. = 46
				functionReturnValue = false;
				break;
			default:
				functionReturnValue = true;
				break;
		}
		return functionReturnValue;
	}

	public string CheckBoxsetStrYN(CheckBox objChk)
	{
		if (objChk.Checked == true) {
			return "Y";
		} else if (objChk.Checked == false) {
			return "N";
        }
            else{
                return "";
            }
		}

	public double chkDivideByZero(double Value)
	{
		double result = 0;
		if (Value == 0) {
			result = 0;
		} else {
			result = Value;
		}

		return result;
	}
	public string chkNullByText(string Value)
	{
		string result = null;
		if (string.IsNullOrEmpty(Value)) {
			result = "-";
		} else {
			result = Value;
		}
		return result;
	}
	public string chkNullByBlank(string Value)
	{
		string result = null;
		if (string.IsNullOrEmpty(Value) | Value == "-") {
			result = "";
		} else {
			result = Value;
		}
		return result;
	}




	public System.DateTime chkNullByDate(string Value)
	{
		System.DateTime result = default(System.DateTime);
		if (string.IsNullOrEmpty(Value)) {
			result = DateTime.Now.Date;
		} else {
			result = System.DateTime.Parse(Value);
		}
		return result;
	}


	public string PrepareFieldLenght(string Field_Value, int Field_Length)
	{
		string result = "";
		string tmp_value = "";
		if ((Field_Value != null)) {
			result = Field_Value;
			if (Field_Length > 0 & Field_Value.Length > Field_Length) {
				int CountChar = 0;
				string str = null;
				int i = 0;
				i = 0;
				tmp_value = Field_Value.Substring(0, Field_Length);
				result = tmp_value;
				for (i = 0; i <= tmp_value.Length - 1; i++) {
					str = tmp_value.Substring(i, 1);
					if (str == "'") {
						CountChar += 1;
					}
				}
				if (!(CountChar % 2 == 0)) {
					i = tmp_value.Length - 1;
					while (i >= 0) {
						str = tmp_value.Substring(i, 1);
						if (str == "'") {
							result = tmp_value.Substring(0, i);
							break; // TODO: might not be correct. Was : Exit While
						}
						i -= 1;
					}
				}
			}
		}
		return result;
	}
	public string PrepareSqlValue(string Field_Value)
	{
		string result = "";
		if ((Field_Value != null)) {
			if (Field_Value.Trim().Length > 0) {
				result = Field_Value.Replace("'", "''");
			}
		}
		return result;
	}
	public string ReverseSqlValue(string Field_Value)
	{
		string result = "";
		if ((Field_Value != null)) {
			if (Field_Value.Trim().Length > 0) {
				result = Field_Value.Replace("''", "'");
			}
		}
		return result;
	}

	public string setTimeFormat(string strTime, bool f_millisec = false)
	{
		string strReturn = null;
		if (strTime.Length < 6) {
			strTime = strTime.PadLeft(6, '0');
		}
		if (f_millisec) {
			strReturn = strTime.Substring(0, 2) + ":" + strTime.Substring(2, 2) + ":" + strTime.Substring(4, 2);
		} else {
			strReturn = strTime.Substring(0, 2) + ":" + strTime.Substring(2, 2);
		}
		return strReturn;
	}

	
	public string setValue(object objValue, string strType = "text", int DecimalPoint = -1)
	{
		string strReturn = null;
		bool f_blank_value = true;

		DataTable dt = default(DataTable);
		int int_scrDPnt = 0;

		//Select Case strDecimalType.ToUpper
		//    Case "O", "N" : int_scrDPnt = int_scrDecimalPoint
		//    Case "Q" : int_scrDPnt = int_qtyDecimalPoint
		//    Case "V" : int_scrDPnt = int_valDecimalPoint
		//    Case "R" : int_scrDPnt = int_rptDecimalPoint
		//End Select


		if ((strType == null))
			strType = "text";



		string DecFormat = ("##################0." + "".PadRight(int_scrDPnt, '#'));
		string strDecFormat = ("#,###,###,###,###,###,##0." + "".PadRight(int_scrDPnt, '0'));
		string strIntFormat = ("#,###,###,###,###,###,##0");

		//If f_use_field_decimal Then strDecFormat = ("#,###,###,###,###,###,##0." & "".PadRight(int_scrDPntDB, "#".Chars(0)))


		if ((objValue== null )) {
			f_blank_value = true;
		} else {
			if ((objValue == null)) {
				f_blank_value = true;
			} else {
				if (objValue.ToString().Length > 0) {
					switch (strType.ToLower()) {
						case "sql":
							strReturn = PrepareSqlValue(objValue.ToString());
							break;
						case "text":
						case "string":
							strReturn = objValue.ToString();

							break;
						//ReverseSqlValue อาจจะเรียกใช้งานเฉพาะตอนที่ load ค่า database รึเปล่าเพราะไม่เช่นนั้นถ้าต้องการ set value string ปกติอาจจะ error ได้ครับ
						case "sql_reverse":
							strReturn = ReverseSqlValue(objValue.ToString());

							break;
						case "date":
							strReturn = BaseGlobalClass.setDateFormat(objValue.ToString());
							break;
						case "time":
							strReturn = setTimeFormat(objValue.ToString(), false);
							break;
						case "timems":
						case "timemillisec":
							strReturn = setTimeFormat(objValue.ToString(), true);
							break;
						case "currency":
							strReturn = objValue.ToString();
							break;
						case "decimal":
						case "double":
						case "long":
                            if (int.TryParse(objValue.ToString(), out num))
                            {
								//strReturn = CDbl(objValue).ToString
								//strReturn = Decimal.Round(CType(objValue, Decimal), int_scrDPnt).ToString(DecFormat)
								strReturn = Convert.ToDouble(objValue).ToString(DecFormat);
							} else {
								strReturn = "0";
							}
							break;
						case "strdecimal":
							if (int.TryParse(objValue.ToString() ,out num)) {
								strReturn = Convert.ToDouble(objValue).ToString(strDecFormat);
								//strReturn = Decimal.Round(CType(objValue, Decimal), int_scrDPnt).ToString(strDecFormat)
								//If f_use_field_decimal Then
								//    strReturn = Decimal.Round(CType(objValue, Decimal), int_scrDPntDB).ToString(strDecFormat)
								//Else
								//    strReturn = Decimal.Round(CType(objValue, Decimal), int_scrDecimalPoint).ToString(strDecFormat)
								//End If
							} else {
								strReturn = "0";
							}
							strReturn = ReverseSqlValue(strReturn);
							break;
						case "number":
						case "int":
						case "integer":
						case "numeric":
                            
                            if (int.TryParse(objValue.ToString() ,out num))
                            {
								strReturn = Convert.ToInt32(objValue).ToString();
							} else {
								strReturn = "0";
							}
							break;
						case "strint":
							if (int.TryParse(objValue.ToString() ,out num)) {
								strReturn = decimal.Round(Convert.ToDecimal(objValue), 0).ToString(strIntFormat);
								//If f_use_field_decimal Then
								//    strReturn = Decimal.Round(CType(objValue, Decimal), int_scrDPntDB).ToString(strDecFormat)
								//Else
								//    strReturn = Decimal.Round(CType(objValue, Decimal), int_scrDecimalPoint).ToString(strDecFormat)
								//End If

							} else {
								strReturn = "0";
							}
							strReturn = ReverseSqlValue(strReturn);
							break;
						case "datatable":
							dt = (DataTable)objValue;
							strReturn = dt.Rows.Count.ToString();
							break;
						default:
							strReturn = objValue.ToString();
							break;
					}
					f_blank_value = false;
				}
			}
		}

		if (f_blank_value) {
			switch (strType.ToLower()) {
				case "text":
				case "string":
					strReturn = "";
					break;
				case "date":
					strReturn = "";
					break;
				case "currency":
					strReturn = "0";
					break;
				case "decimal":
				case "double":
				case "long":
					strReturn = "0";
					break;
				case "strdecimal":
					strReturn = "0";
					break;
				case "strint":
					strReturn = "0";
					break;
				case "number":
				case "int":
				case "integer":
				case "numeric":
					strReturn = "0";
					break;
				case "datatable":
					strReturn = "0";
					break;
			}
		}
		return strReturn;
	}
	#endregion


	//Public Sub getConnectionString()
	//    Dim sr As StreamReader
	//    Dim ds As New DataSet
	//    sr = New StreamReader("DataConfig2021.xml")
	//    ds.ReadXml(sr)
	//    sr.Close()
	//    sqlServer = Convert.ToString(ds.Tables(0).Rows(0)("ServerPath"))
	//    sqlDatabase = Convert.ToString(ds.Tables(0).Rows(0)("DatabaseName"))
	//    sqlUsername = Convert.ToString(ds.Tables(0).Rows(0)("Username"))
	//    sqlPassword = Convert.ToString(ds.Tables(0).Rows(0)("Password"))
	//    sqlReport = Convert.ToString(ds.Tables(0).Rows(0)("ReportPath"))

	//    ds = Nothing
	//End Sub

	
	public string addSQLString(string strSQL, string chkVal, string strSQLAdd)
	{
		chkVal = setValue(chkVal);
		strSQL = setValue(strSQL);
		if (strSQL.Length > 0 & chkVal.Length > 0) {
			strSQL += " and " + strSQLAdd;
		} else if (chkVal.Length > 0) {
			strSQL = strSQLAdd;
		}
		return strSQL;
	}


	public string getCheckboxValue(CheckBox chkbox)
	{
		if (chkbox.Checked) {
			return "Y";
		} else {
			return "N";
		}
	}



	public bool chkFieldTableIsExist(string field_name, string table_name)
	{
		string sql = null;
		bool ReturnValue = false;
		//Dim objDB As New baseclass
		field_name = setValue(field_name).Trim();
		table_name = setValue(table_name).Trim();
		sql = " SELECT     dbo.sysobjects.name AS table_name, dbo.syscolumns.name";
		sql += " FROM         dbo.sysobjects INNER JOIN";
		sql += " dbo.syscolumns ON dbo.sysobjects.id = dbo.syscolumns.id";
		sql += " WHERE     (UPPER(LTRIM(RTRIM(dbo.sysobjects.xtype))) = 'U' OR";
		sql += " UPPER(LTRIM(RTRIM(dbo.sysobjects.xtype))) = 'V') AND (UPPER(LTRIM(RTRIM(dbo.sysobjects.name))) = '" + table_name + "') AND ";
		sql += " (UPPER(LTRIM(RTRIM(dbo.syscolumns.name))) = '" + field_name + "')";
		//If  OpenDatabase Then
		DataTable dt = ExecuteQuery(sql);
		if ((dt != null)) {
			ReturnValue = (dt.Rows.Count > 0);
		}
		// CloseDatabase()
		//End If
		//objDB = Nothing
		return ReturnValue;
	}


	#region "veriable for use control"
	//จำนวนแถวที่ต้องการแสดงใน listview
	public int int_showLimtLSVRoW = 100;
	public bool bln_UseDistinct = true;
	public class Display_fld_UscControls
	{
		public const string code = "code";
		public const string name = "name";
	}
	public string chkfldDisplay_UscControls(string value)
	{
		value = setValue(value).Trim().ToLower();
		if (!(value == Display_fld_UscControls.code | value == Display_fld_UscControls.name)) {
			value = Display_fld_UscControls.code;
		}
		return value;
	}
	#endregion

    //public string getCurStrDateNormal(string th_or_en = "")
    //{
    //    int y = 0;
    //    int m = 0;
    //    int d = 0;
     
    //    if (th_or_en.Trim.ToLower == "en") {
    //        y = getYearEng(DateTime.Now.Date.Year);
    //    } else if (th_or_en.Trim.ToLower == "th") {
    //        y = getYearThai(DateTime.Now.Date.Year);
    //    } else {
    //        y = getYearByCurrentLang(DateTime.Now.Date.Year);
    //    }
    //    m = DateTime.Now.Date.Month;
    //    d = DateTime.Now.Date.Day;
    //    return d.ToString("00") + "/" + m.ToString("00") + "/" + y.ToString("0000");
    //}


    //public string getCurStrTime()
    //{
    //    return Strings.Format(DateTime.Now, "HHmmss");
    //}
    //public string getCurStrDateDB(string th_or_en = "")
    //{
    //    int y = 0;
    //    int m = 0;
    //    int d = 0;
    //    if (th_or_en.Trim.ToLower == "en") {
    //        y = getYearEng(DateTime.Now.Date.Year);
    //    } else if (th_or_en.Trim.ToLower == "th") {
    //        y = getYearThai(DateTime.Now.Date.Year);
    //    } else {
    //        y = getYearByCurrentLang(DateTime.Now.Date.Year);
    //    }
    //    m = DateTime.Now.Date.Month;
    //    d = DateTime.Now.Date.Day;
    //    return y.ToString("0000") + m.ToString("00") + d.ToString("00");
    //}
    //public int getYearEng(int y)
    //{
    //    if (y > MAX_ENG_YEAR) {
    //        y -= 543;
    //    }
    //    return y;
    //}
    //public int getYearThai(int y)
    //{
    //    if (y < MAX_ENG_YEAR) {
    //        y += 543;
    //    }
    //    return y;
    //}
    //public int getYearByCurrentLang(int y)
    //{
    //    if (STR_LANGUAGE.Trim.ToLower == "en") {
    //        if (y > MAX_ENG_YEAR) {
    //            y -= 543;
    //        }
    //    } else if (STR_LANGUAGE.Trim.ToLower == "th") {
    //        if (y < MAX_ENG_YEAR) {
    //            y += 543;
    //        }
    //    }
    //    return y;
    //}


    //public bool DeleteByID(string pID, string sTable, string pfeild)
    //{

    //    bool success = false;

    //    if (ExecuteNonQuery("Delete from  " + sTable + "  Where " + pfeild + "='" + pID + "'")) {
    //        success = true;
    //    }
    //    return success;
    //}

#region "check Data Value"

    public  string Zero2StrNull(object str)
    {
        if (int.TryParse(str.ToString(), out num))
        {
            return "";
        }
        else
        {
            if (str == "0")
            {
                return "";
            }
            else
            {
                return Convert.ToString(str);
            }

        }
    }

    public static string DBNull2Str(object str)
    {
        if (str != null)
        { 
         if (string.IsNullOrEmpty(str.ToString()))
        {
            return "";
        }
        else
        {
            return Convert.ToString(str);
        }
           
        }
        else
            {
                return "";            
        }
    }

    public static int DBNull2Zero(object str)
    {
        if (string.IsNullOrEmpty(str.ToString()))
        {
            return 0;
        }
        else
        {
            return Convert.ToInt32(str);
        }
    }
    public static double DBNull2Dbl(object str)
    {
        if (string.IsNullOrEmpty(str.ToString()))
        {
            return 0;
        }
        else
        {
            return Convert.ToDouble(str);
        }
    }

    public long DBNull2Lng(object str)
    {
        if (string.IsNullOrEmpty(str.ToString()))
        {
            return 0;
        }
        else
        {
            if (str.ToString().Length != 0 & int.TryParse(str.ToString(), out num))
            {
                return Convert.ToInt32(str);
            }
            else
            {
                return 0;
            }
        }
    }

    public static int StrNull2Zero(string str)
    {
        if (str=="")
        {
            return 0;
        }
        else
        {
            return Convert.ToInt32(str);
        }
    }

    public static double StrNull2Dbl(string str)
        {
            if (string.IsNullOrEmpty(str.ToString()) || str=="")
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(str);
            }
        }


        public static string ConvertStrDate2DDMMYYYY(string dt)
    {

        if (!string.IsNullOrEmpty(dt))
        {
            string y = "";
            string d = "";
            string m = "";

            string[] str = null;
            str = dt.Split('/');

            d = str[0].ToString();
            m = str[1].ToString();

            while ((d.Length < 2))
            {
                d = ("0" + d);
            }

            while ((m.Length < 2))
            {
                m = ("0" + m);
            }

            if (Convert.ToInt32(BaseGlobalClass.Left(str[2],4)) > 2300)
            {
                y = BaseGlobalClass.Left(str[2], 4); 
            }
            else
            {
                y = (Convert.ToInt32(BaseGlobalClass.Left(str[2], 4)) + 543).ToString();
            }

            return d + "/" + m + "/" + y;

        }
        else
        {
            return "";
        }
    }
    public static string ConvertStrDate2ShortDateQueryString(string dt)
    {

        if (!string.IsNullOrEmpty(dt))
        {
            string y = "";
            string d = "";
            string m = "";

            string[] str = null;
            str = dt.Split('/');

            d = str[0].ToString();
            m = str[1].ToString();

            while ((d.Length < 2))
            {
                d = ("0" + d);
            }

            while ((m.Length < 2))
            {
                m = ("0" + m);
            }

            if (Convert.ToInt32(BaseGlobalClass.Left(str[2], 4)) > 2300)
            {
                y = (Convert.ToInt32(BaseGlobalClass.Left(str[2], 4)) - 543).ToString();
            }
            else
            {
                y = BaseGlobalClass.Left(str[2], 4);
            }

            return m + "/" + d + "/" + y;

        }
        else
        {
            return "";
        }
    }
        public static string ConvertStr2DBDateString(string d,string m,string y)
        {

            if (d!="" && m!= "" && y!="")
            {         
                while ((d.Length < 2))
                {
                    d = ("0" + d);
                }
                
                while ((m.Length < 2))
                {
                    m = ("0" + m);
                }

                if (Convert.ToInt32(y) > 2500)
                {
                    y = (Convert.ToInt32(y) - 543).ToString();
                }
                
                return m + "/" + d + "/" + y;

            }
            else
            {
                return "";
            }
        }
        public static string ConvertStrDate2YYYYMMDDString(string dt)
    {

        if (!string.IsNullOrEmpty(dt))
        {
            string y = "";
            string d = "";
            string m = "";

            string[] str = null;
            str = dt.Split('/');

            d = str[0].ToString();
            m = str[1].ToString();

            while ((d.Length < 2))
            {
                d = ("0" + d);
            }

            while ((m.Length < 2))
            {
                m = ("0" + m);
            }

            if (Convert.ToInt32(BaseGlobalClass.Left(str[2], 4)) > 2300)
            {
                y = (Convert.ToInt32(BaseGlobalClass.Left(str[2], 4)) - 543).ToString();
            }
            else
            {
                y = BaseGlobalClass.Left(str[2], 4);
            }

                return y + m + d;

        }
        else
        {
            return "";
        }
    }
#endregion



    }
}