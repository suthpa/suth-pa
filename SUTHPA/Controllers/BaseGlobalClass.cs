﻿using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlClient;
using System.Drawing.Printing;


namespace  SUTHPA
{
    public static class BaseGlobalClass
    {
        public static DataTable dt = new DataTable();

        #region "Definitoin Parameter"

        public static bool f_debug;
        public static int MAX_ENG_YEAR = 2013;
        public static string STR_LANGUAGE = "th";
        public static string SQL = "";
        public static string SQLwhere = "";
        public static bool has_data = false;

        public static DateTimeFormatInfo dtfInfo;

        public static bool pointDup = false;
        //Public ConnectionString As String = ""

        public static int num;

        public static DataSet ds = new DataSet();
        public static DataRow currentRow = null;

       
        //public static string ReportFormula = "";
        //public static string ReportName = "";
        //public static string ReportTitle = "";
        //public static bool actionPrint = true;
        //public static bool viewClose = false;

        //public static string ReportPath = "Reports/";
        //public static string Reportskey = "";
        //public static string FagRPT = "";
        //public static string RPTTYPE = "";
        //public static string RPTKEY = "";
        //public static string RPTMODE = "";

        public static string[] RPTParameter;



        public static string Datakey = "";
        public static int gLocationUID;
        public static int gServiceUID;
        public static string gPatientHN;
        public static long gPatientUID;
        public static long gPatientVisitUID;


        public static bool gAudiogram;
        public static string gUsername;

        public static string[] ReportParameter;
        public static PrintDocument prDoc = new PrintDocument();

        public static PrintDialog prDlg = new PrintDialog();

      
        #endregion

        #region "Const_Data"
        public const string ACTTYPE_LOG = "LOGIN";
        public const string ACTTYPE_ADD = "ADD";
        public const string ACTTYPE_UPD = "UPDATE";
        public const string ACTTYPE_DEL = "DELETE";
        public const string ACTTYPE_FND = "FIND";

        //public const string ACTTYPE_FGT = "FORGOT PASS";
        //public const string CFG_STARTDATE = "STARTDATE";
        //public const string CFG_ENDDATE = "ENDDATE";

        //public const string CFG_MAXLOCATION = "MAXLOCATION";
        #endregion


        
        #region "General Functions"

        public static bool chkFileExist(string FileRptPath)
        {
            FileInfo F = new FileInfo(FileRptPath);
            return F.Exists;
        }

        public static void SetControlFocus(System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }


        //Public Sub BindDataToCombo(ByVal cbo As ComboBox, ByVal dt As DataSet, ByVal displayFld As String, ByVal valueFld As String, Optional ByVal currRow As Integer = 0)
        //    With cbo
        //        .DataSource = dt
        //        .DisplayMember = displayFld
        //        .ValueMember = valueFld
        //        .SelectedIndex = currRow
        //    End With
        //End Sub

        public static string PrepareFieldLenght(string Field_Value, int Field_Length)
        {
            string result = "";
            string tmp_value = "";
            if ((Field_Value != null))
            {
                result = Field_Value;
                if (Field_Length > 0 & Field_Value.Length > Field_Length)
                {
                    int CountChar = 0;
                    string str = null;
                    int i = 0;
                    i = 0;
                    tmp_value = Field_Value.Substring(0, Field_Length);
                    result = tmp_value;
                    for (i = 0; i <= tmp_value.Length - 1; i++)
                    {
                        str = tmp_value.Substring(i, 1);
                        if (str == "'")
                        {
                            CountChar += 1;
                        }
                    }
                    if (!(CountChar % 2 == 0))
                    {
                        i = tmp_value.Length - 1;
                        while (i >= 0)
                        {
                            str = tmp_value.Substring(i, 1);
                            if (str == "'")
                            {
                                result = tmp_value.Substring(0, i);
                                break; // TODO: might not be correct. Was : Exit While
                            }
                            i -= 1;
                        }
                    }
                }
            }
            return result;
        }
        public static string PrepareSqlValue(string Field_Value)
        {
            string result = "";
            if ((Field_Value != null))
            {
                if (Field_Value.Trim().Length > 0)
                {
                    result = Field_Value.Replace("'", "''");
                }
            }
            return result;
        }

        public static string ReverseSqlValue(string Field_Value)
        {
            string result = "";
            if ((Field_Value != null))
            {
                if (Field_Value.Trim().Length > 0)
                {
                    result = Field_Value.Replace("''", "'");
                }
            }
            return result;
        }

         public static string setTimeFormat(string strTime, bool f_millisec = false)
        {
            string strReturn = null;
            if (strTime.Length < 6)
            {
                strTime = strTime.PadLeft(6, '0');
            }
            if (f_millisec)
            {
                strReturn = strTime.Substring(0, 2) + ":" + strTime.Substring(2, 2) + ":" + strTime.Substring(4, 2);
            }
            else
            {
                strReturn = strTime.Substring(0, 2) + ":" + strTime.Substring(2, 2);
            }
            return strReturn;
        }

        #endregion

           public static int getYearEng(int y)
        {
            if (y > MAX_ENG_YEAR)
            {
                y -= 543;
            }
            return y;
        }
        public static int getYearThai(int y)
        {
            if (y < MAX_ENG_YEAR)
            {
                y += 543;
            }
            return y;
        }
  
          public static bool IsInstallPrinter()
        {
            bool functionReturnValue = false;
            functionReturnValue = false;
            if (prDoc.PrinterSettings.PrinterName == "<no default printer>")
            {
                functionReturnValue = false;
            }
            else
            {
                functionReturnValue = true;
            }
            return functionReturnValue;
        }


          #region "StringExpression"
          public static string Left(this string value, int maxLength)
          {
              if (string.IsNullOrEmpty(value)) return value;
              maxLength = Math.Abs(maxLength);

              return (value.Length <= maxLength ? value : value.Substring(0, maxLength)
                     );
          }
          public static string Right(this string value, int length)
          {
              if (String.IsNullOrEmpty(value)) return string.Empty;

              return value.Length <= length ? value : value.Substring(value.Length - length);
          }

          public static string Mid(this string value, int startindex, int length)
          {
              if (String.IsNullOrEmpty(value))
              {
                  return string.Empty;
              }
              else
              {
                  return value.Substring(startindex, length);
              }



          }


          #endregion

          public  static string setDateFormat(string strDate)
          {
              string functionReturnValue = null;
              int strYear = 0;
              switch (strDate.Trim().Length)
              {
                  case 8:
                      strYear = Convert.ToInt32(BaseGlobalClass.Left(strDate, 4));
                      if (strYear < 2500)
                      {
                          strYear = (strYear + 543);
                      }
                      else
                      {
                          strYear = strYear;
                      }
                      functionReturnValue = BaseGlobalClass.Right(strDate, 2) + "/" + BaseGlobalClass.Mid(strDate, 5, 2) + "/" + strYear;

                      break;
                  case 12:
                      strYear = Convert.ToInt32(BaseGlobalClass.Left(strDate, 4));

                      if (strYear < 2500)
                      {
                          strYear = (strYear + 543);
                      }
                      else
                      {
                          strYear = strYear;
                      }

                      functionReturnValue = BaseGlobalClass.Mid(strDate, 7, 2) + "/" + BaseGlobalClass.Mid(strDate, 5, 2) + "/" + strYear + "@" + BaseGlobalClass.Mid(strDate, 9, 2) + ":" + BaseGlobalClass.Right(strDate, 2);
                      break;
                  default:
                      functionReturnValue = "";

                      break;
              }
              return functionReturnValue;

          }
        //Convert DB string date to normal string date
        #region  "DateTime Format"

        public static string ConvertDate2DB(DateTime strDate, string th_or_en)
        {
            int y, m, d;

            if (th_or_en.Trim().ToLower() == "en") y = getYearEng(strDate.Year);
            else if (th_or_en.Trim().ToLower() == "th") y = getYearThai(strDate.Year);
            else y = getYearEng(strDate.Year);


            m = strDate.Month;
            d = strDate.Day;
            return m.ToString("00") + "/" + d.ToString("00") + "/" + y.ToString("0000");
        }

        public static string ConvertDate2DBFormatString(DateTime strDate, string th_or_en)

        {
            int y, m, d;

            if (th_or_en.Trim().ToLower() == "en") y = getYearEng(strDate.Year);
            else if (th_or_en.Trim().ToLower() == "th") y = getYearThai(strDate.Year);
            else y = getYearEng(strDate.Year);

            m = strDate.Month;
            d = strDate.Day;

            return y.ToString("0000") + "-" + m.ToString("00") + "-" + d.ToString("00");
        }


        public static string ConvertStrDate2DBFormat(string strDate)
        {
            int y, m, d;
            string[] ArrDate = new string[3];
            ArrDate[0] = "";
            ArrDate[1] = "";
            ArrDate[2] = "";
            y = 0;
            m = 0;
            d = 0;
            strDate = Left(strDate, 10);

            if (strDate.Length == 10)
            {
                ArrDate = strDate.ToString().Split('/');
                d = Convert.ToInt32(ArrDate[0]);
                m = Convert.ToInt32(ArrDate[1]);
                y = Convert.ToInt32(ArrDate[2]);

                if (y > 2500)
                {
                    y = y - 543;
                }

            }

            return y.ToString("0000") + "-" + m.ToString("00") + "-" + d.ToString("00");

        }

        public static string ConvertDate2DBString(System.DateTime dt)
        {
            string y = "";
            string d = "";
            string m = "";
            d = dt.Day.ToString();
            m = dt.Month.ToString();

            while ((d.Length < 2))
            {
                d = ("0" + d);

            }

            while ((m.Length < 2))
            {
                m = ("0" + m);

            }

            if (dt.Year < 2500)
            {
                y = (dt.Year + 543).ToString();
            }
            else
            {
                y = dt.Year.ToString();
            }

            return y + m + d;
        }
        public static string ConvertStrDate2DBString(string dt)
        {

            if (!string.IsNullOrEmpty(dt))
            {
                string y = "";
                string d = "";
                string m = "";

                string[] str = null;
                str = dt.Split('/');

                d = str[0].ToString();
                m = str[0].ToString();


                while ((d.Length < 2))
                {
                    d = ("0" + d);
                }

                while ((m.Length < 2))
                {
                    m = ("0" + m);
                }

                if (Convert.ToInt32(str[2]) < 2500)
                {
                    y = (Convert.ToInt32(str[2]) + 543).ToString();
                }
                else
                {
                    y = str[2];
                }

                return y + m + d;
            }
            else
            {
                return "0";
            }
        }
        public static string ConvertStrDate2DateQueryString(string dt)
        {

            if (!string.IsNullOrEmpty(dt))
            {
                string y = "";
                string d = "";
                string m = "";

                string[] str = null;
                str =  Left(dt, 10).Split('/');

                d = str[0].ToString();
                m = str[1].ToString();

                while ((d.Length < 2))
                {
                    d = ("0" + d);
                }

                while ((m.Length < 2))
                {
                    m = ("0" + m);
                }

                if (Convert.ToInt32(str[2]) > 2300)
                {
                    y = (Convert.ToInt32(str[2]) - 543).ToString();
                }
                else
                {
                    y = str[2];
                }

                return y + "-" + m + "-" + d; // + " 00:00:00";

            }
            else
            {
                return "";
            }
        }
        public static string ConvertStrDate2ShortDateQueryString(string dt)
        {

            if (!string.IsNullOrEmpty(dt))
            {
                string y = "";
                string d = "";
                string m = "";

                string[] str = null;
                str = dt.Split('/');

                d = str[0].ToString();
                m = str[0].ToString();

                while ((d.Length < 2))
                {
                    d = ("0" + d);
                }

                while ((m.Length < 2))
                {
                    m = ("0" + m);
                }

                if (Convert.ToInt32(str[2]) > 2300)
                {
                    y = (Convert.ToInt32(str[2]) - 543).ToString();
                }
                else
                {
                    y = str[2];
                }

                return m + "/" + d + "/" + y;

            }
            else
            {
                return "";
            }
        }
        public static string DisplayFullDateTH(System.DateTime dt)
        {
            string y = "";
            string d1 = "";
            string d2 = "";
            string m = "";

            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    d1 = "อาทิตย์";
                    break;
                case DayOfWeek.Monday:
                    d1 = "จันทร์";
                    break;
                case DayOfWeek.Tuesday:
                    d1 = "อังคาร";
                    break;
                case DayOfWeek.Wednesday:
                    d1 = "พุธ";
                    break;
                case DayOfWeek.Thursday:
                    d1 = "พฤหัสบดี";
                    break;
                case DayOfWeek.Friday:
                    d1 = "ศุกร์";
                    break;
                case DayOfWeek.Saturday:
                    d1 = "เสาร์";

                    break;
            }

            d2 = dt.Day.ToString();

            switch (dt.Month)
            {
                case 1:
                    m = "มกราคม";
                    break;
                case 2:
                    m = "กุมภาพันธ์";
                    break;
                case 3:
                    m = "มีนาคม";
                    break;
                case 4:
                    m = "เมษายน";
                    break;
                case 5:
                    m = "พฤษภาคม";
                    break;
                case 6:
                    m = "มิถุนายน";
                    break;
                case 7:
                    m = "กรกฎาคม";
                    break;
                case 8:
                    m = "สิงหาคม";
                    break;
                case 9:
                    m = "กันยายน";
                    break;
                case 10:
                    m = "ตุลาคม";
                    break;
                case 11:
                    m = "พฤศจิกายน";
                    break;
                case 12:
                    m = "ธันวาคม";
                    break;
            }

            if (dt.Year < 2500)
            {
                y = (dt.Year + 543).ToString();
            }
            else
            {
                y = dt.Year.ToString();
            }

            return "วัน" + d1 + "ที่ " + d2 + " " + m + " พ.ศ. " + y;

        }

        public static string DisplayFullDateTHwithoutDOW(System.DateTime dt)
        {
            string y = "";
            string d2 = "";
            string m = "";
            d2 = dt.Day.ToString();

            switch (dt.Month)
            {
                case 1:
                    m = "มกราคม";
                    break;
                case 2:
                    m = "กุมภาพันธ์";
                    break;
                case 3:
                    m = "มีนาคม";
                    break;
                case 4:
                    m = "เมษายน";
                    break;
                case 5:
                    m = "พฤษภาคม";
                    break;
                case 6:
                    m = "มิถุนายน";
                    break;
                case 7:
                    m = "กรกฎาคม";
                    break;
                case 8:
                    m = "สิงหาคม";
                    break;
                case 9:
                    m = "กันยายน";
                    break;
                case 10:
                    m = "ตุลาคม";
                    break;
                case 11:
                    m = "พฤศจิกายน";
                    break;
                case 12:
                    m = "ธันวาคม";
                    break;
            }

            if (dt.Year < 2500)
            {
                y = (dt.Year + 543).ToString();
            }
            else
            {
                y = dt.Year.ToString();
            }

            return "วันที่  " + d2 + "   เดือน  " + m + "   พ.ศ. " + y;
        }

        public static string DisplayDateTH(System.DateTime dt)
        {
            string y = "";
            string d2 = "";
            string m = "";
            d2 = dt.Day.ToString();
            switch (dt.Month)
            {
                case 1:
                    m = "มกราคม";
                    break;
                case 2:
                    m = "กุมภาพันธ์";
                    break;
                case 3:
                    m = "มีนาคม";
                    break;
                case 4:
                    m = "เมษายน";
                    break;
                case 5:
                    m = "พฤษภาคม";
                    break;
                case 6:
                    m = "มิถุนายน";
                    break;
                case 7:
                    m = "กรกฎาคม";
                    break;
                case 8:
                    m = "สิงหาคม";
                    break;
                case 9:
                    m = "กันยายน";
                    break;
                case 10:
                    m = "ตุลาคม";
                    break;
                case 11:
                    m = "พฤศจิกายน";
                    break;
                case 12:
                    m = "ธันวาคม";
                    break;
            }

            if (dt.Year < 2500)
            {
                y = (dt.Year + 543).ToString();
            }
            else
            {
                y = dt.Year.ToString();
            }

            return d2 + " " + m + " " + y;
        }

        #endregion

        public static string Convert2LetterNo(string dt)
        {
            string y = "";
            string i = "";
            y = dt.Substring(0, 4);
            i = dt.Substring(4, 3);

            return Convert.ToInt32(i) + "/" + y;
        }

        public static string DisplayStr2DateTH(string dt)
        {
            string y = "";
            string d2 = "";
            string m = "";
            dt = dt.Left(10);

            d2 = Convert.ToInt32(dt.Left(2)).ToString();
            if ((d2 == "0"))
            {
                d2 = "";
            }
            else
            {
                d2 = d2;
            }
            m = Convert.ToInt32(dt.Substring(3, 2)).ToString();

            switch (m)
            {
                case "0":
                    m = "";
                    break;
                case "1":
                    m = "มกราคม";
                    break;
                case "2":
                    m = "กุมภาพันธ์";
                    break;
                case "3":
                    m = "มีนาคม";
                    break;
                case "4":
                    m = "เมษายน";
                    break;
                case "5":
                    m = "พฤษภาคม";
                    break;
                case "6":
                    m = "มิถุนายน";
                    break;
                case "7":
                    m = "กรกฎาคม";
                    break;
                case "8":
                    m = "สิงหาคม";
                    break;
                case "9":
                    m = "กันยายน";
                    break;
                case "10":
                    m = "ตุลาคม";
                    break;
                case "11":
                    m = "พฤศจิกายน";
                    break;
                case "12":
                    m = "ธันวาคม";
                    break;
            }

            y = dt.Right(4);

            if (BaseClass.StrNull2Zero(y)<2500)
            {
                y = (BaseClass.StrNull2Zero(y) + 543).ToString();
            }
            
            return d2 + " " + m + " " + y;

        }

        public static string DisplayMiniDateTH(System.DateTime dt)
        {
            string y = "";
            string d2 = "";
            string m = "";
            d2 = dt.Day.ToString();

            switch (dt.Month)
            {
                case 1:
                    m = "ม.ค.";
                    break;
                case 2:
                    m = "ก.พ.";
                    break;
                case 3:
                    m = "มี.ค.";
                    break;
                case 4:
                    m = "เม.ย.";
                    break;
                case 5:
                    m = "พ.ค.";
                    break;
                case 6:
                    m = "มิ.ค.";
                    break;
                case 7:
                    m = "ก.ค.";
                    break;
                case 8:
                    m = "ส.ค.";
                    break;
                case 9:
                    m = "ก.ย.";
                    break;
                case 10:
                    m = "ต.ค.";
                    break;
                case 11:
                    m = "พ.ย.";
                    break;
                case 12:
                    m = "ธ.ค.";
                    break;
            }

            if (dt.Year < 2500)
            {
                y = (dt.Year + 543).ToString();
            }
            else
            {
                y = dt.Year.ToString();
            }

            return d2 + " " + m + " " + y;

        }

        public static string DisplayShortDateTH(System.DateTime dt)
        {
            string y = "";
            string d = "";
            string m = "";
            d = dt.Day.ToString();
            m = dt.Month.ToString();

            while ((d.Length < 2))
            {
                d = ("0" + d);

            }

            while ((m.Length < 2))
            {
                m = ("0" + m);

            }
            if (dt.Year < 2500)
            {
                y = (dt.Year + 543).ToString();
            }
            else
            {
                y = dt.Year.ToString();
            }
            return d + "/" + m + "/" + y;
        }

        public static string DisplayShortDateEN(System.DateTime dt)
        {
            string y = "";
            string d = "";
            string m = "";

            d = dt.Day.ToString();
            m = dt.Month.ToString();

            while ((d.Length < 2))
            {
                d = ("0" + d);

            }

            while ((m.Length < 2))
            {
                m = ("0" + m);

            }
            y = dt.Year.ToString();
            return (d + ("/" + (m + ("/" + y))));
        }

        public static string DisplayStr2ShortDateEN(string dt)
        {
            string dd = dt.Substring(6, 2);
            string mm = dt.Substring(4, 2);
            string yy = dt.Substring(0, 4);

            while ((dd.Length < 2))
            {
                dd = ("0" + dd);

            }

            while ((mm.Length < 2))
            {
                mm = ("0" + mm);

            }

            return dd + "/" + mm + "/" + yy;
        }

        public static string DisplayStr2ShortDateTH(string dt)
        {
            if (!string.IsNullOrEmpty(dt))
            {
                string dd = dt.Substring(6, 2);
                string mm = dt.Substring(4, 2);
                string yy = dt.Substring(0, 4);

                while ((dd.Length < 2))
                {
                    dd = ("0" + dd);

                }

                while ((mm.Length < 2))
                {
                    mm = ("0" + mm);

                }
                if (Convert.ToInt32(yy) < 2500)
                {
                    yy = (Convert.ToInt32(yy) + 543).ToString();
                }

                return dd + "/" + mm + "/" + yy;
            }
            else
            {
                return "";
            }

        }

        public static string DisplayTime(System.DateTime dt)
        {
            string m = null;
            string h = null;
            h = dt.Hour.ToString();
            m = dt.Minute.ToString();

            while ((m.Length < 2))
            {
                m = ("0" + m);

            }
            return (h + ("." + (m + " \u0019.")));
        }

    }
}
