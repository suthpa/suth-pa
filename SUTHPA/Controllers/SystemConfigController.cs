﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;

namespace SUTHPA 
{
   
    public class ConfigurationController : BaseClass
    {

        public DataSet ds = new DataSet();
        public string Configuration_GetByCode(string Code)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Configuration_GetByCode", Code);
            return ds.Tables[0].Rows[0].Field<string>(0);
        }
        public DataTable Configuration_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Configuration_Get"));
            return ds.Tables[0];
        }
        public DataTable Configuration_GetByUID(int UID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Configuration_GetByUID"),UID);
            return ds.Tables[0];
        }
        public int Configuration_Save(int UID, string Code, string Name, string Value)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Configuration_Save"), UID,Code,Name,Value);
        }
    }
}
