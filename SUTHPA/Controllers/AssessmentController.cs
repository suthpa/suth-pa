﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUTHPA
{
    public class AssessmentController : BaseClass
    {
        public DataSet ds = new DataSet();

        public DataTable AssessmentCourse_Get(string EmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentCourse_Get"), EmployeeID);
            return ds.Tables[0];
        }


        public DataTable Evaluation_Get(int BYear, int CompetencyTypeUID,int EmployeeLevel)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Evaluation_Get"),BYear, CompetencyTypeUID,EmployeeLevel);
            return ds.Tables[0];
        }

        public DataTable Evaluation_GetByAssessee(int BYear, int EvaluationUID ,  string EmpUIDAssessee,string AssessorID,int EmployeeLevel, int AssessorLevel)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Evaluation_GetByAssessee"), BYear, EvaluationUID,  EmpUIDAssessee,AssessorID, EmployeeLevel, AssessorLevel);
            return ds.Tables[0];
        }
        public DataTable Evaluation_GetByAssesseeNew(int BYear, int CompetencyTypeUID, string EmpUIDAssessee, int EmployeeLevel)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Evaluation_GetByAssesseeNew"), BYear, CompetencyTypeUID, EmpUIDAssessee, EmployeeLevel);
            return ds.Tables[0];
        }



        public double Evaluation_GetTotalScore( int EvaluationUID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Evaluation_GetTotalScore"),   EvaluationUID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDouble(ds.Tables[0].Rows[0][0]);
            }
            else
                return 0;
        }



        public string Assessment_GetComment(int BYear , string EmpUIDAssessor, string EmpUIDAssessee, int EvaluationUID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetComment"),BYear, EmpUIDAssessor, EmpUIDAssessee, EvaluationUID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0][0]);
            }
            else
                return "";             
        }


        public int AssessmentProbation_Save( string AssessorID, string  EmployeeID, int EvaluationUID,double TotalScore,double NetScore, int SEQNO,string Outstanding,string Recessive,string Comments)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentProbation_Save"),AssessorID,EmployeeID,EvaluationUID,TotalScore,NetScore,SEQNO,Outstanding,Recessive,Comments);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
                return 0;
             
        }
        public DataTable AssessmentProbation_Get(string EmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentProbation_Get"),EmployeeID);
            return ds.Tables[0];
        }


        public int AssessmentProbation_GetNextSEQ(string  EmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentProbation_GetNextSEQ"), EmployeeID);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
                return 1;         
        }
        public int Assessment_Save(int BYear , string EmpUIDAssessor, string EmpUIDAssessee, int EvaluationUID,double NetScore1, double NetScore2, string Comment,int RNDNO,string AsmClass,int AssessmentLevelTypeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_Save"),BYear, EmpUIDAssessor, EmpUIDAssessee, EvaluationUID,NetScore1,NetScore2, Comment,RNDNO,AsmClass, AssessmentLevelTypeID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
                return 0;             
        }

        public void AssessmentNewScore_Save(int BYear, string EmployeeID, string CompetencyUID, int Score, int SEQNO,string AssessorID)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentNewScore_Save"),BYear, EmployeeID, CompetencyUID, Score,SEQNO,AssessorID);             
        }
        public void AssessmentScore_Save(int BYear, string AssessmentUID, string CompetencyUID, string Score,int RNDNO)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentScore_Save"), BYear, AssessmentUID, CompetencyUID, Score,RNDNO);
        }

        public void AssessmentCourse_Save(string EmpID,int CourseUID,int PhaseNo, string DeptApprove, string HRApprove)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentCourse_Save"),EmpID,CourseUID,PhaseNo,DeptApprove,HRApprove);
        }
        public void Assessment_ApproveByHR(string EmpID, int PhaseNo)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessment_ApproveByHR"), EmpID, PhaseNo);
        }
        public void Probation_Approve(string EmpID)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Probation_Approve"), EmpID);
        }
        public void Probation_CancelApprove(string EmpID)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Probation_CancelApprove"), EmpID);
        }

        public DataTable Assessment_GetYear()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetYear"));
            return ds.Tables[0];
        }

        public void AssessmentScoreFinal_Save(int BYear, string EmployeeID, string AssessorID, int EvaluationUID,double WeightScore ,double Score, double FinalScore, int RNDNO,string AsmClass, int AssessmentLevelTypeID, int AssessmentID)
        {          
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentScoreFinal_Save"), BYear, EmployeeID,AssessorID, EvaluationUID,WeightScore,Score, FinalScore,  RNDNO,AsmClass,AssessmentLevelTypeID,AssessmentID);
        }
               
        public DataTable AssessmentGrade_Get(int BYear)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentGrade_Get"),BYear);
            return ds.Tables[0];
        }
        public DataTable AssessmentGrade_GetView(int BYear,int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentGrade_GetView"), BYear,RNDNO);
            return ds.Tables[0];
        }

        public DataTable AssessmentScore_GetByEmployee(int BYear, string EmployeeID )
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentScore_GetByEmployee"), BYear, EmployeeID);
            return ds.Tables[0];
        }
        public void AssessmentScore_ConfirmByEmployee(int BYear, string EmployeeID)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentScore_ConfirmByEmployee"), BYear, EmployeeID);
            
        }

        public void AssessmentGrade_Save(int BYear, string EmployeeID, int EvaluationUID, double FinalScore,string MUser,int RNDNO )
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentGrade2021_Save"), BYear, EmployeeID, EvaluationUID,FinalScore, MUser,RNDNO);
        }

        public void AssessmentGrade_Update(int BYear)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentGrade_Update"), BYear);
        }
        public void AssessmentGrade_Save(int BYear, string EmployeeID,   double CQI , double Lean ,   double Discipline , double Training , double Checkup ,   double TotalScore,string MUser)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentGrade_Save"), BYear, EmployeeID,  CQI , Lean ,   Discipline , Training , Checkup ,  TotalScore,MUser);
        }
        
      
        public DataTable Note_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Note_Get"));
            return ds.Tables[0];
        }

        public double LevelScore_Get(int BYear, int EmployeeLevel,  int EvaluationUID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LevelScore_Get"), BYear, EmployeeLevel, EvaluationUID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return BaseClass.DBNull2Dbl(ds.Tables[0].Rows[0][0]);
            }
            else
                return 0;

        }

        public string AssessmentStatusByEmployee(int BYear, string EmployeeID, int LevelID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentStatusByEmployee"), BYear, EmployeeID,LevelID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return  ds.Tables[0].Rows[0][0].ToString();
            }
            else
                return "";
        }


    }
}
