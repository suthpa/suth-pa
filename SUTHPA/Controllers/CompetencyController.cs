using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;

namespace  SUTHPA
{
    public class CompetencyController : BaseClass
    {
        
        public DataSet ds = new DataSet();
        public DataTable Competency_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Competency_Get"));
            return ds.Tables[0];
        }

        public DataTable Evaluation_GetByAssessmentLevel(int BYear,string  LevelUID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Evaluation_GetByAssessmentLevel", BYear,   LevelUID);
            return ds.Tables[0];
        }

        public DataTable Evaluation_GetByEmployee(int BYear,string EmployeeID , string LevelUID,string AssessorID,string AsmClass,int RNDNO, int  AssessmentLevelTypeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Evaluation_GetByEmployee", BYear, EmployeeID, LevelUID,AssessorID,AsmClass,RNDNO, AssessmentLevelTypeID);
            return ds.Tables[0];
        }
        public DataTable Evaluation_GetByEmployeeNew(int BYear, string EmployeeID, string LevelUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Evaluation_GetByEmployeeNew", BYear, EmployeeID, LevelUID, AssessorID);
            return ds.Tables[0];
        }


        public bool Competency_IsSuperCompetency(string Competencyname)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Competency_IsSuperCompetency"), Competencyname);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (DBNull2Zero(ds.Tables[0].Rows[0][0]) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
              

            }
            else
            {
                return false;
            }
        }
        public DataTable Competency_GetByID(int pID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Competency_GetByID"), pID);
            return ds.Tables[0];
        }
        public DataTable Competency_GetAssessorDetail(string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Competency_GetAssessorDetail"), EmpID);
            return ds.Tables[0];
        }
        public DataTable Competency_GetAssessees(string LevelUID,string DivisionUID,string DepartmentUID,string CompetencyID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Competency_GetAssessees"), LevelUID,DivisionUID,DepartmentUID,CompetencyID);
            return ds.Tables[0];
        }
        public string Competency_GetName(int Competencyname)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "LabAutoPrint_Competency_GetName", Competencyname);
            if (ds.Tables[0].Rows.Count>0)
            {
                return string.Concat(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return "";
            }
           
        }


        public int Competency_LastLog_Update(string pCompetencyname)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_LastLog_Update"), pCompetencyname);
        }
        public int Competency_Delete(int pID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_Delete"), pID);
        }

        public int Competency_Add(string Competencyname, string Password, string FName, string LName, string Email, int IsSuperCompetency, int Status, int CompetencyProfileID, string ProfileID)
        {

            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_Add"), Competencyname, Password, FName, LName, Email, IsSuperCompetency, Status, CompetencyProfileID,
            ProfileID);
        }

        public int Competency_Update(string Competencyname, string Password, string FName, string LName, string Email, int IsSuperCompetency, int Status, int CompetencyProfileID, string ProfileID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_Update"), Competencyname, Password, FName, LName, Email, IsSuperCompetency, Status, CompetencyProfileID,
            ProfileID);
        }

        public int CompetencyRole_Add(string Competencyname, int RoleID, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CompetencyRole_Add"), Competencyname, RoleID, UpdBy);
        }

        public int Competency_UpdateProfileID(string Competencyname)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_UpdateProfileID"), Competencyname);
        }



        public int CompetencyRole_UpdateStatus(string Competencyname, int RoleID, int bActive, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CompetencyRole_UpdateStatus"), Competencyname, RoleID, bActive, UpdBy);
        }

        public int Competency_ChangePassword(string Competencyname, string Password)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_ChangePassword"), Competencyname, Password);
        }

        public int Competency_GenLogfile(string Competencyname, string Act_Type, string DB_Effective, string Descrp, string Remark)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_genLogfile"), Competencyname, Act_Type, DB_Effective, Descrp, Remark);
        }

        public int Competency_UpdateMail(string Competencyname, string email)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_UpdateEmail"), Competencyname, email);
        }
        public int Competency_ReasonFinalSave(long PatientUID, long PatientVisitUID, string Competencyname, string ReasonTxT)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Competency_ReasonFinalSave"), PatientUID, PatientVisitUID, Competencyname, ReasonTxT);
        }

    }
    public class CompetencyRoleController : BaseClass
    {

        public DataSet ds = new DataSet();
        public DataTable CompetencyRole_GetByCompetencyID(int pCompetencyID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CompetencyRole_GetByCompetencyID"), pCompetencyID);
            return ds.Tables[0];
        }

        public DataTable CompetencyRole_GetActiveRoleByUID(int pCompetencyID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CompetencyRole_GetActiveRoleByUID"), pCompetencyID);
            return ds.Tables[0];
        }


        public bool CompetencyAction_CheckByCompetency(string pCompetency, int pLocation)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CompetencyAction_CheckByCompetency"), pCompetency, pLocation);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CompetencyAction_Checked(string pCompetency, int pLocation)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CompetencyAction_Checked"), pCompetency, pLocation);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public int CompetencyAction_Add(string Competencyname, int LocationID, int RoleAction, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CompetencyAction_Add"), Competencyname, LocationID, RoleAction, UpdBy);
        }
        public int CompetencyAction_Update(string Competencyname, int LocationID, int RoleAction, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CompetencyAction_Update"), Competencyname, LocationID, RoleAction, UpdBy);
        }


    }


}