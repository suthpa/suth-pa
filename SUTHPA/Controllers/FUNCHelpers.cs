﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUTHPA
{
    public class FUNCHelpers
    {
        #region ClearData
        public static IEnumerable<Control> ClearData(Control container)
        {
            List<Control> controlList = new List<Control>();
            foreach (Control ctrl in container.Controls)
            {
                controlList.AddRange(ClearData(ctrl));
                if (ctrl is TextBox)
                {
                    if (ctrl.Text != "0")
                        ctrl.Text = string.Empty;
                }
                if(ctrl is DevExpress.XtraEditors.TextEdit)
                {
                    (ctrl as DevExpress.XtraEditors.TextEdit).Text = "";
                }
                if (ctrl is ComboBox)
                {
                    ctrl.Text = null;
                }
                if (ctrl is DevExpress.XtraEditors.LookUpEdit)
                {
                    (ctrl as DevExpress.XtraEditors.LookUpEdit).EditValue = null;
                }
                if (ctrl is DevExpress.XtraEditors.SpinEdit)
                {
                    (ctrl as DevExpress.XtraEditors.SpinEdit).Value = 0;
                }
                if (ctrl is CheckBox)
                {
                    (ctrl as CheckBox).Checked = false;
                }
                if (ctrl is DevExpress.XtraEditors.RadioGroup)
                    (ctrl as DevExpress.XtraEditors.RadioGroup).SelectedIndex = -1;
                if (ctrl is DevExpress.XtraEditors.CheckEdit)
                    (ctrl as DevExpress.XtraEditors.CheckEdit).Checked = false;
            }
            return controlList;
        }
        #endregion
        #region SetRowsColorStyle
        public static void SetGridviewColorStyle(DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if ((e.RowHandle % 2) == 0)
            {
                e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#F9F9F9");
            }
            else
            {
                e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#F5FAFF");
            }
        }
        #endregion
        #region CheckDuplicateInGridView
        public static bool CheckDuplicateInGridView(DevExpress.XtraGrid.Views.Grid.GridView view, int ColumnIndex, string Value)
        {
            bool result = false;
            for (int i = 0; i < view.RowCount; i++)
            {
                if (view.GetRowCellValue(i, view.Columns[ColumnIndex]).ToString() == Value)
                    result = true;
            }
            return result;
        }
        #endregion
        #region Remove Duplicate Words in text
        public static string RemoveDuplicateWords(string v)
        {
            // 1
            // Keep track of words found in this Dictionary.
            var d = new Dictionary<string, bool>();

            // 2
            // Build up string into this StringBuilder.
            StringBuilder b = new StringBuilder();

            // 3
            // Split the input and handle spaces and punctuation.
            string[] a = v.Split(new char[] { ' ', ',', ';', '.', ']' },
                StringSplitOptions.RemoveEmptyEntries);

            // 4
            // Loop over each word
            foreach (string current in a)
            {
                // 5
                // Lowercase each word
                string lower = current.ToLower();

                // 6
                // If we haven't already encountered the word,
                // append it to the result.
                if (!d.ContainsKey(lower))
                {
                    b.Append(current).Append(']');
                    d.Add(lower, true);
                }
            }
            // 7
            // Return the duplicate words removed
            return b.ToString().Trim();
        }
        #endregion
        #region Get And Set CheckListBox
        public static string GetCheckedListBoxValue(DevExpress.XtraEditors.CheckedListBoxControl ctrl)
        {
            string result = "";
            for (int i = 0; i < ctrl.ItemCount; i++)
            {
                if (ctrl.GetItemChecked(i))
                    result += ctrl.GetItemValue(i) + ",";
            }
            try
            {
                result = result.Substring(0, result.Length - 1);
            }
            catch { result = ""; }
            return result;
        }
        public static void SetCheckListEdit(DevExpress.XtraEditors.CheckedListBoxControl checkcontrol, string values)
        {
            try
            {
                string accessright = RemoveDuplicateWords(values);

                for (int index = 0; index < checkcontrol.ItemCount; index++)
                {
                    checkcontrol.SetItemChecked(index, false);
                }
                string[] arr = accessright.Split(new[] { ' ', ',', ';', '.', ']' });
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    for (int j = 0; j < checkcontrol.ItemCount; j++)
                    {
                        if (checkcontrol.GetItemValue(j).ToString() == arr[i].Substring(0).ToString())
                        {
                            checkcontrol.SetItemChecked(j, true);
                        }
                    }
                }
            }
            catch { }
        }
        #endregion
       
        #region MessageBox
        public static void PopupMessageWarning(string msg)
        {
            MessageBox.Show(msg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static void PopupMessageError(string msg)
        {
            MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static void PopupMessageSuccess(string msg)
        {
            MessageBox.Show(msg, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        #endregion
        #region SetTextSelectAll
        public static void SetTextSelectAll(object sender)
        {
            var edit = ((DevExpress.XtraEditors.TextEdit)sender);
            edit.BeginInvoke(new MethodInvoker(() =>
            {
                edit.SelectionStart = 0;
                edit.SelectionLength = edit.Text.Length;
            }));
        }
        #endregion
        #region SetKeyboardLayout
        public static void SetKeyboardLayout(string Language)
        {
            foreach (InputLanguage Lng in InputLanguage.InstalledInputLanguages)
            {
                Console.WriteLine(Lng.Culture.TwoLetterISOLanguageName);
                if (Lng.Culture.TwoLetterISOLanguageName.StartsWith(Language))
                    InputLanguage.CurrentInputLanguage = Lng;
            }
        }
        #endregion
        #region GeneratePersonID
        public static string GeneratePersonID()
        {
            Random rnd = new Random();

            string ID = "024060" + rnd.Next(100000, 999999).ToString();
            var rndDigit = ID.Select(t => int.Parse(t.ToString())).ToArray();
            int digit = 13;
            int result = 0;
            string tmpID = "";
            foreach (int i in rndDigit)
            {
                result += (i * digit);
                digit--;
            }
            result = 11 - (result % 11);
            if (result.ToString().Length > 1)
                result = Convert.ToInt32(result.ToString().Substring(1, 1));
            tmpID = ID + result.ToString();
            return String.Format("{0:0-####-#####-##-#}", Convert.ToInt64(tmpID));
        }
        #endregion
        #region CID Format
        public static string CID_Format(string CID)
        {
            if (!string.IsNullOrEmpty(CID))
            {
                var Digit = CID.Select(t => int.Parse(t.ToString())).ToArray();
                if (Digit[0] == 0)
                    return String.Format("{0:0-####-#####-##-#}", Convert.ToInt64(CID));
                else
                    return String.Format("{0:#-####-#####-##-#}", Convert.ToInt64(CID));
            }
            else
            {
                return null;
            }
        }
        #endregion
        #region Format String
        public static string FormatString(string Format,string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var Digit = value.Select(t => int.Parse(t.ToString())).ToArray();
                if (Digit[0] == 0)
                    return String.Format(Format, Convert.ToInt64(value));
                else
                    return String.Format(Format, Convert.ToInt64(value));
            }
            else
            {
                return null;
            }
        }
        #endregion
        #region Check format PID
        public static bool IsValidCheckPersonID(string PID)
        {
            //ตรวจสอบว่าทุกตัวเป็นตัวเลข
            if (PID.ToCharArray().All(c => char.IsNumber(c)) == false)
                return false;

            //ตรวจสอบว่ามีทั้งหมด 13 หลัก
            if (PID.Trim().Length != 13)
                return false;

            char[] numberChars = PID.ToCharArray();

            int total = 0;

            for(int i =0; i < numberChars.Length-1; i++)
            {
                total += int.Parse(PID[i].ToString()) * (13-i);
            }
            int result = 11 - (total % 11);
            if (result.ToString().Length > 1)
                result = Convert.ToInt32(result.ToString().Substring(1, 1));
            return PID[12].ToString() == result.ToString();
        }
        #endregion
        #region InputBox
        public static DialogResult InputBox(string title,string promptText,ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOK = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOK.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOK.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 10, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOK.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            label.Font = new System.Drawing.Font("Segoe UI", 10);
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOK, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOK;
            form.CancelButton = buttonCancel;
            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }
        #endregion
        #region Read and Write Image
        public static byte[] ImageToByte(Image img)
        {
            byte[] arr;
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                arr = ms.ToArray();
            }
            return arr;
        }
        public static Image ByteToImage(byte[] ImageBinary)
        {
            try
            {
                byte[] image = (ImageBinary);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(image);
                return Image.FromStream(ms);
            }
            catch { return null; }
        }
        #endregion
        #region Show Search Dialog
        //public static string ShowSearchDlg(Control Ctrl, System.Data.DataTable dtSearch, string Flag)
        //{
        //    Point locationOnForm = Ctrl.PointToScreen(Point.Empty);

        //    frmSearchDlg frmSearch = new frmSearchDlg();
        //    frmMain m = new frmMain();
        //    frmSearch.X = locationOnForm.X;//(locationOnForm.X - frmSearch.Width) + txtOccID.Width + 100;
        //    frmSearch.Y = locationOnForm.Y + Ctrl.Height + 10;

        //    frmSearch.dtSearch = dtSearch.Copy();
        //    frmSearch.Flag = Flag;
        //    frmSearch.ShowDialog();
        //    return frmSearch.Code;
        //}
        //public static string ShowSearchDlg_Left(Control Ctrl, System.Data.DataTable dtSearch, string Flag)
        //{
        //    Point locationOnForm = Ctrl.PointToScreen(Point.Empty);

        //    frmSearchDlg frmSearch = new frmSearchDlg();
        //    frmMain m = new frmMain();
        //    frmSearch.X = (locationOnForm.X - frmSearch.Width) + Ctrl.Width;
        //    frmSearch.Y = locationOnForm.Y + Ctrl.Height + 10;
        //    frmSearch.dtSearch = dtSearch.Copy();
        //    frmSearch.Flag = Flag;
        //    frmSearch.ShowDialog();
        //    return frmSearch.Code;
        //}
        #endregion
        #region ShowTooltip
        public static void ShowToolTip(Control Ctrl, string Message)
        {
            DevExpress.Utils.ToolTipController tooltip = new DevExpress.Utils.ToolTipController();
            tooltip.AutoPopDelay = 3000;
            if (string.IsNullOrEmpty(Message))
                Message = "เคาะวรรคเพื่อค้นหา";
            Point toolTipLocation = Ctrl.PointToScreen(new Point(0, 0));
            tooltip.ShowHint(Message, toolTipLocation);
        }
        #endregion
        #region get Address Auto
        public static string GetAddress(string ChwID, string AmpID, string TmbID, ref string zipcode, ref DataTable dtThaiAddress)
        {
            string addrResult = "";
            DataRow[] result = null;
            if (!string.IsNullOrEmpty(ChwID))
                result = dtThaiAddress.Select("codetype = '1' and chwpart='" + ChwID + "'");
            if (!string.IsNullOrEmpty(AmpID))
                result = dtThaiAddress.Select("codetype = '2' and chwpart='" + ChwID + "' and amppart='" + AmpID + "'");
            if (!string.IsNullOrEmpty(TmbID))
                result = dtThaiAddress.Select("codetype = '3' and chwpart='" + ChwID + "' and amppart='" + AmpID + "' and tmbpart='" + TmbID + "'");
            if (result.Length > 0)
            {
                addrResult = result[0].Field<string>("Description").ToString();
                try
                {
                    zipcode = result[0].Field<string>("zipcode").ToString();
                }
                catch { zipcode = ""; }
            }
            return addrResult;
        }

        public static string GetAddressNameByZipCode(string Zipcode, string sKey, string fKey, ref DataTable dtThaiAddress)
        {
            string NameResult = string.Empty;
            string IDResult = string.Empty;
            string addrResult = string.Empty;
            DataRow[] result = null;

            string V_ProvinceID, V_AmphorID, V_TumbolID;
            //int V_COUNT;

            result = dtThaiAddress.Select("zipcode ='" + Zipcode + "'");
            if (result.Length > 0)
            {


                V_ProvinceID = result[0].Field<string>("chwpart").ToString();
                V_AmphorID = result[0].Field<string>("amppart").ToString();
                V_TumbolID = result[0].Field<string>("tmbpart").ToString();

                switch (sKey)
                {
                    case "1":
                        result = dtThaiAddress.Select("codetype = '1' and chwpart='" + V_ProvinceID + "'");
                        NameResult = result[0].Field<string>("Description").ToString();
                        IDResult = V_ProvinceID;
                        break;
                    case "2":
                        result = dtThaiAddress.Select("codetype = '2' and chwpart='" + V_ProvinceID + "' and amppart='" + V_AmphorID + "'");
                        IDResult = V_AmphorID;
                        NameResult = result[0].Field<string>("Description").ToString();
                        break;
                    case "3":
                        result = dtThaiAddress.Select("codetype = '3' and chwpart='" + V_ProvinceID + "' and amppart='" + V_AmphorID + "' and zipcode='" + Zipcode + "'");

                        if (result.Length == 1)
                        {
                            NameResult = result[0].Field<string>("Description").ToString();
                            IDResult = V_TumbolID;
                        }
                        else
                        {
                            NameResult = "";
                            IDResult = "";

                        }

                        break;
                }

                if (result.Length > 0)
                    if (fKey == "ID")
                    {
                        addrResult = IDResult;
                    }
                    else
                    {
                        addrResult = NameResult;
                    }

            }

            return addrResult;
        }
        #endregion
        #region ADD_View_to_Datatable
        public static DataTable ADD_View_to_Datatable(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            DataTable dtResult = new DataTable();
            foreach (DevExpress.XtraGrid.Columns.GridColumn column in view.Columns)
            {
                dtResult.Columns.Add(column.FieldName, column.ColumnType);
            }
            for (int i = 0; i < view.DataRowCount; i++)
            {
                DataRow row = dtResult.NewRow();
                foreach (DevExpress.XtraGrid.Columns.GridColumn column in view.Columns)
                {
                    row[column.FieldName] = view.GetRowCellValue(i, column);
                }
                dtResult.Rows.Add(row);
            }
            return dtResult;
        }
        #endregion
        #region Date/Time Stamp
     
        #endregion
        #region Set Control Autocomplete
        public static void SetAutoCompleteString(DevExpress.XtraEditors.TextEdit Ctrl,DataTable Table)
        {
            AutoCompleteStringCollection collection = new AutoCompleteStringCollection();
            foreach (DataRow row in Table.Rows)
            {
                collection.Add(row["name"].ToString());
            }
            Ctrl.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            Ctrl.MaskBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            Ctrl.MaskBox.AutoCompleteCustomSource = collection;
        }
        #endregion
        #region Form Style Fixed Single
        public static void FormStyleFixedSingle(Form form)
        {
            form.FormBorderStyle = FormBorderStyle.FixedSingle;
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowIcon = false;
            form.ShowInTaskbar = false;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
        }
        #endregion
    }
}
