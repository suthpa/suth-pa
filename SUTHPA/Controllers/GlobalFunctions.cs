﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUTHPA
{
    public class GlobalFunctions
    {

        #region Convert CheckBox Status

        public static string ConvertCheckBoxStatus2YN(Boolean s)
        {
            if (s == true) return "Y"; else return "N";
        }
        public static Boolean ConvertYN2CheckBoxStatus(string s)
        {
            if (s == "Y") return true; else return false;
        }

        public static string InvertCheckBoxStatus2YN(Boolean s) 
        {
            if (s == true) return "N"; else return "Y";
        }
        public static Boolean InvertYN2CheckBoxStatus(string s)
        {
            if (s == "Y") return false; else return true;
        }

        public static string ConvertCheckBox2StatusActive(Boolean s)
        {
            if (s == true) return "A"; else return "D";
        }
        public static Boolean ConvertStatusActive2CheckBox(string s)
        {
            if (s == "A") return true; else return false;
        }
        #endregion

        #region Convert Null to Datatype
        //public static int StrNull2Zero(string str)
        //{
        //    if (str == "") return 0; else return Convert.ToInt32(str);
        //}

        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { } // just dismiss errors but return false
            return false;
        }
    #endregion
}
}
