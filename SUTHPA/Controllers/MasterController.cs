using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;

namespace  SUTHPA
{
    public class MasterController : BaseClass
    {

        DataSet ds = new DataSet();

        public DataTable Level_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Level_Get"));
            return ds.Tables[0];
        }


        #region "Direction"
        public DataTable Direction_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Direction_Get"));
            return ds.Tables[0];
        }
        #endregion

        #region "Division"
        public DataTable Division_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_Get"));
            return ds.Tables[0];
        }
        public DataTable Division_GetActive()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_GetActive"));
            return ds.Tables[0];
        }

        public DataTable Division_GetByUID(int UID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_GetByUID"),UID);
            return ds.Tables[0];
        }
        public int Division_Save(int UID, string Code, string Name,string NameEN ,int DirectionUID ,string StatusFlag)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Division_Save"), UID, Code, Name, NameEN, DirectionUID, StatusFlag);
        }

        public int Division_Delete(int UID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Division_Delete"), UID);              
        }
        public int Division_GetUID(string name)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_GetUID"), name);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            else return 0;
        }
        #endregion

        #region "Department"
        public DataTable Department_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_Get"));
            return ds.Tables[0];
        }
        public DataTable Department_GetByUID(int UID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_GetByUID"), UID);
            return ds.Tables[0];
        }
        public int Department_Save(int UID, string Code, string Name, string NameEN, int DivistionUID, string StatusFlag)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Department_Save"), UID, Code, Name, NameEN, DivistionUID, StatusFlag);
        }

        public int Department_Delete(int UID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Department_Delete"), UID);
        }
        public int Department_GetUID(string name)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_GetUID"), name);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            else return 0;
        }
        #endregion

        #region "Position"
        public DataTable Position_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_Get"));
            return ds.Tables[0];
        }
        public DataTable Position_GetByUID(int UID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_GetByUID"), UID);
            return ds.Tables[0];
        }
        public int Position_Save(int UID,  string Name,   int LevelID, string StatusFlag)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Position_Save"), UID,  Name,LevelID, StatusFlag);
        }

        public int Position_Delete(int UID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Position_Delete"), UID);
        }
        public int Position_GetUID(string name)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_GetUID"), name);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            else return 0;
        }
        #endregion

    }
}