using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;

namespace  SUTHPA
{
    public class EmployeeController : BaseClass
    {
        
        public DataSet ds = new DataSet();

        #region "Employee"
              

        public DataTable Employee_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_Get"));
            return ds.Tables[0];
        }

        public DataTable Employee_GetJob(string EmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetJob"), EmployeeID);
            return ds.Tables[0];
        }



        public DataTable Employee_GetBannerInfo(int BYear, string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetBannerInfo"),  BYear,  EmpID);
            return ds.Tables[0];
        }

        public DataTable Employee_GetLevel()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetLevel"));
            return ds.Tables[0];
        }
        public DataTable Employee_GetDepartment()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDepartment"));
            return ds.Tables[0];
        }
        public DataTable Employee_GetDepartmentByDivision(int DivUID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDepartmentByDivision"),DivUID);
            return ds.Tables[0];
        }

        public DataTable GetDirection()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Direction_Get"));
            return ds.Tables[0];
        }
        public DataTable Employee_GetDirection(int BYear, string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDirection"), BYear, EmpID);
            return ds.Tables[0];
        }

        public DataTable Employee_GetDivision()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDivision"));
            return ds.Tables[0];
        }
        public DataTable Employee_GetDivisionByDirection(int DirectionUID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDivisionByDirection"), DirectionUID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetPosition()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetPosition"));
            return ds.Tables[0];
        }


        public DataTable Director_GetDirection(string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Director_GetDirection"), EmpID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetEmployeeDirection(int BYear , string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetEmployeeDirection"), BYear, EmpID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetEmployeeDivision(string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetEmployeeDivision"), EmpID);
            return ds.Tables[0];
        }

        public DataTable Employee_GetEmployeeDepartment(string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetEmployeeDepartment"), EmpID);
            return ds.Tables[0];
        }


        public DataTable Employee_Login(int BYear , string pEmployeename, string pPassword)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Employee_Login",BYear, pEmployeename, pPassword);
            return ds.Tables[0];
        }

        public string Employee_GetPassword(string pEmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Employee_GetPassword", pEmployeeID);
            if (ds.Tables[0].Rows.Count>0)
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else return "N/A";
        }

        public int Employee_ChangePassword(string pEmployeeID, string pPassword)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, "Employee_ChangePassword", pEmployeeID, pPassword);          
        }

        public DataTable Employee_NotAssessment(int pYear)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_Employee_NotAssessment", pYear);
            return ds.Tables[0];
        }
        
        public DataTable EmployeeRelationAssessment(int pYear)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_EmployeeRelationAssessment", pYear);
            return ds.Tables[0];
        }

        public bool Employee_IsSuperEmployee(string Employeename)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_IsSuperEmployee"), Employeename);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (DBNull2Zero(ds.Tables[0].Rows[0][0]) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
              

            }
            else
            {
                return false;
            }
        }
        public DataTable Employee_GetByCode(string pCode)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetByCode"), pCode);
            return ds.Tables[0];
        }

        public DataTable Employee_GetByID(int pID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetByID"), pID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssessorStatus(string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorStatus"),  EmpID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssessorTOPDetail(int BYear ,string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorTOPDetail"),BYear,EmpID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssessorDetail(int BYear, string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorDetail"), BYear, EmpID);
            return ds.Tables[0];
        }

        public DataTable Employee_GetAssessorDetail4Relation(string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorDetail4Relation"),EmpID);
            return ds.Tables[0];
        }

        public DataTable Employee_GetAssessorDetailNew(int BYear, string EmpID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorDetailNew"), BYear, EmpID);
            return ds.Tables[0];
        }

        public DataTable Employee_GetAssessees(int BYear)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees"), BYear );
            return ds.Tables[0];
        }

        public DataTable Employee_GetAssessees(int BYear,string LevelUID, string DivisionUID, string DepartmentUID, string EmployeeID, string AsmClass)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2021"),BYear, LevelUID, DivisionUID, DepartmentUID, EmployeeID, AsmClass);
            return ds.Tables[0];
        }

        public DataTable Employee_GetAssesseeManager(int BYear,string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseeManager"),BYear, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetNewAssesseeHeader(int BYear, string DivisionUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetNewAssesseeHeader"), BYear, DivisionUID, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssesseeHeader(int BYear,string DivisionUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseeHeader"),BYear, DivisionUID, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssesseesByDepartment(int BYear,string DepartmentUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByDepartment"),BYear, DepartmentUID, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetNewAssesseesByDepartment(int BYear, string DepartmentUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetNewAssesseesByDepartment"), BYear, DepartmentUID, AssessorID);
            return ds.Tables[0];
        }


        //-----2020--------
        public DataTable Employee_GetAssessees2LevelByTOP(int BYear,string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByTOP"), BYear,AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssessees2LevelByDirector(int BYear,string DirectionUID,  string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByDirector"), BYear, DirectionUID , AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssessees2LevelByManager(int BYear, string DivisionUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByManager"), BYear, DivisionUID, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssessees2LevelByHeader(int BYear, string DepartmentUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByHeader"), BYear, DepartmentUID, AssessorID);
            return ds.Tables[0];
        }

        //-----------------
        //-----2021--------
        public DataTable Employee_GetAssesseesByTOP2021(int BYear, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByTOP2021"), BYear, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssesseesByDirector2021(int BYear, string DirectionUID, string AssessorID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByDirector2021"), BYear, DirectionUID, AssessorID);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssesseesByManager2021(int BYear, string DivisionUID, string AssessorID, int LevelID, string AsmClass)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByManager2021"), BYear, DivisionUID, AssessorID, LevelID, AsmClass);
            return ds.Tables[0];
        }
        public DataTable Employee_GetAssesseesByHeader2021(int BYear,string DivisionUID,string DepartmentUID, string AssessorID,int LevelID,string AsmClass)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByHeader2021"), BYear, DivisionUID, DepartmentUID, AssessorID,LevelID,AsmClass);
            return ds.Tables[0];
        }

        //---------
        public DataTable Employee_GetRelationByTOP(int BYear, string AssessorID, int LevelID, int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByTOP"), BYear, AssessorID, LevelID, RNDNO);
            return ds.Tables[0];
        }

        public DataTable RPT_HODRelation(int BYear)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_HODRelation"), BYear);
            return ds.Tables[0];
        }

        public DataTable Employee_GetRelationByDirector(int BYear, string AssessorID, int LevelID, int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByDirector"), BYear, AssessorID, LevelID, RNDNO);
            return ds.Tables[0];
        }
        public DataTable Employee_GetRelationByManager(int BYear, string AssessorID, int LevelID, int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByManager"), BYear, AssessorID, LevelID, RNDNO);
            return ds.Tables[0];
        }
        public DataTable Employee_GetRelationByHeader(int BYear, string AssessorID, int LevelID, int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByHeader"), BYear, AssessorID, LevelID, RNDNO);
            return ds.Tables[0];
        }

        public DataTable Employee_GetRelationBySubHeader(int BYear, string AssessorID, int LevelID, int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationBySubHeader"), BYear, AssessorID, LevelID, RNDNO);
            return ds.Tables[0];
        }


        public DataTable Employee_GetRelationByAssessor(int BYear, string AssessorID, int LevelID, int RNDNO)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByAssessor"), BYear, AssessorID, LevelID, RNDNO);
            return ds.Tables[0];
        }


        //------------
        public DataTable Employee_GetNewAssesseesByHR(int BYear)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetNewAssesseesByHR"),BYear);
            return ds.Tables[0];
        }
        public int Employee_LastLog_Update(string pEmployeename)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_LastLog_Update"), pEmployeename);
        }
        public int Employee_Delete(int pID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Delete"), pID);
        }

        public int Employee_Add(string Employeename, string Password, string FName, string LName, string Email, int IsSuperEmployee, int Status, int EmployeeProfileID, string ProfileID)
        {

            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Add"), Employeename, Password, FName, LName, Email, IsSuperEmployee, Status, EmployeeProfileID,
            ProfileID);
        }

        public int Employee_Update(string Employeename, string Password, string FName, string LName, string Email, int IsSuperEmployee, int Status, int EmployeeProfileID, string ProfileID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Update"), Employeename, Password, FName, LName, Email, IsSuperEmployee, Status, EmployeeProfileID,
            ProfileID);
        }

        public int EmployeeRole_Add(string Employeename, int RoleID, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeRole_Add"), Employeename, RoleID, UpdBy);
        }

        public int GEN_Employee_PrintAssessment(int Year, int LevelID, int DirID, int DivID, int DeptID,string StartCode,string EndCode,string Username)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_Employee_PrintAssessment"), Year,LevelID,DirID,DivID, DeptID,StartCode,EndCode,Username);
        }
        
        public int GEN_Employee_FinalScoreByAssessor(int Year, int DeptID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_Employee_FinalScoreByAssessor"), Year, DeptID);
        }

        public int GEN_EmployeeAssessmentFinalScore(int Year, int DeptID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_EmployeeAssessmentFinalScore"), Year, DeptID);
        }

        public int GEN_EmployeeAssessmentFinalScoreDirection(int Year, int DirectionID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_EmployeeAssessmentFinalScoreDirection"), Year, DirectionID);
        }
        

        public int GEN_EmployeeAssessmentFinalScoreDivision(int Year, int DivisionUID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_EmployeeAssessmentFinalScoreDivision"), Year, DivisionUID);
        }

        public int Employee_UpdateProfileID(string Employeename)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_UpdateProfileID"), Employeename);
        }

        
        //public int xAssessorAssessment_Add(int BYear, string AssessorID, string EmployeeUID)
        //{
        //    return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessorAssessmentHistory_Add"), BYear,AssessorID, EmployeeUID);
        //}
        //public int xAssessorAssessment_Delete(int BYear, string AssessorID)
        //{
        //    return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessorAssessmentHistory_Delete"), BYear, AssessorID);
        //}

       
        public int Employee_GenLogfile(string Employeename, string Act_Type, string DB_Effective, string Descrp, string Remark)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_genLogfile"), Employeename, Act_Type, DB_Effective, Descrp, Remark);
        }

        public int Employee_UpdateMail(string Employeename, string email)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_UpdateEmail"), Employeename, email);
        }
        public int Employee_ReasonFinalSave(long PatientUID, long PatientVisitUID, string Employeename, string ReasonTxT)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_ReasonFinalSave"), PatientUID, PatientVisitUID, Employeename, ReasonTxT);
        }

        public Boolean User_GetAllowPermission(string Username)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "User_GetAllowPermission", Username);
            if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public Boolean Employee_Duplicate(string EmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Employee_Duplicate", EmployeeID);
            if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }



        public int Employee_Save(int UID, string EmployeeID, int PositionUID, int DepartmentUID, int DivisionUID,int DirectionUID,string Title,string Name,string Password, string isAdmin, string isReporter, string isSuperAdmin,   string isMedical, string isHR, string NationalID, string HireDate, string StatusFlag,string StatusAssessment,int LevelID,string isProbation)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Save"), UID
      , EmployeeID
      , DepartmentUID
      , DivisionUID
      , DirectionUID
      , Title
      , Name
      , PositionUID
      , Password
      , isAdmin
      , isReporter
      , isSuperAdmin
      , isMedical
      , isHR
      , NationalID
      , HireDate
      , StatusFlag
      , StatusAssessment
      , LevelID
      , isProbation);
        }

        public int Employee_SaveByImport(string EmployeeID,string Title,string Fname,string Lname,string PositionName, string DepartmentName, string DivisionName,string HireDate,  string isMedical)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_SaveByImport"),EmployeeID,Title,Fname,Lname,PositionName,DepartmentName,DivisionName,HireDate,isMedical);
        }

        public int Employee_Disable(int UID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Disable"), UID);
        }

        #endregion
        #region "EmployeeStatus"
        public int EmployeeStatus_Save(int UID ,int BYear, string EmployeeID, int PositionUID, int DepartmentUID, int DivisionUID,int DirectionUID ,string StatusFlag)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeStatus_Save"),UID, BYear, EmployeeID, PositionUID, DepartmentUID, DivisionUID, DirectionUID, StatusFlag);
        }

        public int EmployeeStatusMain_Save(int BYear, string EmployeeID, int PositionUID, int DepartmentUID, int DivisionUID, int DirectionUID, string StatusFlag)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeStatusMain_Save"), BYear, EmployeeID, PositionUID, DepartmentUID, DivisionUID, DirectionUID, StatusFlag);
        }


        public int EmployeeStatus_Delete(int UID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeStatus_Delete"), UID);
        }

        #endregion

    }
    public class EmployeeRoleController : BaseClass
    {

        public DataSet ds = new DataSet();
        public DataTable EmployeeRole_GetByEmployeeID(int pEmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EmployeeRole_GetByEmployeeID"), pEmployeeID);
            return ds.Tables[0];
        }

        public DataTable EmployeeRole_GetActiveRoleByUID(int pEmployeeID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EmployeeRole_GetActiveRoleByUID"), pEmployeeID);
            return ds.Tables[0];
        }


        public bool EmployeeAction_CheckByEmployee(string pEmployee, int pLocation)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EmployeeAction_CheckByEmployee"), pEmployee, pLocation);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EmployeeAction_Checked(string pEmployee, int pLocation)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EmployeeAction_Checked"), pEmployee, pLocation);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public int EmployeeAction_Add(string Employeename, int LocationID, int RoleAction, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeAction_Add"), Employeename, LocationID, RoleAction, UpdBy);
        }
        public int EmployeeAction_Update(string Employeename, int LocationID, int RoleAction, string UpdBy)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeAction_Update"), Employeename, LocationID, RoleAction, UpdBy);
        }


    }


}