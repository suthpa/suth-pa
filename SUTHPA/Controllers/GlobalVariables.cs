﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUTHPA
{
    public class GlobalVariables
    {
        public static int BYear = 2021;
        public static string username;
        public static string usercode;
        public static string LoginUser;
        public static bool IsHR;
        public static bool IsAdmin;   
        public static bool IsSuperAdmin;
        public static bool IsReporter;
        public static bool IsDirector;



        public static int AssessorLevelUID;
        public static int AssesseeLevelUID;
        public static int DivisionUID;
        public static int DepartmentUID;

        public static string AssessmentClass; // Self , Colleague, Underling


        public static string accessright;
        public static string UseDatabase;
        public static string UserEmployeeID;
        public static string UserEmployeeName;
        public static string UserEmployeeLevelID;
        public static string UserEmployeeLevelName;
        public static string DateTimeStamp;
        public static string LocationClinic;
        //Add By Teerapol 28-6-2017
        public static string ReportPath = "";
        public static string Reportskey = "";
        public static string ReportName = "";
        public static string ReportTitle = "";
        public static string FagRPT = "";
        public static string RPTTYPE = "";
        public static string RPTKEY = "";
        public static string CFG_STARTDATE = "STARTDATE";
        public static string CFG_ENDDATE = "ENDDATE";
        public static string CFG_PAYEAR = "PAYEAR";
        public static string CFG_RNDNO = "RNDNO";

        public static bool FlagRefresh = false;



    }
}
