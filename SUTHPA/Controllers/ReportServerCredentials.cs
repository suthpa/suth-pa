﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.IO;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Security.Principal;
using System.Net;

namespace  SUTHPA
{
    public class ReportServerCredentials 
    {
        private string _userName;
        private string _password;
        private string _domain;
        private string _rptserverurl;
        private string _rptserverpath;

        public ReportServerCredentials()
        {
            StreamReader sr = default(StreamReader);
            DataSet ds = new DataSet();
            sr = new StreamReader(@"C:\ProgramData\SUTHPA\DataConfig2021.xml");
            ds.ReadXml(sr);
            sr.Close();

            _userName = ds.Tables[0].Rows[0].Field<string>("ReportServerUser");
            _password = ds.Tables[0].Rows[0].Field<string>("ReportServerPassword");
            _domain = ds.Tables[0].Rows[0].Field<string>("ReportServerLocation");
            _rptserverurl = ds.Tables[0].Rows[0].Field<string>("ReportServerURL");
            _rptserverpath = ds.Tables[0].Rows[0].Field<string>("ReportServerPath");   
        }

        public Uri ReportServerUrl()
        {
            return new Uri(_rptserverurl);
        }

        public string ReportPath(string path)
        {
            return _rptserverpath + "/" + path;
        }

        public WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public ICredentials NetworkCredentials
        {
            get { return new NetworkCredential(_userName, _password, _domain); }
        }

        //public bool GetFormsCredentials(ref Cookie authCookie, ref string userName, ref string password, ref string authority)
        //{
        //    userName = _userName;
        //    password = _password;
        //    authority = _domain;
        //    return false;
        //}
    }
}
