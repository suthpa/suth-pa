using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;
 
namespace  SUTHPA
{

    public class ReferenceDataController : BaseClass
    {

        DataSet ds = new DataSet();
        public DataTable ReferenceData_GetByDomain(string DomainCode)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ReferenceData_GetByDomain"), DomainCode);
            return  ds.Tables[0];
        }


        public int RunProcessReport_Add(string RPTNAME, string Descriptions, string UserName, string Condition)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("RunProcessReport_Add"), RPTNAME, Descriptions, UserName, Condition);
        }
        public int RunProcessReport_UpdateStatus(long RNPID, string RPTNAME, string UserName, int Status)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("RunProcessReport_UpdateStatus"), RNPID, RPTNAME, UserName, Status);
        }
        public int RunProcessReport_GetStatus(long RNPID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RunProcessReport_GetStatus"), RNPID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DBNull2Zero(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public DataTable RunProcessReport_Get()
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RunProcessReport_Get"));
            return ds.Tables[0];
        }
        public DataTable RunProcessReport_GetByUser(string pUser)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RunProcessReport_GetByUser"), pUser);
            return ds.Tables[0];
        }

        public long RunProcessReport_GetLastUID(string RPTNAME, string UserName)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RunProcessReport_GetLastUID"), RPTNAME, UserName);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DBNull2Lng(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public int TempReportData_GetCount(string StartDate, string EndDate, int PayorUID, string UserID, long RNPID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CheckupForNewStudent_GetCount"), StartDate, EndDate, PayorUID, UserID, RNPID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DBNull2Zero(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public int TempReportData_CheckLoadCount(string StartDate, string EndDate, string ReportUID, string UserID, long RNPID)
        {
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("TempReportData_CheckLoadCount"), StartDate, EndDate, ReportUID, UserID, RNPID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DBNull2Zero(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }
        public int TempReportData_Add(string StartDate, string EndDate, int PayorUID, string UserID, long RNPID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CheckupForNewStudent_Load"), StartDate, EndDate, PayorUID, UserID, RNPID);
        }

        public int TempReportData_AddDoctorReport(string StartDate, string EndDate, string UserID, long RNPID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CheckupDocotorList_Load"), StartDate, EndDate, UserID, RNPID);
        }

        public int TempReportData_Delete(string UserID, long RNPID)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CheckupForNewStudent_Delete"), UserID, RNPID);
        }
    }
}