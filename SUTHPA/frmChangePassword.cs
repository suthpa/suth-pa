﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUTHPA
{
    public partial class frmChangePassword : Form
    {
        public frmChangePassword()
        {
            InitializeComponent();
        }
        EmployeeController ctlE = new EmployeeController();
        DataTable dt;
        private void frmNote_Load(object sender, EventArgs e)
        {
           
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (txtOld.Text=="" || txtNew.Text =="" || txtConfirm.Text=="")
            {
                MessageBox.Show("กรุณาระบุรหัสผ่านให้ครบทุกช่อง", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (  txtNew.Text   != txtConfirm.Text )
            {
                MessageBox.Show("ท่านยืนยันรหัสผ่านใหม่ไม่ตรงกัน", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtOld.Text != ctlE.Employee_GetPassword(GlobalVariables.username))
            {
                MessageBox.Show("ท่านระบุรหัสผ่านเดิมไม่ถูกต้อง", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            ctlE.Employee_ChangePassword(GlobalVariables.username, txtNew.Text.ToString());
            MessageBox.Show("บันทึกเปลี่ยนรหัสผ่านของท่านเรียบร้อย", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
