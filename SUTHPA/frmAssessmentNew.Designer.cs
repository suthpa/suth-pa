﻿namespace SUTHPA
{
    partial class frmAssessmentNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAssessmentNew));
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtComment = new DevExpress.XtraEditors.MemoEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBad = new DevExpress.XtraEditors.MemoEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGood = new DevExpress.XtraEditors.MemoEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkModify = new System.Windows.Forms.CheckBox();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNetScore4 = new System.Windows.Forms.TextBox();
            this.txtNetScore3 = new System.Windows.Forms.TextBox();
            this.txtNetScore2 = new System.Windows.Forms.TextBox();
            this.txtNetScore1 = new System.Windows.Forms.TextBox();
            this.btSave = new DevExpress.XtraEditors.SimpleButton();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.cmdApprove = new DevExpress.XtraEditors.SimpleButton();
            this.pnRating = new System.Windows.Forms.Panel();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.grdCourse = new DevExpress.XtraGrid.GridControl();
            this.grdViewCourse = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeptApprove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.optAssResult = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.colHRApprove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhaseNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colisRowActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.label3 = new System.Windows.Forms.Label();
            this.pnHR = new DevExpress.XtraEditors.GroupControl();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.label5 = new System.Windows.Forms.Label();
            this.cmdPrint = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblSEQ = new System.Windows.Forms.Label();
            this.lblHireDate = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDepartmentName = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPositionName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEmployeeID = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.picEmp = new DevExpress.XtraEditors.PictureEdit();
            this.lblEmpName = new System.Windows.Forms.Label();
            this.pScore = new System.Windows.Forms.Panel();
            this.grdAssessment = new DevExpress.XtraGrid.GridControl();
            this.grdViewAssessment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameCompetency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencyGroupUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencyUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEvaluationUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.label12 = new System.Windows.Forms.Label();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGood.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.pnRating.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAssResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHR)).BeginInit();
            this.pnHR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmp.Properties)).BeginInit();
            this.pScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1520, 40);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblTitle);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.HotTrack;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1520, 40);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(2, 2);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(421, 28);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "แบบประเมินผลการปฏิบัติงานสำหรับพนักงานใหม่";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1493, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 36);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel3);
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 609);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1520, 200);
            this.pnBottom.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtComment);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.txtBad);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.txtGood);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1058, 200);
            this.panel3.TabIndex = 35;
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(9, 102);
            this.txtComment.Name = "txtComment";
            this.txtComment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtComment.Properties.Appearance.Options.UseFont = true;
            this.txtComment.Size = new System.Drawing.Size(1126, 50);
            this.txtComment.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 79);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(310, 20);
            this.label14.TabIndex = 11;
            this.label14.Text = "ข้อคิดเห็นเพิ่มเติมของการประเมินพนักงานใหม่";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(571, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 20);
            this.label13.TabIndex = 9;
            this.label13.Text = "ลักษณะที่เป็นข้อจำกัด";
            // 
            // txtBad
            // 
            this.txtBad.Location = new System.Drawing.Point(575, 26);
            this.txtBad.Name = "txtBad";
            this.txtBad.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtBad.Properties.Appearance.Options.UseFont = true;
            this.txtBad.Size = new System.Drawing.Size(560, 50);
            this.txtBad.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "ลักษณะเด่น";
            // 
            // txtGood
            // 
            this.txtGood.Location = new System.Drawing.Point(9, 26);
            this.txtGood.Name = "txtGood";
            this.txtGood.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtGood.Properties.Appearance.Options.UseFont = true;
            this.txtGood.Size = new System.Drawing.Size(560, 50);
            this.txtGood.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkModify);
            this.panel1.Controls.Add(this.groupControl2);
            this.panel1.Controls.Add(this.btSave);
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1058, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(462, 200);
            this.panel1.TabIndex = 33;
            // 
            // chkModify
            // 
            this.chkModify.AutoSize = true;
            this.chkModify.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkModify.ForeColor = System.Drawing.Color.Red;
            this.chkModify.Location = new System.Drawing.Point(83, 90);
            this.chkModify.Name = "chkModify";
            this.chkModify.Size = new System.Drawing.Size(370, 21);
            this.chkModify.TabIndex = 35;
            this.chkModify.Text = "คลืกเลือกหากเป็นการแก้ไขคะแนนย้อนหลัง แล้วกดปุ่ม Save";
            this.chkModify.UseVisualStyleBackColor = true;
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.5F, System.Drawing.FontStyle.Bold);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.txtNetScore4);
            this.groupControl2.Controls.Add(this.txtNetScore3);
            this.groupControl2.Controls.Add(this.txtNetScore2);
            this.groupControl2.Controls.Add(this.txtNetScore1);
            this.groupControl2.Location = new System.Drawing.Point(169, 6);
            this.groupControl2.LookAndFeel.SkinName = "Office 2010 Blue";
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(289, 77);
            this.groupControl2.TabIndex = 34;
            this.groupControl2.Text = "คะแนนเฉลี่ย คิดเป็น %";
            this.groupControl2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(10, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(271, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "เดือนที่ 1      เดือนที่ 2      เดือนที่ 3     เดือนที่ 4";
            // 
            // txtNetScore4
            // 
            this.txtNetScore4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtNetScore4.Location = new System.Drawing.Point(218, 43);
            this.txtNetScore4.Name = "txtNetScore4";
            this.txtNetScore4.Size = new System.Drawing.Size(65, 27);
            this.txtNetScore4.TabIndex = 3;
            this.txtNetScore4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNetScore3
            // 
            this.txtNetScore3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtNetScore3.Location = new System.Drawing.Point(147, 43);
            this.txtNetScore3.Name = "txtNetScore3";
            this.txtNetScore3.Size = new System.Drawing.Size(65, 27);
            this.txtNetScore3.TabIndex = 2;
            this.txtNetScore3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNetScore2
            // 
            this.txtNetScore2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtNetScore2.Location = new System.Drawing.Point(76, 43);
            this.txtNetScore2.Name = "txtNetScore2";
            this.txtNetScore2.Size = new System.Drawing.Size(65, 27);
            this.txtNetScore2.TabIndex = 1;
            this.txtNetScore2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNetScore1
            // 
            this.txtNetScore1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtNetScore1.Location = new System.Drawing.Point(5, 43);
            this.txtNetScore1.Name = "txtNetScore1";
            this.txtNetScore1.Size = new System.Drawing.Size(65, 27);
            this.txtNetScore1.TabIndex = 0;
            this.txtNetScore1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btSave
            // 
            this.btSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.btSave.Appearance.Options.UseFont = true;
            this.btSave.Appearance.Options.UseForeColor = true;
            this.btSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btSave.ImageOptions.Image")));
            this.btSave.Location = new System.Drawing.Point(250, 117);
            this.btSave.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.btSave.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btSave.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btSave.Name = "btSave";
            this.btSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btSave.Size = new System.Drawing.Size(100, 35);
            this.btSave.TabIndex = 31;
            this.btSave.Text = "Save";
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btClose.ImageOptions.Image")));
            this.btClose.Location = new System.Drawing.Point(358, 117);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.ActiveCaption;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(100, 35);
            this.btClose.TabIndex = 30;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // cmdApprove
            // 
            this.cmdApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdApprove.Appearance.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdApprove.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdApprove.Appearance.Options.UseFont = true;
            this.cmdApprove.Appearance.Options.UseForeColor = true;
            this.cmdApprove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdApprove.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdApprove.ImageOptions.Image")));
            this.cmdApprove.Location = new System.Drawing.Point(12, 79);
            this.cmdApprove.LookAndFeel.SkinMaskColor = System.Drawing.Color.SlateBlue;
            this.cmdApprove.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdApprove.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdApprove.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdApprove.Name = "cmdApprove";
            this.cmdApprove.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdApprove.Size = new System.Drawing.Size(206, 35);
            this.cmdApprove.TabIndex = 35;
            this.cmdApprove.Text = "Approve";
            this.cmdApprove.Click += new System.EventHandler(this.cmdApprove_Click);
            // 
            // pnRating
            // 
            this.pnRating.BackColor = System.Drawing.Color.White;
            this.pnRating.Controls.Add(this.groupControl6);
            this.pnRating.Controls.Add(this.pnHR);
            this.pnRating.Controls.Add(this.groupControl1);
            this.pnRating.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnRating.Location = new System.Drawing.Point(0, 40);
            this.pnRating.Name = "pnRating";
            this.pnRating.Size = new System.Drawing.Size(1520, 282);
            this.pnRating.TabIndex = 2;
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.grdCourse);
            this.groupControl6.Controls.Add(this.groupControl4);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl6.Location = new System.Drawing.Point(0, 88);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.ShowCaption = false;
            this.groupControl6.Size = new System.Drawing.Size(1296, 194);
            this.groupControl6.TabIndex = 10;
            this.groupControl6.Text = "groupControl6";
            // 
            // grdCourse
            // 
            this.grdCourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCourse.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdCourse.Location = new System.Drawing.Point(2, 32);
            this.grdCourse.MainView = this.grdViewCourse;
            this.grdCourse.Name = "grdCourse";
            this.grdCourse.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.optAssResult});
            this.grdCourse.Size = new System.Drawing.Size(1292, 160);
            this.grdCourse.TabIndex = 9;
            this.grdCourse.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewCourse});
            // 
            // grdViewCourse
            // 
            this.grdViewCourse.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.grdViewCourse.Appearance.FooterPanel.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.grdViewCourse.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewCourse.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewCourse.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewCourse.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewCourse.Appearance.GroupFooter.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.grdViewCourse.Appearance.GroupFooter.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grdViewCourse.Appearance.GroupFooter.Options.UseFont = true;
            this.grdViewCourse.Appearance.GroupFooter.Options.UseForeColor = true;
            this.grdViewCourse.Appearance.GroupRow.BackColor = System.Drawing.Color.LemonChiffon;
            this.grdViewCourse.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewCourse.Appearance.GroupRow.Options.UseBackColor = true;
            this.grdViewCourse.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewCourse.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewCourse.Appearance.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewCourse.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.grdViewCourse.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewCourse.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewCourse.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewCourse.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewCourse.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewCourse.Appearance.Row.Options.UseFont = true;
            this.grdViewCourse.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewCourse.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewCourse.ColumnPanelRowHeight = 25;
            this.grdViewCourse.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colCourseName,
            this.colScheduleTime,
            this.colScheduleDay,
            this.colDeptApprove,
            this.colHRApprove,
            this.colDepartment,
            this.colHR,
            this.colPhaseNo,
            this.colCourseUID,
            this.colisRowActive});
            this.grdViewCourse.GridControl = this.grdCourse;
            this.grdViewCourse.GroupFormat = "{0} [#image]{1} {2}";
            this.grdViewCourse.GroupRowHeight = 50;
            this.grdViewCourse.Name = "grdViewCourse";
            this.grdViewCourse.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewCourse.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewCourse.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewCourse.OptionsBehavior.AllowGroupExpandAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewCourse.OptionsBehavior.AutoExpandAllGroups = true;
            this.grdViewCourse.OptionsFilter.AllowColumnMRUFilterList = false;
            this.grdViewCourse.OptionsFilter.AllowFilterEditor = false;
            this.grdViewCourse.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.grdViewCourse.OptionsFilter.AllowMRUFilterList = false;
            this.grdViewCourse.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewCourse.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewCourse.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewCourse.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.grdViewCourse.OptionsView.RowAutoHeight = true;
            this.grdViewCourse.OptionsView.ShowGroupPanel = false;
            this.grdViewCourse.OptionsView.ShowIndicator = false;
            this.grdViewCourse.RowHeight = 25;
            this.grdViewCourse.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewCourse_RowStyle);
            this.grdViewCourse.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.grdViewCourse_CustomRowCellEdit);
            // 
            // colNo
            // 
            this.colNo.Caption = "No.";
            this.colNo.FieldName = "UID";
            this.colNo.Name = "colNo";
            this.colNo.OptionsColumn.AllowEdit = false;
            this.colNo.OptionsColumn.AllowFocus = false;
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 30;
            // 
            // colCourseName
            // 
            this.colCourseName.Caption = "หลักสูตร/หัวข้อ";
            this.colCourseName.FieldName = "Name";
            this.colCourseName.Name = "colCourseName";
            this.colCourseName.OptionsColumn.AllowEdit = false;
            this.colCourseName.OptionsColumn.AllowFocus = false;
            this.colCourseName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score", "{0:0.##}")});
            this.colCourseName.Visible = true;
            this.colCourseName.VisibleIndex = 1;
            this.colCourseName.Width = 308;
            // 
            // colScheduleTime
            // 
            this.colScheduleTime.AppearanceCell.Options.UseTextOptions = true;
            this.colScheduleTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScheduleTime.Caption = "ระยะเวลา";
            this.colScheduleTime.FieldName = "ScheduleTime";
            this.colScheduleTime.Name = "colScheduleTime";
            this.colScheduleTime.OptionsColumn.AllowEdit = false;
            this.colScheduleTime.OptionsColumn.AllowFocus = false;
            this.colScheduleTime.Visible = true;
            this.colScheduleTime.VisibleIndex = 2;
            this.colScheduleTime.Width = 127;
            // 
            // colScheduleDay
            // 
            this.colScheduleDay.AppearanceCell.Options.UseTextOptions = true;
            this.colScheduleDay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScheduleDay.Caption = "ช่วงเวลา/วันที่อบรม";
            this.colScheduleDay.FieldName = "ScheduleDay";
            this.colScheduleDay.Name = "colScheduleDay";
            this.colScheduleDay.OptionsColumn.AllowEdit = false;
            this.colScheduleDay.OptionsColumn.AllowFocus = false;
            this.colScheduleDay.Visible = true;
            this.colScheduleDay.VisibleIndex = 3;
            this.colScheduleDay.Width = 204;
            // 
            // colDeptApprove
            // 
            this.colDeptApprove.AppearanceCell.Options.UseTextOptions = true;
            this.colDeptApprove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeptApprove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDeptApprove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDeptApprove.Caption = "หัวหน้าแผนกต้นสังกัด";
            this.colDeptApprove.ColumnEdit = this.optAssResult;
            this.colDeptApprove.FieldName = "DeptApproved";
            this.colDeptApprove.Name = "colDeptApprove";
            this.colDeptApprove.Visible = true;
            this.colDeptApprove.VisibleIndex = 4;
            this.colDeptApprove.Width = 294;
            // 
            // optAssResult
            // 
            this.optAssResult.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.optAssResult.EnableFocusRect = true;
            this.optAssResult.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Y", "ผ่าน"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("N", "ไม่ผ่าน")});
            this.optAssResult.ItemsLayout = DevExpress.XtraEditors.RadioGroupItemsLayout.Flow;
            this.optAssResult.LookAndFeel.SkinName = "VS2010";
            this.optAssResult.LookAndFeel.UseDefaultLookAndFeel = false;
            this.optAssResult.Name = "optAssResult";
            // 
            // colHRApprove
            // 
            this.colHRApprove.AppearanceHeader.BackColor = System.Drawing.Color.Gainsboro;
            this.colHRApprove.AppearanceHeader.BackColor2 = System.Drawing.Color.Gainsboro;
            this.colHRApprove.AppearanceHeader.Options.UseBackColor = true;
            this.colHRApprove.Caption = "แผนกทรัพยากรมนุษย์";
            this.colHRApprove.ColumnEdit = this.optAssResult;
            this.colHRApprove.FieldName = "HRApproved";
            this.colHRApprove.Name = "colHRApprove";
            this.colHRApprove.OptionsColumn.AllowShowHide = false;
            this.colHRApprove.Visible = true;
            this.colHRApprove.VisibleIndex = 5;
            this.colHRApprove.Width = 309;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "isDepartment";
            this.colDepartment.Name = "colDepartment";
            // 
            // colHR
            // 
            this.colHR.FieldName = "isHR";
            this.colHR.Name = "colHR";
            // 
            // colPhaseNo
            // 
            this.colPhaseNo.FieldName = "PhaseNo";
            this.colPhaseNo.Name = "colPhaseNo";
            // 
            // colCourseUID
            // 
            this.colCourseUID.FieldName = "CourseUID";
            this.colCourseUID.Name = "colCourseUID";
            // 
            // colisRowActive
            // 
            this.colisRowActive.Caption = "isRowActive";
            this.colisRowActive.FieldName = "isRowActive";
            this.colisRowActive.Name = "colisRowActive";
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupControl4.Appearance.Options.UseBackColor = true;
            this.groupControl4.Controls.Add(this.label3);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(2, 2);
            this.groupControl4.LookAndFeel.SkinMaskColor = System.Drawing.Color.LightSteelBlue;
            this.groupControl4.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.ShowCaption = false;
            this.groupControl4.Size = new System.Drawing.Size(1292, 30);
            this.groupControl4.TabIndex = 8;
            this.groupControl4.Text = "groupControl4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(5, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(221, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "ส่วนที่ 1 การบันทึกการประเมินผล";
            // 
            // pnHR
            // 
            this.pnHR.Controls.Add(this.groupControl8);
            this.pnHR.Controls.Add(this.cmdPrint);
            this.pnHR.Controls.Add(this.cmdApprove);
            this.pnHR.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnHR.Location = new System.Drawing.Point(1296, 88);
            this.pnHR.Name = "pnHR";
            this.pnHR.ShowCaption = false;
            this.pnHR.Size = new System.Drawing.Size(224, 194);
            this.pnHR.TabIndex = 11;
            this.pnHR.Text = "groupControl7";
            // 
            // groupControl8
            // 
            this.groupControl8.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupControl8.Appearance.Options.UseBackColor = true;
            this.groupControl8.Controls.Add(this.label5);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl8.Location = new System.Drawing.Point(2, 2);
            this.groupControl8.LookAndFeel.SkinMaskColor = System.Drawing.Color.LightSteelBlue;
            this.groupControl8.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl8.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.ShowCaption = false;
            this.groupControl8.Size = new System.Drawing.Size(220, 30);
            this.groupControl8.TabIndex = 37;
            this.groupControl8.Text = "groupControl8";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(5, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "HR Approval";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdPrint.Appearance.Options.UseFont = true;
            this.cmdPrint.Appearance.Options.UseForeColor = true;
            this.cmdPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.ImageOptions.Image")));
            this.cmdPrint.Location = new System.Drawing.Point(12, 38);
            this.cmdPrint.LookAndFeel.SkinMaskColor = System.Drawing.Color.Green;
            this.cmdPrint.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdPrint.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdPrint.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdPrint.Size = new System.Drawing.Size(206, 35);
            this.cmdPrint.TabIndex = 36;
            this.cmdPrint.Text = "Print ";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.lblSEQ);
            this.groupControl1.Controls.Add(this.lblHireDate);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.lblDepartmentName);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.lblPositionName);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.lblEmployeeID);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.picEmp);
            this.groupControl1.Controls.Add(this.lblEmpName);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinMaskColor = System.Drawing.Color.SteelBlue;
            this.groupControl1.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1520, 88);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "groupControl1";
            // 
            // lblSEQ
            // 
            this.lblSEQ.AutoSize = true;
            this.lblSEQ.Location = new System.Drawing.Point(1032, 29);
            this.lblSEQ.Name = "lblSEQ";
            this.lblSEQ.Size = new System.Drawing.Size(27, 13);
            this.lblSEQ.TabIndex = 13;
            this.lblSEQ.Text = "SEQ";
            this.lblSEQ.Visible = false;
            // 
            // lblHireDate
            // 
            this.lblHireDate.AutoSize = true;
            this.lblHireDate.BackColor = System.Drawing.Color.Transparent;
            this.lblHireDate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHireDate.ForeColor = System.Drawing.Color.White;
            this.lblHireDate.Location = new System.Drawing.Point(178, 62);
            this.lblHireDate.Name = "lblHireDate";
            this.lblHireDate.Size = new System.Drawing.Size(0, 20);
            this.lblHireDate.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(85, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 20);
            this.label10.TabIndex = 11;
            this.label10.Text = "วันที่เริ่มงาน";
            // 
            // lblDepartmentName
            // 
            this.lblDepartmentName.AutoSize = true;
            this.lblDepartmentName.BackColor = System.Drawing.Color.Transparent;
            this.lblDepartmentName.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartmentName.ForeColor = System.Drawing.Color.White;
            this.lblDepartmentName.Location = new System.Drawing.Point(612, 33);
            this.lblDepartmentName.Name = "lblDepartmentName";
            this.lblDepartmentName.Size = new System.Drawing.Size(0, 20);
            this.lblDepartmentName.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(512, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "แผนก";
            // 
            // lblPositionName
            // 
            this.lblPositionName.AutoSize = true;
            this.lblPositionName.BackColor = System.Drawing.Color.Transparent;
            this.lblPositionName.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPositionName.ForeColor = System.Drawing.Color.White;
            this.lblPositionName.Location = new System.Drawing.Point(155, 32);
            this.lblPositionName.Name = "lblPositionName";
            this.lblPositionName.Size = new System.Drawing.Size(0, 20);
            this.lblPositionName.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(85, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "ตำแหน่ง";
            // 
            // lblEmployeeID
            // 
            this.lblEmployeeID.AutoSize = true;
            this.lblEmployeeID.BackColor = System.Drawing.Color.Transparent;
            this.lblEmployeeID.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeID.ForeColor = System.Drawing.Color.White;
            this.lblEmployeeID.Location = new System.Drawing.Point(612, 4);
            this.lblEmployeeID.Name = "lblEmployeeID";
            this.lblEmployeeID.Size = new System.Drawing.Size(0, 20);
            this.lblEmployeeID.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(512, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "รหัสพนักงาน";
            // 
            // picEmp
            // 
            this.picEmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.picEmp.Location = new System.Drawing.Point(5, 4);
            this.picEmp.Name = "picEmp";
            this.picEmp.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.picEmp.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.Image;
            this.picEmp.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picEmp.Size = new System.Drawing.Size(74, 79);
            this.picEmp.TabIndex = 4;
            // 
            // lblEmpName
            // 
            this.lblEmpName.AutoSize = true;
            this.lblEmpName.BackColor = System.Drawing.Color.Transparent;
            this.lblEmpName.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpName.ForeColor = System.Drawing.Color.White;
            this.lblEmpName.Location = new System.Drawing.Point(85, 3);
            this.lblEmpName.Name = "lblEmpName";
            this.lblEmpName.Size = new System.Drawing.Size(0, 20);
            this.lblEmpName.TabIndex = 3;
            // 
            // pScore
            // 
            this.pScore.Controls.Add(this.grdAssessment);
            this.pScore.Controls.Add(this.groupControl5);
            this.pScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pScore.Location = new System.Drawing.Point(0, 322);
            this.pScore.Name = "pScore";
            this.pScore.Size = new System.Drawing.Size(1520, 287);
            this.pScore.TabIndex = 1;
            // 
            // grdAssessment
            // 
            this.grdAssessment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAssessment.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdAssessment.Location = new System.Drawing.Point(0, 30);
            this.grdAssessment.MainView = this.grdViewAssessment;
            this.grdAssessment.Name = "grdAssessment";
            this.grdAssessment.Size = new System.Drawing.Size(1520, 257);
            this.grdAssessment.TabIndex = 0;
            this.grdAssessment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewAssessment});
            // 
            // grdViewAssessment
            // 
            this.grdViewAssessment.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdViewAssessment.Appearance.FocusedCell.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grdViewAssessment.Appearance.FocusedCell.Options.UseFont = true;
            this.grdViewAssessment.Appearance.FocusedCell.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.FocusedCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.FocusedCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.FooterPanel.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.GroupFooter.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.GroupFooter.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grdViewAssessment.Appearance.GroupFooter.Options.UseFont = true;
            this.grdViewAssessment.Appearance.GroupFooter.Options.UseForeColor = true;
            this.grdViewAssessment.Appearance.GroupRow.BackColor = System.Drawing.Color.LemonChiffon;
            this.grdViewAssessment.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewAssessment.Appearance.GroupRow.Options.UseBackColor = true;
            this.grdViewAssessment.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewAssessment.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewAssessment.Appearance.Row.Options.UseFont = true;
            this.grdViewAssessment.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.ColumnPanelRowHeight = 25;
            this.grdViewAssessment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameCompetency,
            this.colRemark,
            this.colCompetencyGroupUID,
            this.colNameGroup,
            this.colDescriptionGroup,
            this.colCompetencyUID,
            this.colRowNo,
            this.colCDescription,
            this.colMinScore,
            this.colMaxScore,
            this.colComment,
            this.colWeight,
            this.colScore1,
            this.colScore2,
            this.colScore3,
            this.colScore4,
            this.colNetScore,
            this.colEvaluationUID});
            this.grdViewAssessment.GridControl = this.grdAssessment;
            this.grdViewAssessment.GroupFormat = "{0} [#image]{1} {2}";
            this.grdViewAssessment.GroupRowHeight = 50;
            this.grdViewAssessment.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Score", this.colCDescription, "รวม"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score1", this.colScore1, "{0:0.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score2", this.colScore2, "{0:0.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score3", this.colScore3, "{0:0.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score4", this.colScore4, "{0:0.##}")});
            this.grdViewAssessment.Name = "grdViewAssessment";
            this.grdViewAssessment.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowGroupExpandAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AutoExpandAllGroups = true;
            this.grdViewAssessment.OptionsFilter.AllowColumnMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterEditor = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.grdViewAssessment.OptionsFilter.AllowMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewAssessment.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewAssessment.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.grdViewAssessment.OptionsView.RowAutoHeight = true;
            this.grdViewAssessment.OptionsView.ShowFooter = true;
            this.grdViewAssessment.OptionsView.ShowGroupPanel = false;
            this.grdViewAssessment.OptionsView.ShowIndicator = false;
            this.grdViewAssessment.RowHeight = 25;
            this.grdViewAssessment.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.grdViewAssessment_CustomDrawGroupRow);
            this.grdViewAssessment.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewAssessment_RowStyle);
            this.grdViewAssessment.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.grdViewAssessment_CustomSummaryCalculate);
            this.grdViewAssessment.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdViewAssessment_FocusedRowChanged);
            this.grdViewAssessment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdViewAssessment_KeyDown);
            this.grdViewAssessment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grdViewAssessment_KeyPress);
            this.grdViewAssessment.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.CellValidate);
            // 
            // colNameCompetency
            // 
            this.colNameCompetency.Caption = "CompetencyTypeName";
            this.colNameCompetency.FieldName = "CompetencyTypeName";
            this.colNameCompetency.Name = "colNameCompetency";
            this.colNameCompetency.OptionsColumn.AllowEdit = false;
            this.colNameCompetency.OptionsColumn.AllowFocus = false;
            this.colNameCompetency.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score", "{0:0.##}")});
            // 
            // colRemark
            // 
            this.colRemark.Caption = "Remark";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.OptionsColumn.AllowEdit = false;
            this.colRemark.OptionsColumn.AllowFocus = false;
            // 
            // colCompetencyGroupUID
            // 
            this.colCompetencyGroupUID.Caption = "CompetencyGroupUID";
            this.colCompetencyGroupUID.FieldName = "CompetencyGroupUID";
            this.colCompetencyGroupUID.Name = "colCompetencyGroupUID";
            this.colCompetencyGroupUID.OptionsColumn.AllowEdit = false;
            // 
            // colNameGroup
            // 
            this.colNameGroup.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.colNameGroup.AppearanceCell.Options.UseFont = true;
            this.colNameGroup.AppearanceCell.Options.UseTextOptions = true;
            this.colNameGroup.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameGroup.Caption = " ";
            this.colNameGroup.FieldName = "CompetencyGroupName";
            this.colNameGroup.FieldNameSortGroup = "CompetencyGroupUID";
            this.colNameGroup.Name = "colNameGroup";
            this.colNameGroup.OptionsColumn.AllowEdit = false;
            this.colNameGroup.OptionsColumn.AllowFocus = false;
            // 
            // colDescriptionGroup
            // 
            this.colDescriptionGroup.AppearanceCell.BackColor = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceCell.BackColor2 = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceCell.Options.UseBackColor = true;
            this.colDescriptionGroup.AppearanceHeader.BackColor = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceHeader.BackColor2 = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceHeader.Options.UseBackColor = true;
            this.colDescriptionGroup.Caption = " ";
            this.colDescriptionGroup.FieldName = "DescriptionGroup";
            this.colDescriptionGroup.Name = "colDescriptionGroup";
            this.colDescriptionGroup.OptionsColumn.AllowEdit = false;
            this.colDescriptionGroup.OptionsColumn.AllowFocus = false;
            this.colDescriptionGroup.OptionsColumn.AllowShowHide = false;
            // 
            // colCompetencyUID
            // 
            this.colCompetencyUID.Caption = "CompetencyUID";
            this.colCompetencyUID.FieldName = "CompetencyUID";
            this.colCompetencyUID.Name = "colCompetencyUID";
            this.colCompetencyUID.OptionsColumn.AllowEdit = false;
            // 
            // colRowNo
            // 
            this.colRowNo.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNo.Caption = "No.";
            this.colRowNo.FieldName = "RowNo";
            this.colRowNo.FieldNameSortGroup = "nRow";
            this.colRowNo.Name = "colRowNo";
            this.colRowNo.OptionsColumn.AllowEdit = false;
            this.colRowNo.OptionsColumn.AllowFocus = false;
            this.colRowNo.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRowNo.OptionsFilter.AllowAutoFilter = false;
            this.colRowNo.OptionsFilter.AllowFilter = false;
            this.colRowNo.Visible = true;
            this.colRowNo.VisibleIndex = 0;
            this.colRowNo.Width = 62;
            // 
            // colCDescription
            // 
            this.colCDescription.AppearanceCell.Options.UseTextOptions = true;
            this.colCDescription.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colCDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCDescription.Caption = "หัวข้อประเมิน";
            this.colCDescription.FieldName = "CDescription";
            this.colCDescription.Name = "colCDescription";
            this.colCDescription.OptionsColumn.AllowEdit = false;
            this.colCDescription.OptionsColumn.AllowFocus = false;
            this.colCDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCDescription.OptionsFilter.AllowAutoFilter = false;
            this.colCDescription.OptionsFilter.AllowFilter = false;
            this.colCDescription.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "CDescription", "คะแนนรวม")});
            this.colCDescription.Visible = true;
            this.colCDescription.VisibleIndex = 1;
            this.colCDescription.Width = 885;
            // 
            // colMinScore
            // 
            this.colMinScore.Caption = "MinScore";
            this.colMinScore.FieldName = "MinScore";
            this.colMinScore.Name = "colMinScore";
            // 
            // colMaxScore
            // 
            this.colMaxScore.Caption = "MaxScore";
            this.colMaxScore.FieldName = "MaxScore";
            this.colMaxScore.Name = "colMaxScore";
            // 
            // colComment
            // 
            this.colComment.Caption = "Comment";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            // 
            // colWeight
            // 
            this.colWeight.AppearanceCell.Options.UseTextOptions = true;
            this.colWeight.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeight.Caption = "ค่าน้ำหนัก";
            this.colWeight.FieldName = "WeightScore";
            this.colWeight.Name = "colWeight";
            // 
            // colScore1
            // 
            this.colScore1.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.colScore1.AppearanceCell.Options.UseFont = true;
            this.colScore1.AppearanceCell.Options.UseTextOptions = true;
            this.colScore1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colScore1.AppearanceHeader.Options.UseTextOptions = true;
            this.colScore1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore1.Caption = "เดือนที่ 1";
            this.colScore1.FieldName = "Score1";
            this.colScore1.Name = "colScore1";
            this.colScore1.OptionsColumn.AllowFocus = false;
            this.colScore1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colScore1.OptionsFilter.AllowAutoFilter = false;
            this.colScore1.OptionsFilter.AllowFilter = false;
            this.colScore1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score1", "{0:0.##}")});
            this.colScore1.Visible = true;
            this.colScore1.VisibleIndex = 2;
            // 
            // colScore2
            // 
            this.colScore2.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.colScore2.AppearanceCell.Options.UseFont = true;
            this.colScore2.AppearanceCell.Options.UseTextOptions = true;
            this.colScore2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colScore2.Caption = "เดือนที่ 2";
            this.colScore2.FieldName = "Score2";
            this.colScore2.Name = "colScore2";
            this.colScore2.OptionsColumn.AllowFocus = false;
            this.colScore2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colScore2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score2", "{0:0.##}")});
            this.colScore2.Visible = true;
            this.colScore2.VisibleIndex = 3;
            // 
            // colScore3
            // 
            this.colScore3.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.colScore3.AppearanceCell.Options.UseFont = true;
            this.colScore3.AppearanceCell.Options.UseTextOptions = true;
            this.colScore3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colScore3.Caption = "เดือนที่ 3";
            this.colScore3.FieldName = "Score3";
            this.colScore3.Name = "colScore3";
            this.colScore3.OptionsColumn.AllowFocus = false;
            this.colScore3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colScore3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score3", "{0:0.##}")});
            this.colScore3.Visible = true;
            this.colScore3.VisibleIndex = 4;
            // 
            // colScore4
            // 
            this.colScore4.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.colScore4.AppearanceCell.Options.UseFont = true;
            this.colScore4.AppearanceCell.Options.UseTextOptions = true;
            this.colScore4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colScore4.Caption = "เดือนที่ 4";
            this.colScore4.FieldName = "Score4";
            this.colScore4.Name = "colScore4";
            this.colScore4.OptionsColumn.AllowFocus = false;
            this.colScore4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colScore4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score4", "{0:0.##}")});
            this.colScore4.Visible = true;
            this.colScore4.VisibleIndex = 5;
            // 
            // colNetScore
            // 
            this.colNetScore.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colNetScore.AppearanceCell.Options.UseFont = true;
            this.colNetScore.AppearanceCell.Options.UseTextOptions = true;
            this.colNetScore.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNetScore.Caption = "คะแนน";
            this.colNetScore.FieldName = "NetScore";
            this.colNetScore.Name = "colNetScore";
            this.colNetScore.OptionsColumn.AllowEdit = false;
            this.colNetScore.OptionsColumn.AllowFocus = false;
            this.colNetScore.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "NetScore", "{0:0.##}", 1)});
            // 
            // colEvaluationUID
            // 
            this.colEvaluationUID.FieldName = "EvaluationUID";
            this.colEvaluationUID.Name = "colEvaluationUID";
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupControl5.Appearance.Options.UseBackColor = true;
            this.groupControl5.Controls.Add(this.label12);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl5.Location = new System.Drawing.Point(0, 0);
            this.groupControl5.LookAndFeel.SkinMaskColor = System.Drawing.Color.LightSteelBlue;
            this.groupControl5.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl5.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.ShowCaption = false;
            this.groupControl5.Size = new System.Drawing.Size(1520, 30);
            this.groupControl5.TabIndex = 9;
            this.groupControl5.Text = "groupControl5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(5, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(345, 20);
            this.label12.TabIndex = 3;
            this.label12.Text = "ส่วนที่ 2 การบันทึกการฝึกอบรมภายในแผนกต้นสังกัด";
            // 
            // frmAssessmentNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1520, 809);
            this.Controls.Add(this.pScore);
            this.Controls.Add(this.pnRating);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAssessmentNew";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แบบประเมินพนังงานใหม่";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmAssessmentNew_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGood.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.pnRating.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAssResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHR)).EndInit();
            this.pnHR.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmp.Properties)).EndInit();
            this.pScore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private System.Windows.Forms.Panel pnRating;
        private DevExpress.XtraGrid.GridControl grdAssessment;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn colNameCompetency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyGroupUID;
        private DevExpress.XtraGrid.Columns.GridColumn colNameGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyUID;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNo;
        private DevExpress.XtraGrid.Columns.GridColumn colCDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMinScore;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxScore;
        private DevExpress.XtraGrid.Columns.GridColumn colScore1;
        private System.Windows.Forms.Panel pScore;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label lblEmpName;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.MemoEdit txtGood;
        private DevExpress.XtraEditors.SimpleButton btSave;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colNetScore;
        private DevExpress.XtraGrid.Columns.GridColumn colEvaluationUID;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.PictureEdit picEmp;
        private System.Windows.Forms.Label lblHireDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblDepartmentName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPositionName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblEmployeeID;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraGrid.GridControl grdCourse;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewCourse;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseName;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleTime;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleDay;
        private DevExpress.XtraGrid.Columns.GridColumn colDeptApprove;
        private DevExpress.XtraGrid.Columns.GridColumn colHRApprove;
        private DevExpress.XtraGrid.Columns.GridColumn colScore2;
        private DevExpress.XtraGrid.Columns.GridColumn colScore3;
        private DevExpress.XtraGrid.Columns.GridColumn colScore4;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup optAssResult;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.MemoEdit txtBad;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colHR;
        private DevExpress.XtraGrid.Columns.GridColumn colPhaseNo;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseUID;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNetScore4;
        private System.Windows.Forms.TextBox txtNetScore3;
        private System.Windows.Forms.TextBox txtNetScore2;
        private System.Windows.Forms.TextBox txtNetScore1;
        private System.Windows.Forms.Label lblSEQ;
        private DevExpress.XtraEditors.MemoEdit txtComment;
        private DevExpress.XtraGrid.Columns.GridColumn colisRowActive;
        private DevExpress.XtraEditors.SimpleButton cmdApprove;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.GroupControl pnHR;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.SimpleButton cmdPrint;
        private System.Windows.Forms.CheckBox chkModify;
    }
}