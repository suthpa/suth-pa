﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
//using SUTHPA.Cryption;
using System.Threading;
using System.Xml;
using System.Deployment.Application;
namespace SUTHPA

{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        Thread th;
        DataTable dt = new DataTable();
        EmployeeController acc = new EmployeeController();
        Version appVersion;

        public frmLogin()
        {
            InitializeComponent();            
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();        
        }

        private void cmdLogin_Click(object sender, EventArgs e)
        {
            GetLogin();
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GetLogin();
            }                       
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPassword.Focus();
            }
        }                  
    
        private void GetLogin()
        {

            if (string.IsNullOrEmpty(txtUsername.Text) | string.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtUsername.Focus();
                return;
            } 

            try
            {
                DataTable dtLogin = acc.Employee_Login(GlobalVariables.BYear ,txtUsername.Text, txtPassword.Text);

               


                if (dtLogin.Rows.Count == 0)
                {

                    MessageBox.Show("Username or Password doesn't match", "Error Login",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {


                    DataRow row = dtLogin.Rows[0];
                    GlobalVariables.username = row["EmployeeID"].ToString();
                    GlobalVariables.usercode = row["UID"].ToString();
                    GlobalVariables.LoginUser = row["EmployeeID"].ToString();
                    GlobalVariables.UserEmployeeName = row["Title"].ToString() + " " + row["Name"].ToString();
                    GlobalVariables.AssessorLevelUID = Convert.ToInt32(row["EmployeeLevelID"]);
                    GlobalVariables.UserEmployeeLevelName =  row["EmployeeLevelName"].ToString();

                    GlobalVariables.IsHR = false;
                    GlobalVariables.IsDirector = false;
                    GlobalVariables.IsSuperAdmin = false;

                    if (row["StatusFlag"].ToString() != "A")
                    {
                        MessageBox.Show("Username นี้ระงับการใช้งาน กรุณาติดต่อแผนกสารสนเทศ (IT) โทร.7620", "Error Login",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                        return;
                    }



                    if (row["isHR"].ToString() == "Y")
                    {
                        GlobalVariables.IsHR = true;
                    }

                    if (row["isDirector"].ToString() == "Y")
                    {
                        GlobalVariables.IsDirector = true;
                    }


                    if (row["isSuperAdmin"].ToString() == "Y")
                    {
                        GlobalVariables.IsSuperAdmin = true;
                    }


                    if (row["isAdmin"].ToString() == "Y")
                    {
                        GlobalVariables.IsAdmin = true; 
                    }
                    else
                    {
                        GlobalVariables.IsAdmin = false;
                    }

                    if (row["isReporter"].ToString() == "Y")
                    {
                        GlobalVariables.IsReporter = true;
                    }
                    else
                    {
                        GlobalVariables.IsReporter = false;
                    }



                    //GlobalVariables.accessright = row["accessright"].ToString();
                    this.Close();

                    th = new Thread(openMainFrom);
                    th.SetApartmentState(ApartmentState.STA);
                    th.Start();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void openMainFrom(object obj)
        {
           Application.Run(new frmMain());
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                //Version ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;              
                    appVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                //this.lblVersion.Text = "v." + string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision) + " ® 2018";
                this.lblVersion.Text = "v." + appVersion + " ® 17.11.2021";
            }
            else
            {
                this.lblVersion.Text = "v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + " ® 17.11.2021";
            }

        }
    }
}