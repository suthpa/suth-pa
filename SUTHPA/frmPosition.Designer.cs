﻿namespace SUTHPA
{
    partial class frmPosition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPosition));
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label5 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblUID = new System.Windows.Forms.Label();
            this.cmdCancel = new DevExpress.XtraEditors.SimpleButton();
            this.cmdDel = new DevExpress.XtraEditors.SimpleButton();
            this.chkStatus = new System.Windows.Forms.CheckBox();
            this.cmdSave = new DevExpress.XtraEditors.SimpleButton();
            this.ddlLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnMain.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLevel.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1075, 35);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.groupControl3.Appearance.Options.UseBackColor = true;
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1075, 35);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(5, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "ข้อมูลตำแหน่งงาน";
            // 
            // lblExit
            // 
            this.lblExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1047, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 22);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 768);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1075, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(878, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 40);
            this.panel1.TabIndex = 33;
            // 
            // cmdClose
            // 
            this.cmdClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdClose.Appearance.Options.UseFont = true;
            this.cmdClose.Appearance.Options.UseForeColor = true;
            this.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.ImageOptions.Image")));
            this.cmdClose.Location = new System.Drawing.Point(99, 6);
            this.cmdClose.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdClose.Size = new System.Drawing.Size(88, 28);
            this.cmdClose.TabIndex = 30;
            this.cmdClose.Text = "Close";
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // pnMain
            // 
            this.pnMain.Controls.Add(this.panel5);
            this.pnMain.Controls.Add(this.panel4);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 35);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(1075, 733);
            this.pnMain.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.grdData);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 72);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1075, 661);
            this.panel5.TabIndex = 3;
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.grdData.Location = new System.Drawing.Point(0, 0);
            this.grdData.LookAndFeel.SkinName = "Office 2010 Blue";
            this.grdData.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.Size = new System.Drawing.Size(1075, 661);
            this.grdData.TabIndex = 32;
            this.grdData.TabStop = false;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData,
            this.gridView1});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewData.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewData.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewData.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grdViewData.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewData.Appearance.GroupRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewData.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.grdViewData.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grdViewData.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewData.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewData.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewData.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grdViewData.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn1,
            this.gridColumn4,
            this.gridColumn15,
            this.gridColumn3});
            this.grdViewData.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsBehavior.Editable = false;
            this.grdViewData.OptionsFind.AlwaysVisible = true;
            this.grdViewData.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowIndicator = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "รหัส";
            this.gridColumn2.FieldName = "UID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn1.Caption = "ตำแหน่ง";
            this.gridColumn1.FieldName = "PositionName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 400;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ระดับ";
            this.gridColumn4.FieldName = "LevelUID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn15.Caption = "ระดับพนักงาน";
            this.gridColumn15.FieldName = "LevelName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            this.gridColumn15.Width = 350;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "สถานะ";
            this.gridColumn3.FieldName = "StatusFlag";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdData;
            this.gridView1.Name = "gridView1";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1075, 72);
            this.panel4.TabIndex = 2;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.lblUID);
            this.groupControl1.Controls.Add(this.cmdCancel);
            this.groupControl1.Controls.Add(this.cmdDel);
            this.groupControl1.Controls.Add(this.chkStatus);
            this.groupControl1.Controls.Add(this.cmdSave);
            this.groupControl1.Controls.Add(this.ddlLevel);
            this.groupControl1.Controls.Add(this.txtName);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Location = new System.Drawing.Point(4, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1063, 63);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "เพิ่ม/แก้ไข";
            // 
            // lblUID
            // 
            this.lblUID.BackColor = System.Drawing.Color.White;
            this.lblUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUID.Location = new System.Drawing.Point(37, 31);
            this.lblUID.Name = "lblUID";
            this.lblUID.Padding = new System.Windows.Forms.Padding(2);
            this.lblUID.Size = new System.Drawing.Size(35, 25);
            this.lblUID.TabIndex = 1;
            this.lblUID.Text = "0";
            this.lblUID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdCancel.Appearance.Options.UseFont = true;
            this.cmdCancel.Appearance.Options.UseForeColor = true;
            this.cmdCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.ImageOptions.Image")));
            this.cmdCancel.Location = new System.Drawing.Point(899, 31);
            this.cmdCancel.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdCancel.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdCancel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdCancel.Size = new System.Drawing.Size(75, 25);
            this.cmdCancel.TabIndex = 33;
            this.cmdCancel.Text = "ล้าง";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdDel
            // 
            this.cmdDel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDel.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdDel.Appearance.Options.UseFont = true;
            this.cmdDel.Appearance.Options.UseForeColor = true;
            this.cmdDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdDel.ImageOptions.Image")));
            this.cmdDel.Location = new System.Drawing.Point(982, 31);
            this.cmdDel.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.cmdDel.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdDel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdDel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdDel.Name = "cmdDel";
            this.cmdDel.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdDel.Size = new System.Drawing.Size(75, 25);
            this.cmdDel.TabIndex = 32;
            this.cmdDel.Text = "ลบ";
            this.cmdDel.Click += new System.EventHandler(this.cmdDel_Click);
            // 
            // chkStatus
            // 
            this.chkStatus.AutoSize = true;
            this.chkStatus.Location = new System.Drawing.Point(744, 33);
            this.chkStatus.Name = "chkStatus";
            this.chkStatus.Size = new System.Drawing.Size(65, 23);
            this.chkStatus.TabIndex = 38;
            this.chkStatus.Text = "Active";
            this.chkStatus.UseVisualStyleBackColor = true;
            // 
            // cmdSave
            // 
            this.cmdSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdSave.Appearance.Options.UseFont = true;
            this.cmdSave.Appearance.Options.UseForeColor = true;
            this.cmdSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.ImageOptions.Image")));
            this.cmdSave.Location = new System.Drawing.Point(816, 31);
            this.cmdSave.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.cmdSave.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdSave.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdSave.Size = new System.Drawing.Size(75, 25);
            this.cmdSave.TabIndex = 31;
            this.cmdSave.Text = "บันทึก";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // ddlLevel
            // 
            this.ddlLevel.Location = new System.Drawing.Point(492, 32);
            this.ddlLevel.Name = "ddlLevel";
            this.ddlLevel.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlLevel.Properties.Appearance.Options.UseFont = true;
            this.ddlLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlLevel.Properties.NullText = "--- เลือก ---";
            this.ddlLevel.Size = new System.Drawing.Size(242, 24);
            this.ddlLevel.TabIndex = 12;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(145, 31);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(303, 25);
            this.txtName.TabIndex = 10;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(452, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "ระดับ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 19);
            this.label3.TabIndex = 3;
            this.label3.Text = "ชื่อตำแหน่ง";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "รหัส";
            // 
            // frmPosition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 808);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "frmPosition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ตำแหน่งงาน";
            this.Load += new System.EventHandler(this.frmPosition_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnMain.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLevel.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton cmdClose;
        private System.Windows.Forms.Panel pnMain;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton cmdSave;
        private DevExpress.XtraEditors.LookUpEdit ddlLevel;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.SimpleButton cmdDel;
        private DevExpress.XtraEditors.SimpleButton cmdCancel;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private System.Windows.Forms.CheckBox chkStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
    }
}