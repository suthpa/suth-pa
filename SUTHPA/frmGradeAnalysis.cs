﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
    public partial class frmGradeAnalysis : DevExpress.XtraEditors.XtraForm
    {
        public frmGradeAnalysis()
        {
            InitializeComponent();
        }

        Controllers.AssessmentController ctlP = new Controllers.AssessmentController();
        EmployeeController  ctlE = new EmployeeController();
        private void frmGradeAnalysis_Load(object sender, EventArgs e)
        {
          
            cboYear.Properties.DataSource = ctlP.Assessment_GetYear();
            cboYear.Properties.DisplayMember = "BYear";
            cboYear.Properties.ValueMember = "BYear";

            cboYear.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            cboYear.Properties.DropDownRows = 3;
            cboYear.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BYear", 100, "");
            cboYear.Properties.Columns.Add(col1);

            cboYear.EditValue = GlobalVariables.BYear;
            lblReportTitle.Text = GlobalVariables.ReportTitle;
            
            //หัวหน้ากลุ่มงาน
            if (GlobalVariables.IsDirector)
            {
                DataTable dt = new DataTable();
                dt = ctlE.Director_GetDirection(GlobalVariables.username);
            
                //cboDirection.Enabled = false;
                //cboDirection.Visible = true;
                //lblDirection.Visible = true;
                //cboDirection.Enabled = true;
                //lblDirection.Enabled = true;
            }    
            else
            {

                DataTable dt = new DataTable();
                dt = ctlE.Employee_GetEmployeeDirection(GlobalVariables.BYear,GlobalVariables.username);
            
                //cboDirection.Enabled = false;
                //cboDirection.Visible = true;
                //lblDirection.Visible = true;
                //cboDirection.Enabled = true;
                //lblDirection.Enabled = true;
              
            }      

        }
                 
        private void cmdView_Click(object sender, EventArgs e)
        {
                     

            GlobalVariables.Reportskey = "SCORE";
            GlobalVariables.RPTTYPE = "XLS";
            GlobalVariables.ReportName = "รายงานคะแนนประเมิน";

            
            if (GlobalVariables.FagRPT == "SUPERFINAL")
            {
                GlobalVariables.RPTTYPE = "RPT";
            }

            frmReportViewer frm = new frmReportViewer();
            frm.BYear = cboYear.EditValue.ToString();

            if (GlobalVariables.FagRPT == "EMPWEIGHT")
            {
                
            }
            else
            {                          
                
            }

            frm.MdiParent = this.MdiParent;
            frm.Show();
  
            this.WindowState = FormWindowState.Normal;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}