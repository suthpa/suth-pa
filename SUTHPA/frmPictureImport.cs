﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExcelDataReader;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;

namespace SUTHPA
{
    public partial class frmPictureImport : DevExpress.XtraEditors.XtraForm
    {
        public frmPictureImport()
        {
            InitializeComponent();
        }

        enum StepChanged
        {
            Import,       
            Processing,
            Finish,
            UpdateValue,
            Error
        }

        #region Variables
        //Size formDefaultSize;
        //Point DefaultLocation;
        Color noneActive = Color.FromArgb(169, 169, 169);
        Color Active = Color.FromArgb(1, 99, 182);
        StepChanged GetStep;
        StepChanged CurrentStep;

        DataTable dtE;
        #endregion

        DataSet dsImport = new DataSet();

        private void frmPictureImport_Load(object sender, EventArgs e)
        {
            pnWait.Visible = false;
            CurrentStep = StepChanged.Import;
            ToStep(StepChanged.Import);
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;            
            Control.CheckForIllegalCrossThreadCalls = false;
            this.WindowState = FormWindowState.Normal;
        }
        private void ToStep(StepChanged step)
        {
            GetStep = step;
            cmdNext.Visible = true;
            switch (GetStep)
            {
                case StepChanged.Import:
                    btnStepImport.Image = Properties.Resources.Chevron_Right_Blue_20px;
                    ButtonStepColor(Active, noneActive, noneActive);
                    cmdNext.Text = "Import";
                   
                    break;
                               
                case StepChanged.Finish:
                    btnStepFinish.Image = null;
                    ButtonStepColor(Active, Active, Active);
                    cmdNext.Text = "Done";
                    break;
                case StepChanged.Error:
                    btnStepFinish.Image = null;
                    ButtonStepColor(Active, Active, Active);
                    cmdNext.Visible = false;
                    break;
            }

        }
        private void ButtonStepColor(Color import, Color compare, Color finish)
        {
            btnStepImport.ForeColor = import;
            btnStepFinish.ForeColor = finish;
        }

        private void cmdNext_Click(object sender, EventArgs e)
        {
            if (CurrentStep == StepChanged.Import)
            {
                backgroundWorker1.RunWorkerAsync();
                string[] files = Directory.GetFiles(@"\\file\SUTHFS01\HR\EmployeePA\");
                foreach (string file in files)
                {
                    //Console.WriteLine(Path.GetFileName(file));
                    FileInfo objFile1 = new FileInfo(file);
                    FileInfo objFile2 = new FileInfo(@"\\172.100.50.120\Application\PA_SUTH\Employee\" + Path.GetFileName(file));

                    if (objFile2.Exists)
                    {
                        if (objFile1.LastWriteTime != objFile2.LastWriteTime)
                        {
                            objFile2.Delete();
                            File.Move(@"\\172.100.50.120\Application\PA_SUTH\Employee\" + Path.GetFileName(file) + ".jpg", @"\\172.100.50.120\Application\PA_SUTH\Employee\" + Path.GetFileName(file) + ".jpg");
                        }
                    }
                    else
                    {
                        File.Move(objFile1.FullName, objFile2.FullName);
                        //File.Move(@"\\172.100.50.120\Application\PA_SUTH\Employee\" + Path.GetFileName(file) + ".jpg", @"\\172.100.50.120\Application\PA_SUTH\Employee\" + Path.GetFileName(file) + ".jpg");
                    }

                }

                ToStep(StepChanged.Finish);
                pnStep.Visible = true;
                CurrentStep = StepChanged.Finish;

            }
            else
            {
                this.Close();
            }
            //if (CurrentStep == StepChanged.Import)
            //{
            //    using (OpenFileDialog dlg = new OpenFileDialog() { Filter = "Excel Workbook|*xlsx;*xls" })
            //    {
            //        if (dlg.ShowDialog() == DialogResult.OK)
            //        {
            //            backgroundWorker1.RunWorkerAsync();
            //            ToStep(StepChanged.Preview);
            //                //FillDataGridView(dlg.FileName);
            //                pnStep.Visible = true;
            //                CurrentStep = StepChanged.Preview;

            //        }

            //    }
            //}
            //else
            //{
            //    backgroundWorker1.RunWorkerAsync();
            //    //ImportDataToTable();
            //}

            //this.WindowState = FormWindowState.Maximized;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //pnWait.Dock = DockStyle.Fill;
            pnWait.Visible = true;

            for (var i = 1; i <= 100; i++)
            {
                backgroundWorker1.ReportProgress(i);
                System.Threading.Thread.Sleep(10);
                lblProgress.Text = ProgressBar1.Value + "%";
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.ProgressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pnWait.Visible = false;
        }
            
        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    }