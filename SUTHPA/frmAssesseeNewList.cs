﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmAssesseeNewList : DevExpress.XtraEditors.XtraForm
    {
        public frmAssesseeNewList()
        {
            InitializeComponent();
        }
        DataTable dtAssessor = new DataTable();
        DataTable dtAssessee = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();

        private void frmAssesseeNewList_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            GlobalVariables.FlagRefresh = false;

            //dtAssessee.Columns.Clear();
           
            dtAssessee.Columns.Add("EmployeeID");
            dtAssessee.Columns.Add("EmployeeName");
            dtAssessee.Columns.Add("PositionName");
            dtAssessee.Columns.Add("DepartmentName");
            dtAssessee.Columns.Add("DivisionName");
            dtAssessee.Columns.Add("EmployeeLevelID");
            dtAssessee.Columns.Add("DepartmentUID");
            dtAssessee.Columns.Add("DivisionUID");
            dtAssessee.Columns.Add("LevelName");
            dtAssessee.Columns.Add("isAssessment");
            dtAssessee.Columns.Add("Assm");
            dtAssessee.Columns.Add("GroupName");
            dtAssessee.Columns.Add("NetScore1");
            dtAssessee.Columns.Add("NetScore2");
            dtAssessee.Columns.Add("NetScore3");
            dtAssessee.Columns.Add("NetScore4");
            dtAssessee.Columns.Add("SortOrder",typeof(int));
            dtAssessee.Columns.Add("Remark");
            
            lblAssesor.Text += " โดย " + GlobalVariables.UserEmployeeName;
            LoadAssessees();
        }

        private bool OpenAssesseeListForm()
        {

            bool AppAlive;
            AppAlive = true;

            int Bdate, Edate, sToday;
            ConfigurationController ctlCf = new ConfigurationController();

            Bdate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_STARTDATE));
            Edate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_ENDDATE));
            sToday = Convert.ToInt32(BaseClass.ConvertStrDate2YYYYMMDDString(ctlCf.GET_DATE_SERVER().ToString()));
            if (sToday >= Bdate && sToday <= Edate)
            {
                AppAlive = true;
            }
            else
            {
                EmployeeController ctlU = new EmployeeController();

                if (ctlU.User_GetAllowPermission(GlobalVariables.username))
                {
                    AppAlive = true;
                }
                else
                {

                    if (sToday < Bdate)
                    {
                        MessageBox.Show("ยังไม่ถึงกำหนดเวลาประเมิน ท่านสามารถเริ่มทำแบบประเมินได้ตั้งแต่วันที่ 12 - 23 กรกฎาคม 2564", "Notice",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                        AppAlive=false;
                    }

                    if (sToday > Edate)
                    {
                        if (!GlobalVariables.IsAdmin)
                        {
                            MessageBox.Show("หมดเขตระยะเวลาที่ให้ประเมินแล้ว", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        AppAlive=false;
                    }
                }

            }

            //if (GlobalVariables.IsSuperAdmin)
            //{
            //    AppAlive = true;
            //}
            
            return AppAlive;
        }
               
        private void LoadAssessees()
        {
            int i,n;
            string iDiv,iDept,isHR;
            DataRow row;
            DataRow rowA;
            DataRow dr;
            DataTable dtA = new DataTable();
            dtAssessee.Rows.Clear();


            dtAssessor = ctlEmp.Employee_GetAssessorDetailNew(GlobalVariables.BYear, GlobalVariables.username);
            for ( i=0;i<dtAssessor.Rows.Count;i++)
             {
                 row = dtAssessor.Rows[i];
                iDiv = "";
                iDept = "";
                iDiv = row["DivisionUID"].ToString();
                iDept = row["DepartmentUID"].ToString();
                isHR = row["isHR"].ToString();

                //if (row["isInDivision"].ToString() == "Y") iDiv =  row["DivisionUID"].ToString();
                //if (row["isInDepartment"].ToString() == "Y") iDept =row["DepartmentUID"].ToString();
                dtA.Clear();
                switch (row["EmployeeLevelID"].ToString())
                {
                    //case "1": //ผอ.
                    //    dtA = ctlEmp.Employee_GetAssesseeManager(GlobalVariables.BYear,GlobalVariables.username);
                    //    break;
                    case "2": //หัวหน้าฝ่าย
                        dtA = ctlEmp.Employee_GetNewAssesseeHeader(GlobalVariables.BYear, iDiv, GlobalVariables.username);
                        lblNotic.Text = "ฝ่ายของท่านไม่มีพนักงานใหม่ให้ประเมิน";
                        break;
                    case "3": //หัวหน้าแผนก
                        dtA = ctlEmp.Employee_GetNewAssesseesByDepartment(GlobalVariables.BYear,iDept, GlobalVariables.username);
                        lblNotic.Text = "แผนกของท่านไม่มีพนักงานใหม่ให้ประเมิน";
                        break;
                    default:
                        if(isHR=="Y")
                        {
                            dtA = ctlEmp.Employee_GetNewAssesseesByHR(GlobalVariables.BYear);
                        }
                        
                        break;
                }              


               // dtA = ctlEmp.Employee_GetAssessees(row["LevelAssessee"].ToString(), iDiv, iDept, GlobalVariables.username);

                if (dtA.Rows.Count>0)
                {
                    for (n = 0; n < dtA.Rows.Count; n++)
                    {
                        rowA = dtA.Rows[n];
                        dr = dtAssessee.NewRow();
                        dr[0] = rowA["EmployeeID"];
                        dr[1] = rowA["EmployeeName"];
                        dr[2] = rowA["PositionName"];
                        dr[3] = rowA["DepartmentName"];
                        dr[4] = rowA["DivisionName"];
                        dr[5] = rowA["EmployeeLevelID"];
                        dr[6] = rowA["DepartmentUID"];
                        dr[7] = rowA["DivisionUID"];
                        dr[8] = rowA["LevelName"];
                        dr[9] = rowA["isAssessment"];
                        dr[10] = "Y";
                        dr[11] = rowA["GroupName"];
                        dr[12] = rowA["NetScore1"];
                        dr[13] = rowA["NetScore2"];
                        dr[14] = rowA["NetScore3"];
                        dr[15] = rowA["NetScore4"];
                        dr[16] = rowA["SortOrder"];
                        dr[17] = rowA["Remark"];

                        dtAssessee.Rows.Add(dr);
                    }              

                }
            }

            //ctlEmp.xAssessorAssessment_Delete(GlobalVariables.BYear, GlobalVariables.username);

            //int k;
            //for (k = 0; k < dtAssessee.Rows.Count; k++)
            //{
            //    ctlEmp.xAssessorAssessment_Add(GlobalVariables.BYear, GlobalVariables.username, dtAssessee.Rows[k].Field<string>("EmployeeID"));
            //}
            
            grdEmployee.DataSource = null;

            if (dtAssessee.Rows.Count > 0)
            {
                grdEmployee.DataSource = dtAssessee;

                grdViewEmployee.Columns[1].Caption = " ";
                grdViewEmployee.Columns[3].BestFit();
                grdViewEmployee.Columns[4].BestFit();
                grdViewEmployee.Columns[5].BestFit();
                grdViewEmployee.Columns[6].BestFit();
                grdViewEmployee.Columns[7].BestFit();  //เดือนที่1
                grdViewEmployee.Columns[8].BestFit();  //เดือนที่2           
                grdViewEmployee.Columns[9].BestFit(); //เดือนที่3
                //grdViewEmployee.Columns[10].AppearanceCell.ForeColor = Color.Red;
                grdViewEmployee.Columns[10].BestFit(); //เดือนที่4
                grdViewEmployee.Columns[11].BestFit();
                grdViewEmployee.Columns[12].BestFit();
                grdViewEmployee.Columns[13].Width = 300;

                lblNotic.Visible = false;
            }
            else
            {
                grdEmployee.Visible = false;
                lblNotic.Visible = true;
            }
                       
            //if (OpenAssesseeListForm() == true)
            //{
            //    grdViewEmployee.Columns[9].Visible = true;
            //}
            //else
            //{
            //    grdViewEmployee.Columns[9].Visible = false;
            //}

        }

        private void grdViewEmployee_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            try
            {
                GridHitInfo hi = view.CalcHitInfo(e.X, e.Y);

                if (hi.Column.FieldName == "Assm" && view.GetRowCellValue(hi.RowHandle, view.Columns["Assm"]).ToString() == "Y")
                {
                    if (e.X >= 1190 && e.X <= 1230)
                    {
                        if (hi.InGroupRow == false)
                            view.GridControl.Cursor = Cursors.Hand;
                    }
                    else
                    {
                        view.GridControl.Cursor = Cursors.Default;
                    }
                }
                else
                {
                    view.GridControl.Cursor = Cursors.Default;
                }
                //if (hi.Column.Caption == "PACS")
                //{
                //    view.GridControl.Cursor = Cursors.Hand;
                //}
            }
            catch
            {
                view.GridControl.Cursor = Cursors.Default;
            }
        }

        private void grdViewEmployee_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) != Keys.Control)
            {
                GridView view = sender as GridView;
                GridHitInfo hi = view.CalcHitInfo(e.Location);
                try
                {
                    if (hi.Column.Name == "colAssessment")
                    {
                        view.FocusedRowHandle = hi.RowHandle;
                        view.FocusedColumn = hi.Column;
                        view.ShowEditor();
                        if (grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString() != "0" && grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString() != "")
                        {
                            DataTable dtC = new DataTable();
                            CompetencyController ctlCom = new CompetencyController();

                            GlobalVariables.AssesseeLevelUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID"));

                            dtC = ctlCom.Evaluation_GetByEmployeeNew(GlobalVariables.BYear, grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString(),  grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID").ToString(),GlobalVariables.username);

                            //if (dtC.Rows.Count > 1)
                            //{
                            //    frmCompetencyEmployee frmComp = new frmCompetencyEmployee();
                            //    frmComp.EmployeeID = grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString();
                            //    frmComp.EmployeeLevelUID =Convert.ToInt32( grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID"));
                              
                            //    frmComp.EmployeeName = grdViewEmployee.GetFocusedRowCellValue("EmployeeName").ToString();
                            //    frmComp.ShowDialog(this);
                               
                            //}
                            //else
                            //{
                                //เปิดฟอร์ม PA
                                frmAssessmentNew fPA = new frmAssessmentNew();
                                //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                                fPA.EmployeeID = grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString();

                                fPA.EvaluationUID = dtC.Rows[0].Field<int>("EvaluationUID");
                                if (dtC.Rows[0]["isAssessment"].ToString() == "Y")
                                {
                                    fPA.flagEdit = true;
                                }                                   

                                fPA.EmployeeName = grdViewEmployee.GetFocusedRowCellValue("EmployeeName").ToString();

                                ////this.Close();
                                ////fPA.MdiParent = this.MdiParent;
                                //fPA.TopMost = true;
                                fPA.ShowDialog();
                               
                            //}

                            LoadAssessees();

                            //frmAssesseeNewList_Load(sender, e);
                        }

                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}