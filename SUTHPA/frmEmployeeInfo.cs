﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace SUTHPA
{
   
    public partial class frmEmployeeInfo : DevExpress.XtraEditors.XtraForm
    {
        public frmEmployeeInfo()
        {
            InitializeComponent();
        }

        DataTable dtEmp = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();
        MasterController ctlM = new MasterController();

        private void frmEmployeeInfo_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            LoadEmployee();
            LoadLevel();
            LoadDirection();
            LoadDivision();
            LoadDepartment();
            LoadPosition();

            LoadDirectionJob();
            LoadDivisionJob();
            LoadPositionJob();
           
        }
        private void LoadLevel()
        {
            ddlLevel.Properties.Columns.Clear();
            ddlLevel.Properties.DataSource = ctlEmp.Employee_GetLevel();
            ddlLevel.Properties.DisplayMember = "LevelName";
            ddlLevel.Properties.ValueMember = "LevelID";

            ddlLevel.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlLevel.Properties.DropDownRows = 10;
            ddlLevel.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LevelName", 300, "");
            ddlLevel.Properties.Columns.Add(col3);
          

        }
        private void LoadDirection()
        {
            //DataTable dt = new DataTable();

            //if (GlobalVariables.IsAdmin)
            //{
            ddlDirection.Properties.Columns.Clear();
            ddlDirection.Properties.DataSource = ctlEmp.GetDirection();
            ddlDirection.Properties.DisplayMember = "Name";
            ddlDirection.Properties.ValueMember = "UID";

            ddlDirection.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlDirection.Properties.DropDownRows = 10;
            ddlDirection.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            ddlDirection.Properties.Columns.Add(col3);
            ddlDirection.EditValue = 0;

        }
        private void LoadDivision()
        {
            //DataTable dt = new DataTable();

            //if (GlobalVariables.IsAdmin)
            //{
            ddlDivision.Properties.Columns.Clear();
            ddlDivision.Properties.DataSource = ctlEmp.Employee_GetDivisionByDirection(Convert.ToInt32(ddlDirection.EditValue));
            ddlDivision.Properties.DisplayMember = "DivisionName";
            ddlDivision.Properties.ValueMember = "DivisionUID";

            ddlDivision.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlDivision.Properties.DropDownRows = 10;
            ddlDivision.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DivisionName", 300, "");
            ddlDivision.Properties.Columns.Add(col3);
            ddlDivision.EditValue = 0;
                        //}
            //else
            //{

            //    dt = ctlEmp.Employee_GetEmployeeDivision(GlobalVariables.username);
            //    lblDivisionName.Text = dt.Rows[0].Field<string>("DivisionName");
            //    lblUID.Text = dt.Rows[0]["DivisionUID"].ToString();
            //    lblLevelLabel.Text = "Division";
            //}




            //dt = null;                      
        }

        private void LoadDepartment()
        {
            if (ddlDivision.EditValue.ToString() != "")
            {

                ddlDepartment.Properties.Columns.Clear();
                ddlDepartment.Properties.DataSource = ctlEmp.Employee_GetDepartmentByDivision(Convert.ToInt32(ddlDivision.EditValue));
                ddlDepartment.Properties.DisplayMember = "DepartmentName";
                ddlDepartment.Properties.ValueMember = "DepartmentUID";

                ddlDepartment.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
                ddlDepartment.Properties.DropDownRows = 10;
                ddlDepartment.Properties.ShowFooter = false;
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", 300, "");
                ddlDepartment.Properties.Columns.Add(col3);
                ddlDepartment.EditValue = 0;
            }
        }
        private void LoadPosition()
        {
            ddlPosition.Properties.Columns.Clear();
            ddlPosition.Properties.DataSource = ctlEmp.Employee_GetPosition();
            ddlPosition.Properties.DisplayMember = "Name";
            ddlPosition.Properties.ValueMember = "UID";

            ddlPosition.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlPosition.Properties.DropDownRows = 10;
            ddlPosition.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            ddlPosition.Properties.Columns.Add(col3);
            ddlPosition.EditValue = 0;

        }

        //private void LoadLevel()
        //{
        //    ddlLevel.Properties.Columns.Clear();
        //    ddlLevel.Properties.DataSource = ctlEmp.Employee_GetLevel();
        //    ddlLevel.Properties.DisplayMember = "LevelName";
        //    ddlLevel.Properties.ValueMember = "LevelID";
        //    lblLevelLabel.Text = "ระดับ";

        //    ddlLevel.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //    ddlLevel.Properties.DropDownRows = 10;
        //    ddlLevel.Properties.ShowFooter = false;
        //    DevExpress.XtraEditors.Controls.LookUpColumnInfo col2 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LevelName", 200, "");
        //    ddlLevel.Properties.Columns.Add(col2);
        //    ddlLevel.EditValue = 0;
        //}
        private void LoadDirectionJob()
        { 
            ddlJobDirection.Properties.Columns.Clear();
            ddlJobDirection.Properties.DataSource = ctlEmp.GetDirection();
            ddlJobDirection.Properties.DisplayMember = "Name";
            ddlJobDirection.Properties.ValueMember = "UID";

            ddlJobDirection.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlJobDirection.Properties.DropDownRows = 10;
            ddlJobDirection.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            ddlJobDirection.Properties.Columns.Add(col3);
            ddlJobDirection.EditValue = 0;
        }

        private void LoadDivisionJob()
        {
            //DataTable dt = new DataTable();

            //if (GlobalVariables.IsAdmin)
            //{
            ddlJobDivision.Properties.Columns.Clear();
            ddlJobDivision.Properties.DataSource = ctlEmp.Employee_GetDivisionByDirection(Convert.ToInt32(ddlJobDirection.EditValue));
            ddlJobDivision.Properties.DisplayMember = "DivisionName";
            ddlJobDivision.Properties.ValueMember = "DivisionUID";

            ddlJobDivision.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlJobDivision.Properties.DropDownRows = 10;
            ddlJobDivision.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DivisionName", 300, "");
            ddlJobDivision.Properties.Columns.Add(col3);
            ddlJobDivision.EditValue = 0;
            //}
            //else
            //{

            //    dt = ctlEmp.Employee_GetEmployeeDivision(GlobalVariables.username);
            //    lblDivisionName.Text = dt.Rows[0].Field<string>("DivisionName");
            //    lblUID.Text = dt.Rows[0]["DivisionUID"].ToString();
            //    lblLevelLabel.Text = "Division";
            //}




            //dt = null;                      
        }

        private void LoadDepartmentJob()
        {
            if (ddlJobDivision.EditValue.ToString() != "")
            {                
                ddlJobDepartment.Properties.Columns.Clear();
                ddlJobDepartment.Properties.DataSource = ctlEmp.Employee_GetDepartmentByDivision(Convert.ToInt32(ddlJobDivision.EditValue));
                ddlJobDepartment.Properties.DisplayMember = "DepartmentName";
                ddlJobDepartment.Properties.ValueMember = "DepartmentUID";

                ddlJobDepartment.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
                ddlJobDepartment.Properties.DropDownRows = 10;
                ddlJobDepartment.Properties.ShowFooter = false;
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", 300, "");
                ddlJobDepartment.Properties.Columns.Add(col3);
                ddlJobDepartment.EditValue = 0;
            }
        }
        private void LoadPositionJob()
        {
            ddlJobPosition.Properties.Columns.Clear();
            ddlJobPosition.Properties.DataSource = ctlEmp.Employee_GetPosition();
            ddlJobPosition.Properties.DisplayMember = "Name";
            ddlJobPosition.Properties.ValueMember = "UID";

            ddlJobPosition.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlJobPosition.Properties.DropDownRows = 10;
            ddlJobPosition.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            ddlJobPosition.Properties.Columns.Add(col3);
            ddlJobPosition.EditValue = 0;

        }


        private void LoadEmployee()
        {

            try
            {
                dtEmp = ctlEmp.Employee_Get(); //GlobalVariables.BYear
                grdEmployee.DataSource = dtEmp;
                grdViewEmployee.BestFitColumns();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }

        private void LoadJob()
        {

            try
            {
                dtEmp = ctlEmp.Employee_GetJob(txtEmployeeID.Text); //GlobalVariables.BYear
                grdJob.DataSource = dtEmp;
                grdViewJob.BestFitColumns();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdEmployee_DoubleClick(object sender, EventArgs e)
        {
            //PatientVisitInfo.SendVisit(Convert.ToInt64(gridViewData.GetFocusedRowCellValue("VisitNumber")),Convert.ToInt64(gridViewData.GetFocusedRowCellValue("PatientUID")));
            //BaseGlobalClass.gPatientUID = Convert.ToInt32(gridViewData.GetFocusedRowCellValue("PatientUID"));
            //BaseGlobalClass.gPatientVisitUID = Convert.ToInt32(gridViewData.GetFocusedRowCellValue("VisitNumber"));
            //ShowPatientResult(BaseGlobalClass.gPatientUID, BaseGlobalClass.gPatientVisitUID);

            

            EditData(grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString());
        }

        private void EditData(string pID)
        {
            DataTable dtE = new DataTable();
 
            dtE = ctlEmp.Employee_GetByCode(pID);
            if (dtE.Rows.Count > 0)
            {

                lblEmpUID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<int>("UID"));                 
                txtEmployeeID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("EmployeeID"));
                txtName.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Name"));
                txtPrefix.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Title"));
                txtCardID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("NationalID"));

                ddlLevel.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("EmployeeLevelID"));
                
                ddlDirection.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("DirectionUID"));
                LoadDivision();

                ddlDivision.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("DivisionUID"));
                LoadDepartment();
                ddlDepartment.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("DepartmentUID"));
                ddlPosition.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("PositionUID"));

                txtPassword.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Password"));                 
                dtpHireDate.EditValue = BaseClass.DBNull2Str(dtE.Rows[0]["HireDate"]);

                chkMedical.Checked = GlobalFunctions.ConvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("isMedical"));
                chkAdmin.Checked = GlobalFunctions.ConvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("isAdmin"));
                chkSuperAdmin.Checked = GlobalFunctions.ConvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("isSuperAdmin"));
                chkReporter.Checked = GlobalFunctions.ConvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("isReporter"));
                chkHR.Checked = GlobalFunctions.ConvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("isHR"));

                chkStatus.Checked = GlobalFunctions.ConvertStatusActive2CheckBox(dtE.Rows[0].Field<string>("StatusFlag"));
                chkAssessment.Checked = GlobalFunctions.ConvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("StatusAssessment"));
                chkIsProbation.Checked = GlobalFunctions.InvertYN2CheckBoxStatus(dtE.Rows[0].Field<string>("isProbation"));



                LoadJob();
            }
            dtE = null;
        }
        private void ClearData()
        {          
                lblEmpUID.Text ="0";
                txtEmployeeID.Text ="";
                txtName.Text ="";
                txtPrefix.Text ="";
                txtCardID.Text = "";
                ddlDivision.Text ="";      
                ddlDepartment.Text ="";
                ddlPosition.Text ="";
                txtPassword.Text ="";
                dtpHireDate.EditValue ="";
                chkMedical.Checked =false;
                chkAdmin.Checked = false;
                chkSuperAdmin.Checked = false;
                chkReporter.Checked = false;
                chkHR.Checked = false;
            chkStatus.Checked = true;
            chkAssessment.Checked = true;
            chkIsProbation.Checked = true;


             
                lblJobUID.Text = "0";
                txtYear.Text = GlobalVariables.BYear.ToString();
                ddlJobDivision.Text = "";
                ddlJobDepartment.Text = "";
                ddlJobPosition.Text = "";
                cmdSaveJob.Text = "เพิ่ม";
                grdJob.DataSource = null;

        }


        private void ddlDivision_EditValueChanged(object sender, EventArgs e)
        {
            LoadDepartment();
        }

        private void ddlJobDivision_EditValueChanged(object sender, EventArgs e)
        {
            LoadDepartmentJob();
        }

        private void grdJob_DoubleClick(object sender, EventArgs e)
        {
            lblJobUID.Text =  grdViewJob.GetFocusedRowCellValue("UID").ToString();
            txtYear.Text = grdViewJob.GetFocusedRowCellValue("BYear").ToString();
            ddlJobDirection.EditValue = Convert.ToInt32(grdViewJob.GetFocusedRowCellValue("DirectionUID").ToString());
            ddlJobDivision.EditValue = Convert.ToInt32(grdViewJob.GetFocusedRowCellValue("DivisionUID").ToString());
            ddlJobDepartment.EditValue = Convert.ToInt32(grdViewJob.GetFocusedRowCellValue("DepartmentUID").ToString());

            if (grdViewJob.GetFocusedRowCellValue("PositionUID").ToString()!="")
            {
                ddlJobPosition.EditValue =Convert.ToInt32(grdViewJob.GetFocusedRowCellValue("PositionUID").ToString());
            }
            

            cmdSaveJob.Text = "บันทึก";
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdCancelJob_Click(object sender, EventArgs e)
        {
            lblJobUID.Text = "0";
            txtYear.Text = GlobalVariables.BYear.ToString();
            ddlJobDivision.EditValue = "";
            ddlJobDepartment.EditValue ="";
            ddlJobPosition.EditValue = "";
            cmdSaveJob.Text = "เพิ่ม";
        }

        private void cmdSaveJob_Click(object sender, EventArgs e)
        {
            ctlEmp.EmployeeStatus_Save(Convert.ToInt32(lblJobUID.Text),Convert.ToInt32(txtYear.Text),txtEmployeeID.Text, Convert.ToInt32(ddlJobPosition.EditValue), Convert.ToInt32(ddlJobDepartment.EditValue), Convert.ToInt32(ddlJobDivision.EditValue), Convert.ToInt32(ddlJobDirection.EditValue), "A");
            MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
            lblJobUID.Text = "0";
            txtYear.Text = GlobalVariables.BYear.ToString();
            ddlJobDivision.EditValue = "";
            ddlJobDepartment.EditValue = "";
            ddlJobPosition.EditValue = "";
            cmdSaveJob.Text = "เพิ่ม";

            LoadJob();

        }

        private void cmdDelJob_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                ctlEmp.EmployeeStatus_Delete(Convert.ToInt32(lblJobUID.Text));
                MessageBox.Show("ลบข้อมูลเรียบร้อย");
                lblJobUID.Text = "0";
                txtYear.Text = GlobalVariables.BYear.ToString();
                ddlJobDivision.EditValue = "";
                ddlJobDirection.EditValue = "";
                ddlJobDepartment.EditValue = "";
                ddlJobPosition.EditValue = "";
                ddlLevel.EditValue = "";
                cmdSaveJob.Text = "เพิ่ม";

                LoadJob();
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            ctlEmp.Employee_Save(Convert.ToInt32(lblEmpUID.Text), txtEmployeeID.Text, Convert.ToInt32(ddlPosition.EditValue), Convert.ToInt32(ddlDepartment.EditValue), Convert.ToInt32(ddlDivision.EditValue), Convert.ToInt32(ddlDirection.EditValue), txtPrefix.Text,txtName.Text,txtPassword.Text,GlobalFunctions.ConvertCheckBoxStatus2YN(chkAdmin.Checked), GlobalFunctions.ConvertCheckBoxStatus2YN(chkReporter.Checked), GlobalFunctions.ConvertCheckBoxStatus2YN(chkSuperAdmin.Checked),GlobalFunctions.ConvertCheckBoxStatus2YN(chkMedical.Checked), GlobalFunctions.ConvertCheckBoxStatus2YN(chkHR.Checked),txtCardID.Text,BaseClass.ConvertStrDate2ShortDateQueryString(dtpHireDate.Text), GlobalFunctions.ConvertCheckBox2StatusActive(chkStatus.Checked), GlobalFunctions.ConvertCheckBoxStatus2YN(chkAssessment.Checked), Convert.ToInt32(ddlLevel.EditValue), GlobalFunctions.InvertCheckBoxStatus2YN(chkIsProbation.Checked));

            ctlEmp.EmployeeStatusMain_Save(Convert.ToInt32(txtCurrentYear.Text), txtEmployeeID.Text, Convert.ToInt32(ddlPosition.EditValue), Convert.ToInt32(ddlDepartment.EditValue), Convert.ToInt32(ddlDivision.EditValue), Convert.ToInt32(ddlDirection.EditValue), "A");


            MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
            ClearData();
            LoadEmployee();

        }

        private void cmdDisable_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                ctlEmp.Employee_Delete(Convert.ToInt32(lblEmpUID.Text));
                MessageBox.Show("ลบข้อมูลเรียบร้อย");
                LoadEmployee();
                ClearData();
            }
        }

        private void ddlDirection_EditValueChanged(object sender, EventArgs e)
        {
            LoadDivision();
        }

        private void ddlJobDirection_EditValueChanged(object sender, EventArgs e)
        {
            LoadDivisionJob();
        }
               

        private void grdViewEmployee_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if ((grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[8]).ToString() != "ผู้อำนวยการ") && (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[8]).ToString() != "หัวหน้ากลุ่มงาน") && (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[16]).ToString() == "A"))
                {               

                // Dim view As GridView = TryCast(sender, GridView)
                    if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[13]) != null)
                    {
                        if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[13]).ToString() == "")
                            e.Appearance.ForeColor = Color.Red;
                    }

                    if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[14]) != null)
                    {
                        if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[14]).ToString() == "")
                            e.Appearance.ForeColor = Color.Red;
                    }

                    if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[15]) != null)
                    {
                        if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[15]).ToString() == "")
                            e.Appearance.ForeColor = Color.Red;
                    }

                    if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[4]) != null)
                    {
                        if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[4]).ToString() == "")
                            e.Appearance.ForeColor = Color.Red;
                    }

                    if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[17]) != null)
                    {
                        if (grdViewEmployee.GetRowCellValue(e.RowHandle, grdViewEmployee.Columns[17]).ToString() == "")
                            e.Appearance.ForeColor = Color.Red;
                    }
                }
            }
        }
    }
}