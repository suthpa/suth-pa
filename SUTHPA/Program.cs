﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.IO;
using System.Xml;

namespace SUTHPA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (File.Exists(@"C:\ProgramData\SUTHPA\DataConfig2021.xml") == false)
            {
                CreateXML();
            }

            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            Application.Run(new frmLogin());
        }
        static void CreateXML()
        {
            
            if (Directory.Exists(@"C:\ProgramData\SUTHPA") == false)
            {
                Directory.CreateDirectory(@"C:\ProgramData\SUTHPA");
            }


            using (XmlWriter xmlWriter = XmlWriter.Create(@"C:\ProgramData\SUTHPA\" + "DataConfig2021.xml"))
            {

                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("appSettings");

                xmlWriter.WriteStartElement("ServerPath");
                xmlWriter.WriteString("172.100.50.211");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("DatabaseName");
                xmlWriter.WriteString("SUTHPA");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Username");
                xmlWriter.WriteString("paadmin");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Password");
                xmlWriter.WriteString("p@adm1n");
                xmlWriter.WriteEndElement();
               
                xmlWriter.WriteStartElement("PassPhase");
                xmlWriter.WriteString("SUTH@");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ReportServerLocation");
                xmlWriter.WriteString("http://172.100.50.211/ReportServer");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ReportServerURL");
                xmlWriter.WriteString("http://172.100.50.211/ReportServer");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ReportServerPath");
                xmlWriter.WriteString("/SUTHPA");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ReportServerUser");
                xmlWriter.WriteString("administrator");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ReportServerPassword");
                xmlWriter.WriteString("SMC@Sut123");
                xmlWriter.WriteEndDocument();

                xmlWriter.Close();
            }
        }

    }
}
