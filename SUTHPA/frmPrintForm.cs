﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
    public partial class frmPrintForm : DevExpress.XtraEditors.XtraForm
    {
        public frmPrintForm()
        {
            InitializeComponent();
        }

        AssessmentController ctlP = new AssessmentController();
        EmployeeController  ctlE = new EmployeeController();
        private void frmPrintForm_Load(object sender, EventArgs e)
        {
            txtCodeA.Text = GlobalVariables.LoginUser;
            txtCodeA.Focus();

        }
 
        private void cmdView_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
 //System.Threading.Thread.Sleep(2000);              

            GlobalVariables.Reportskey = "NEWFORM";
            GlobalVariables.RPTTYPE = "";
            GlobalVariables.ReportName = "พิมพ์แบบประเมินพนักงานใหม่";
            string StartCode;
            StartCode = txtCodeA.Text;
        

            if (txtCodeA.Text == "") StartCode = "0";

                                  
            frmReportViewer frm = new frmReportViewer();
            frm.MdiParent = this.MdiParent;
            frm.EmployeeID = this.txtCodeA.Text;
            frm.Show();
  
            this.WindowState = FormWindowState.Normal;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
        private void txtCodeA_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((cInt >= 48 && cInt <= 57) || cInt == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCodeA_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                LoadReport();
            }
        }
    }
}