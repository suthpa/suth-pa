﻿namespace SUTHPA
{
    partial class frmPrintCondition

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblReportTitle = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.cmdView = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new System.Windows.Forms.Panel();
            this.txtCodeB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodeA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.progressLoad = new DevExpress.XtraWaitForm.ProgressPanel();
            this.cboDirection = new DevExpress.XtraEditors.LookUpEdit();
            this.lblDirection = new System.Windows.Forms.Label();
            this.cboDepartment = new DevExpress.XtraEditors.LookUpEdit();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.cboDivision = new DevExpress.XtraEditors.LookUpEdit();
            this.lblDivision = new System.Windows.Forms.Label();
            this.cboLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.cboYear = new DevExpress.XtraEditors.LookUpEdit();
            this.lblLevelLabel = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDirection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDivision.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboYear.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(377, 35);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblReportTitle);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(377, 35);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // lblReportTitle
            // 
            this.lblReportTitle.AutoSize = true;
            this.lblReportTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblReportTitle.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReportTitle.ForeColor = System.Drawing.Color.White;
            this.lblReportTitle.Location = new System.Drawing.Point(5, 8);
            this.lblReportTitle.Name = "lblReportTitle";
            this.lblReportTitle.Size = new System.Drawing.Size(156, 20);
            this.lblReportTitle.TabIndex = 3;
            this.lblReportTitle.Text = "รายงานคะแนนประเมิน";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(350, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 31);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 204);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(377, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Controls.Add(this.cmdView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(48, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 40);
            this.panel1.TabIndex = 33;
            // 
            // btClose
            // 
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.Location = new System.Drawing.Point(222, 6);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(95, 30);
            this.btClose.TabIndex = 8;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // cmdView
            // 
            this.cmdView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdView.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdView.Appearance.Options.UseFont = true;
            this.cmdView.Appearance.Options.UseForeColor = true;
            this.cmdView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdView.Location = new System.Drawing.Point(119, 6);
            this.cmdView.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.HotTrack;
            this.cmdView.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdView.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdView.Name = "cmdView";
            this.cmdView.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdView.Size = new System.Drawing.Size(97, 30);
            this.cmdView.TabIndex = 7;
            this.cmdView.Text = "View";
            this.cmdView.Click += new System.EventHandler(this.cmdView_Click);
            // 
            // pnMain
            // 
            this.pnMain.BackColor = System.Drawing.Color.White;
            this.pnMain.Controls.Add(this.txtCodeB);
            this.pnMain.Controls.Add(this.label2);
            this.pnMain.Controls.Add(this.txtCodeA);
            this.pnMain.Controls.Add(this.label1);
            this.pnMain.Controls.Add(this.progressLoad);
            this.pnMain.Controls.Add(this.cboDirection);
            this.pnMain.Controls.Add(this.lblDirection);
            this.pnMain.Controls.Add(this.cboDepartment);
            this.pnMain.Controls.Add(this.lblDepartment);
            this.pnMain.Controls.Add(this.cboDivision);
            this.pnMain.Controls.Add(this.lblDivision);
            this.pnMain.Controls.Add(this.cboLevel);
            this.pnMain.Controls.Add(this.cboYear);
            this.pnMain.Controls.Add(this.lblLevelLabel);
            this.pnMain.Controls.Add(this.lbl1);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 35);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(377, 169);
            this.pnMain.TabIndex = 2;
            // 
            // txtCodeB
            // 
            this.txtCodeB.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCodeB.Location = new System.Drawing.Point(216, 137);
            this.txtCodeB.MaxLength = 6;
            this.txtCodeB.Name = "txtCodeB";
            this.txtCodeB.Size = new System.Drawing.Size(93, 25);
            this.txtCodeB.TabIndex = 6;
            this.txtCodeB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center; 
            this.txtCodeB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeB_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label2.Location = new System.Drawing.Point(188, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 19);
            this.label2.TabIndex = 195;
            this.label2.Text = "ถึง";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCodeA
            // 
            this.txtCodeA.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCodeA.Location = new System.Drawing.Point(89, 137);
            this.txtCodeA.MaxLength = 6;
            this.txtCodeA.Name = "txtCodeA";
            this.txtCodeA.Size = new System.Drawing.Size(93, 25);
            this.txtCodeA.TabIndex = 5;
            this.txtCodeA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodeA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodeA_KeyDown);
            this.txtCodeA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeA_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label1.Location = new System.Drawing.Point(5, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 19);
            this.label1.TabIndex = 193;
            this.label1.Text = "รหัสพนักงาน";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // progressLoad
            // 
            this.progressLoad.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressLoad.Appearance.Options.UseBackColor = true;
            this.progressLoad.BarAnimationElementThickness = 2;
            this.progressLoad.Location = new System.Drawing.Point(89, 49);
            this.progressLoad.LookAndFeel.SkinName = "McSkin";
            this.progressLoad.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressLoad.Name = "progressLoad";
            this.progressLoad.Size = new System.Drawing.Size(246, 66);
            this.progressLoad.TabIndex = 186;
            // 
            // cboDirection
            // 
            this.cboDirection.Location = new System.Drawing.Point(63, 47);
            this.cboDirection.Name = "cboDirection";
            this.cboDirection.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cboDirection.Properties.Appearance.Options.UseFont = true;
            this.cboDirection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.cboDirection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDirection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDirection.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.cboDirection.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cboDirection.Properties.NullText = "";
            this.cboDirection.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cboDirection.Size = new System.Drawing.Size(302, 24);
            this.cboDirection.TabIndex = 2;
            this.cboDirection.EditValueChanged += new System.EventHandler(this.cboDirection_EditValueChanged);
            // 
            // lblDirection
            // 
            this.lblDirection.AutoSize = true;
            this.lblDirection.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblDirection.Location = new System.Drawing.Point(5, 49);
            this.lblDirection.Name = "lblDirection";
            this.lblDirection.Size = new System.Drawing.Size(52, 19);
            this.lblDirection.TabIndex = 191;
            this.lblDirection.Text = "กลุ่มงาน";
            this.lblDirection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboDepartment
            // 
            this.cboDepartment.Location = new System.Drawing.Point(63, 107);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cboDepartment.Properties.Appearance.Options.UseFont = true;
            this.cboDepartment.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.cboDepartment.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDepartment.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.cboDepartment.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cboDepartment.Properties.NullText = "";
            this.cboDepartment.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cboDepartment.Size = new System.Drawing.Size(302, 24);
            this.cboDepartment.TabIndex = 4;
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblDepartment.Location = new System.Drawing.Point(5, 109);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(40, 19);
            this.lblDepartment.TabIndex = 189;
            this.lblDepartment.Text = "แผนก";
            this.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboDivision
            // 
            this.cboDivision.Location = new System.Drawing.Point(63, 77);
            this.cboDivision.Name = "cboDivision";
            this.cboDivision.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cboDivision.Properties.Appearance.Options.UseFont = true;
            this.cboDivision.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.cboDivision.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDivision.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDivision.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.cboDivision.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cboDivision.Properties.NullText = "";
            this.cboDivision.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cboDivision.Size = new System.Drawing.Size(302, 24);
            this.cboDivision.TabIndex = 3;
            this.cboDivision.EditValueChanged += new System.EventHandler(this.cboDivision_EditValueChanged);
            // 
            // lblDivision
            // 
            this.lblDivision.AutoSize = true;
            this.lblDivision.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblDivision.Location = new System.Drawing.Point(5, 79);
            this.lblDivision.Name = "lblDivision";
            this.lblDivision.Size = new System.Drawing.Size(30, 19);
            this.lblDivision.TabIndex = 187;
            this.lblDivision.Text = "ฝ่าย";
            this.lblDivision.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboLevel
            // 
            this.cboLevel.Location = new System.Drawing.Point(188, 17);
            this.cboLevel.Name = "cboLevel";
            this.cboLevel.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cboLevel.Properties.Appearance.Options.UseFont = true;
            this.cboLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.cboLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLevel.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.cboLevel.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cboLevel.Properties.NullText = "";
            this.cboLevel.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cboLevel.Size = new System.Drawing.Size(177, 24);
            this.cboLevel.TabIndex = 1;
            // 
            // cboYear
            // 
            this.cboYear.Location = new System.Drawing.Point(63, 17);
            this.cboYear.Name = "cboYear";
            this.cboYear.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cboYear.Properties.Appearance.Options.UseFont = true;
            this.cboYear.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.cboYear.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboYear.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.cboYear.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cboYear.Properties.NullText = "";
            this.cboYear.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cboYear.Size = new System.Drawing.Size(77, 24);
            this.cboYear.TabIndex = 0;
            // 
            // lblLevelLabel
            // 
            this.lblLevelLabel.AutoSize = true;
            this.lblLevelLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblLevelLabel.Location = new System.Drawing.Point(146, 19);
            this.lblLevelLabel.Name = "lblLevelLabel";
            this.lblLevelLabel.Size = new System.Drawing.Size(36, 19);
            this.lblLevelLabel.TabIndex = 1;
            this.lblLevelLabel.Text = "ระดับ";
            this.lblLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lbl1.Location = new System.Drawing.Point(12, 20);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(17, 19);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "ปี";
            // 
            // frmPrintCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 244);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "frmPrintCondition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายงานผลการประเมิน";
            this.Load += new System.EventHandler(this.frmPrintCondition_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnMain.ResumeLayout(false);
            this.pnMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDirection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDivision.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboYear.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label lblReportTitle;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private System.Windows.Forms.Panel pnMain;
        private DevExpress.XtraEditors.SimpleButton cmdView;
        private DevExpress.XtraEditors.LookUpEdit cboLevel;
        private DevExpress.XtraEditors.LookUpEdit cboYear;
        private System.Windows.Forms.Label lblLevelLabel;
        private System.Windows.Forms.Label lbl1;
        private DevExpress.XtraWaitForm.ProgressPanel progressLoad;
        private DevExpress.XtraEditors.LookUpEdit cboDepartment;
        private System.Windows.Forms.Label lblDepartment;
        private DevExpress.XtraEditors.LookUpEdit cboDivision;
        private System.Windows.Forms.Label lblDivision;
        private DevExpress.XtraEditors.LookUpEdit cboDirection;
        private System.Windows.Forms.Label lblDirection;
        private System.Windows.Forms.TextBox txtCodeB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodeA;
        private System.Windows.Forms.Label label1;
    }
}