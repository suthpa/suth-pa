﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
   
    public partial class frmDepartment : DevExpress.XtraEditors.XtraForm
    {
        public frmDepartment()
        {
            InitializeComponent();
        }

        DataTable dtM = new DataTable();
        MasterController ctlM = new MasterController();
        private void frmDepartment_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            LoadDivision();               
            LoadData();
            if (!GlobalVariables.IsAdmin && !GlobalVariables.IsSuperAdmin && !GlobalVariables.IsHR)
            {
                cmdSave.Enabled = false;
                cmdDel.Enabled = false;
                cmdCancel.Enabled = false;
            }
        }

        private void LoadDivision()
        {
            //DataTable dt = new DataTable();

            //if (GlobalVariables.IsAdmin)
            //{
            ddlDivision.Properties.Columns.Clear();
            ddlDivision.Properties.DataSource = ctlM.Division_GetActive();
            ddlDivision.Properties.DisplayMember = "Name";
            ddlDivision.Properties.ValueMember = "UID";

            ddlDivision.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlDivision.Properties.DropDownRows = 10;
            ddlDivision.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            ddlDivision.Properties.Columns.Add(col3);
            ddlDivision.EditValue = 0;

            ddlDivision.Width = 250;
            //}
            //else
            //{

            //    dt = ctlEmp.Employee_GetEmployeeDivision(GlobalVariables.username);
            //    lblDivisionName.Text = dt.Rows[0].Field<string>("DivisionName");
            //    lblUID.Text = dt.Rows[0]["DivisionUID"].ToString();
            //    lblLevelLabel.Text = "Division";
            //}




            //dt = null;                      
        }

        //private void LoadDepartment()
        //{
        //    if (ddlDirection.EditValue.ToString() != "")
        //    {

        //        ddlDepartment.Properties.Columns.Clear();
        //        ddlDepartment.Properties.DataSource = ctlEmp.Employee_GetDepartmentByDivision(Convert.ToInt32(ddlDirection.EditValue));
        //        ddlDepartment.Properties.DisplayMember = "DepartmentName";
        //        ddlDepartment.Properties.ValueMember = "DepartmentUID";

        //        ddlDepartment.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //        ddlDepartment.Properties.DropDownRows = 10;
        //        ddlDepartment.Properties.ShowFooter = false;
        //        DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", 300, "");
        //        ddlDepartment.Properties.Columns.Add(col3);
        //        ddlDepartment.EditValue = 0;
        //        ddlDepartment.Width = 250;
        //    }
        //}
        //private void LoadPosition()
        //{
        //    ddlPosition.Properties.Columns.Clear();
        //    ddlPosition.Properties.DataSource = ctlEmp.Employee_GetPosition();
        //    ddlPosition.Properties.DisplayMember = "Name";
        //    ddlPosition.Properties.ValueMember = "UID";

        //    ddlPosition.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //    ddlPosition.Properties.DropDownRows = 10;
        //    ddlPosition.Properties.ShowFooter = false;
        //    DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
        //    ddlPosition.Properties.Columns.Add(col3);
        //    ddlPosition.EditValue = 0;
        //    ddlPosition.Width = 250;

        //}

        //private void LoadLevel()
        //{
        //    ddlLevel.Properties.Columns.Clear();
        //    ddlLevel.Properties.DataSource = ctlEmp.Employee_GetLevel();
        //    ddlLevel.Properties.DisplayMember = "LevelName";
        //    ddlLevel.Properties.ValueMember = "LevelID";
        //    lblLevelLabel.Text = "ระดับ";

        //    ddlLevel.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //    ddlLevel.Properties.DropDownRows = 10;
        //    ddlLevel.Properties.ShowFooter = false;
        //    DevExpress.XtraEditors.Controls.LookUpColumnInfo col2 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LevelName", 200, "");
        //    ddlLevel.Properties.Columns.Add(col2);
        //    ddlLevel.EditValue = 0;
        //}


        private void LoadData()
        {

            try
            {
                dtM = ctlM.Department_Get(); //GlobalVariables.BYear
                grdData.DataSource = dtM;
                //grdViewEmployee.BestFitColumns();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }
              

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {        
            EditData(grdViewData.GetFocusedRowCellValue("UID").ToString());
        }

        private void EditData(string pID)
        {
            DataTable dtE = new DataTable();
 
            dtE = ctlM.Department_GetByUID(Convert.ToInt32(pID));
            if (dtE.Rows.Count > 0)
            {

                lblUID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<int>("UID"));                 
                txtCode.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Code"));
                txtNameTH.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("DepartmentName"));
                txtNameEN.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("NameEN"));      
                ddlDivision.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("DivisionUID"));
                chkStatus.Checked = GlobalFunctions.ConvertStatusActive2CheckBox(dtE.Rows[0].Field<string>("StatusFlag"));
            }
            dtE = null;
        }
        private void ClearData()
        {          
                lblUID.Text ="0";
                txtCode.Text ="";
                txtNameEN.Text ="";
                txtNameTH.Text ="";             
                ddlDivision.Text =""; 
            chkStatus.Checked = true;

        }                   
       
        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
               
              

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                ctlM.Department_Delete(Convert.ToInt32(lblUID.Text));
                MessageBox.Show("ลบข้อมูลเรียบร้อย");
                ClearData();
            }            
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            ctlM.Department_Save(Convert.ToInt32(lblUID.Text), txtCode.Text,txtNameTH.Text,txtNameEN.Text , Convert.ToInt32(ddlDivision.EditValue), GlobalFunctions.ConvertCheckBox2StatusActive(chkStatus.Checked));
            MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
            ClearData();
            LoadData();

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }
    }
}