﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmEmployeeList : DevExpress.XtraEditors.XtraForm
    {
        public frmEmployeeList()
        {
            InitializeComponent();
        }
        DataTable dtAssessor = new DataTable();
        DataTable dtAssessee = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();
        int RNDNO;
        private void frmAssesseeList_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            GlobalVariables.FlagRefresh = false;
            ConfigurationController ctlCf = new ConfigurationController();
            RNDNO = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO));
            LoadAssessees();
            //lblAssesor.Text += " โดย " + GlobalVariables.UserEmployeeName;

        }

        private void LoadAssessees()
        {
            int i,n;
            string iDiv,iDept;
            DataRow row;
            DataRow rowA;
            DataRow dr;
            DataTable dtA = new DataTable();
            dtAssessee.Rows.Clear();
                
            dtA = ctlEmp.Employee_GetAssessees(GlobalVariables.BYear);      
                          
            grdEmployee.DataSource = null;
            grdEmployee.DataSource = dtA;
            grdViewEmployee.Columns[3].Width = 300;
            grdViewEmployee.Columns[4].BestFit();
            grdViewEmployee.Columns[5].BestFit();
            grdViewEmployee.Columns[6].BestFit();
            //grdViewEmployee.Columns[9].Width = 30;
            grdViewEmployee.Columns[1].Caption = " ";
        }

        private void grdViewEmployee_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            try
            {
                GridHitInfo hi = view.CalcHitInfo(e.X, e.Y);

                if (hi.Column.FieldName == "Assm" && view.GetRowCellValue(hi.RowHandle, view.Columns["Assm"]).ToString() == "Y")
                {
                    if (e.X >= 1070 && e.X <= 1100)
                    {
                        if (hi.InGroupRow == false)
                            view.GridControl.Cursor = Cursors.Hand;
                    }
                    else
                    {
                        view.GridControl.Cursor = Cursors.Default;
                    }
                }
                else
                {
                    view.GridControl.Cursor = Cursors.Default;
                }
                //if (hi.Column.Caption == "PACS")
                //{
                //    view.GridControl.Cursor = Cursors.Hand;
                //}
            }
            catch
            {
                view.GridControl.Cursor = Cursors.Default;
            }
        }

        private void grdViewEmployee_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) != Keys.Control)
            {
                GridView view = sender as GridView;
                GridHitInfo hi = view.CalcHitInfo(e.Location);
                try
                {
                    if (hi.Column.Name == "colAssessment")
                    {
                        view.FocusedRowHandle = hi.RowHandle;
                        view.FocusedColumn = hi.Column;
                        view.ShowEditor();
                        if (grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString() != "0" && grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString() != "")
                        {
                            DataTable dtC = new DataTable();
                            CompetencyController ctlCom = new CompetencyController();

                            GlobalVariables.AssesseeLevelUID = Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID"));

                            dtC = ctlCom.Evaluation_GetByEmployee(GlobalVariables.BYear, grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString(),  grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID").ToString(),GlobalVariables.username, GlobalVariables.AssessmentClass, RNDNO, Convert.ToInt32(grdViewEmployee.GetFocusedRowCellValue("AssessmentLevelTypeID")));

                            if (dtC.Rows.Count > 1)
                            {
                                frmCompetencyEmployee frmComp = new frmCompetencyEmployee();
                                frmComp.EmployeeID = grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString();
                                frmComp.EmployeeLevelUID =Convert.ToInt32( grdViewEmployee.GetFocusedRowCellValue("EmployeeLevelID"));
                              
                                frmComp.EmployeeName = grdViewEmployee.GetFocusedRowCellValue("EmployeeName").ToString();
                                frmComp.ShowDialog(this);
                               
                            }
                            else
                            {
                                //เปิดฟอร์ม PA
                                frmAssessmentGrade fPA = new frmAssessmentGrade();
                                //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                                fPA.EmployeeID = grdViewEmployee.GetFocusedRowCellValue("EmployeeID").ToString();

                                fPA.EvaluationUID = dtC.Rows[0].Field<int>("UID");
                                if (dtC.Rows[0]["isAssessment"].ToString() == "Y")
                                    fPA.flagEdit = true;

                                fPA.EmployeeName = grdViewEmployee.GetFocusedRowCellValue("EmployeeName").ToString();

                                //this.Close();
                                //fPA.MdiParent = this.MdiParent;
                                fPA.TopMost = true;
                                fPA.ShowDialog();
                               
                            }

                            LoadAssessees();

                            //frmAssesseeList_Load(sender, e);
                        }

                    }
                }
                catch { }
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}