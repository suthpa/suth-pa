﻿namespace SUTHPA
{
    partial class frmAssessment2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtComment = new DevExpress.XtraEditors.MemoEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btSave = new DevExpress.XtraEditors.SimpleButton();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnRating = new System.Windows.Forms.Panel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRemark = new DevExpress.XtraEditors.MemoEdit();
            this.pScore = new System.Windows.Forms.Panel();
            this.grdAssessment = new DevExpress.XtraGrid.GridControl();
            this.grdViewAssessment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameCompetency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencyGroupUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencyUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colMinScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEvaluationUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesc4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetScore1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetScore2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnRating.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemark.Properties)).BeginInit();
            this.pScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1522, 40);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.groupControl3.Appearance.Options.UseBackColor = true;
            this.groupControl3.Controls.Add(this.lblTitle);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1522, 40);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(2, 2);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(109, 28);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "Form Title";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1495, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 36);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel3);
            this.pnBottom.Controls.Add(this.panel2);
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 639);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1522, 120);
            this.pnBottom.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtComment);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1399, 93);
            this.panel3.TabIndex = 35;
            // 
            // txtComment
            // 
            this.txtComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComment.Location = new System.Drawing.Point(0, 0);
            this.txtComment.Name = "txtComment";
            this.txtComment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtComment.Properties.Appearance.Options.UseFont = true;
            this.txtComment.Size = new System.Drawing.Size(1399, 93);
            this.txtComment.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupControl2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1399, 27);
            this.panel2.TabIndex = 34;
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.LookAndFeel.SkinMaskColor = System.Drawing.Color.LightSteelBlue;
            this.groupControl2.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.ShowCaption = false;
            this.groupControl2.Size = new System.Drawing.Size(1399, 27);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "groupControl2";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(5, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "ข้อเสนอแนะ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btSave);
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1399, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(123, 120);
            this.panel1.TabIndex = 33;
            // 
            // btSave
            // 
            this.btSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.btSave.Appearance.Options.UseFont = true;
            this.btSave.Appearance.Options.UseForeColor = true;
            this.btSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btSave.Location = new System.Drawing.Point(10, 6);
            this.btSave.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.btSave.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btSave.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btSave.Name = "btSave";
            this.btSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btSave.Size = new System.Drawing.Size(100, 28);
            this.btSave.TabIndex = 31;
            this.btSave.Text = "Save";
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.Location = new System.Drawing.Point(10, 42);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(100, 28);
            this.btClose.TabIndex = 30;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnRating
            // 
            this.pnRating.BackColor = System.Drawing.Color.White;
            this.pnRating.Controls.Add(this.groupControl1);
            this.pnRating.Controls.Add(this.txtRemark);
            this.pnRating.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnRating.Location = new System.Drawing.Point(0, 40);
            this.pnRating.Name = "pnRating";
            this.pnRating.Size = new System.Drawing.Size(1522, 82);
            this.pnRating.TabIndex = 2;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinMaskColor = System.Drawing.Color.LightSteelBlue;
            this.groupControl1.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1522, 35);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "groupControl1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Competency Rating";
            // 
            // txtRemark
            // 
            this.txtRemark.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtRemark.Location = new System.Drawing.Point(0, 35);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtRemark.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtRemark.Properties.Appearance.Options.UseBackColor = true;
            this.txtRemark.Properties.Appearance.Options.UseFont = true;
            this.txtRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRemark.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.txtRemark.Properties.ReadOnly = true;
            this.txtRemark.Size = new System.Drawing.Size(1522, 47);
            this.txtRemark.TabIndex = 7;
            // 
            // pScore
            // 
            this.pScore.Controls.Add(this.grdAssessment);
            this.pScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pScore.Location = new System.Drawing.Point(0, 122);
            this.pScore.Name = "pScore";
            this.pScore.Size = new System.Drawing.Size(1522, 517);
            this.pScore.TabIndex = 1;
            // 
            // grdAssessment
            // 
            this.grdAssessment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAssessment.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdAssessment.Location = new System.Drawing.Point(0, 0);
            this.grdAssessment.MainView = this.grdViewAssessment;
            this.grdAssessment.Name = "grdAssessment";
            this.grdAssessment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.grdAssessment.Size = new System.Drawing.Size(1522, 517);
            this.grdAssessment.TabIndex = 0;
            this.grdAssessment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewAssessment}); 
            // 
            // grdViewAssessment
            // 
            this.grdViewAssessment.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdViewAssessment.Appearance.FocusedCell.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grdViewAssessment.Appearance.FocusedCell.Options.UseFont = true;
            this.grdViewAssessment.Appearance.FocusedCell.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.FocusedCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.FocusedCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.FooterPanel.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.GroupFooter.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.GroupFooter.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grdViewAssessment.Appearance.GroupFooter.Options.UseFont = true;
            this.grdViewAssessment.Appearance.GroupFooter.Options.UseForeColor = true;
            this.grdViewAssessment.Appearance.GroupRow.BackColor = System.Drawing.Color.LemonChiffon;
            this.grdViewAssessment.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.GroupRow.Options.UseBackColor = true;
            this.grdViewAssessment.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewAssessment.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewAssessment.Appearance.Row.Options.UseFont = true;
            this.grdViewAssessment.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.ColumnPanelRowHeight = 25;
            this.grdViewAssessment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameCompetency,
            this.colRemark,
            this.colCompetencyGroupUID,
            this.colNameGroup,
            this.colDescriptionGroup,
            this.colCompetencyUID,
            this.colRowNo,
            this.colCDescription,
            this.colMinScore,
            this.colMaxScore,
            this.colComment,
            this.colWeight,
            this.colEvaluationUID,
            this.colDesc1,
            this.colDesc2,
            this.colDesc3,
            this.colDesc4,
            this.colScore1,
            this.colNetScore1,
            this.colScore2,
            this.colNetScore2});
            this.grdViewAssessment.GridControl = this.grdAssessment;
            this.grdViewAssessment.GroupCount = 1;
            this.grdViewAssessment.GroupFormat = "{0} [#image]{1} {2}";
            this.grdViewAssessment.GroupRowHeight = 50;
            this.grdViewAssessment.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "", this.colCDescription, "รวม"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score1", this.colScore1, "{0:0.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "NetScore1", this.colNetScore1, "{0:0.##}", 1),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score2", this.colScore2, "{0:0.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "NetScore2", this.colNetScore2, "{0:0.##}", "2")});
            this.grdViewAssessment.Name = "grdViewAssessment";
            this.grdViewAssessment.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowGroupExpandAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AutoExpandAllGroups = true;
            this.grdViewAssessment.OptionsFilter.AllowColumnMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterEditor = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.grdViewAssessment.OptionsFilter.AllowMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewAssessment.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewAssessment.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.grdViewAssessment.OptionsView.RowAutoHeight = true;
            this.grdViewAssessment.OptionsView.ShowFooter = true;
            this.grdViewAssessment.OptionsView.ShowGroupPanel = false;
            this.grdViewAssessment.OptionsView.ShowIndicator = false;
            this.grdViewAssessment.RowHeight = 25;
            this.grdViewAssessment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNameGroup, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.grdViewAssessment.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.grdViewAssessment_CustomDrawGroupRow);
            this.grdViewAssessment.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewAssessment_RowStyle);
            this.grdViewAssessment.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.grdViewAssessment_CustomSummaryCalculate);
            this.grdViewAssessment.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdViewAssessment_FocusedRowChanged);
            this.grdViewAssessment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdViewAssessment_KeyDown);
            this.grdViewAssessment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grdViewAssessment_KeyPress);
            this.grdViewAssessment.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.CellValidate);
            // 
            // colNameCompetency
            // 
            this.colNameCompetency.Caption = "CompetencyTypeName";
            this.colNameCompetency.FieldName = "CompetencyTypeName";
            this.colNameCompetency.Name = "colNameCompetency";
            this.colNameCompetency.OptionsColumn.AllowEdit = false;
            this.colNameCompetency.OptionsColumn.AllowFocus = false;
            this.colNameCompetency.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score", "{0:0.##}")});
            // 
            // colRemark
            // 
            this.colRemark.Caption = "Remark";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.OptionsColumn.AllowEdit = false;
            this.colRemark.OptionsColumn.AllowFocus = false;
            // 
            // colCompetencyGroupUID
            // 
            this.colCompetencyGroupUID.Caption = "CompetencyGroupUID";
            this.colCompetencyGroupUID.FieldName = "CompetencyGroupUID";
            this.colCompetencyGroupUID.Name = "colCompetencyGroupUID";
            this.colCompetencyGroupUID.OptionsColumn.AllowEdit = false;
            // 
            // colNameGroup
            // 
            this.colNameGroup.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.colNameGroup.AppearanceCell.Options.UseFont = true;
            this.colNameGroup.AppearanceCell.Options.UseTextOptions = true;
            this.colNameGroup.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameGroup.Caption = " ";
            this.colNameGroup.FieldName = "CompetencyGroupName";
            this.colNameGroup.FieldNameSortGroup = "CompetencyGroupUID";
            this.colNameGroup.Name = "colNameGroup";
            this.colNameGroup.OptionsColumn.AllowEdit = false;
            this.colNameGroup.OptionsColumn.AllowFocus = false;
            this.colNameGroup.Visible = true;
            this.colNameGroup.VisibleIndex = 0;
            // 
            // colDescriptionGroup
            // 
            this.colDescriptionGroup.AppearanceCell.BackColor = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceCell.BackColor2 = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceCell.Options.UseBackColor = true;
            this.colDescriptionGroup.AppearanceHeader.BackColor = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceHeader.BackColor2 = System.Drawing.Color.Gainsboro;
            this.colDescriptionGroup.AppearanceHeader.Options.UseBackColor = true;
            this.colDescriptionGroup.Caption = " ";
            this.colDescriptionGroup.FieldName = "DescriptionGroup";
            this.colDescriptionGroup.Name = "colDescriptionGroup";
            this.colDescriptionGroup.OptionsColumn.AllowEdit = false;
            this.colDescriptionGroup.OptionsColumn.AllowFocus = false;
            this.colDescriptionGroup.OptionsColumn.AllowShowHide = false;
            // 
            // colCompetencyUID
            // 
            this.colCompetencyUID.Caption = "CompetencyUID";
            this.colCompetencyUID.FieldName = "CompetencyUID";
            this.colCompetencyUID.Name = "colCompetencyUID";
            this.colCompetencyUID.OptionsColumn.AllowEdit = false;
            // 
            // colRowNo
            // 
            this.colRowNo.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNo.Caption = " ";
            this.colRowNo.FieldName = "RowNo";
            this.colRowNo.FieldNameSortGroup = "nRow";
            this.colRowNo.Name = "colRowNo";
            this.colRowNo.OptionsColumn.AllowEdit = false;
            this.colRowNo.OptionsColumn.AllowFocus = false;
            this.colRowNo.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRowNo.OptionsFilter.AllowAutoFilter = false;
            this.colRowNo.OptionsFilter.AllowFilter = false;
            this.colRowNo.Width = 73;
            // 
            // colCDescription
            // 
            this.colCDescription.AppearanceCell.Options.UseTextOptions = true;
            this.colCDescription.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colCDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCDescription.Caption = " ";
            this.colCDescription.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colCDescription.FieldName = "CDescription";
            this.colCDescription.Name = "colCDescription";
            this.colCDescription.OptionsColumn.AllowEdit = false;
            this.colCDescription.OptionsColumn.AllowFocus = false;
            this.colCDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCDescription.OptionsFilter.AllowAutoFilter = false;
            this.colCDescription.OptionsFilter.AllowFilter = false;
            this.colCDescription.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "CDescription", "คะแนนรวม ")});
            this.colCDescription.Visible = true;
            this.colCDescription.VisibleIndex = 0;
            this.colCDescription.Width = 350;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colMinScore
            // 
            this.colMinScore.Caption = "MinScore";
            this.colMinScore.FieldName = "MinScore";
            this.colMinScore.Name = "colMinScore";
            // 
            // colMaxScore
            // 
            this.colMaxScore.Caption = "MaxScore";
            this.colMaxScore.FieldName = "MaxScore";
            this.colMaxScore.Name = "colMaxScore";
            // 
            // colComment
            // 
            this.colComment.AppearanceCell.Options.UseTextOptions = true;
            this.colComment.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComment.Caption = "Comment";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            // 
            // colWeight
            // 
            this.colWeight.AppearanceCell.Options.UseTextOptions = true;
            this.colWeight.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeight.Caption = "ค่าน้ำหนัก";
            this.colWeight.FieldName = "WeightScore";
            this.colWeight.Name = "colWeight";
            // 
            // colEvaluationUID
            // 
            this.colEvaluationUID.FieldName = "EvaluationUID";
            this.colEvaluationUID.Name = "colEvaluationUID";
            // 
            // colDesc1
            // 
            this.colDesc1.AppearanceCell.Options.UseTextOptions = true;
            this.colDesc1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDesc1.Caption = "ดีเยี่ยม  ( 90 – 100 )";
            this.colDesc1.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colDesc1.FieldName = "Desc1";
            this.colDesc1.Name = "colDesc1";
            this.colDesc1.OptionsColumn.AllowEdit = false;
            this.colDesc1.OptionsColumn.AllowFocus = false;
            this.colDesc1.Visible = true;
            this.colDesc1.VisibleIndex = 1;
            this.colDesc1.Width = 191;
            // 
            // colDesc2
            // 
            this.colDesc2.AppearanceCell.Options.UseTextOptions = true;
            this.colDesc2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDesc2.Caption = "ดีมาก (70-89)";
            this.colDesc2.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colDesc2.FieldName = "Desc2";
            this.colDesc2.Name = "colDesc2";
            this.colDesc2.OptionsColumn.AllowEdit = false;
            this.colDesc2.OptionsColumn.AllowFocus = false;
            this.colDesc2.Visible = true;
            this.colDesc2.VisibleIndex = 2;
            this.colDesc2.Width = 191;
            // 
            // colDesc3
            // 
            this.colDesc3.AppearanceCell.Options.UseTextOptions = true;
            this.colDesc3.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDesc3.Caption = "ดี (50-69)";
            this.colDesc3.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colDesc3.FieldName = "Desc3";
            this.colDesc3.Name = "colDesc3";
            this.colDesc3.OptionsColumn.AllowEdit = false;
            this.colDesc3.OptionsColumn.AllowFocus = false;
            this.colDesc3.Visible = true;
            this.colDesc3.VisibleIndex = 3;
            this.colDesc3.Width = 191;
            // 
            // colDesc4
            // 
            this.colDesc4.AppearanceCell.Options.UseTextOptions = true;
            this.colDesc4.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDesc4.Caption = "ต้องปรับปรุง (0-49 )";
            this.colDesc4.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colDesc4.FieldName = "Desc4";
            this.colDesc4.Name = "colDesc4";
            this.colDesc4.OptionsColumn.AllowEdit = false;
            this.colDesc4.OptionsColumn.AllowFocus = false;
            this.colDesc4.Visible = true;
            this.colDesc4.VisibleIndex = 4;
            this.colDesc4.Width = 191;
            // 
            // colScore1
            // 
            this.colScore1.AppearanceCell.BackColor = System.Drawing.Color.Honeydew;
            this.colScore1.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Bold);
            this.colScore1.AppearanceCell.Options.UseBackColor = true;
            this.colScore1.AppearanceCell.Options.UseFont = true;
            this.colScore1.AppearanceCell.Options.UseTextOptions = true;
            this.colScore1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colScore1.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colScore1.AppearanceHeader.Options.UseTextOptions = true;
            this.colScore1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore1.Caption = "ครั้งที่ 1";
            this.colScore1.FieldName = "Score1";
            this.colScore1.Name = "colScore1";
            this.colScore1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colScore1.OptionsFilter.AllowAutoFilter = false;
            this.colScore1.OptionsFilter.AllowFilter = false;
            this.colScore1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score1", "{0:0.##}")});
            this.colScore1.Visible = true;
            this.colScore1.VisibleIndex = 5;
            this.colScore1.Width = 73;
            // 
            // colNetScore1
            // 
            this.colNetScore1.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colNetScore1.AppearanceCell.Options.UseFont = true;
            this.colNetScore1.AppearanceCell.Options.UseTextOptions = true;
            this.colNetScore1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNetScore1.Caption = "คะแนน (1)";
            this.colNetScore1.FieldName = "NetScore1";
            this.colNetScore1.Name = "colNetScore1";
            this.colNetScore1.OptionsColumn.AllowEdit = false;
            this.colNetScore1.OptionsColumn.AllowFocus = false;
            this.colNetScore1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "NetScore1", "{0:0.##}", 1)});
            this.colNetScore1.Visible = true;
            this.colNetScore1.VisibleIndex = 6;
            this.colNetScore1.Width = 85;
            // 
            // colScore2
            // 
            this.colScore2.AppearanceCell.BackColor = System.Drawing.Color.Honeydew;
            this.colScore2.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Bold);
            this.colScore2.AppearanceCell.Options.UseBackColor = true;
            this.colScore2.AppearanceCell.Options.UseFont = true;
            this.colScore2.AppearanceCell.Options.UseTextOptions = true;
            this.colScore2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colScore2.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colScore2.AppearanceHeader.Options.UseFont = true;
            this.colScore2.AppearanceHeader.Options.UseTextOptions = true;
            this.colScore2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore2.Caption = "ครั้งที่ 2";
            this.colScore2.FieldName = "Score2";
            this.colScore2.Name = "colScore2";
            this.colScore2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colScore2.OptionsFilter.AllowAutoFilter = false;
            this.colScore2.OptionsFilter.AllowFilter = false;
            this.colScore2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Score2", "{0:0.##}")});
            this.colScore2.Visible = true;
            this.colScore2.VisibleIndex = 7;
            this.colScore2.Width = 82;
            // 
            // colNetScore2
            // 
            this.colNetScore2.Caption = "คะแนน (2)";
            this.colNetScore2.FieldName = "NetScore2";
            this.colNetScore2.Name = "colNetScore2";
            this.colNetScore2.OptionsColumn.AllowEdit = false;
            this.colNetScore2.OptionsColumn.AllowFocus = false;
            this.colNetScore2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "NetScore2", "{0:0.##}", "2")});
            this.colNetScore2.Visible = true;
            this.colNetScore2.VisibleIndex = 8;
            this.colNetScore2.Width = 93;
            // 
            // frmAssessment2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1522, 759);
            this.Controls.Add(this.pScore);
            this.Controls.Add(this.pnRating);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAssessment2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แบบประเมินคะแนน";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmAssessment2_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtComment.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pnRating.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemark.Properties)).EndInit();
            this.pScore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private System.Windows.Forms.Panel pnRating;
        private DevExpress.XtraGrid.GridControl grdAssessment;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn colNameCompetency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyGroupUID;
        private DevExpress.XtraGrid.Columns.GridColumn colNameGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyUID;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNo;
        private DevExpress.XtraGrid.Columns.GridColumn colCDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMinScore;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxScore;
        private DevExpress.XtraGrid.Columns.GridColumn colScore1;
        private System.Windows.Forms.Panel pScore;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.MemoEdit txtComment;
        private DevExpress.XtraEditors.SimpleButton btSave;
        private DevExpress.XtraEditors.MemoEdit txtRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colNetScore1;
        private DevExpress.XtraGrid.Columns.GridColumn colEvaluationUID;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraGrid.Columns.GridColumn colScore2;
        private DevExpress.XtraGrid.Columns.GridColumn colDesc1;
        private DevExpress.XtraGrid.Columns.GridColumn colDesc2;
        private DevExpress.XtraGrid.Columns.GridColumn colDesc3;
        private DevExpress.XtraGrid.Columns.GridColumn colDesc4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colNetScore2;
    }
}