﻿namespace SUTHPA
{
    partial class frmCompetencyEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompetencyEmployee));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label5 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new System.Windows.Forms.Panel();
            this.grdEmployee = new DevExpress.XtraGrid.GridControl();
            this.grdViewEmployee = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkAssessment = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssessment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnAssessment = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCompetencyLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAssessment)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(584, 35);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(584, 35);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(5, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(185, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Competency ที่ต้องประเมิน";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(557, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 31);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 271);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(584, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(388, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(196, 40);
            this.panel1.TabIndex = 33;
            // 
            // btClose
            // 
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.Location = new System.Drawing.Point(90, 3);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(97, 32);
            this.btClose.TabIndex = 30;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // pnMain
            // 
            this.pnMain.BackColor = System.Drawing.Color.White;
            this.pnMain.Controls.Add(this.grdEmployee);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 35);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(584, 236);
            this.pnMain.TabIndex = 2;
            // 
            // grdEmployee
            // 
            this.grdEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdEmployee.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.grdEmployee.Location = new System.Drawing.Point(0, 0);
            this.grdEmployee.LookAndFeel.SkinName = "Office 2010 Blue";
            this.grdEmployee.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdEmployee.MainView = this.grdViewEmployee;
            this.grdEmployee.Name = "grdEmployee";
            this.grdEmployee.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnAssessment,
            this.chkAssessment});
            this.grdEmployee.Size = new System.Drawing.Size(584, 236);
            this.grdEmployee.TabIndex = 5;
            this.grdEmployee.TabStop = false;
            this.grdEmployee.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewEmployee});
            // 
            // grdViewEmployee
            // 
            this.grdViewEmployee.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdViewEmployee.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.grdViewEmployee.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewEmployee.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewEmployee.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewEmployee.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grdViewEmployee.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewEmployee.Appearance.Row.Options.UseFont = true;
            this.grdViewEmployee.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdViewEmployee.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grdViewEmployee.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn4,
            this.gridColumn2,
            this.colStatus,
            this.colScore,
            this.colAssessment,
            this.colCompetencyLevel});
            this.grdViewEmployee.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewEmployee.GridControl = this.grdEmployee;
            this.grdViewEmployee.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.grdViewEmployee.Name = "grdViewEmployee";
            this.grdViewEmployee.OptionsBehavior.AutoExpandAllGroups = true;
            this.grdViewEmployee.OptionsBehavior.Editable = false;
            this.grdViewEmployee.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewEmployee.OptionsView.ColumnAutoWidth = false;
            this.grdViewEmployee.OptionsView.EnableAppearanceEvenRow = true;
            this.grdViewEmployee.OptionsView.EnableAppearanceOddRow = true;
            this.grdViewEmployee.OptionsView.ShowGroupPanel = false;
            this.grdViewEmployee.OptionsView.ShowIndicator = false;
            this.grdViewEmployee.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.LiveVertScroll;
            this.grdViewEmployee.MouseDown += new System.Windows.Forms.MouseEventHandler(this.grdViewEmployee_MouseDown);
            this.grdViewEmployee.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grdViewEmployee_MouseMove);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "UID";
            this.gridColumn8.FieldName = "UID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Width = 50;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "ลำดับ";
            this.gridColumn4.FieldName = "nRow";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 45;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "หัวข้อเรื่อง";
            this.gridColumn2.FieldName = "NameCompetency";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 400;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "สถานะ";
            this.colStatus.ColumnEdit = this.chkAssessment;
            this.colStatus.FieldName = "isAssessment";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 2;
            this.colStatus.Width = 50;
            // 
            // chkAssessment
            // 
            this.chkAssessment.AutoHeight = false;
            this.chkAssessment.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkAssessment.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("chkAssessment.ImageOptions.ImageChecked")));
            this.chkAssessment.Name = "chkAssessment";
            this.chkAssessment.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkAssessment.ValueChecked = "Y";
            this.chkAssessment.ValueUnchecked = "N";
            // 
            // colScore
            // 
            this.colScore.AppearanceCell.Options.UseTextOptions = true;
            this.colScore.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore.AppearanceHeader.Options.UseTextOptions = true;
            this.colScore.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScore.Caption = "คะแนน";
            this.colScore.FieldName = "NetScore";
            this.colScore.Name = "colScore";
            this.colScore.Visible = true;
            this.colScore.VisibleIndex = 3;
            this.colScore.Width = 60;
            // 
            // colAssessment
            // 
            this.colAssessment.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.colAssessment.AppearanceCell.Options.UseBackColor = true;
            this.colAssessment.Caption = "ทำการประมิน";
            this.colAssessment.ColumnEdit = this.btnAssessment;
            this.colAssessment.FieldName = "Assm";
            this.colAssessment.Name = "colAssessment";
            this.colAssessment.OptionsColumn.ShowCaption = false;
            this.colAssessment.Visible = true;
            this.colAssessment.VisibleIndex = 4;
            this.colAssessment.Width = 30;
            // 
            // btnAssessment
            // 
            this.btnAssessment.AutoHeight = false;
            editorButtonImageOptions1.Image = global::SUTHPA.Properties.Resources.edit;
            this.btnAssessment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "คลิกเพื่อให้คะแนนประเมิน", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnAssessment.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnAssessment.Name = "btnAssessment";
            this.btnAssessment.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colCompetencyLevel
            // 
            this.colCompetencyLevel.Caption = "gridColumn1";
            this.colCompetencyLevel.FieldName = "CompetencyLevel";
            this.colCompetencyLevel.Name = "colCompetencyLevel";
            // 
            // frmCompetencyEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 311);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "frmCompetencyEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Competency";
            this.Load += new System.EventHandler(this.frmCompetencyEmployee_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAssessment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private System.Windows.Forms.Panel pnMain;
        private DevExpress.XtraGrid.GridControl grdEmployee;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewEmployee;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnAssessment;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn colScore;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyLevel;
    }
}