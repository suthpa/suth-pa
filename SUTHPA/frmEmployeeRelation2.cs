﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmEmployeeRelation2 : DevExpress.XtraEditors.XtraForm
    {
        public frmEmployeeRelation2()
        {
            InitializeComponent();
        }
        DataTable dtAssessor = new DataTable();
        DataTable dtAssessee = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();
        int RNDNO;
        private void frmEmployeeRelation2_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            GlobalVariables.FlagRefresh = false;
            ConfigurationController ctlCf = new ConfigurationController();
            RNDNO = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO));
            //dtAssessee.Columns.Clear();

            dtAssessee.Columns.Add("EmployeeID");
            dtAssessee.Columns.Add("EmployeeName");
            dtAssessee.Columns.Add("PositionName");
            dtAssessee.Columns.Add("DepartmentName");
            dtAssessee.Columns.Add("DivisionName");
            dtAssessee.Columns.Add("EmployeeLevelID");
            dtAssessee.Columns.Add("DepartmentUID");
            dtAssessee.Columns.Add("DivisionUID");
            dtAssessee.Columns.Add("LevelName");
            dtAssessee.Columns.Add("GroupName");
            dtAssessee.Columns.Add("isAsm");
            dtAssessee.Columns.Add("AsmRemark");
            dtAssessee.Columns.Add("NameCompetency");

            LoadRelation();
        }

        private bool OpenAssesseeListForm()
        {

            bool AppAlive;
            AppAlive = true;

            int Bdate, Edate, sToday;
            ConfigurationController ctlCf = new ConfigurationController();

            Bdate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_STARTDATE));
            Edate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_ENDDATE));
            sToday = Convert.ToInt32(BaseClass.ConvertStrDate2YYYYMMDDString(ctlCf.GET_DATE_SERVER().ToString()));
            if (sToday >= Bdate && sToday <= Edate)
            {
                AppAlive = true;
            }
            else
            {
                EmployeeController ctlU = new EmployeeController();

                if (ctlU.User_GetAllowPermission(GlobalVariables.username))
                {
                    AppAlive = true;
                }
                else
                {

                    if (sToday < Bdate)
                    {
                        MessageBox.Show("ยังไม่ถึงกำหนดเวลาประเมิน ท่านสามารถเริ่มทำแบบประเมินได้ตั้งแต่วันที่ 12 - 23 กรกฎาคม 2564","Notice",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                        AppAlive=false;
                    }

                    if (sToday > Edate)
                    {
                        if (!GlobalVariables.IsAdmin)
                        {
                            MessageBox.Show("หมดเขตระยะเวลาที่ให้ประเมินแล้ว", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        AppAlive=false;
                    }
                }

            }

            //if (GlobalVariables.IsSuperAdmin)
            //{
            //    AppAlive = true;
            //}
            
            return AppAlive;
        }



        private void LoadRelation()
        {

            //dtAssessor = ctlEmp.Employee_GetAssessorStatus(GlobalVariables.username);

            //if (string.Concat(dtAssessor.Rows[0][0])=="N")
            //{
            //    return;
            //}

            int i,n;
            DataRow row;
            DataRow rowA;
            DataRow dr;
            DataTable dtA = new DataTable();
            dtAssessee.Rows.Clear();
            
            dtAssessor = ctlEmp.Employee_GetAssessorDetail4Relation(GlobalVariables.username);           

            //ถ้าประเมิน level เดียว ให้เปิด for
            for (i = 0; i < dtAssessor.Rows.Count; i++)
            {
                row = dtAssessor.Rows[i];
             
                //กรณีประเมิน หลาย level ปี  2021------------------------------
                switch (row["EmployeeLevelID"].ToString())
                {
                    case "0": //ผอ.
                        dtA = ctlEmp.Employee_GetRelationByTOP(GlobalVariables.BYear, GlobalVariables.username, 0, RNDNO);
                        break;
                    case "1": //หน.กลุ่มงาน
                        dtA = ctlEmp.Employee_GetRelationByDirector(GlobalVariables.BYear, GlobalVariables.username,1, RNDNO);
                        break;
                    case "2": //หัวหน้าฝ่าย
                        dtA = ctlEmp.Employee_GetRelationByManager(GlobalVariables.BYear, GlobalVariables.username, 2, RNDNO);
                        break;
                    case "3": //หัวหน้าแผนก
                        dtA = ctlEmp.Employee_GetRelationByHeader(GlobalVariables.BYear, GlobalVariables.username,3, RNDNO);
                        break;
                    case "4": //รองหัวหน้าแผนก
                        dtA = ctlEmp.Employee_GetRelationBySubHeader(GlobalVariables.BYear, GlobalVariables.username,4, RNDNO);
                        break;
                    default: //ระดับปฏิบัติการ
                        dtA = ctlEmp.Employee_GetRelationByAssessor(GlobalVariables.BYear, GlobalVariables.username,Convert.ToInt32(row["EmployeeLevelID"].ToString()), RNDNO );
                        break;
                }
                //-----------End 2021---------------------------

                
                // dtA = ctlEmp.Employee_GetRelation(row["LevelAssessee"].ToString(), iDiv, iDept, GlobalVariables.username);

                if (dtA.Rows.Count>0)
                {
                    for (n = 0; n < dtA.Rows.Count; n++)
                    {
                        rowA = dtA.Rows[n];
                        dr = dtAssessee.NewRow();
                        dr[0] = rowA["EmployeeID"];
                        dr[1] = rowA["EmployeeName"];
                        dr[2] = rowA["PositionName"];
                        dr[3] = rowA["DepartmentName"];
                        dr[4] = rowA["DivisionName"];
                        dr[5] = rowA["EmployeeLevelID"];
                        dr[6] = rowA["DepartmentUID"];
                        dr[7] = rowA["DivisionUID"];
                        dr[8] = rowA["LevelName"];
                        dr[9] = rowA["GroupName"];
                        dr[10] = rowA["isAsm"];
                        dr[11] = rowA["AsmRemark"];
                        dr[12] = rowA["NameCompetency"];
                        dtAssessee.Rows.Add(dr);
                    }              

                }
            }

            //ctlEmp.xAssessorAssessment_Delete(GlobalVariables.BYear, GlobalVariables.username);

            //int k;
            //for (k = 0; k < dtAssessee.Rows.Count; k++)
            //{
            //    ctlEmp.xAssessorAssessment_Add(GlobalVariables.BYear, GlobalVariables.username, dtAssessee.Rows[k].Field<string>("EmployeeID"));
            //}

            //if (dtAssessee.Rows.Count > 0)
            //{
            //    dtAssessee = dtAssessee.AsEnumerable().GroupBy(r => new { Col1 = r["GroupName"] }).Select(g => g.First()).CopyToDataTable();
            //}
            grdEmployee.DataSource = null;
            grdEmployee.DataSource = dtAssessee;


            lblAssessor.Text = "รายการแบบประเมินที่ยังไม่สมบูรณ์ ที่ต้องถูกประเมิน โดย " + GlobalVariables.UserEmployeeName + " ทั้งหมด " + dtAssessee.Rows.Count.ToString() + " รายการ";
            //+ GlobalVariables.UserEmployeeLevelName + ")";
            
            grdViewEmployee.Columns[2].AppearanceCell.ForeColor = Color.Blue;

            grdViewEmployee.BestFitColumns();

            grdViewEmployee.Columns[2].Width = 280;
            grdViewEmployee.Columns[3].Width = 320;
            grdViewEmployee.Columns[4].Width = 320;
            grdViewEmployee.Columns[5].Width = 320;
            grdViewEmployee.Columns[6].Width = 420;
            grdViewEmployee.Columns[7].Width = 150;

        }

       
        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}