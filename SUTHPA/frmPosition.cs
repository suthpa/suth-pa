﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
   
    public partial class frmPosition : DevExpress.XtraEditors.XtraForm
    {
        public frmPosition()
        {
            InitializeComponent();
        }

        DataTable dtM = new DataTable();
        MasterController ctlM = new MasterController();
        private void frmPosition_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            LoadLevel();               
            LoadData();
            if (!GlobalVariables.IsAdmin && !GlobalVariables.IsSuperAdmin && !GlobalVariables.IsHR)
            {
                cmdSave.Enabled = false;
                cmdDel.Enabled = false;
                cmdCancel.Enabled = false;
            }
        }

        private void LoadLevel()
        {
           
            ddlLevel.Properties.Columns.Clear();
            ddlLevel.Properties.DataSource = ctlM.Level_Get();
            ddlLevel.Properties.DisplayMember = "LevelName";
            ddlLevel.Properties.ValueMember = "LevelID";

            ddlLevel.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlLevel.Properties.DropDownRows = 10;
            ddlLevel.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LevelName", 300, "");
            ddlLevel.Properties.Columns.Add(col3);
            ddlLevel.EditValue = 0;

            ddlLevel.Width = 250;
                               
        }

      

        private void LoadData()
        {

            try
            {
                dtM = ctlM.Position_Get(); //GlobalVariables.BYear
                grdData.DataSource = dtM;
                //grdViewEmployee.BestFitColumns();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }
              

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {        
            EditData(grdViewData.GetFocusedRowCellValue("UID").ToString());
        }

        private void EditData(string pID)
        {
            DataTable dtE = new DataTable();
 
            dtE = ctlM.Position_GetByUID(Convert.ToInt32(pID));
            if (dtE.Rows.Count > 0)
            {

                lblUID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<int>("UID"));       
                txtName.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Name"));                 
                ddlLevel.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("LevelUID"));
                chkStatus.Checked = GlobalFunctions.ConvertStatusActive2CheckBox(dtE.Rows[0].Field<string>("StatusFlag"));
            }
            dtE = null;
        }
        private void ClearData()
        {
           lblUID.Text ="0";               
           txtName.Text ="";             
           ddlLevel.Text =""; 
           chkStatus.Checked = true;
        }                   
       
        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
               
              

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                ctlM.Position_Delete(Convert.ToInt32(lblUID.Text));
                MessageBox.Show("ลบข้อมูลเรียบร้อย");
                ClearData();
            }            
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            ctlM.Position_Save(Convert.ToInt32(lblUID.Text) ,txtName.Text,Convert.ToInt32(ddlLevel.EditValue), GlobalFunctions.ConvertCheckBox2StatusActive(chkStatus.Checked));
            MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
            ClearData();
            LoadData();

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }
    }
}