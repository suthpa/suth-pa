﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUTHPA
{
    public partial class frmReportViewer 
    {
        public frmReportViewer()
        {
            InitializeComponent();
        }

        public string BYear;
        public string LevelUID;
        public string DeptUID;
        public string DivisionUID;
        public string DirectionUID;
        public string EmployeeID;


        ConfigurationController ctlCf = new ConfigurationController();
        private void frmReportViewer_Load(object sender, EventArgs e)
        {
            lblResult.Visible = false;
            this.reportViewer1.RefreshReport();
            this.Text = GlobalVariables.ReportName;
            LoadReport();
         
            
        }
        
        private void LoadReport()
        {
            //System.Threading.Thread.Sleep(1000)
            //UpdateProgress1.Visible = True
             
            string strHostName = System.Environment.MachineName;
            
            reportViewer1.Reset();
            reportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote;
            reportViewer1.SetDisplayMode(DisplayMode.Normal);
            reportViewer1.ZoomMode = ZoomMode.PageWidth;

            ReportServerCredentials credential = new ReportServerCredentials();
            reportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl();

            string reportName;

            switch (GlobalVariables.FagRPT)
            {
                case "SCORE":
                    reportName="EmployeeAssessment2";
                    break;
                case "FINALEX":
                    reportName = "EmployeeAssessmentFinalScoreEx";
                    break;

                case "NEWFORM":
                    reportName = "NewEmployeeAssessmentForm";
                    reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    break;

                //case "AVG":
                //    reportName = "EmployeeAssessmentFinalScore";
                //    break;
                //case "FINALEX":
                //    reportName = "EmployeeAssessmentFinalScoreEx";
                //    break;
                case "FINAL":
                    reportName = "EmployeeAssessmentFinal";
                    reportViewer1.SetDisplayMode(DisplayMode.Normal);
                    break;
                case "DIRSCORE":
                    reportName = "EmployeeAssessmentByDirection";
                    reportViewer1.ZoomMode = ZoomMode.Percent;
                    reportViewer1.ZoomPercent = 100;
                    break;
                case "DIRFINAL":
                    reportName = "EmployeeAssessmentFinalForDirection";
                    break;

                case "MANSCORE":
                    reportName = "EmployeeAssessmentByAssessor";
                    reportViewer1.SetDisplayMode(DisplayMode.Normal);
                    break;
                case "MANFINAL":
                    reportName = "EmployeeAssessmentFinalForManager";
                    reportViewer1.SetDisplayMode(DisplayMode.Normal);
                    break;
                //case "SUPERFINAL":
                //    reportName = "EmployeeAssessmentFinalScoreEx";
                //    break;
                //case "EMPWEIGHT":
                //    reportName = "EmployeeAssessmentScoreWeight";
                //    break;
                case "SUBSTANDARD":
                    reportName = "AssessmentScoreSubStandard";
                    break;
                case "OVERSTANDARD":
                    reportName = "AssessmentScoreOverStandard";
                    break;
                case "GRADE":
                    reportName = "AssessmentGrade";
                    break;

                case "COMMENT":
                    reportName = "EmployeeComment";
                    break;
                case "ASSNEW":
                    reportName = "EmployeeAssessment";
                    break; 
                case "SELFSCORE":
                    reportName = "SelfAssessmentScoreByItem";
                    break;
                case "COSCORE":
                    reportName = "CoAssessmentScoreByItem";
                    break;
                case "ASMPRNT":
                    reportName = "PRT_EmployeeAssessmentSummary";
                    reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    break;

                default:
                    reportName = "";
                    break;                    

            }

            reportViewer1.ServerReport.ReportPath = credential.ReportPath(reportName);

            reportViewer1.ShowPrintButton = true;
            reportViewer1.ShowParameterPrompts = false;

            //'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
           
            ArrayList reportParam = new ArrayList();
            reportParam = ReportDefaultPatam();

            ReportParameter[] param = new ReportParameter[reportParam.Count];
            for (int k = 0; k < reportParam.Count; k++)
            {
                param[k] = (ReportParameter)reportParam[k];
            }
            
            reportViewer1.ServerReport.ReportServerCredentials.NetworkCredentials = credential.NetworkCredentials;
            reportViewer1.ServerReport.SetParameters(param);

            if (GlobalVariables.RPTTYPE == "XLS") 
            {
                string mimeType;
                string encoding;
                string extension;
                string[] streams;
                Warning[] warnings;
                byte[] pdfBytes = reportViewer1.ServerReport.Render("EXCEL", string.Empty, out mimeType,
                    out encoding, out extension, out streams, out warnings);

                // save the file
                //Convert.ToString("attachment; filename=" + BaseGlobalClase.ReportName + "_" + BaseClass.ConvertDateToString(DateTime.Now.Date) + "." + extension)
                string outputPath; // = "C:/Temp/ScheduleReport.xls";

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel WorkBook|*.xls";
                saveFileDialog1.Title = "Save an Excel File";
                saveFileDialog1.FileName = reportName;
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    outputPath = saveFileDialog1.FileName;

                    using (FileStream fs = new FileStream(outputPath, FileMode.Create))
                    {
                        fs.Write(pdfBytes, 0, pdfBytes.Length);
                        fs.Close();
                    }

                }

                lblResult.Visible = true;
                reportViewer1.Visible = false;
            }
            else
            {
                lblResult.Visible = false;
                reportViewer1.Visible = true;
                reportViewer1.RefreshReport();
                //reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                this.WindowState = FormWindowState.Maximized;
            }

            if (GlobalVariables.FagRPT=="COMMENT")
            {
                reportViewer1.SetDisplayMode(DisplayMode.Normal);
            }
        }

        private ArrayList ReportDefaultPatam()
        {
            ArrayList arrLstDefaultParam = new ArrayList();

            switch (GlobalVariables.FagRPT)
            {
                case "SCORE":
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("EmployeeLevelID", this.LevelUID));
                    break;
                case "AVG": case "FINALEX":
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("DepartmentUID", this.DeptUID));
                    break;
                case "FINAL":
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("DirectionUID", this.DirectionUID));
                    arrLstDefaultParam.Add(CreateReportParameter("DivisionUID", this.DivisionUID));
                    arrLstDefaultParam.Add(CreateReportParameter("DepartmentUID", this.DeptUID));
                    break;
                case "MANSCORE":case "MANFINAL":case "SUPERFINAL":               
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("DivisionUID", this.DivisionUID));
                    arrLstDefaultParam.Add(CreateReportParameter("EmployeeID", GlobalVariables.username));
                    break;
                case "EMPWEIGHT":case "GRADE":case "SUBSTANDARD":case "OVERSTANDARD":
                    GlobalVariables.RPTTYPE = "RPT";
                    arrLstDefaultParam.Add(CreateReportParameter("BYear",this.BYear));
                    break;
                case "COMMENT":
                    GlobalVariables.RPTTYPE = "RPT";
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", GlobalVariables.BYear.ToString()));
                    break;
                //case "MANFINAL":
                case "ASSNEW":        
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("EmployeeID", this.EmployeeID));
                    break;

                case "DIRSCORE":
                case "DIRFINAL":
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("DirectionUID", this.DirectionUID));
                    arrLstDefaultParam.Add(CreateReportParameter("EmployeeID", GlobalVariables.username));
                    break;
                case "SELFSCORE":
                case "COSCORE":
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("EmployeeID", GlobalVariables.username));
                    GlobalVariables.RPTTYPE = "RPT";
                    break;
                case "ASMPRNT":
                    arrLstDefaultParam.Add(CreateReportParameter("BYear", this.BYear));
                    arrLstDefaultParam.Add(CreateReportParameter("RNDNO", ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO)));
                    arrLstDefaultParam.Add(CreateReportParameter("Username", GlobalVariables.username));
                    GlobalVariables.RPTTYPE = "RPT";
                    break;
                case "NEWFORM":
                    arrLstDefaultParam.Add(CreateReportParameter("EmployeeID", this.EmployeeID));
                    break;

                default:

                    break;

            }

           
            return arrLstDefaultParam;
        }
        private ReportParameter CreateReportParameter(string paramName, string pramValue)
        {
            ReportParameter aParam = new ReportParameter(paramName, pramValue);
            return aParam;
        }

    }
}
