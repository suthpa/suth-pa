﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmEmployeeRelation : DevExpress.XtraEditors.XtraForm
    {
        public frmEmployeeRelation()
        {
            InitializeComponent();
        }
        DataTable dtEmp = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();

        private void frmAssessmentStatus_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            GlobalVariables.FlagRefresh = false;
            LoadEmployee();
        }

        private void LoadEmployee()
        {
            dtEmp.Rows.Clear();
            dtEmp = ctlEmp.EmployeeRelationAssessment(GlobalVariables.BYear);
          
                if (dtEmp.Rows.Count>0)
                {

                //lblAssesor.Text = "พนักงาน " + dtEmp.Rows.Count.ToString() + " คน";
            grdEmployee.DataSource = null;
            grdEmployee.DataSource = dtEmp;
                grdViewEmployee.BestFitColumns();
 }
        }

     

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}