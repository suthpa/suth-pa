﻿namespace SUTHPA
{
    partial class frmAssessmentScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAssessmentScore));
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblTitlePA = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pScore = new System.Windows.Forms.Panel();
            this.grdAssessment = new DevExpress.XtraGrid.GridControl();
            this.grdViewAssessment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPositionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevelName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDivision = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoreComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuncComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeaderComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoreComTest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCQI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLean = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colICRTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colICR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscipline = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTraining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActivity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboYear = new DevExpress.XtraEditors.LookUpEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdConfirm = new DevExpress.XtraEditors.SimpleButton();
            this.cmdView = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboYear.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1540, 40);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblTitlePA);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.HotTrack;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1540, 40);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // lblTitlePA
            // 
            this.lblTitlePA.AutoSize = true;
            this.lblTitlePA.BackColor = System.Drawing.Color.Transparent;
            this.lblTitlePA.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold);
            this.lblTitlePA.ForeColor = System.Drawing.Color.White;
            this.lblTitlePA.Location = new System.Drawing.Point(10, 2);
            this.lblTitlePA.Name = "lblTitlePA";
            this.lblTitlePA.Size = new System.Drawing.Size(261, 28);
            this.lblTitlePA.TabIndex = 3;
            this.lblTitlePA.Text = "สรุปผลคะแนนประเมินประจำปี";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1508, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(30, 36);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.BackColor = System.Drawing.Color.White;
            this.pnBottom.Controls.Add(this.label3);
            this.pnBottom.Controls.Add(this.label2);
            this.pnBottom.Controls.Add(this.pictureBox1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 321);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1540, 448);
            this.pnBottom.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(0, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1540, 30);
            this.label3.TabIndex = 36;
            this.label3.Text = "เนื่องจากผู้บังคับบัญชายังประเมินไม่ครบทุกท่าน คะแนนประเมินจากผู้บังคับบัญชาจะแจ้" +
    "งให้ทราบในโอกาสต่อไป";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1540, 30);
            this.label2.TabIndex = 35;
            this.label2.Text = "คะแนนข้างต้นเป็นคะแนนเก็บรวม 35 คะแนนเท่านั้น ยังไม่รวมในส่วนของผู้บังคับบัญชาประ" +
    "เมินอีก 65 คะแนน";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1540, 386);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // pScore
            // 
            this.pScore.Controls.Add(this.grdAssessment);
            this.pScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pScore.Location = new System.Drawing.Point(0, 76);
            this.pScore.Name = "pScore";
            this.pScore.Size = new System.Drawing.Size(1540, 245);
            this.pScore.TabIndex = 1;
            // 
            // grdAssessment
            // 
            this.grdAssessment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAssessment.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdAssessment.Location = new System.Drawing.Point(0, 0);
            this.grdAssessment.MainView = this.grdViewAssessment;
            this.grdAssessment.Name = "grdAssessment";
            this.grdAssessment.Size = new System.Drawing.Size(1540, 245);
            this.grdAssessment.TabIndex = 0;
            this.grdAssessment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewAssessment});
            // 
            // grdViewAssessment
            // 
            this.grdViewAssessment.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdViewAssessment.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewAssessment.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.FooterPanel.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewAssessment.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewAssessment.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.grdViewAssessment.Appearance.Row.Options.UseFont = true;
            this.grdViewAssessment.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.ColumnPanelRowHeight = 25;
            this.grdViewAssessment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colYear,
            this.colEmployeeID,
            this.colEmployeeName,
            this.colPositionName,
            this.colLevelName,
            this.colDepartment,
            this.colDivision,
            this.colCoreComp,
            this.colFuncComp,
            this.colLeaderComp,
            this.colCoreComTest,
            this.colExR,
            this.colCQI,
            this.colLean,
            this.colICRTeam,
            this.colICR,
            this.colDiscipline,
            this.colTraining,
            this.colCheckup,
            this.colActivity,
            this.colTotalScore});
            this.grdViewAssessment.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewAssessment.GridControl = this.grdAssessment;
            this.grdViewAssessment.GroupFormat = "{0} [#image]{1} {2}";
            this.grdViewAssessment.Name = "grdViewAssessment";
            this.grdViewAssessment.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowGroupExpandAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsFilter.AllowColumnMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterEditor = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.grdViewAssessment.OptionsFilter.AllowMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewAssessment.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewAssessment.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewAssessment.OptionsView.EnableAppearanceEvenRow = true;
            this.grdViewAssessment.OptionsView.EnableAppearanceOddRow = true;
            this.grdViewAssessment.OptionsView.RowAutoHeight = true;
            this.grdViewAssessment.OptionsView.ShowGroupPanel = false;
            this.grdViewAssessment.OptionsView.ShowIndicator = false;
            this.grdViewAssessment.RowHeight = 25;
            // 
            // colYear
            // 
            this.colYear.AppearanceCell.Options.UseTextOptions = true;
            this.colYear.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYear.Caption = "ปี";
            this.colYear.FieldName = "BYear";
            this.colYear.Name = "colYear";
            this.colYear.OptionsColumn.AllowEdit = false;
            this.colYear.Visible = true;
            this.colYear.VisibleIndex = 0;
            this.colYear.Width = 60;
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "รหัสพนักงาน";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.AllowShowHide = false;
            this.colEmployeeID.Visible = true;
            this.colEmployeeID.VisibleIndex = 1;
            this.colEmployeeID.Width = 80;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "ชื่อ - นามสกุล";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.VisibleIndex = 2;
            this.colEmployeeName.Width = 224;
            // 
            // colPositionName
            // 
            this.colPositionName.AppearanceCell.Options.UseTextOptions = true;
            this.colPositionName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPositionName.AppearanceHeader.Options.UseTextOptions = true;
            this.colPositionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPositionName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPositionName.Caption = "ตำแหน่ง";
            this.colPositionName.FieldName = "PositionName";
            this.colPositionName.Name = "colPositionName";
            this.colPositionName.OptionsColumn.AllowEdit = false;
            this.colPositionName.OptionsColumn.AllowFocus = false;
            this.colPositionName.OptionsFilter.AllowAutoFilter = false;
            this.colPositionName.OptionsFilter.AllowFilter = false;
            this.colPositionName.Visible = true;
            this.colPositionName.VisibleIndex = 3;
            this.colPositionName.Width = 224;
            // 
            // colLevelName
            // 
            this.colLevelName.Caption = "ระดับ";
            this.colLevelName.FieldName = "LevelName";
            this.colLevelName.Name = "colLevelName";
            this.colLevelName.OptionsColumn.AllowEdit = false;
            this.colLevelName.OptionsColumn.AllowFocus = false;
            this.colLevelName.Visible = true;
            this.colLevelName.VisibleIndex = 4;
            this.colLevelName.Width = 84;
            // 
            // colDepartment
            // 
            this.colDepartment.Caption = "แผนก";
            this.colDepartment.FieldName = "DepartmentName";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.AllowFocus = false;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 5;
            // 
            // colDivision
            // 
            this.colDivision.Caption = "ฝ่าย";
            this.colDivision.FieldName = "DivisionName";
            this.colDivision.Name = "colDivision";
            this.colDivision.OptionsColumn.AllowEdit = false;
            this.colDivision.OptionsColumn.AllowFocus = false;
            this.colDivision.Visible = true;
            this.colDivision.VisibleIndex = 6;
            // 
            // colCoreComp
            // 
            this.colCoreComp.AppearanceCell.Options.UseTextOptions = true;
            this.colCoreComp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoreComp.Caption = "CC";
            this.colCoreComp.DisplayFormat.FormatString = "#.##";
            this.colCoreComp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCoreComp.Name = "colCoreComp";
            this.colCoreComp.OptionsColumn.AllowEdit = false;
            this.colCoreComp.OptionsColumn.AllowFocus = false;
            this.colCoreComp.Visible = true;
            this.colCoreComp.VisibleIndex = 7;
            this.colCoreComp.Width = 78;
            // 
            // colFuncComp
            // 
            this.colFuncComp.AppearanceCell.Options.UseTextOptions = true;
            this.colFuncComp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuncComp.Caption = "FC";
            this.colFuncComp.DisplayFormat.FormatString = "#.##";
            this.colFuncComp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuncComp.Name = "colFuncComp";
            this.colFuncComp.OptionsColumn.AllowEdit = false;
            this.colFuncComp.OptionsColumn.AllowFocus = false;
            this.colFuncComp.Visible = true;
            this.colFuncComp.VisibleIndex = 8;
            this.colFuncComp.Width = 84;
            // 
            // colLeaderComp
            // 
            this.colLeaderComp.AppearanceCell.Options.UseTextOptions = true;
            this.colLeaderComp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLeaderComp.Caption = "JA";
            this.colLeaderComp.DisplayFormat.FormatString = "#.##";
            this.colLeaderComp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLeaderComp.Name = "colLeaderComp";
            this.colLeaderComp.OptionsColumn.AllowEdit = false;
            this.colLeaderComp.OptionsColumn.AllowFocus = false;
            this.colLeaderComp.Visible = true;
            this.colLeaderComp.VisibleIndex = 9;
            this.colLeaderComp.Width = 78;
            // 
            // colCoreComTest
            // 
            this.colCoreComTest.AppearanceCell.Options.UseTextOptions = true;
            this.colCoreComTest.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoreComTest.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCoreComTest.AppearanceHeader.Options.UseTextOptions = true;
            this.colCoreComTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoreComTest.Caption = "ทดสอบ Core Comp.";
            this.colCoreComTest.FieldName = "CoreComp_Test";
            this.colCoreComTest.Name = "colCoreComTest";
            this.colCoreComTest.OptionsColumn.AllowEdit = false;
            this.colCoreComTest.OptionsFilter.AllowAutoFilter = false;
            this.colCoreComTest.OptionsFilter.AllowFilter = false;
            this.colCoreComTest.Width = 70;
            // 
            // colExR
            // 
            this.colExR.AppearanceCell.Options.UseTextOptions = true;
            this.colExR.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExR.Caption = "In. Survey";
            this.colExR.FieldName = "ExecutiveRound";
            this.colExR.Name = "colExR";
            this.colExR.OptionsColumn.AllowEdit = false;
            this.colExR.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colExR.Width = 78;
            // 
            // colCQI
            // 
            this.colCQI.AppearanceCell.Options.UseTextOptions = true;
            this.colCQI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCQI.Caption = "R2R/CQI";
            this.colCQI.FieldName = "CQI";
            this.colCQI.Name = "colCQI";
            this.colCQI.OptionsColumn.AllowEdit = false;
            this.colCQI.Visible = true;
            this.colCQI.VisibleIndex = 10;
            this.colCQI.Width = 78;
            // 
            // colLean
            // 
            this.colLean.Caption = "Lean";
            this.colLean.FieldName = "Lean";
            this.colLean.Name = "colLean";
            this.colLean.OptionsColumn.AllowEdit = false;
            this.colLean.Visible = true;
            this.colLean.VisibleIndex = 11;
            // 
            // colICRTeam
            // 
            this.colICRTeam.AppearanceCell.Options.UseTextOptions = true;
            this.colICRTeam.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colICRTeam.Caption = "งานคุณภาพ";
            this.colICRTeam.FieldName = "ICR_Team";
            this.colICRTeam.Name = "colICRTeam";
            this.colICRTeam.OptionsColumn.AllowEdit = false;
            this.colICRTeam.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colICRTeam.Width = 78;
            // 
            // colICR
            // 
            this.colICR.AppearanceCell.Options.UseTextOptions = true;
            this.colICR.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colICR.Caption = "การคีย์ ICR";
            this.colICR.FieldName = "ICR";
            this.colICR.Name = "colICR";
            this.colICR.OptionsColumn.AllowEdit = false;
            this.colICR.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colICR.Width = 70;
            // 
            // colDiscipline
            // 
            this.colDiscipline.AppearanceCell.Options.UseTextOptions = true;
            this.colDiscipline.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscipline.Caption = "Discipline";
            this.colDiscipline.FieldName = "Discipline";
            this.colDiscipline.Name = "colDiscipline";
            this.colDiscipline.OptionsColumn.AllowEdit = false;
            this.colDiscipline.Visible = true;
            this.colDiscipline.VisibleIndex = 12;
            this.colDiscipline.Width = 78;
            // 
            // colTraining
            // 
            this.colTraining.AppearanceCell.Options.UseTextOptions = true;
            this.colTraining.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTraining.Caption = "Training";
            this.colTraining.FieldName = "Training";
            this.colTraining.Name = "colTraining";
            this.colTraining.OptionsColumn.AllowEdit = false;
            this.colTraining.Visible = true;
            this.colTraining.VisibleIndex = 13;
            this.colTraining.Width = 78;
            // 
            // colCheckup
            // 
            this.colCheckup.AppearanceCell.Options.UseTextOptions = true;
            this.colCheckup.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCheckup.Caption = "Checkup";
            this.colCheckup.FieldName = "Checkup";
            this.colCheckup.Name = "colCheckup";
            this.colCheckup.OptionsColumn.AllowEdit = false;
            this.colCheckup.Visible = true;
            this.colCheckup.VisibleIndex = 14;
            this.colCheckup.Width = 78;
            // 
            // colActivity
            // 
            this.colActivity.AppearanceCell.Options.UseTextOptions = true;
            this.colActivity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActivity.Caption = "Activity";
            this.colActivity.FieldName = "Activity";
            this.colActivity.Name = "colActivity";
            this.colActivity.OptionsColumn.AllowEdit = false;
            this.colActivity.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colActivity.Width = 78;
            // 
            // colTotalScore
            // 
            this.colTotalScore.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTotalScore.AppearanceCell.Options.UseFont = true;
            this.colTotalScore.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalScore.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalScore.Caption = "รวม (เต็ม 35)";
            this.colTotalScore.DisplayFormat.FormatString = "#.##";
            this.colTotalScore.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalScore.FieldName = "TotalScore";
            this.colTotalScore.Name = "colTotalScore";
            this.colTotalScore.OptionsColumn.AllowEdit = false;
            this.colTotalScore.OptionsColumn.AllowFocus = false;
            this.colTotalScore.Visible = true;
            this.colTotalScore.VisibleIndex = 15;
            this.colTotalScore.Width = 140;
            // 
            // colQA
            // 
            this.colQA.AppearanceCell.Options.UseTextOptions = true;
            this.colQA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQA.Caption = "Lean Proj.";
            this.colQA.FieldName = "Lean";
            this.colQA.Name = "colQA";
            this.colQA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colQA.Width = 70;
            // 
            // cboYear
            // 
            this.cboYear.Location = new System.Drawing.Point(37, 6);
            this.cboYear.Name = "cboYear";
            this.cboYear.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.cboYear.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cboYear.Properties.Appearance.Options.UseBackColor = true;
            this.cboYear.Properties.Appearance.Options.UseFont = true;
            this.cboYear.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cboYear.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboYear.Properties.NullText = "--- เลือก ---";
            this.cboYear.Size = new System.Drawing.Size(117, 24);
            this.cboYear.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.cmdView);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cboYear);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1540, 36);
            this.panel2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdConfirm);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1280, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 36);
            this.panel1.TabIndex = 188;
            // 
            // cmdConfirm
            // 
            this.cmdConfirm.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdConfirm.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdConfirm.Appearance.Options.UseFont = true;
            this.cmdConfirm.Appearance.Options.UseForeColor = true;
            this.cmdConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdConfirm.Location = new System.Drawing.Point(7, 6);
            this.cmdConfirm.LookAndFeel.SkinMaskColor = System.Drawing.Color.DarkGreen;
            this.cmdConfirm.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdConfirm.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdConfirm.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdConfirm.Name = "cmdConfirm";
            this.cmdConfirm.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdConfirm.Size = new System.Drawing.Size(217, 25);
            this.cmdConfirm.TabIndex = 187;
            this.cmdConfirm.Text = "รับทราบและยอมรับผลการประเมิน";
            this.cmdConfirm.Visible = false;
            this.cmdConfirm.Click += new System.EventHandler(this.cmdConfirm_Click);
            // 
            // cmdView
            // 
            this.cmdView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdView.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdView.Appearance.Options.UseFont = true;
            this.cmdView.Appearance.Options.UseForeColor = true;
            this.cmdView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdView.Location = new System.Drawing.Point(161, 6);
            this.cmdView.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.HotTrack;
            this.cmdView.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdView.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdView.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdView.Name = "cmdView";
            this.cmdView.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdView.Size = new System.Drawing.Size(97, 25);
            this.cmdView.TabIndex = 186;
            this.cmdView.Text = "View";
            this.cmdView.Click += new System.EventHandler(this.cmdView_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(12, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 21);
            this.label1.TabIndex = 44;
            this.label1.Text = "ปี";
            // 
            // frmAssessmentScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1540, 769);
            this.Controls.Add(this.pScore);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAssessmentScore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "สรุปผลคะแนนประเมินประจำปี";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmAssessment_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.pnBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pScore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboYear.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label lblTitlePA;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private DevExpress.XtraGrid.GridControl grdAssessment;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colPositionName;
        private DevExpress.XtraGrid.Columns.GridColumn colCoreComp;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaderComp;
        private DevExpress.XtraGrid.Columns.GridColumn colCoreComTest;
        private System.Windows.Forms.Panel pScore;
        private DevExpress.XtraGrid.Columns.GridColumn colCQI;
        private DevExpress.XtraGrid.Columns.GridColumn colExR;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalScore;
        private DevExpress.XtraGrid.Columns.GridColumn colICRTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colICR;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscipline;
        private DevExpress.XtraGrid.Columns.GridColumn colTraining;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckup;
        private DevExpress.XtraGrid.Columns.GridColumn colActivity;
        private DevExpress.XtraGrid.Columns.GridColumn colDivision;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colLevelName;
        private DevExpress.XtraGrid.Columns.GridColumn colFuncComp;
        private DevExpress.XtraGrid.Columns.GridColumn colLean;
        private DevExpress.XtraGrid.Columns.GridColumn colQA;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraEditors.LookUpEdit cboYear;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton cmdView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton cmdConfirm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}