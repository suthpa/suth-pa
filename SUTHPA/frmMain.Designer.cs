﻿namespace SUTHPA
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.barAndDockingController = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.defaultLookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageCollection48 = new DevExpress.Utils.ImageCollection(this.components);
            this.menuAssesseeList = new DevExpress.XtraBars.BarButtonItem();
            this.barHearderUser = new DevExpress.XtraBars.BarStaticItem();
            this.btnChangePassword = new DevExpress.XtraBars.BarButtonItem();
            this.lblStatus = new DevExpress.XtraBars.BarStaticItem();
            this.lblInfoDB = new DevExpress.XtraBars.BarStaticItem();
            this.barHeaderItem1 = new DevExpress.XtraBars.BarHeaderItem();
            this.barNameOfUser = new DevExpress.XtraBars.BarStaticItem();
            this.mnuEmployee = new DevExpress.XtraBars.BarButtonItem();
            this.mnuEmpNotAssessment = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRptScore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuReportAvg = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRPTFinalScore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRPTManagerScore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRPTManagerFinalScore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuEmpWeight = new DevExpress.XtraBars.BarButtonItem();
            this.mnuAssessmentGrade = new DevExpress.XtraBars.BarButtonItem();
            this.mnuNote = new DevExpress.XtraBars.BarButtonItem();
            this.barContact = new DevExpress.XtraBars.BarStaticItem();
            this.barVersion = new DevExpress.XtraBars.BarStaticItem();
            this.mnuRPTScoreSubstandard = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRPTAssessmentGrade = new DevExpress.XtraBars.BarButtonItem();
            this.barLog = new DevExpress.XtraBars.BarButtonItem();
            this.mnuDownload = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRPTOverStandard = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRPTComment = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.mnuEmployeeRelation = new DevExpress.XtraBars.BarButtonItem();
            this.mnuLogout = new DevExpress.XtraBars.BarButtonItem();
            this.mnuAssesseeNew = new DevExpress.XtraBars.BarButtonItem();
            this.mnuEmpImport = new DevExpress.XtraBars.BarButtonItem();
            this.mnuDivision = new DevExpress.XtraBars.BarButtonItem();
            this.mnuDepartment = new DevExpress.XtraBars.BarButtonItem();
            this.mnuPosition = new DevExpress.XtraBars.BarButtonItem();
            this.mnuImportImage = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRptSelf = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRptCoWork = new DevExpress.XtraBars.BarButtonItem();
            this.barIP = new DevExpress.XtraBars.BarStaticItem();
            this.mnuPrintAsm = new DevExpress.XtraBars.BarButtonItem();
            this.mnuHOD = new DevExpress.XtraBars.BarButtonItem();
            this.mnuSystemConfig = new DevExpress.XtraBars.BarButtonItem();
            this.mnuFinalGrade = new DevExpress.XtraBars.BarButtonItem();
            this.mnuCheckScore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuPrintFormEmpNew = new DevExpress.XtraBars.BarButtonItem();
            this.pageHome = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.GroupNewEmployee = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupAssessment = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupNote = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.groupAbout = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageMaster = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageReport = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.GroupManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupReport = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageAdmin = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.GroupEmployee = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupStatus = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupGrade = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.GroupConfig = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.mnuAsmRelation = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // barAndDockingController
            // 
            this.barAndDockingController.AppearancesBar.ItemsFont = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.barAndDockingController.AppearancesRibbon.FormCaption.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.FormCaption.Options.UseFont = true;
            this.barAndDockingController.AppearancesRibbon.Item.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.Item.Options.UseFont = true;
            this.barAndDockingController.AppearancesRibbon.ItemHovered.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.ItemHovered.Options.UseFont = true;
            this.barAndDockingController.AppearancesRibbon.ItemPressed.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.ItemPressed.Options.UseFont = true;
            this.barAndDockingController.AppearancesRibbon.PageCategory.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.PageCategory.Options.UseFont = true;
            this.barAndDockingController.AppearancesRibbon.PageGroupCaption.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.PageGroupCaption.Options.UseFont = true;
            this.barAndDockingController.AppearancesRibbon.PageHeader.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barAndDockingController.AppearancesRibbon.PageHeader.Options.UseFont = true;
            this.barAndDockingController.LookAndFeel.SkinName = "VS2010";
            this.barAndDockingController.LookAndFeel.UseDefaultLookAndFeel = false;
            this.barAndDockingController.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "Office 2010 Blue";
            // 
            // ribbonControl
            // 
            this.ribbonControl.AllowCustomization = true;
            this.ribbonControl.ApplicationCaption = "Performance Appraisal 2021 : ระบบประเมินพนักงาน";
            this.ribbonControl.ApplicationIcon = global::SUTHPA.Properties.Resources.paiconnew;
            this.ribbonControl.Controller = this.barAndDockingController;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.ribbonControl.Images = this.imageCollection48;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.menuAssesseeList,
            this.barHearderUser,
            this.btnChangePassword,
            this.lblStatus,
            this.lblInfoDB,
            this.barHeaderItem1,
            this.barNameOfUser,
            this.mnuEmployee,
            this.mnuEmpNotAssessment,
            this.mnuRptScore,
            this.mnuReportAvg,
            this.mnuRPTFinalScore,
            this.mnuRPTManagerScore,
            this.mnuRPTManagerFinalScore,
            this.mnuEmpWeight,
            this.mnuAssessmentGrade,
            this.mnuNote,
            this.barContact,
            this.barVersion,
            this.mnuRPTScoreSubstandard,
            this.mnuRPTAssessmentGrade,
            this.barLog,
            this.mnuDownload,
            this.mnuRPTOverStandard,
            this.mnuRPTComment,
            this.barStaticItem1,
            this.mnuEmployeeRelation,
            this.mnuLogout,
            this.mnuAssesseeNew,
            this.mnuEmpImport,
            this.mnuDivision,
            this.mnuDepartment,
            this.mnuPosition,
            this.mnuImportImage,
            this.mnuRptSelf,
            this.mnuRptCoWork,
            this.barIP,
            this.mnuPrintAsm,
            this.mnuHOD,
            this.mnuSystemConfig,
            this.mnuFinalGrade,
            this.mnuCheckScore,
            this.mnuPrintFormEmpNew,
            this.mnuAsmRelation});
            this.ribbonControl.LargeImages = this.imageCollection48;
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonControl.MaxItemId = 11;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.pageHome,
            this.pageMaster,
            this.pageReport,
            this.pageAdmin});
            this.ribbonControl.QuickToolbarItemLinks.Add(this.barHearderUser);
            this.ribbonControl.QuickToolbarItemLinks.Add(this.btnChangePassword);
            this.ribbonControl.QuickToolbarItemLinks.Add(this.barLog);
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2007;
            this.ribbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.Size = new System.Drawing.Size(1517, 159);
            this.ribbonControl.StatusBar = this.ribbonStatusBar1;
            // 
            // imageCollection48
            // 
            this.imageCollection48.ImageSize = new System.Drawing.Size(48, 48);
            this.imageCollection48.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection48.ImageStream")));
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.abc48, "abc48", typeof(global::SUTHPA.Properties.Resources), 0);
            this.imageCollection48.Images.SetKeyName(0, "abc48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.adduser50, "adduser50", typeof(global::SUTHPA.Properties.Resources), 1);
            this.imageCollection48.Images.SetKeyName(1, "adduser50");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.Admin_Settings_Male, "Admin_Settings_Male", typeof(global::SUTHPA.Properties.Resources), 2);
            this.imageCollection48.Images.SetKeyName(2, "Admin_Settings_Male");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.businessman48, "businessman48", typeof(global::SUTHPA.Properties.Resources), 3);
            this.imageCollection48.Images.SetKeyName(3, "businessman48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.collaboration48, "collaboration48", typeof(global::SUTHPA.Properties.Resources), 4);
            this.imageCollection48.Images.SetKeyName(4, "collaboration48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.combochart_50px, "combochart_50px", typeof(global::SUTHPA.Properties.Resources), 5);
            this.imageCollection48.Images.SetKeyName(5, "combochart_50px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.commercial48, "commercial48", typeof(global::SUTHPA.Properties.Resources), 6);
            this.imageCollection48.Images.SetKeyName(6, "commercial48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.company, "company", typeof(global::SUTHPA.Properties.Resources), 7);
            this.imageCollection48.Images.SetKeyName(7, "company");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.devpost, "devpost", typeof(global::SUTHPA.Properties.Resources), 8);
            this.imageCollection48.Images.SetKeyName(8, "devpost");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.Dictionary, "Dictionary", typeof(global::SUTHPA.Properties.Resources), 9);
            this.imageCollection48.Images.SetKeyName(9, "Dictionary");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.doctormale48, "doctormale48", typeof(global::SUTHPA.Properties.Resources), 10);
            this.imageCollection48.Images.SetKeyName(10, "doctormale48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.employee_card, "employee_card", typeof(global::SUTHPA.Properties.Resources), 11);
            this.imageCollection48.Images.SetKeyName(11, "employee_card");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.exam48, "exam48", typeof(global::SUTHPA.Properties.Resources), 12);
            this.imageCollection48.Images.SetKeyName(12, "exam48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.flowchart48, "flowchart48", typeof(global::SUTHPA.Properties.Resources), 13);
            this.imageCollection48.Images.SetKeyName(13, "flowchart48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.gears50, "gears50", typeof(global::SUTHPA.Properties.Resources), 14);
            this.imageCollection48.Images.SetKeyName(14, "gears50");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.genealogy, "genealogy", typeof(global::SUTHPA.Properties.Resources), 15);
            this.imageCollection48.Images.SetKeyName(15, "genealogy");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.import, "import", typeof(global::SUTHPA.Properties.Resources), 16);
            this.imageCollection48.Images.SetKeyName(16, "import");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.info, "info", typeof(global::SUTHPA.Properties.Resources), 17);
            this.imageCollection48.Images.SetKeyName(17, "info");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.layers, "layers", typeof(global::SUTHPA.Properties.Resources), 18);
            this.imageCollection48.Images.SetKeyName(18, "layers");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.mind_map, "mind_map", typeof(global::SUTHPA.Properties.Resources), 19);
            this.imageCollection48.Images.SetKeyName(19, "mind_map");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.ms_excel, "ms_excel", typeof(global::SUTHPA.Properties.Resources), 20);
            this.imageCollection48.Images.SetKeyName(20, "ms_excel");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.nursefemale48, "nursefemale48", typeof(global::SUTHPA.Properties.Resources), 21);
            this.imageCollection48.Images.SetKeyName(21, "nursefemale48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.org_unit, "org_unit", typeof(global::SUTHPA.Properties.Resources), 22);
            this.imageCollection48.Images.SetKeyName(22, "org_unit");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.parallel_tasks, "parallel_tasks", typeof(global::SUTHPA.Properties.Resources), 23);
            this.imageCollection48.Images.SetKeyName(23, "parallel_tasks");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.pdf48, "pdf48", typeof(global::SUTHPA.Properties.Resources), 24);
            this.imageCollection48.Images.SetKeyName(24, "pdf48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.settings_3, "settings_3", typeof(global::SUTHPA.Properties.Resources), 25);
            this.imageCollection48.Images.SetKeyName(25, "settings_3");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.shutdown48, "shutdown48", typeof(global::SUTHPA.Properties.Resources), 26);
            this.imageCollection48.Images.SetKeyName(26, "shutdown48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.todo48, "todo48", typeof(global::SUTHPA.Properties.Resources), 27);
            this.imageCollection48.Images.SetKeyName(27, "todo48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.touchid48, "touchid48", typeof(global::SUTHPA.Properties.Resources), 28);
            this.imageCollection48.Images.SetKeyName(28, "touchid48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.verifiedaccount48, "verifiedaccount48", typeof(global::SUTHPA.Properties.Resources), 29);
            this.imageCollection48.Images.SetKeyName(29, "verifiedaccount48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.warranty48, "warranty48", typeof(global::SUTHPA.Properties.Resources), 30);
            this.imageCollection48.Images.SetKeyName(30, "warranty48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.word48, "word48", typeof(global::SUTHPA.Properties.Resources), 31);
            this.imageCollection48.Images.SetKeyName(31, "word48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.working48, "working48", typeof(global::SUTHPA.Properties.Resources), 32);
            this.imageCollection48.Images.SetKeyName(32, "working48");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.xls, "xls", typeof(global::SUTHPA.Properties.Resources), 33);
            this.imageCollection48.Images.SetKeyName(33, "xls");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.area_chart_48px, "area_chart_48px", typeof(global::SUTHPA.Properties.Resources), 34);
            this.imageCollection48.Images.SetKeyName(34, "area_chart_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.average_value_48px, "average_value_48px", typeof(global::SUTHPA.Properties.Resources), 35);
            this.imageCollection48.Images.SetKeyName(35, "average_value_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.business_report_48px, "business_report_48px", typeof(global::SUTHPA.Properties.Resources), 36);
            this.imageCollection48.Images.SetKeyName(36, "business_report_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.doughnut_chart_48px, "doughnut_chart_48px", typeof(global::SUTHPA.Properties.Resources), 37);
            this.imageCollection48.Images.SetKeyName(37, "doughnut_chart_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.fund_accounting_48px, "fund_accounting_48px", typeof(global::SUTHPA.Properties.Resources), 38);
            this.imageCollection48.Images.SetKeyName(38, "fund_accounting_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.graph_48px, "graph_48px", typeof(global::SUTHPA.Properties.Resources), 39);
            this.imageCollection48.Images.SetKeyName(39, "graph_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.graph_report_48px, "graph_report_48px", typeof(global::SUTHPA.Properties.Resources), 40);
            this.imageCollection48.Images.SetKeyName(40, "graph_report_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.line_chart_48px, "line_chart_48px", typeof(global::SUTHPA.Properties.Resources), 41);
            this.imageCollection48.Images.SetKeyName(41, "line_chart_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.negative_dynamic_48px, "negative_dynamic_48px", typeof(global::SUTHPA.Properties.Resources), 42);
            this.imageCollection48.Images.SetKeyName(42, "negative_dynamic_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.neutral_trading_48px, "neutral_trading_48px", typeof(global::SUTHPA.Properties.Resources), 43);
            this.imageCollection48.Images.SetKeyName(43, "neutral_trading_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.pie_chart_48px, "pie_chart_48px", typeof(global::SUTHPA.Properties.Resources), 44);
            this.imageCollection48.Images.SetKeyName(44, "pie_chart_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.portrait_48px, "portrait_48px", typeof(global::SUTHPA.Properties.Resources), 45);
            this.imageCollection48.Images.SetKeyName(45, "portrait_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.positive_dynamic_48px, "positive_dynamic_48px", typeof(global::SUTHPA.Properties.Resources), 46);
            this.imageCollection48.Images.SetKeyName(46, "positive_dynamic_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.timeline_48px, "timeline_48px", typeof(global::SUTHPA.Properties.Resources), 47);
            this.imageCollection48.Images.SetKeyName(47, "timeline_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.xlarge_icons_48px, "xlarge_icons_48px", typeof(global::SUTHPA.Properties.Resources), 48);
            this.imageCollection48.Images.SetKeyName(48, "xlarge_icons_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.accessibility2_48px, "accessibility2_48px", typeof(global::SUTHPA.Properties.Resources), 49);
            this.imageCollection48.Images.SetKeyName(49, "accessibility2_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.queen_gb_48px, "queen_gb_48px", typeof(global::SUTHPA.Properties.Resources), 50);
            this.imageCollection48.Images.SetKeyName(50, "queen_gb_48px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.counter, "counter", typeof(global::SUTHPA.Properties.Resources), 51);
            this.imageCollection48.Images.SetKeyName(51, "counter");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.imac, "imac", typeof(global::SUTHPA.Properties.Resources), 52);
            this.imageCollection48.Images.SetKeyName(52, "imac");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.reportcard, "reportcard", typeof(global::SUTHPA.Properties.Resources), 53);
            this.imageCollection48.Images.SetKeyName(53, "reportcard");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.reportcard2, "reportcard2", typeof(global::SUTHPA.Properties.Resources), 54);
            this.imageCollection48.Images.SetKeyName(54, "reportcard2");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.scoreboard, "scoreboard", typeof(global::SUTHPA.Properties.Resources), 55);
            this.imageCollection48.Images.SetKeyName(55, "scoreboard");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.stargold, "stargold", typeof(global::SUTHPA.Properties.Resources), 56);
            this.imageCollection48.Images.SetKeyName(56, "stargold");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.friends_50px, "friends_50px", typeof(global::SUTHPA.Properties.Resources), 57);
            this.imageCollection48.Images.SetKeyName(57, "friends_50px");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources.printer, "printer", typeof(global::SUTHPA.Properties.Resources), 58);
            this.imageCollection48.Images.SetKeyName(58, "printer");
            this.imageCollection48.InsertImage(global::SUTHPA.Properties.Resources._123, "_123", typeof(global::SUTHPA.Properties.Resources), 59);
            this.imageCollection48.Images.SetKeyName(59, "_123");
            // 
            // menuAssesseeList
            // 
            this.menuAssesseeList.AccessibleDescription = "";
            this.menuAssesseeList.AccessibleName = "AssesseeList";
            this.menuAssesseeList.Caption = "ประเมินประจำปี";
            this.menuAssesseeList.Id = 1;
            this.menuAssesseeList.ImageOptions.LargeImageIndex = 56;
            this.menuAssesseeList.LargeWidth = 100;
            this.menuAssesseeList.Name = "menuAssesseeList";
            this.menuAssesseeList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuAssesseeList_ItemClick);
            // 
            // barHearderUser
            // 
            this.barHearderUser.Id = 5;
            this.barHearderUser.ImageOptions.Image = global::SUTHPA.Properties.Resources.user16;
            this.barHearderUser.Name = "barHearderUser";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Caption = "เปลี่ยนรหัสผ่าน";
            this.btnChangePassword.Id = 6;
            this.btnChangePassword.ImageOptions.Image = global::SUTHPA.Properties.Resources.touchid16;
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnChangePassword_ItemClick);
            // 
            // lblStatus
            // 
            this.lblStatus.Caption = "Use Database :";
            this.lblStatus.Id = 7;
            this.lblStatus.Name = "lblStatus";
            // 
            // lblInfoDB
            // 
            this.lblInfoDB.Id = 8;
            this.lblInfoDB.Name = "lblInfoDB";
            // 
            // barHeaderItem1
            // 
            this.barHeaderItem1.Caption = "barHeaderItem1";
            this.barHeaderItem1.Id = 18;
            this.barHeaderItem1.Name = "barHeaderItem1";
            // 
            // barNameOfUser
            // 
            this.barNameOfUser.Caption = "Name of User";
            this.barNameOfUser.Id = 19;
            this.barNameOfUser.Name = "barNameOfUser";
            // 
            // mnuEmployee
            // 
            this.mnuEmployee.Caption = "ข้อมูลพนักงาน";
            this.mnuEmployee.Id = 21;
            this.mnuEmployee.ImageOptions.LargeImageIndex = 3;
            this.mnuEmployee.Name = "mnuEmployee";
            this.mnuEmployee.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEmployee_ItemClick);
            // 
            // mnuEmpNotAssessment
            // 
            this.mnuEmpNotAssessment.AccessibleName = "EmployeeNoAssessment";
            this.mnuEmpNotAssessment.Caption = "พนักงานที่ยังไม่ถูก ผู้บังคับบัญชาประเมิน";
            this.mnuEmpNotAssessment.Id = 23;
            this.mnuEmpNotAssessment.ImageOptions.ImageIndex = 54;
            this.mnuEmpNotAssessment.ImageOptions.LargeImageIndex = 21;
            this.mnuEmpNotAssessment.Name = "mnuEmpNotAssessment";
            this.mnuEmpNotAssessment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEmpNotAssessment_ItemClick);
            // 
            // mnuRptScore
            // 
            this.mnuRptScore.Caption = "รายงานคะแนนแต่ละข้อ";
            this.mnuRptScore.Id = 24;
            this.mnuRptScore.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRptScore.ImageOptions.Image")));
            this.mnuRptScore.ImageOptions.LargeImageIndex = 41;
            this.mnuRptScore.Name = "mnuRptScore";
            this.mnuRptScore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRptScore_ItemClick);
            // 
            // mnuReportAvg
            // 
            this.mnuReportAvg.Caption = "รายงานคะแนนสรุป";
            this.mnuReportAvg.Id = 25;
            this.mnuReportAvg.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuReportAvg.ImageOptions.Image")));
            this.mnuReportAvg.ImageOptions.LargeImageIndex = 37;
            this.mnuReportAvg.Name = "mnuReportAvg";
            this.mnuReportAvg.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuReportAvg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuReportAvg_ItemClick);
            // 
            // mnuRPTFinalScore
            // 
            this.mnuRPTFinalScore.Caption = "รายงานคะแนนสุทธิ (Final)";
            this.mnuRPTFinalScore.Id = 26;
            this.mnuRPTFinalScore.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTFinalScore.ImageOptions.Image")));
            this.mnuRPTFinalScore.ImageOptions.LargeImageIndex = 43;
            this.mnuRPTFinalScore.Name = "mnuRPTFinalScore";
            this.mnuRPTFinalScore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTFinalScore_ItemClick);
            // 
            // mnuRPTManagerScore
            // 
            this.mnuRPTManagerScore.Caption = "รายงานคะแนน";
            this.mnuRPTManagerScore.Id = 27;
            this.mnuRPTManagerScore.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTManagerScore.ImageOptions.Image")));
            this.mnuRPTManagerScore.ImageOptions.LargeImageIndex = 46;
            this.mnuRPTManagerScore.Name = "mnuRPTManagerScore";
            this.mnuRPTManagerScore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTManagerScore_ItemClick);
            // 
            // mnuRPTManagerFinalScore
            // 
            this.mnuRPTManagerFinalScore.Caption = "คะแนนสุทธิ (Final)";
            this.mnuRPTManagerFinalScore.Id = 28;
            this.mnuRPTManagerFinalScore.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTManagerFinalScore.ImageOptions.Image")));
            this.mnuRPTManagerFinalScore.ImageOptions.LargeImageIndex = 47;
            this.mnuRPTManagerFinalScore.Name = "mnuRPTManagerFinalScore";
            this.mnuRPTManagerFinalScore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTManagerFinalScore_ItemClick);
            // 
            // mnuEmpWeight
            // 
            this.mnuEmpWeight.Caption = "คะแนนน้ำหนัก";
            this.mnuEmpWeight.Id = 29;
            this.mnuEmpWeight.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuEmpWeight.ImageOptions.Image")));
            this.mnuEmpWeight.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuEmpWeight.ImageOptions.LargeImage")));
            this.mnuEmpWeight.Name = "mnuEmpWeight";
            this.mnuEmpWeight.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuEmpWeight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEmpWeight_ItemClick);
            // 
            // mnuAssessmentGrade
            // 
            this.mnuAssessmentGrade.AccessibleName = "AssessmentGrade";
            this.mnuAssessmentGrade.Caption = "คะแนนตัดเกรด";
            this.mnuAssessmentGrade.Id = 30;
            this.mnuAssessmentGrade.ImageOptions.LargeImageIndex = 0;
            this.mnuAssessmentGrade.Name = "mnuAssessmentGrade";
            this.mnuAssessmentGrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuAssessmentGrade_ItemClick);
            // 
            // mnuNote
            // 
            this.mnuNote.Caption = "แจ้งเพื่อทราบ";
            this.mnuNote.Id = 31;
            this.mnuNote.ImageOptions.ImageIndex = 9;
            this.mnuNote.ImageOptions.LargeImageIndex = 6;
            this.mnuNote.Name = "mnuNote";
            this.mnuNote.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuNote_ItemClick);
            // 
            // barContact
            // 
            this.barContact.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.barContact.Caption = "<color=#ffffff>แผนกทรัพยากรมนุษย์ ฯ โทร. 6683</color>";
            this.barContact.Id = 1;
            this.barContact.ImageOptions.Image = global::SUTHPA.Properties.Resources.phonegreen16;
            this.barContact.Name = "barContact";
            // 
            // barVersion
            // 
            this.barVersion.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barVersion.Caption = "v.1.7.64 ® 2021";
            this.barVersion.Id = 2;
            this.barVersion.Name = "barVersion";
            // 
            // mnuRPTScoreSubstandard
            // 
            this.mnuRPTScoreSubstandard.Caption = "คะแนนต่ำกว่าค่าคาดหวัง";
            this.mnuRPTScoreSubstandard.Id = 3;
            this.mnuRPTScoreSubstandard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTScoreSubstandard.ImageOptions.Image")));
            this.mnuRPTScoreSubstandard.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuRPTScoreSubstandard.ImageOptions.LargeImage")));
            this.mnuRPTScoreSubstandard.Name = "mnuRPTScoreSubstandard";
            this.mnuRPTScoreSubstandard.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuRPTScoreSubstandard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTScoreSubstandard_ItemClick);
            // 
            // mnuRPTAssessmentGrade
            // 
            this.mnuRPTAssessmentGrade.Caption = "คะแนนประเมินรวม (เกรด)";
            this.mnuRPTAssessmentGrade.Id = 4;
            this.mnuRPTAssessmentGrade.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTAssessmentGrade.ImageOptions.Image")));
            this.mnuRPTAssessmentGrade.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuRPTAssessmentGrade.ImageOptions.LargeImage")));
            this.mnuRPTAssessmentGrade.Name = "mnuRPTAssessmentGrade";
            this.mnuRPTAssessmentGrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTAssessmentGrade_ItemClick);
            // 
            // barLog
            // 
            this.barLog.Caption = "Logout";
            this.barLog.Id = 5;
            this.barLog.ImageOptions.Image = global::SUTHPA.Properties.Resources.logout16;
            this.barLog.Name = "barLog";
            this.barLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLog_ItemClick);
            // 
            // mnuDownload
            // 
            this.mnuDownload.Caption = "ดาวน์โหลดแบบฟอร์ม";
            this.mnuDownload.Id = 6;
            this.mnuDownload.ImageOptions.ImageIndex = 7;
            this.mnuDownload.ImageOptions.LargeImageIndex = 31;
            this.mnuDownload.Name = "mnuDownload";
            this.mnuDownload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuDownload_ItemClick);
            // 
            // mnuRPTOverStandard
            // 
            this.mnuRPTOverStandard.Caption = "คะแนนที่สูงกว่าค่าคาดหวัง";
            this.mnuRPTOverStandard.Id = 7;
            this.mnuRPTOverStandard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTOverStandard.ImageOptions.Image")));
            this.mnuRPTOverStandard.ImageOptions.LargeImageIndex = 5;
            this.mnuRPTOverStandard.Name = "mnuRPTOverStandard";
            this.mnuRPTOverStandard.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuRPTOverStandard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTOverStandard_ItemClick);
            // 
            // mnuRPTComment
            // 
            this.mnuRPTComment.Caption = "ข้อเสนอแนะจากหัวหน้า";
            this.mnuRPTComment.Id = 8;
            this.mnuRPTComment.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRPTComment.ImageOptions.Image")));
            this.mnuRPTComment.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuRPTComment.ImageOptions.LargeImage")));
            this.mnuRPTComment.Name = "mnuRPTComment";
            this.mnuRPTComment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRPTComment_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.barStaticItem1.Caption = "<color=#ffffff> แผนกสารสนเทศ โทร. 7620 </color>";
            this.barStaticItem1.Id = 24;
            this.barStaticItem1.ImageOptions.Image = global::SUTHPA.Properties.Resources.phone16;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // mnuEmployeeRelation
            // 
            this.mnuEmployeeRelation.AccessibleName = "EmployeeRelation";
            this.mnuEmployeeRelation.Caption = "ตรวจสอบสถานะ ค้างประเมิน";
            this.mnuEmployeeRelation.Hint = "ตรวจสอบ พนง. ค้างประเมิน";
            this.mnuEmployeeRelation.Id = 25;
            this.mnuEmployeeRelation.ImageOptions.LargeImageIndex = 4;
            this.mnuEmployeeRelation.Name = "mnuEmployeeRelation";
            this.mnuEmployeeRelation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEmployeeRelation_ItemClick);
            // 
            // mnuLogout
            // 
            this.mnuLogout.Caption = "Logout";
            this.mnuLogout.Id = 26;
            this.mnuLogout.ImageOptions.LargeImageIndex = 26;
            this.mnuLogout.LargeWidth = 65;
            this.mnuLogout.Name = "mnuLogout";
            this.mnuLogout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLogout_ItemClick);
            // 
            // mnuAssesseeNew
            // 
            this.mnuAssesseeNew.Caption = "ประเมินผ่านงาน (Probation)";
            this.mnuAssesseeNew.Id = 27;
            this.mnuAssesseeNew.ImageOptions.LargeImageIndex = 49;
            this.mnuAssesseeNew.LargeWidth = 104;
            this.mnuAssesseeNew.Name = "mnuAssesseeNew";
            this.mnuAssesseeNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuAssesseeNew_ItemClick);
            // 
            // mnuEmpImport
            // 
            this.mnuEmpImport.Caption = "Import พนักงานใหม่";
            this.mnuEmpImport.Id = 28;
            this.mnuEmpImport.ImageOptions.LargeImageIndex = 33;
            this.mnuEmpImport.Name = "mnuEmpImport";
            this.mnuEmpImport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEmpImport_ItemClick);
            // 
            // mnuDivision
            // 
            this.mnuDivision.Caption = "ฝ่าย";
            this.mnuDivision.Id = 29;
            this.mnuDivision.ImageOptions.LargeImageIndex = 19;
            this.mnuDivision.Name = "mnuDivision";
            this.mnuDivision.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuDivision_ItemClick);
            // 
            // mnuDepartment
            // 
            this.mnuDepartment.Caption = "แผนก";
            this.mnuDepartment.Id = 30;
            this.mnuDepartment.ImageOptions.LargeImageIndex = 13;
            this.mnuDepartment.Name = "mnuDepartment";
            this.mnuDepartment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuDepartment_ItemClick);
            // 
            // mnuPosition
            // 
            this.mnuPosition.Caption = "ตำแหน่งงาน";
            this.mnuPosition.Id = 31;
            this.mnuPosition.ImageOptions.LargeImageIndex = 10;
            this.mnuPosition.Name = "mnuPosition";
            this.mnuPosition.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuPosition_ItemClick);
            // 
            // mnuImportImage
            // 
            this.mnuImportImage.Caption = "Load Image";
            this.mnuImportImage.Id = 32;
            this.mnuImportImage.ImageOptions.LargeImageIndex = 45;
            this.mnuImportImage.Name = "mnuImportImage";
            this.mnuImportImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuImportImage_ItemClick);
            // 
            // mnuRptSelf
            // 
            this.mnuRptSelf.Caption = "การประเมินตนเอง";
            this.mnuRptSelf.Id = 1;
            this.mnuRptSelf.ImageOptions.LargeImageIndex = 49;
            this.mnuRptSelf.Name = "mnuRptSelf";
            this.mnuRptSelf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRptSelf_ItemClick);
            // 
            // mnuRptCoWork
            // 
            this.mnuRptCoWork.Caption = "การประเมินเพื่อนร่วมงาน";
            this.mnuRptCoWork.Id = 2;
            this.mnuRptCoWork.ImageOptions.LargeImageIndex = 57;
            this.mnuRptCoWork.Name = "mnuRptCoWork";
            this.mnuRptCoWork.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRptCoWork_ItemClick);
            // 
            // barIP
            // 
            this.barIP.Caption = "IP : ";
            this.barIP.Id = 3;
            this.barIP.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barIP.ImageOptions.Image")));
            this.barIP.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barIP.ImageOptions.LargeImage")));
            this.barIP.Name = "barIP";
            // 
            // mnuPrintAsm
            // 
            this.mnuPrintAsm.Caption = "พิมพ์สรุปผลประเมิน";
            this.mnuPrintAsm.Id = 4;
            this.mnuPrintAsm.ImageOptions.LargeImageIndex = 58;
            this.mnuPrintAsm.Name = "mnuPrintAsm";
            this.mnuPrintAsm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuPrintAsm_ItemClick);
            // 
            // mnuHOD
            // 
            this.mnuHOD.Caption = "รายชื่อ HOD";
            this.mnuHOD.Id = 5;
            this.mnuHOD.ImageOptions.ImageIndex = 2;
            this.mnuHOD.ImageOptions.LargeImageIndex = 2;
            this.mnuHOD.LargeWidth = 80;
            this.mnuHOD.Name = "mnuHOD";
            this.mnuHOD.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuHOD_ItemClick);
            // 
            // mnuSystemConfig
            // 
            this.mnuSystemConfig.Caption = "Configuration";
            this.mnuSystemConfig.Id = 6;
            this.mnuSystemConfig.ImageOptions.LargeImageIndex = 14;
            this.mnuSystemConfig.Name = "mnuSystemConfig";
            this.mnuSystemConfig.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuSystemConfig_ItemClick);
            // 
            // mnuFinalGrade
            // 
            this.mnuFinalGrade.Caption = "คะแนนรวมสุทธิ";
            this.mnuFinalGrade.Id = 7;
            this.mnuFinalGrade.ImageOptions.LargeImageIndex = 38;
            this.mnuFinalGrade.Name = "mnuFinalGrade";
            this.mnuFinalGrade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuFinalGrade_ItemClick);
            // 
            // mnuCheckScore
            // 
            this.mnuCheckScore.Caption = "ตรวจสอบคะแนนประเมิน";
            this.mnuCheckScore.Id = 8;
            this.mnuCheckScore.ImageOptions.LargeImageIndex = 59;
            this.mnuCheckScore.Name = "mnuCheckScore";
            this.mnuCheckScore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuCheckScore_ItemClick);
            // 
            // mnuPrintFormEmpNew
            // 
            this.mnuPrintFormEmpNew.Caption = "แบบฟอร์มประเมิน พนักงานใหม่";
            this.mnuPrintFormEmpNew.Id = 9;
            this.mnuPrintFormEmpNew.ImageOptions.LargeImageIndex = 58;
            this.mnuPrintFormEmpNew.Name = "mnuPrintFormEmpNew";
            this.mnuPrintFormEmpNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuPrintFormEmpNew_ItemClick);
            // 
            // pageHome
            // 
            this.pageHome.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.GroupNewEmployee,
            this.GroupAssessment,
            this.GroupNote,
            this.groupAbout});
            this.pageHome.Name = "pageHome";
            this.pageHome.Text = "Home";
            // 
            // GroupNewEmployee
            // 
            this.GroupNewEmployee.ItemLinks.Add(this.mnuAssesseeNew);
            this.GroupNewEmployee.Name = "GroupNewEmployee";
            this.GroupNewEmployee.Text = "ประเมินพนักงานใหม่";
            // 
            // GroupAssessment
            // 
            this.GroupAssessment.ItemLinks.Add(this.menuAssesseeList);
            this.GroupAssessment.ItemLinks.Add(this.mnuEmployeeRelation);
            this.GroupAssessment.ItemLinks.Add(this.mnuCheckScore);
            this.GroupAssessment.Name = "GroupAssessment";
            this.GroupAssessment.Text = "ประเมินพนักงานประจำปี";
            // 
            // GroupNote
            // 
            this.GroupNote.ItemLinks.Add(this.mnuNote);
            this.GroupNote.ItemLinks.Add(this.mnuPrintFormEmpNew);
            this.GroupNote.ItemLinks.Add(this.mnuDownload);
            this.GroupNote.Name = "GroupNote";
            this.GroupNote.Text = "Note";
            // 
            // groupAbout
            // 
            this.groupAbout.ItemLinks.Add(this.mnuLogout);
            this.groupAbout.Name = "groupAbout";
            this.groupAbout.Text = "Help";
            // 
            // pageMaster
            // 
            this.pageMaster.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.pageMaster.Name = "pageMaster";
            this.pageMaster.Text = "Master";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.mnuDivision);
            this.ribbonPageGroup1.ItemLinks.Add(this.mnuDepartment);
            this.ribbonPageGroup1.ItemLinks.Add(this.mnuPosition);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Master";
            // 
            // pageReport
            // 
            this.pageReport.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.GroupManager,
            this.GroupReport});
            this.pageReport.Name = "pageReport";
            this.pageReport.Text = "Report";
            // 
            // GroupManager
            // 
            this.GroupManager.ItemLinks.Add(this.mnuRptSelf);
            this.GroupManager.ItemLinks.Add(this.mnuRptCoWork);
            this.GroupManager.ItemLinks.Add(this.mnuRPTManagerScore);
            this.GroupManager.ItemLinks.Add(this.mnuRPTManagerFinalScore);
            this.GroupManager.ItemLinks.Add(this.mnuPrintAsm);
            this.GroupManager.Name = "GroupManager";
            this.GroupManager.Text = "รายงานสำหรับผู้บังคับบัญชา";
            // 
            // GroupReport
            // 
            this.GroupReport.ItemLinks.Add(this.mnuRptScore);
            this.GroupReport.ItemLinks.Add(this.mnuReportAvg);
            this.GroupReport.ItemLinks.Add(this.mnuRPTFinalScore);
            this.GroupReport.ItemLinks.Add(this.mnuRPTOverStandard);
            this.GroupReport.ItemLinks.Add(this.mnuRPTScoreSubstandard);
            this.GroupReport.ItemLinks.Add(this.mnuRPTAssessmentGrade);
            this.GroupReport.ItemLinks.Add(this.mnuRPTComment);
            this.GroupReport.Name = "GroupReport";
            this.GroupReport.Text = "รายงาน";
            // 
            // pageAdmin
            // 
            this.pageAdmin.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.GroupEmployee,
            this.GroupStatus,
            this.GroupGrade,
            this.GroupConfig});
            this.pageAdmin.Name = "pageAdmin";
            this.pageAdmin.Text = "Admin";
            // 
            // GroupEmployee
            // 
            this.GroupEmployee.ItemLinks.Add(this.mnuEmployee);
            this.GroupEmployee.ItemLinks.Add(this.mnuEmpImport);
            this.GroupEmployee.ItemLinks.Add(this.mnuImportImage);
            this.GroupEmployee.Name = "GroupEmployee";
            this.GroupEmployee.Text = "พนักงาน";
            // 
            // GroupStatus
            // 
            this.GroupStatus.ItemLinks.Add(this.mnuEmpNotAssessment);
            this.GroupStatus.ItemLinks.Add(this.mnuHOD);
            this.GroupStatus.ItemLinks.Add(this.mnuAsmRelation);
            this.GroupStatus.Name = "GroupStatus";
            this.GroupStatus.Text = "สถานะการประเมินประจำปี";
            // 
            // GroupGrade
            // 
            this.GroupGrade.ItemLinks.Add(this.mnuAssessmentGrade);
            this.GroupGrade.ItemLinks.Add(this.mnuEmpWeight);
            this.GroupGrade.ItemLinks.Add(this.mnuFinalGrade);
            this.GroupGrade.Name = "GroupGrade";
            this.GroupGrade.Text = "ตัดเกรด";
            // 
            // GroupConfig
            // 
            this.GroupConfig.ItemLinks.Add(this.mnuSystemConfig);
            this.GroupConfig.Name = "GroupConfig";
            this.GroupConfig.Text = "Setting";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.barContact);
            this.ribbonStatusBar1.ItemLinks.Add(this.barVersion);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.barIP);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 492);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1517, 34);
            // 
            // xtraTabbedMdiManager
            // 
            this.xtraTabbedMdiManager.AppearancePage.Header.Image = global::SUTHPA.Properties.Resources.suthlogo;
            this.xtraTabbedMdiManager.AppearancePage.Header.Options.UseImage = true;
            this.xtraTabbedMdiManager.AppearancePage.PageClient.Image = global::SUTHPA.Properties.Resources.devpost;
            this.xtraTabbedMdiManager.AppearancePage.PageClient.Options.UseImage = true;
            this.xtraTabbedMdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtraTabbedMdiManager.Controller = this.barAndDockingController;
            this.xtraTabbedMdiManager.MdiParent = this;
            this.xtraTabbedMdiManager.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.xtraTabbedMdiManager_PageAdded);
            // 
            // mnuAsmRelation
            // 
            this.mnuAsmRelation.Caption = "Assessment Relation";
            this.mnuAsmRelation.Id = 10;
            this.mnuAsmRelation.ImageOptions.ImageIndex = 4;
            this.mnuAsmRelation.ImageOptions.LargeImageIndex = 4;
            this.mnuAsmRelation.Name = "mnuAsmRelation";
            this.mnuAsmRelation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuAsmRelation_ItemClick);
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1517, 526);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControl;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "Performance Appraisal 2021 : ระบบประเมินพนักงาน";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.BarButtonItem menuAssesseeList;
        private DevExpress.XtraBars.BarStaticItem barHearderUser;
        private DevExpress.XtraBars.BarButtonItem btnChangePassword;
        private DevExpress.XtraBars.BarStaticItem lblStatus;
        private DevExpress.XtraBars.BarStaticItem lblInfoDB;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItem1;
        private DevExpress.XtraBars.BarStaticItem barNameOfUser;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageHome;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupAssessment;
        private DevExpress.XtraBars.BarButtonItem mnuEmployee;
        private DevExpress.XtraBars.BarButtonItem mnuEmpNotAssessment;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupEmployee;
        private DevExpress.XtraBars.BarButtonItem mnuRptScore;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupReport;
        private DevExpress.XtraBars.BarButtonItem mnuReportAvg;
        private DevExpress.XtraBars.BarButtonItem mnuRPTFinalScore;
        private DevExpress.XtraBars.BarButtonItem mnuRPTManagerScore;
        private DevExpress.XtraBars.BarButtonItem mnuRPTManagerFinalScore;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupManager;
        private DevExpress.XtraBars.BarButtonItem mnuEmpWeight;
        private DevExpress.XtraBars.BarButtonItem mnuAssessmentGrade;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupGrade;
        private DevExpress.XtraBars.BarButtonItem mnuNote;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupNote;
        private DevExpress.XtraBars.BarStaticItem barContact;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem barVersion;
        private DevExpress.XtraBars.BarButtonItem mnuRPTScoreSubstandard;
        private DevExpress.XtraBars.BarButtonItem mnuRPTAssessmentGrade;
        private DevExpress.XtraBars.BarButtonItem barLog;
        private DevExpress.XtraBars.BarButtonItem mnuDownload;
        private DevExpress.XtraBars.BarButtonItem mnuRPTOverStandard;
        private DevExpress.XtraBars.BarButtonItem mnuRPTComment;
        private DevExpress.Utils.ImageCollection imageCollection48;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem mnuEmployeeRelation;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageReport;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageAdmin;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupStatus;
        private DevExpress.XtraBars.BarButtonItem mnuLogout;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup groupAbout;
        private DevExpress.XtraBars.BarButtonItem mnuAssesseeNew;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupNewEmployee;
        private DevExpress.XtraBars.BarButtonItem mnuEmpImport;
        private DevExpress.XtraBars.BarButtonItem mnuDivision;
        private DevExpress.XtraBars.BarButtonItem mnuDepartment;
        private DevExpress.XtraBars.BarButtonItem mnuPosition;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageMaster;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem mnuImportImage;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager;
        private DevExpress.XtraBars.BarButtonItem mnuRptSelf;
        private DevExpress.XtraBars.BarButtonItem mnuRptCoWork;
        private DevExpress.XtraBars.BarStaticItem barIP;
        private DevExpress.XtraBars.BarButtonItem mnuPrintAsm;
        private DevExpress.XtraBars.BarButtonItem mnuHOD;
        private DevExpress.XtraBars.BarButtonItem mnuSystemConfig;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup GroupConfig;
        private DevExpress.XtraBars.BarButtonItem mnuFinalGrade;
        private DevExpress.XtraBars.BarButtonItem mnuCheckScore;
        private DevExpress.XtraBars.BarButtonItem mnuPrintFormEmpNew;
        private DevExpress.XtraBars.BarButtonItem mnuAsmRelation;
    }
}

