﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using System.Collections;
using System.IO;

namespace SUTHPA
{
    public partial class frmAssessmentNew : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtPA = new DataTable();
        AssessmentController ctlPA = new AssessmentController();
        public string EmployeeID;
        public int EvaluationUID;
        public string EmployeeName;
        public bool flagEdit = false;
        public int row;

        DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup emp = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();

        public frmAssessmentNew()
        {
            InitializeComponent();
        }


        private void frmAssessmentNew_Load(object sender, EventArgs e)
        {
            try
            {
                this.TopMost = false;
                lblSEQ.Text = "0";
                LoadCourse();
                LoadEmployeeInfo();
                groupControl2.Visible = true;

                if (flagEdit)
                    //if (GlobalVariables.IsHR)
                    //{
                    //    dtPA = ctlPA.Evaluation_GetByAssesseeNew(GlobalVariables.BYear, this.EvaluationUID, EmployeeID, GlobalVariables.AssesseeLevelUID);
                    //}
                    //else
                    //{
                        dtPA = ctlPA.Evaluation_GetByAssesseeNew(GlobalVariables.BYear, this.EvaluationUID, EmployeeID, GlobalVariables.AssesseeLevelUID);
                    //}                    
                else
                    dtPA = ctlPA.Evaluation_Get(GlobalVariables.BYear, this.EvaluationUID, GlobalVariables.AssesseeLevelUID);

                row = dtPA.Rows.Count;
                grdAssessment.DataSource = dtPA;
                //grdViewAssessment.BestFitColumns();
                //viewPAAll.Columns[3].AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                //viewPAAll.Columns[3].Width = 500;
                grdViewAssessment.Columns[0].Width = 30;
                grdViewAssessment.Columns[2].Width = 50;
                grdViewAssessment.Columns[3].Width = 50;
                grdViewAssessment.Columns[4].Width = 50;
                grdViewAssessment.Columns[5].Width = 50;
                //grdViewAssessment.Columns[6].Width = 50;
                //grdViewAssessment.Columns[12].Width = 50;
                //grdViewAssessment.Columns[13].Width = 70;
                //
                grdViewAssessment.BestFitColumns();

                DataTable dtP = new DataTable();
                dtP = ctlPA.AssessmentProbation_Get(EmployeeID);
                if (dtP.Rows.Count > 0)
                {
                    //lblTitle.Text = dtPA.Rows[0]["EvaluationName"].ToString();
                    txtComment.Text = dtP.Rows[0]["Comments"].ToString();
                    txtGood.Text = dtP.Rows[0]["Outstanding"].ToString();
                    txtBad.Text = dtP.Rows[0]["Recessive"].ToString();
                }

                emp.Items.Clear();
                CheckSEQNO();
                pnBottom.Height = 200;
            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }
        private void LoadEmployeeInfo()
        {
            EmployeeController ctlE = new EmployeeController();
            dtPA = ctlE.Employee_GetBannerInfo(GlobalVariables.BYear,EmployeeID);
            
            if (dtPA.Rows.Count > 0)
                {
                    lblEmployeeID.Text = dtPA.Rows[0]["EmployeeID"].ToString();
                    lblEmpName.Text = dtPA.Rows[0]["EmployeeName"].ToString();
                    lblPositionName.Text = dtPA.Rows[0]["PositionName"].ToString();
                    lblDepartmentName.Text = dtPA.Rows[0]["DepartmentName"].ToString();
                    lblHireDate.Text = BaseGlobalClass.DisplayStr2DateTH(dtPA.Rows[0]["HireDate"].ToString());
                    if (File.Exists(@"\\172.100.50.120\Application\PA_SUTH\Employee\" + lblEmployeeID.Text + ".jpg") == true)
                    {
                        picEmp.Image = Image.FromFile(@"\\172.100.50.120\Application\PA_SUTH\Employee\" + lblEmployeeID.Text + ".jpg");
                    //picEmp.Image = Image.FromFile(@"\\172.100.50.120\Application\PA_SUTH\Employee\" + lblEmployeeID.Text + ".jpg");
                }
                    else
                {
                    picEmp.Image = Image.FromFile(@"\\172.100.50.120\Application\PA_SUTH\Employee\noPhoto.jpg");
                }
            }

        }      

        private void LoadCourse()
        {
            dtPA = ctlPA.AssessmentCourse_Get(EmployeeID);
            grdCourse.DataSource = dtPA;

            //grdViewCourse.BestFitColumns();
            grdViewCourse.Columns[0].Width = 30;
            grdViewCourse.Columns[2].Width = 100;
            grdViewCourse.Columns[3].Width = 100;
            grdViewCourse.Columns[4].Width = 125;
            grdViewCourse.Columns[5].Width = 125;
            //grdViewCourse.BestFitColumns();
        }
        int pSEQNO;
        private void CheckSEQNO()
        {
           pSEQNO = ctlPA.AssessmentProbation_GetNextSEQ(EmployeeID);
            lblSEQ.Text = pSEQNO.ToString();
            grdViewAssessment.Columns[12].OptionsColumn.AllowEdit = true;
            grdViewAssessment.Columns[13].OptionsColumn.AllowEdit = true;
            grdViewAssessment.Columns[14].OptionsColumn.AllowEdit = true;
            grdViewAssessment.Columns[15].OptionsColumn.AllowEdit = true;


            grdViewAssessment.Columns[12].OptionsColumn.AllowFocus = true;
            grdViewAssessment.Columns[13].OptionsColumn.AllowFocus = true;
            grdViewAssessment.Columns[14].OptionsColumn.AllowFocus = true;
            grdViewAssessment.Columns[15].OptionsColumn.AllowFocus = true;

            switch (pSEQNO)
            {
                case 1:
                    grdViewAssessment.Columns[12].OptionsColumn.AllowEdit = true;
                    grdViewAssessment.Columns[12].OptionsColumn.AllowFocus = true;
                    grdViewAssessment.Columns[12].AppearanceCell.BackColor = Color.Honeydew;
                    break;
                case 2:
                    grdViewAssessment.Columns[13].OptionsColumn.AllowEdit = true;
                    grdViewAssessment.Columns[13].OptionsColumn.AllowFocus = true;
                    grdViewAssessment.Columns[13].AppearanceCell.BackColor = Color.Honeydew;
                    break;
                case 3:
                    grdViewAssessment.Columns[14].OptionsColumn.AllowEdit = true;
                    grdViewAssessment.Columns[14].OptionsColumn.AllowFocus = true;
                    grdViewAssessment.Columns[14].AppearanceCell.BackColor = Color.Honeydew;
                    break;
                case 4:
                    grdViewAssessment.Columns[15].OptionsColumn.AllowEdit = true;
                    grdViewAssessment.Columns[15].OptionsColumn.AllowFocus = true;
                    grdViewAssessment.Columns[15].AppearanceCell.BackColor = Color.Honeydew;
                    break;
            }

            if (GlobalVariables.IsHR == true)             
            {
                grdViewAssessment.Columns[12].OptionsColumn.AllowEdit = false;
                grdViewAssessment.Columns[13].OptionsColumn.AllowEdit = false;
                grdViewAssessment.Columns[14].OptionsColumn.AllowEdit = false;
                grdViewAssessment.Columns[15].OptionsColumn.AllowEdit = false;


                cmdApprove.Visible = true;
                //pnHR.Visible = true;
            }
            else
            {
                cmdApprove.Visible = false;
                //pnHR.Visible = false;
            }

            pnHR.Visible = true;
        }


        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool CheckZero()
        {
            bool HasZero = false;

            for (int i = 0; i < row; i++)
            {
                switch (pSEQNO)
                {
                    case 1:
                        if (grdViewAssessment.GetRowCellValue(i, colScore1).ToString() == "0")
                        {
                            HasZero = true;
                            break;
                        }
                        else
                            HasZero = false;
                        break;
                    case 2:
                        if (grdViewAssessment.GetRowCellValue(i, colScore2).ToString() == "0")
                        {
                            HasZero = true;
                            break;
                        }
                        else
                            HasZero = false;
                        break;
                    case 3:
                        if (grdViewAssessment.GetRowCellValue(i, colScore3).ToString() == "0")
                        {
                            HasZero = true;
                            break;
                        }
                        else
                            HasZero = false;
                        break;
                    case 4:
                        if (grdViewAssessment.GetRowCellValue(i, colScore4).ToString() == "0")
                        {
                            HasZero = true;
                            break;
                        }
                        else
                            HasZero = false;
                        break;
                    case 5:
                        HasZero = false;
                        break;
                       

                }

                
            }

            return HasZero;
        }
        private void btSave_Click(object sender, EventArgs e)
        {

            for (int i = 0; i <=4; i++)
            {
                //ทำการบันทึก AssessmentCourse
                ctlPA.AssessmentCourse_Save(EmployeeID, Convert.ToInt32(grdViewCourse.GetRowCellValue(i, colCourseUID)), Convert.ToInt32(grdViewCourse.GetRowCellValue(i, colPhaseNo)),  grdViewCourse.GetRowCellValue(i, colDeptApprove).ToString(), grdViewCourse.GetRowCellValue(i, colHRApprove).ToString());
            }

            bool flagStatus = true;
            double NetScore = 0, TotalScore = 0;
            int AssessmentUID = 0;

            //if (!GlobalVariables.IsHR)
            //{

            if (chkModify.Checked==false) //ถ้าไม่ใช่การแก้ไข
                {
                    if (CheckZero() == true)
                    {
                        MessageBox.Show("กรุณาตรวจสอบคะแนน คะแนนต้องไม่มี 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                //string CompetencyGroup = "";
                
                for (int i=1;i<=4;i++)
                {                
                    switch (i) //Convert.ToInt32(lblSEQ.Text)
                {
                        case 1:
                            TotalScore = BaseClass.StrNull2Dbl(grdViewAssessment.Columns[12].SummaryItem.SummaryValue.ToString());
                            NetScore = BaseClass.StrNull2Dbl(txtNetScore1.Text);
                            break;
                        case 2:
                            TotalScore = BaseClass.StrNull2Dbl(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString());
                            NetScore = BaseClass.StrNull2Dbl(txtNetScore2.Text);
                            break;
                        case 3:
                            TotalScore = BaseClass.StrNull2Dbl(grdViewAssessment.Columns[14].SummaryItem.SummaryValue.ToString());
                            NetScore = BaseClass.StrNull2Dbl( txtNetScore3.Text);
                            break;
                        case 4:
                            TotalScore = BaseClass.StrNull2Dbl(grdViewAssessment.Columns[15].SummaryItem.SummaryValue.ToString());
                            NetScore = BaseClass.StrNull2Dbl(txtNetScore4.Text);
                            break;
                    }               

                    AssessmentUID = 0;
                    if (NetScore>0)
                    {              
                        AssessmentUID = ctlPA.AssessmentProbation_Save(GlobalVariables.username, EmployeeID, this.EvaluationUID, TotalScore, NetScore, i, txtGood.Text, txtBad.Text, txtComment.Text);
                    }
                }
            //}
            //double FinalWeight = ctlPA.LevelScore_Get(GlobalVariables.BYear, GlobalVariables.AssesseeLevelUID, this.EvaluationUID);
            //double FinalScore = (BaseClass.StrNull2Dbl(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString()) * FinalWeight)/ 100;

            //ctlPA.AssessmentGrade_Save(GlobalVariables.BYear, EmployeeID, this.EvaluationUID, FinalScore, GlobalVariables.username);

            if (!GlobalVariables.IsHR)
             {
                    for (int i = 0; i < row; i++)
                {
                    if (grdViewAssessment.GetRowCellValue(i, colScore1).ToString().Length <= 0)
                    {
                        flagStatus = false;
                        break;
                    }
                    else
                        flagStatus = true;
                }
         
                if (flagStatus)
                {

                    for (int n = 1; n <= 4; n++) //คอลัมน์เดือน
                    {

                        for (int i = 0; i < row; i++)
                        {
                            //ทำการบันทึก AssessmentScore
                            int iScore = 0;
                            switch (n)
                            {
                                case 1:
                                    iScore = Convert.ToInt32(grdViewAssessment.GetRowCellValue(i, colScore1).ToString());
                                    break;
                                case 2:
                                    iScore = Convert.ToInt32(grdViewAssessment.GetRowCellValue(i, colScore2).ToString());
                                    break;
                                case 3:
                                    iScore = Convert.ToInt32(grdViewAssessment.GetRowCellValue(i, colScore3).ToString());
                                    break;
                                case 4:
                                    iScore = Convert.ToInt32(grdViewAssessment.GetRowCellValue(i, colScore4).ToString());
                                    break;
                            }

                            if (iScore > 0)
                            {
                                ctlPA.AssessmentNewScore_Save(GlobalVariables.BYear, EmployeeID, grdViewAssessment.GetRowCellValue(i, colCompetencyUID).ToString(),iScore,n, GlobalVariables.username);
                            }
                        }

                    }
                    MessageBox.Show("บันทึกคะแนนประเมินเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);



                    GlobalVariables.FlagRefresh = true;
                    this.Close();
                }
                else
                    MessageBox.Show("กรุณาประเมินให้ครบทุกข้อ", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("บันทึกเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //private void txtScore1_Leave(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        double MaxValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
        //        double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
        //        double Score =  Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colScore1).ToString());
        //        double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

        //        if (Score < MinValue || Score > MaxValue)
        //        {                   
        //                MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            grdViewAssessment.SetFocusedRowCellValue(colScore1, "");
        //                grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;                                        
        //        }
        //        else
        //        {
        //            grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle , colNetScore, (Score * Weight) / MaxValue);
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;                   
        //        }
        //    }
        //    catch { }
        //}

        //private void txtScore2_Leave(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        double MaxValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
        //        double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
        //        double Score = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colScore2).ToString());
        //        double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

        //        if (Score < MinValue || Score > MaxValue)
        //        {
        //            MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            grdViewAssessment.SetFocusedRowCellValue(colScore2, "");
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;
        //        }
        //        else
        //        {
        //            grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colNetScore, (Score * Weight) / MaxValue);
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
        //        }
        //    }
        //    catch { }
        //}
        //private void txtScore3_Leave(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        double MaxValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
        //        double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
        //        double Score = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colScore3).ToString());
        //        double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

        //        if (Score < MinValue || Score > MaxValue)
        //        {
        //            MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            grdViewAssessment.SetFocusedRowCellValue(colScore3, "");
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;
        //        }
        //        else
        //        {
        //            grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colNetScore, (Score * Weight) / MaxValue);
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
        //        }
        //    }
        //    catch { }
        //}
        //private void txtScore4_Leave(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        double MaxValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
        //        double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
        //        double Score = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colScore4).ToString());
        //        double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

        //        if (Score < MinValue || Score > MaxValue)
        //        {
        //            MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            grdViewAssessment.SetFocusedRowCellValue(colScore4, "");
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;
        //        }
        //        else
        //        {
        //            grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colNetScore, (Score * Weight) / MaxValue);
        //            grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
        //        }
        //    }
        //    catch { }
        //}

        private void CellValidate(object sender,DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            try
            {
                double MaxValue =  Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
                double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
                double Score = Convert.ToDouble(e.Value);
                double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

                if (Score < MinValue || Score > MaxValue)
                {
                    MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    grdViewAssessment.SetFocusedRowCellValue(grdViewAssessment.FocusedColumn, "");
                    grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;
                }
                else
                {
                    grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, grdViewAssessment.FocusedColumn, e.Value);
                    grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colNetScore, (Score * Weight) / MaxValue);
                    grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
            catch { }
        }
    
        string[] strGroupName;
        //string[,] grpSum=new string[4,2];
        private void grdViewAssessment_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;

            info.GroupText = info.GroupValueText;
            if (info.Level == 0)
            {
                strGroupName = info.GroupValueText.Split(':');                
                if (strGroupName.Length>1)
                {
                    info.GroupText = info.Column.Caption + "<color=#000066><b><size=12>" + strGroupName[0] + "</size></b></color><color=#333333> : " + strGroupName[1] + "</color>";
                }
                else
                {
                    info.GroupText = info.Column.Caption + "<color=#000066><b><size=12>" + strGroupName[0] + "</size></b></color>";
                }
            }
        }
        double netScore1, netScore2, netScore3, netScore4;
        double grpScore1, grpScore2, grpScore3, grpScore4;
        double sc1,sc2,sc3,sc4, w;

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            //progressLoad.Visible = true;
            //progressLoad.LookAndFeel.SkinName = "McSkin";
            //System.Threading.Thread.Sleep(2000);              
            GlobalVariables.FagRPT = "ASSNEW";
            GlobalVariables.Reportskey = "ASSNEW";          
            GlobalVariables.ReportName = "รายงานคะแนนประเมิน";
            GlobalVariables.RPTTYPE = "RPT";
         

            frmReportViewer frm = new frmReportViewer();
            frm.BYear = GlobalVariables.BYear.ToString();
            frm.EmployeeID = lblEmployeeID.Text;

            frm.MdiParent = this.MdiParent;
            frm.Show();
            frm.TopMost = true;

            //progressLoad.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }

        private void cmdApprove_Click(object sender, EventArgs e)
        {
            //ทำการบันทึก Approve
            //ctlPA.Assessment_ApproveByHR(EmployeeID,  Convert.ToInt32(pSEQNO));
            if (cmdApprove.Text == "Approve")
            {
                ctlPA.Probation_Approve(EmployeeID);
                cmdApprove.Text = "Cancel Approve";
            }
            else
            {
                ctlPA.Probation_CancelApprove(EmployeeID);
                cmdApprove.Text = "Approve";
            }
            
        }

        private void grdViewCourse_RowStyle(object sender, RowStyleEventArgs e)
        {
            try
            {
                if (grdViewCourse.GetRowCellValue(e.RowHandle, colisRowActive).ToString() == "Y")
                {
                    e.Appearance.ForeColor = Color.Black;
                    grdViewCourse.OptionsSelection.EnableAppearanceFocusedRow = true;
                    grdViewCourse.OptionsSelection.EnableAppearanceFocusedCell = true;                   
                }
                else
                {
                    e.Appearance.ForeColor = Color.Gray;
                    grdViewCourse.OptionsSelection.EnableAppearanceFocusedRow = false;
                    grdViewCourse.OptionsSelection.EnableAppearanceFocusedCell = false;
                }
            }
            catch { }
        }

        private void grdViewAssessment_RowStyle(object sender, RowStyleEventArgs e)
        {
            //GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {

                if (grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[12]) != null)
                {
                    if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[12]).ToString())!=0)
                    {                  

                        if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[12]).ToString()) < 3)
                            {
                                e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCCCC");
                                e.Appearance.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                            }
                        //else
                        //{

                        //    e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#fbc8c8");
                        //}                  
                    }
                }

                if (grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[13]) != null)
                {
                    if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[13]).ToString()) != 0)
                    {
                        if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[13]).ToString()) < 3)
                        {
                            e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCCCC");
                            e.Appearance.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                        }              
                    }
                }
                if (grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[14]) != null)
                {
                    if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[14]).ToString()) != 0)
                    {

                        if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[14]).ToString()) < 3)
                        {
                            e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCCCC");
                            e.Appearance.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                        }                
                    }
                }
                if (grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[15]) != null)
                {
                    if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[15]).ToString()) != 0)
                    {

                        if (BaseClass.StrNull2Zero(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[15]).ToString()) < 3)
                        {
                            e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCCCC");
                            e.Appearance.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                        }                
                    }
                }

            }

        }

        private void grdViewAssessment_KeyDown(object sender, KeyEventArgs e)
        {
            if (grdViewAssessment.FocusedColumn.FieldName=="Score1")
            {
                if (e.KeyCode==Keys.Enter)
                {
                    grdViewAssessment.MoveNext();
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
            if (grdViewAssessment.FocusedColumn.FieldName == "Score2")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    grdViewAssessment.MoveNext();
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
            if (grdViewAssessment.FocusedColumn.FieldName == "Score3")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    grdViewAssessment.MoveNext();
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
            if (grdViewAssessment.FocusedColumn.FieldName == "Score4")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    grdViewAssessment.MoveNext();
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }

        }

        private void grdViewAssessment_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {            
            if (e.FocusedRowHandle<0)
            {
                grdViewAssessment.MoveNext();
            }
        }

        private void grdViewAssessment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (grdViewAssessment.FocusedColumn.FieldName == "Score")
            {
                int cInt = Convert.ToInt32(e.KeyChar);
                if ((cInt>=48 && cInt<=53) || cInt==8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }
        
        private void grdViewCourse_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "DeptApproved")
            {
                if (grdViewCourse.GetRowCellValue(e.RowHandle, colisRowActive).ToString() == "N")
                {
                    //ซ่อน Control
                    e.RepositoryItem = emp;
                }
            }


            if (e.Column.FieldName == "HRApproved")
            {                 
                if (grdViewCourse.GetRowCellValue(e.RowHandle,"isHR") != null)
                {
                    if (grdViewCourse.GetRowCellValue(e.RowHandle, "isHR").ToString() == "N")
                    {
                        //ซ่อน Control
                        e.RepositoryItem = emp;
                        
                    }

                }
                

                if (GlobalVariables.IsHR == true)
                {
                    e.Column.OptionsColumn.AllowEdit = true;
                }
                else
                {
                    e.Column.OptionsColumn.AllowEdit = false;
                }


                //if (grdViewCourse.GetRowCellValue(e.RowHandle, "HRApproved") != null)
                //{
                //    if (grdViewCourse.GetRowCellValue(e.RowHandle, "HRApproved").ToString() == "Y")
                //    {
                //        //ซ่อน Control
                //        e.Column.OptionsColumn.AllowEdit = false;

                //        switch (grdViewCourse.GetRowCellValue(e.RowHandle, "PhaseNo").ToString())
                //        {
                //            case "1":
                //                grdViewCourse.Columns[12].OptionsColumn.AllowEdit = false;
                //                break;
                //            case "2":
                //                grdViewCourse.Columns[13].OptionsColumn.AllowEdit = false;
                //                break;
                //            case "3":
                //                grdViewCourse.Columns[14].OptionsColumn.AllowEdit = false;
                //                break;
                //            case "4":
                //                grdViewCourse.Columns[15].OptionsColumn.AllowEdit = false;
                //                break;
                //        }


                //    }

                //}

            }
        }


        double ptScore1, ptScore2, ptScore3, ptScore4;

        private void CalScore()
        {

            //double FinalWeight = ctlPA.LevelScore_Get(GlobalVariables.BYear, GlobalVariables.AssesseeLevelUID, this.EvaluationUID);
            //double FinalScore = (Convert.ToDouble(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString()) * FinalWeight) / 100;
            txtNetScore1.Text = ((Convert.ToDouble(grdViewAssessment.Columns[12].SummaryItem.SummaryValue.ToString()) * 100) / 35).ToString("##.##");
            txtNetScore2.Text = ((Convert.ToDouble(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString()) * 100) / 35).ToString("##.##");
            txtNetScore3.Text = ((Convert.ToDouble(grdViewAssessment.Columns[14].SummaryItem.SummaryValue.ToString()) * 100) / 35).ToString("##.##");
            txtNetScore4.Text = ((Convert.ToDouble(grdViewAssessment.Columns[15].SummaryItem.SummaryValue.ToString()) * 100) / 35).ToString("##.##");
        }
        private void grdViewAssessment_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            GridView view = sender as GridView;
            int summaryID = Convert.ToInt32((e.Item as GridSummaryItem).Tag);
            
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {
                netScore1 = 0;
                grpScore1 = 0;
                ptScore1 = 0;
                netScore2 = 0;
                grpScore2 = 0;
                ptScore2 = 0;
                netScore3 = 0;
                grpScore3 = 0;
                ptScore3 = 0;
                netScore4 = 0;
                grpScore4 = 0;
                ptScore4 = 0;
            }

            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                switch (summaryID)
                {
                    case 1:
                        sc1 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score1").ToString());
                        sc2 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score2").ToString());
                        sc3 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score3").ToString());
                        sc4 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score4").ToString());


                        w = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "WeightScore").ToString());

                        netScore1 += (sc1 * w) / 5;
                        ptScore1 = ptScore1 + sc1;

                        netScore2 += (sc2 * w) / 5;
                        ptScore2 = ptScore2 + sc2;

                        netScore3 += (sc3 * w) / 5;
                        ptScore3 = ptScore3 + sc3;

                        netScore4 += (sc4 * w) / 5;
                        ptScore4 = ptScore4 + sc4;

                        break;
                    case 2:
                        sc1 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score1").ToString());
                        sc2 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score2").ToString());
                        sc3 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score3").ToString());
                        sc4 = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score4").ToString());
                        w = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "WeightScore").ToString());
                        grpScore1 += (sc1 * w) / 5;
                        grpScore2 += (sc1 * w) / 5;
                        grpScore3 += (sc1 * w) / 5;
                        grpScore4 += (sc1 * w) / 5;
                        break;
                }
            }

            //if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            //{
            //    switch (summaryID)
            //    {
            //        case 1:
            //            //e.TotalValue = netScore1;
            //            txtNetScore1.Text = ((ptScore1 * 100) / 35).ToString("##.##");
            //            txtNetScore2.Text = ((ptScore2 * 100) / 35).ToString("##.##");
            //            txtNetScore3.Text = ((ptScore3 * 100) / 35).ToString("##.##");
            //            txtNetScore4.Text = ((ptScore4 * 100) / 35).ToString("##.##");
            //            break;
            //        case 2:
            //            //e.TotalValue = grpScore1;
            //            //grpSum[k, 0] = grpScore.ToString();
            //            //grpSum[k, 1] = view.GetRowCellValue(e.RowHandle, "CompetencyGroupUID").ToString();
            //            //k++;                        
            //            break;
                   
            //    }
            //}
            CalScore();
        }
    }
}