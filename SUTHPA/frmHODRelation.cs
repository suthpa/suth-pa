﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SUTHPA
{
    public partial class frmHODRelation : DevExpress.XtraEditors.XtraForm
    {
        public frmHODRelation()
        {
            InitializeComponent();
        } 

        DataTable dtAssessee = new DataTable();
        EmployeeController ctlEmp = new EmployeeController();
  
        private void frmHODRelation_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            GlobalVariables.FlagRefresh = false; 
            //dtAssessee.Columns.Clear();

            //dtAssessee.Columns.Add("EmployeeID");
            //dtAssessee.Columns.Add("EmployeeName");
            //dtAssessee.Columns.Add("PositionName");
            //dtAssessee.Columns.Add("DepartmentName");
            //dtAssessee.Columns.Add("DivisionName");
            //dtAssessee.Columns.Add("EmployeeLevelID");
            //dtAssessee.Columns.Add("DepartmentUID");
            //dtAssessee.Columns.Add("DivisionUID");
            //dtAssessee.Columns.Add("LevelName");
            //dtAssessee.Columns.Add("GroupName");
            //dtAssessee.Columns.Add("isAsm");
            //dtAssessee.Columns.Add("AsmRemark");
            //dtAssessee.Columns.Add("NameCompetency");

            LoadRelation();
        }

              private void LoadRelation()
        {
            dtAssessee.Rows.Clear();
            dtAssessee = ctlEmp.RPT_HODRelation(GlobalVariables.BYear); 

            grdEmployee.DataSource = null;
            grdEmployee.DataSource = dtAssessee;
            
            grdViewEmployee.Columns[2].AppearanceCell.ForeColor = Color.Blue;
            grdViewEmployee.BestFitColumns();
        }

       
        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}