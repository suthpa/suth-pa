﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
    public partial class frmReportCondition : DevExpress.XtraEditors.XtraForm
    {
        public frmReportCondition()
        {
            InitializeComponent();
        }

        AssessmentController ctlP = new AssessmentController();
        EmployeeController  ctlE = new EmployeeController();
        private void frmReportCondition_Load(object sender, EventArgs e)
        {
            progressLoad.Visible = false;
            cboYear.Properties.DataSource = ctlP.Assessment_GetYear();
            cboYear.Properties.DisplayMember = "BYear";
            cboYear.Properties.ValueMember = "BYear";

            cboYear.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            cboYear.Properties.DropDownRows = 3;
            cboYear.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BYear", 100, "");
            cboYear.Properties.Columns.Add(col1);

            cboYear.EditValue = GlobalVariables.BYear;
            lblReportTitle.Text = GlobalVariables.ReportTitle;

            cboLevel.Visible = false;
            lblLevelLabel.Visible = false;
            cboDirection.Visible = false;
            lblDirection.Visible = false;
            cboDivision.Visible = false;
            lblDivision.Visible = false;
            lblDepartment.Visible = false;
            cboDepartment.Visible = false;

            LoadDirection();
            LoadDivision();
            LoadDepartment();
            LoadLevel();

            cboLevel.EditValue = 0;
            cboDirection.EditValue = 0;
            cboDivision.EditValue = 0;
            cboDepartment.EditValue = 0;

            //หัวหน้ากลุ่มงาน
            if (GlobalVariables.IsDirector)
            {
                DataTable dt = new DataTable();
                dt = ctlE.Director_GetDirection(GlobalVariables.username);
                cboDirection.EditValue = dt.Rows[0]["DirectionUID"];
                //cboDirection.Enabled = false;
                //cboDirection.Visible = true;
                //lblDirection.Visible = true;
                //cboDirection.Enabled = true;
                //lblDirection.Enabled = true;
            }    
            else
            {

                DataTable dt = new DataTable();
                dt = ctlE.Employee_GetEmployeeDirection(GlobalVariables.BYear,GlobalVariables.username);
                cboDirection.EditValue = dt.Rows[0]["DirectionUID"];
                //cboDirection.Enabled = false;
                //cboDirection.Visible = true;
                //lblDirection.Visible = true;
                //cboDirection.Enabled = true;
                //lblDirection.Enabled = true;
              
            }

            if (GlobalVariables.IsAdmin || GlobalVariables.IsSuperAdmin)
            {
                cboLevel.Visible = true;
                lblLevelLabel.Visible = true;
            }

            switch (GlobalVariables.FagRPT)
            {
                case "SCORE":
                    LoadLevel();
                    cboLevel.Visible = true;
                    lblLevelLabel.Visible = true;
                    break;
                case "FINAL":
                    cboLevel.Visible = false;
                    lblLevelLabel.Visible = false;

                    cboDirection.Visible = true;
                    lblDirection.Visible = true;
                    cboDivision.Visible = true;
                    lblDivision.Visible = true;
                    lblDepartment.Visible = true;
                    cboDepartment.Visible = true;

                    break;
                case "DIRSCORE": case "DIRFINAL":

                    cboDirection.Visible = true;
                    lblDirection.Visible = true;
                    cboLevel.Visible = false;
                    lblLevelLabel.Visible = false;
                    lblDepartment.Visible = false;
                    cboDepartment.Visible = false;

                    break;

                case "MANSCORE": case "MANFINAL": case "SUPERFINAL":
                case "SELFSCORE":
                case "COSCORE":
                        cboLevel.Visible = false;
                        lblLevelLabel.Visible = false;

                        lblDepartment.Visible = false;
                        cboDepartment.Visible = false;                                      
                 
                    LoadDivision();
                    
                    //if (GlobalVariables.IsAdmin)
                    //{
                    //        cboLevel.Visible = true;
                    //        lblLevelLabel.Visible = true;
                    //}
                    //else
                    //{
                        DataTable dt = new DataTable();
                        dt = ctlE.Employee_GetEmployeeDivision(GlobalVariables.username);
                        cboDivision.EditValue = dt.Rows[0]["DivisionUID"];
                    lblDirection.Visible = true;
                    cboDirection.Visible = true;
                    lblDivision.Visible = true;
                    cboDivision.Visible = true;


                    if (GlobalVariables.IsAdmin || GlobalVariables.IsSuperAdmin)
                    {
                        cboLevel.Visible = false;
                        lblLevelLabel.Visible = false;
                        cboDirection.Visible = false;
                        lblDirection.Visible = false;
                        cboDivision.Visible = false;
                        lblDivision.Visible = false;
                        lblDepartment.Visible = false;
                        cboDepartment.Visible = false;
                    }

                    break;
                case "EMPWEIGHT":
                    cboLevel.Visible = false;
                    lblLevelLabel.Visible = false;
                    break;
                case "SUBSTANDARD":case "GRADE":case "OVERSTANDARD":
                    cboLevel.Visible = false;
                    lblLevelLabel.Visible = false;
                    GlobalVariables.RPTTYPE = "RPT";
                    break;
                default:
                    LoadDirection();
                    LoadDivision();
                    LoadDepartment();

                    cboLevel.Visible = true;
                    lblLevelLabel.Visible = true;
                    cboDirection.Visible = true;
                    lblDirection.Visible = true;
                    cboDivision.Visible = true;
                    lblDivision.Visible = true;
                    lblDepartment.Visible = true;
                    cboDepartment.Visible = true; 
                    break;
            }

            switch  (GlobalVariables.AssessorLevelUID)
            {
                case 1:                               
                    DataTable dt1 = new DataTable();
                    dt1 = ctlE.Director_GetDirection(GlobalVariables.username);
                    cboDirection.EditValue = dt1.Rows[0]["DirectionUID"];
                    cboDirection.Enabled = false;
                    cboDirection.Visible = true;
                    lblDirection.Visible = true; 
                    break;
                case 2:
                    DataTable dt2 = new DataTable();
                    dt2 = ctlE.Employee_GetEmployeeDivision(GlobalVariables.username);
                    cboDivision.EditValue = dt2.Rows[0]["DivisionUID"];
                    cboDivision.Enabled = false;
                    cboDivision.Visible = true;
                    lblDivision.Visible = true;

                    cboDirection.Enabled = false; 

                    break;

                case 3:
                    DataTable dt3 = new DataTable();
                    dt3 = ctlE.Employee_GetEmployeeDepartment(GlobalVariables.username);
                    cboDepartment.EditValue = dt3.Rows[0]["DepartmentUID"];
                    cboDepartment.Enabled = false;
                    cboDepartment.Visible = true;
                    lblDepartment.Visible = true;

                    cboDirection.Enabled = false;
                    cboDivision.Enabled = false;
                    break;
            }



        }
        private void LoadDirection()
        {

            cboDirection.Properties.Columns.Clear();
            cboDirection.Properties.DataSource = ctlE.Employee_GetDirection(GlobalVariables.BYear, GlobalVariables.username);
            cboDirection.Properties.DisplayMember = "Name";
            cboDirection.Properties.ValueMember = "UID";
 
            cboDirection.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            cboDirection.Properties.DropDownRows = 10;
            cboDirection.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            cboDirection.Properties.Columns.Add(col3);
        
            cboDirection.Width = 250;
                               
        }
        private void LoadDivision()
        {
            //DataTable dt = new DataTable();

            //if (GlobalVariables.IsAdmin)
            //{
            cboDivision.Properties.Columns.Clear();
            cboDivision.Properties.DataSource = ctlE.Employee_GetDivisionByDirection(Convert.ToInt32(cboDirection.EditValue));
                cboDivision.Properties.DisplayMember = "DivisionName";
                cboDivision.Properties.ValueMember = "DivisionUID";
     
                cboDivision.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
                cboDivision.Properties.DropDownRows = 10;
                cboDivision.Properties.ShowFooter = false;
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DivisionName", 300, "");
                cboDivision.Properties.Columns.Add(col3);
                cboDivision.EditValue = 0;
           
                cboDivision.Width = 250;
            //}
            //else
            //{
               
            //    dt = ctlE.Employee_GetEmployeeDivision(GlobalVariables.username);
            //    lblDivisionName.Text = dt.Rows[0].Field<string>("DivisionName");
            //    lblUID.Text = dt.Rows[0]["DivisionUID"].ToString();
            //    lblLevelLabel.Text = "Division";
            //}




           //dt = null;                      
        }

        private void LoadDepartment()
        {
            cboDepartment.Properties.Columns.Clear();
            cboDepartment.Properties.DataSource = ctlE.Employee_GetDepartmentByDivision(Convert.ToInt32(cboDivision.EditValue));
            cboDepartment.Properties.DisplayMember = "DepartmentName";
            cboDepartment.Properties.ValueMember = "DepartmentUID";

            cboDepartment.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            cboDepartment.Properties.DropDownRows = 10;
            cboDepartment.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", 300, "");
            cboDepartment.Properties.Columns.Add(col3);
            cboDepartment.EditValue = 0;
            cboDepartment.Width = 250;

        }
        private void LoadLevel()
        {
            cboLevel.Properties.Columns.Clear();
            cboLevel.Properties.DataSource = ctlE.Employee_GetLevel();
            cboLevel.Properties.DisplayMember = "LevelName";
            cboLevel.Properties.ValueMember = "LevelID";
            lblLevelLabel.Text = "ระดับ";

            cboLevel.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            cboLevel.Properties.DropDownRows = 10;
            cboLevel.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col2 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LevelName", 200, "");
            cboLevel.Properties.Columns.Add(col2);
            cboLevel.EditValue = 0;
        }
        private void cmdView_Click(object sender, EventArgs e)
        {
            progressLoad.Visible = true;
            progressLoad.LookAndFeel.SkinName = "McSkin";
            //System.Threading.Thread.Sleep(2000);              

            GlobalVariables.Reportskey = "SCORE";
            GlobalVariables.RPTTYPE = "XLS";
            GlobalVariables.ReportName = "รายงานคะแนนประเมิน";

            if (GlobalVariables.FagRPT == "AVG")
            {
                ctlE.GEN_Employee_FinalScoreByAssessor(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero((cboLevel.EditValue.ToString())));
            }

            if (GlobalVariables.FagRPT == "FINAL")
            {
                ctlE.GEN_EmployeeAssessmentFinalScore(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero((cboDepartment.EditValue.ToString())));
            }


            if (GlobalVariables.FagRPT == "DIRSCORE") //รายงานคะแนนสำหรับ ผจก. 
            {
                ctlE.GEN_EmployeeAssessmentFinalScoreDirection(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero((cboDirection.EditValue.ToString())));
                GlobalVariables.RPTTYPE = "RPT";
            }

            if (GlobalVariables.FagRPT == "DIRFINAL")
            {
                ctlE.GEN_EmployeeAssessmentFinalScoreDirection(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero((cboDirection.EditValue.ToString())));
                GlobalVariables.RPTTYPE = "RPT";
            }



            if (GlobalVariables.FagRPT == "MANSCORE") //รายงานคะแนนสำหรับ ผจก. 
            {
                ctlE.GEN_EmployeeAssessmentFinalScoreDivision(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero((cboDivision.EditValue.ToString())));
                GlobalVariables.RPTTYPE = "RPT";
            }
            if (GlobalVariables.FagRPT == "MANFINAL")
            {
                ctlE.GEN_EmployeeAssessmentFinalScoreDivision(BaseClass.StrNull2Zero(cboYear.EditValue.ToString()), BaseClass.StrNull2Zero((cboDivision.EditValue.ToString())));
                GlobalVariables.RPTTYPE = "RPT";
            }

            if (GlobalVariables.FagRPT == "SUPERFINAL")
            {
                GlobalVariables.RPTTYPE = "RPT";
            }




            frmReportViewer frm = new frmReportViewer();
            frm.BYear = cboYear.EditValue.ToString();

            if (GlobalVariables.FagRPT == "EMPWEIGHT")
            {
                
            }
            else
            {               
           
                frm.LevelUID = string.Concat(cboLevel.EditValue);
                frm.DeptUID = string.Concat(cboDepartment.EditValue);
                frm.DivisionUID = string.Concat(cboDivision.EditValue);
                frm.DirectionUID = string.Concat(cboDirection.EditValue);
            }

            frm.MdiParent = this.MdiParent;
            frm.Show();
  
            progressLoad.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboDirection_EditValueChanged(object sender, EventArgs e)
        {
            LoadDivision();
        }

        private void cboDivision_EditValueChanged(object sender, EventArgs e)
        {
            LoadDepartment();
        }
    }
}