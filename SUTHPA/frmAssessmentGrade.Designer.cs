﻿namespace SUTHPA
{
    partial class frmAssessmentGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblTitlePA = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.pScore = new System.Windows.Forms.Panel();
            this.grdAssessment = new DevExpress.XtraGrid.GridControl();
            this.grdViewAssessment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDivision = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPositionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevelName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoreComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuncComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeaderComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoreComTest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtCoreCom = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtExR = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCQI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtCQI = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colQA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtQA = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colICRTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtICRTeam = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colICR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtICR = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDiscipline = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtDiscipline = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTraining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtTraining = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCheckup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtCheckup = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActivity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtActivity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoreCom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCQI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtICRTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtICR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscipline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivity)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1540, 40);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblTitlePA);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.HotTrack;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1540, 40);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // lblTitlePA
            // 
            this.lblTitlePA.AutoSize = true;
            this.lblTitlePA.BackColor = System.Drawing.Color.Transparent;
            this.lblTitlePA.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold);
            this.lblTitlePA.ForeColor = System.Drawing.Color.White;
            this.lblTitlePA.Location = new System.Drawing.Point(10, 2);
            this.lblTitlePA.Name = "lblTitlePA";
            this.lblTitlePA.Size = new System.Drawing.Size(479, 28);
            this.lblTitlePA.TabIndex = 3;
            this.lblTitlePA.Text = "Performance Evaluation Grade : คะแนนรวมตัดเกรด";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1508, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(30, 36);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 377);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1540, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1310, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 40);
            this.panel1.TabIndex = 33;
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.Location = new System.Drawing.Point(120, 5);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(97, 30);
            this.btClose.TabIndex = 30;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pScore
            // 
            this.pScore.Controls.Add(this.grdAssessment);
            this.pScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pScore.Location = new System.Drawing.Point(0, 40);
            this.pScore.Name = "pScore";
            this.pScore.Size = new System.Drawing.Size(1540, 337);
            this.pScore.TabIndex = 1;
            // 
            // grdAssessment
            // 
            this.grdAssessment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAssessment.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdAssessment.Location = new System.Drawing.Point(0, 0);
            this.grdAssessment.MainView = this.grdViewAssessment;
            this.grdAssessment.Name = "grdAssessment";
            this.grdAssessment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtCoreCom,
            this.txtExR,
            this.txtCQI,
            this.txtQA,
            this.txtICRTeam,
            this.txtICR,
            this.txtDiscipline,
            this.txtTraining,
            this.txtCheckup,
            this.txtActivity});
            this.grdAssessment.Size = new System.Drawing.Size(1540, 337);
            this.grdAssessment.TabIndex = 0;
            this.grdAssessment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewAssessment});
            // 
            // grdViewAssessment
            // 
            this.grdViewAssessment.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdViewAssessment.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewAssessment.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.FooterPanel.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewAssessment.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewAssessment.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewAssessment.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewAssessment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewAssessment.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewAssessment.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.grdViewAssessment.Appearance.Row.Options.UseFont = true;
            this.grdViewAssessment.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewAssessment.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewAssessment.ColumnPanelRowHeight = 25;
            this.grdViewAssessment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDivision,
            this.colDepartment,
            this.colEmployeeID,
            this.colEmployeeName,
            this.colPositionName,
            this.colLevelName,
            this.colCoreComp,
            this.colFuncComp,
            this.colLeaderComp,
            this.colCoreComTest,
            this.colExR,
            this.colCQI,
            this.colQA,
            this.colICRTeam,
            this.colICR,
            this.colDiscipline,
            this.colTraining,
            this.colCheckup,
            this.colActivity,
            this.colTotalScore});
            this.grdViewAssessment.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewAssessment.GridControl = this.grdAssessment;
            this.grdViewAssessment.GroupCount = 2;
            this.grdViewAssessment.GroupFormat = "{0} [#image]{1} {2}";
            this.grdViewAssessment.Name = "grdViewAssessment";
            this.grdViewAssessment.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsBehavior.AllowGroupExpandAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsFilter.AllowColumnMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterEditor = false;
            this.grdViewAssessment.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.grdViewAssessment.OptionsFilter.AllowMRUFilterList = false;
            this.grdViewAssessment.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewAssessment.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewAssessment.OptionsFind.AlwaysVisible = true;
            this.grdViewAssessment.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewAssessment.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewAssessment.OptionsView.EnableAppearanceEvenRow = true;
            this.grdViewAssessment.OptionsView.EnableAppearanceOddRow = true;
            this.grdViewAssessment.OptionsView.RowAutoHeight = true;
            this.grdViewAssessment.OptionsView.ShowGroupPanel = false;
            this.grdViewAssessment.OptionsView.ShowIndicator = false;
            this.grdViewAssessment.RowHeight = 25;
            this.grdViewAssessment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDivision, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartment, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDivision
            // 
            this.colDivision.Caption = "ฝ่าย";
            this.colDivision.FieldName = "DivisionName";
            this.colDivision.Name = "colDivision";
            this.colDivision.OptionsColumn.AllowEdit = false;
            this.colDivision.OptionsColumn.AllowFocus = false;
            this.colDivision.Visible = true;
            this.colDivision.VisibleIndex = 0;
            // 
            // colDepartment
            // 
            this.colDepartment.Caption = "แผนก";
            this.colDepartment.FieldName = "DepartmentName";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.AllowFocus = false;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 0;
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "รหัสพนักงาน";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.AllowShowHide = false;
            this.colEmployeeID.Visible = true;
            this.colEmployeeID.VisibleIndex = 0;
            this.colEmployeeID.Width = 80;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "ชื่อ - นามสกุล";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.VisibleIndex = 1;
            this.colEmployeeName.Width = 224;
            // 
            // colPositionName
            // 
            this.colPositionName.AppearanceCell.Options.UseTextOptions = true;
            this.colPositionName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPositionName.AppearanceHeader.Options.UseTextOptions = true;
            this.colPositionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPositionName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPositionName.Caption = "ตำแหน่ง";
            this.colPositionName.FieldName = "PositionName";
            this.colPositionName.Name = "colPositionName";
            this.colPositionName.OptionsColumn.AllowEdit = false;
            this.colPositionName.OptionsColumn.AllowFocus = false;
            this.colPositionName.OptionsFilter.AllowAutoFilter = false;
            this.colPositionName.OptionsFilter.AllowFilter = false;
            this.colPositionName.Visible = true;
            this.colPositionName.VisibleIndex = 2;
            this.colPositionName.Width = 224;
            // 
            // colLevelName
            // 
            this.colLevelName.Caption = "ระดับ";
            this.colLevelName.FieldName = "LevelName";
            this.colLevelName.Name = "colLevelName";
            this.colLevelName.OptionsColumn.AllowEdit = false;
            this.colLevelName.OptionsColumn.AllowFocus = false;
            this.colLevelName.Visible = true;
            this.colLevelName.VisibleIndex = 3;
            this.colLevelName.Width = 84;
            // 
            // colCoreComp
            // 
            this.colCoreComp.AppearanceCell.Options.UseTextOptions = true;
            this.colCoreComp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoreComp.Caption = "CC";
            this.colCoreComp.DisplayFormat.FormatString = "#.##";
            this.colCoreComp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCoreComp.FieldName = "CoreComp_PA";
            this.colCoreComp.Name = "colCoreComp";
            this.colCoreComp.OptionsColumn.AllowEdit = false;
            this.colCoreComp.OptionsColumn.AllowFocus = false;
            this.colCoreComp.Visible = true;
            this.colCoreComp.VisibleIndex = 4;
            this.colCoreComp.Width = 78;
            // 
            // colFuncComp
            // 
            this.colFuncComp.AppearanceCell.Options.UseTextOptions = true;
            this.colFuncComp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuncComp.Caption = "FC";
            this.colFuncComp.DisplayFormat.FormatString = "#.##";
            this.colFuncComp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuncComp.FieldName = "FuncComp_PA";
            this.colFuncComp.Name = "colFuncComp";
            this.colFuncComp.OptionsColumn.AllowEdit = false;
            this.colFuncComp.OptionsColumn.AllowFocus = false;
            this.colFuncComp.Visible = true;
            this.colFuncComp.VisibleIndex = 5;
            this.colFuncComp.Width = 84;
            // 
            // colLeaderComp
            // 
            this.colLeaderComp.AppearanceCell.Options.UseTextOptions = true;
            this.colLeaderComp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLeaderComp.Caption = "JA";
            this.colLeaderComp.DisplayFormat.FormatString = "#.##";
            this.colLeaderComp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLeaderComp.FieldName = "JobAssignment";
            this.colLeaderComp.Name = "colLeaderComp";
            this.colLeaderComp.OptionsColumn.AllowEdit = false;
            this.colLeaderComp.OptionsColumn.AllowFocus = false;
            this.colLeaderComp.Visible = true;
            this.colLeaderComp.VisibleIndex = 6;
            this.colLeaderComp.Width = 78;
            // 
            // colCoreComTest
            // 
            this.colCoreComTest.AppearanceCell.Options.UseTextOptions = true;
            this.colCoreComTest.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoreComTest.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCoreComTest.AppearanceHeader.Options.UseTextOptions = true;
            this.colCoreComTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoreComTest.Caption = "ทดสอบ Core Comp.";
            this.colCoreComTest.ColumnEdit = this.txtCoreCom;
            this.colCoreComTest.FieldName = "CoreComp_Test";
            this.colCoreComTest.Name = "colCoreComTest";
            this.colCoreComTest.OptionsFilter.AllowAutoFilter = false;
            this.colCoreComTest.OptionsFilter.AllowFilter = false;
            this.colCoreComTest.Width = 70;
            // 
            // txtCoreCom
            // 
            this.txtCoreCom.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txtCoreCom.Appearance.Options.UseTextOptions = true;
            this.txtCoreCom.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCoreCom.AppearanceDisabled.Font = new System.Drawing.Font("Segoe UI", 13F);
            this.txtCoreCom.AppearanceDisabled.Options.UseFont = true;
            this.txtCoreCom.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtCoreCom.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCoreCom.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCoreCom.AppearanceFocused.Font = new System.Drawing.Font("Segoe UI", 13F);
            this.txtCoreCom.AppearanceFocused.Options.UseBackColor = true;
            this.txtCoreCom.AppearanceFocused.Options.UseFont = true;
            this.txtCoreCom.AppearanceFocused.Options.UseTextOptions = true;
            this.txtCoreCom.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCoreCom.AutoHeight = false;
            this.txtCoreCom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtCoreCom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCoreCom.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCoreCom.Mask.BeepOnError = true;
            this.txtCoreCom.Mask.EditMask = "##.##";
            this.txtCoreCom.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCoreCom.Name = "txtCoreCom";
            this.txtCoreCom.ValidateOnEnterKey = true;
            this.txtCoreCom.Leave += new System.EventHandler(this.txtCoreComp_Leave);
            // 
            // colExR
            // 
            this.colExR.AppearanceCell.Options.UseTextOptions = true;
            this.colExR.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExR.Caption = "In. Survey";
            this.colExR.ColumnEdit = this.txtExR;
            this.colExR.FieldName = "ExecutiveRound";
            this.colExR.Name = "colExR";
            this.colExR.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colExR.Width = 78;
            // 
            // txtExR
            // 
            this.txtExR.Appearance.Options.UseTextOptions = true;
            this.txtExR.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtExR.AutoHeight = false;
            this.txtExR.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtExR.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExR.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExR.LookAndFeel.SkinName = "Metropolis";
            this.txtExR.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtExR.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExR.Name = "txtExR";
            this.txtExR.Leave += new System.EventHandler(this.txtExR_Leave);
            // 
            // colCQI
            // 
            this.colCQI.AppearanceCell.Options.UseTextOptions = true;
            this.colCQI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCQI.Caption = "R2R/CQI";
            this.colCQI.ColumnEdit = this.txtCQI;
            this.colCQI.FieldName = "CQI";
            this.colCQI.Name = "colCQI";
            this.colCQI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCQI.Visible = true;
            this.colCQI.VisibleIndex = 7;
            this.colCQI.Width = 78;
            // 
            // txtCQI
            // 
            this.txtCQI.Appearance.Options.UseTextOptions = true;
            this.txtCQI.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCQI.AutoHeight = false;
            this.txtCQI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtCQI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCQI.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCQI.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCQI.Name = "txtCQI";
            this.txtCQI.Leave += new System.EventHandler(this.txtCQI_Leave);
            // 
            // colQA
            // 
            this.colQA.AppearanceCell.Options.UseTextOptions = true;
            this.colQA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQA.Caption = "Lean Proj.";
            this.colQA.ColumnEdit = this.txtQA;
            this.colQA.FieldName = "Lean";
            this.colQA.Name = "colQA";
            this.colQA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colQA.Visible = true;
            this.colQA.VisibleIndex = 8;
            this.colQA.Width = 70;
            // 
            // txtQA
            // 
            this.txtQA.Appearance.Options.UseTextOptions = true;
            this.txtQA.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtQA.AutoHeight = false;
            this.txtQA.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtQA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQA.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQA.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQA.Name = "txtQA";
            this.txtQA.Leave += new System.EventHandler(this.txtQA_Leave);
            // 
            // colICRTeam
            // 
            this.colICRTeam.AppearanceCell.Options.UseTextOptions = true;
            this.colICRTeam.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colICRTeam.Caption = "งานคุณภาพ";
            this.colICRTeam.ColumnEdit = this.txtICRTeam;
            this.colICRTeam.FieldName = "ICR_Team";
            this.colICRTeam.Name = "colICRTeam";
            this.colICRTeam.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colICRTeam.Width = 78;
            // 
            // txtICRTeam
            // 
            this.txtICRTeam.Appearance.Options.UseTextOptions = true;
            this.txtICRTeam.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtICRTeam.AutoHeight = false;
            this.txtICRTeam.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtICRTeam.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtICRTeam.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtICRTeam.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtICRTeam.Name = "txtICRTeam";
            this.txtICRTeam.Leave += new System.EventHandler(this.txtICRTeam_Leave);
            // 
            // colICR
            // 
            this.colICR.AppearanceCell.Options.UseTextOptions = true;
            this.colICR.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colICR.Caption = "การคีย์ ICR";
            this.colICR.ColumnEdit = this.txtICR;
            this.colICR.FieldName = "ICR";
            this.colICR.Name = "colICR";
            this.colICR.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colICR.Width = 70;
            // 
            // txtICR
            // 
            this.txtICR.Appearance.Options.UseTextOptions = true;
            this.txtICR.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtICR.AutoHeight = false;
            this.txtICR.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtICR.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtICR.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtICR.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtICR.Name = "txtICR";
            this.txtICR.Leave += new System.EventHandler(this.txtICR_Leave);
            // 
            // colDiscipline
            // 
            this.colDiscipline.AppearanceCell.Options.UseTextOptions = true;
            this.colDiscipline.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscipline.Caption = "Discipline";
            this.colDiscipline.ColumnEdit = this.txtDiscipline;
            this.colDiscipline.FieldName = "Discipline";
            this.colDiscipline.Name = "colDiscipline";
            this.colDiscipline.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDiscipline.Visible = true;
            this.colDiscipline.VisibleIndex = 9;
            this.colDiscipline.Width = 78;
            // 
            // txtDiscipline
            // 
            this.txtDiscipline.Appearance.Options.UseTextOptions = true;
            this.txtDiscipline.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtDiscipline.AutoHeight = false;
            this.txtDiscipline.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtDiscipline.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDiscipline.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDiscipline.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiscipline.Name = "txtDiscipline";
            this.txtDiscipline.Leave += new System.EventHandler(this.txtDiscipline_Leave);
            // 
            // colTraining
            // 
            this.colTraining.AppearanceCell.Options.UseTextOptions = true;
            this.colTraining.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTraining.Caption = "Training";
            this.colTraining.ColumnEdit = this.txtTraining;
            this.colTraining.FieldName = "Training";
            this.colTraining.Name = "colTraining";
            this.colTraining.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTraining.Visible = true;
            this.colTraining.VisibleIndex = 10;
            this.colTraining.Width = 78;
            // 
            // txtTraining
            // 
            this.txtTraining.Appearance.Options.UseTextOptions = true;
            this.txtTraining.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTraining.AutoHeight = false;
            this.txtTraining.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtTraining.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTraining.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTraining.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTraining.Name = "txtTraining";
            this.txtTraining.Leave += new System.EventHandler(this.txtTraining_Leave);
            // 
            // colCheckup
            // 
            this.colCheckup.AppearanceCell.Options.UseTextOptions = true;
            this.colCheckup.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCheckup.Caption = "Checkup";
            this.colCheckup.ColumnEdit = this.txtCheckup;
            this.colCheckup.FieldName = "Checkup";
            this.colCheckup.Name = "colCheckup";
            this.colCheckup.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCheckup.Visible = true;
            this.colCheckup.VisibleIndex = 11;
            this.colCheckup.Width = 78;
            // 
            // txtCheckup
            // 
            this.txtCheckup.Appearance.Options.UseTextOptions = true;
            this.txtCheckup.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCheckup.AutoHeight = false;
            this.txtCheckup.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtCheckup.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCheckup.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCheckup.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCheckup.Name = "txtCheckup";
            this.txtCheckup.Leave += new System.EventHandler(this.txtCheckup_Leave);
            // 
            // colActivity
            // 
            this.colActivity.AppearanceCell.Options.UseTextOptions = true;
            this.colActivity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActivity.Caption = "Activity";
            this.colActivity.ColumnEdit = this.txtActivity;
            this.colActivity.FieldName = "Activity";
            this.colActivity.Name = "colActivity";
            this.colActivity.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colActivity.Width = 78;
            // 
            // txtActivity
            // 
            this.txtActivity.Appearance.Options.UseTextOptions = true;
            this.txtActivity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtActivity.AutoHeight = false;
            this.txtActivity.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtActivity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtActivity.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtActivity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtActivity.Name = "txtActivity";
            this.txtActivity.Leave += new System.EventHandler(this.txtActivity_Leave);
            // 
            // colTotalScore
            // 
            this.colTotalScore.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTotalScore.AppearanceCell.Options.UseFont = true;
            this.colTotalScore.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalScore.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalScore.Caption = "รวม";
            this.colTotalScore.DisplayFormat.FormatString = "#.##";
            this.colTotalScore.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalScore.FieldName = "Total";
            this.colTotalScore.Name = "colTotalScore";
            this.colTotalScore.OptionsColumn.AllowEdit = false;
            this.colTotalScore.OptionsColumn.AllowFocus = false;
            this.colTotalScore.Visible = true;
            this.colTotalScore.VisibleIndex = 12;
            this.colTotalScore.Width = 140;
            // 
            // frmAssessmentGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1540, 417);
            this.Controls.Add(this.pScore);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAssessmentGrade";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แก้ไขคะแนนตัดเกรด";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmAssessment_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pScore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoreCom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCQI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtICRTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtICR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscipline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label lblTitlePA;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private DevExpress.XtraGrid.GridControl grdAssessment;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colPositionName;
        private DevExpress.XtraGrid.Columns.GridColumn colCoreComp;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaderComp;
        private DevExpress.XtraGrid.Columns.GridColumn colCoreComTest;
        private System.Windows.Forms.Panel pScore;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtCoreCom;
        private DevExpress.XtraGrid.Columns.GridColumn colCQI;
        private DevExpress.XtraGrid.Columns.GridColumn colExR;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalScore;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtExR;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtCQI;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtQA;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtICRTeam;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtICR;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtDiscipline;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtTraining;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtCheckup;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtActivity;
        private DevExpress.XtraGrid.Columns.GridColumn colQA;
        private DevExpress.XtraGrid.Columns.GridColumn colICRTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colICR;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscipline;
        private DevExpress.XtraGrid.Columns.GridColumn colTraining;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckup;
        private DevExpress.XtraGrid.Columns.GridColumn colActivity;
        private DevExpress.XtraGrid.Columns.GridColumn colDivision;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colLevelName;
        private DevExpress.XtraGrid.Columns.GridColumn colFuncComp;
    }
}