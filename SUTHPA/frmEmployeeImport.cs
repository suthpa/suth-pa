﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExcelDataReader;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;

namespace SUTHPA
{
    public partial class frmEmployeeImport : DevExpress.XtraEditors.XtraForm
    {
        public frmEmployeeImport()
        {
            InitializeComponent();
        }

        enum StepChanged
        {
            Import,
            Preview,
            Processing,
            Compare,
            Finish,
            UpdateValue,
            Error,
            Reload
        }

        #region Variables
        //Size formDefaultSize;
        //Point DefaultLocation;
        Color noneActive = Color.FromArgb(169, 169, 169);
        Color Active = Color.FromArgb(1, 99, 182);
        StepChanged GetStep;
        StepChanged CurrentStep;

        DataTable dtE;
        #endregion

        DataSet dsImport = new DataSet();

        private void frmEmployeeImport_Load(object sender, EventArgs e)
        {
            pnWait.Visible = false;
            CurrentStep = StepChanged.Import;
            ToStep(StepChanged.Import);
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;            
            Control.CheckForIllegalCrossThreadCalls = false;
        }
        private void ToStep(StepChanged step)
        {
            GetStep = step;
            cmdNext.Visible = true;
            switch (GetStep)
            {
                case StepChanged.Import:
                    btnStepImport.Image = Properties.Resources.Chevron_Right_Blue_20px;
                    ButtonStepColor(Active, noneActive, noneActive);
                    cmdNext.Text = "Browns";
                   
                    break;
                case StepChanged.Preview:
                    btnStepCompare.Image = Properties.Resources.Chevron_Right_Blue_20px;
                    ButtonStepColor(Active, Active, noneActive);
                    cmdNext.Text = "Next";
                    break;
                case StepChanged.Reload:
                    btnStepFinish.Image = null;
                    ButtonStepColor(Active, Active, Active);
                    cmdNext.Text = "Reload";
                    break;
                case StepChanged.Finish:
                    btnStepFinish.Image = null;
                    ButtonStepColor(Active, Active, Active);
                    cmdNext.Text = "Done";
                    break;
                case StepChanged.Error:
                    btnStepFinish.Image = null;
                    ButtonStepColor(Active, Active, Active);
                    cmdNext.Visible = false;
                    break;
            }

        }
        private void ButtonStepColor(Color import, Color compare, Color finish)
        {
            btnStepImport.ForeColor = import;
            btnStepCompare.ForeColor = compare;
            btnStepFinish.ForeColor = finish;
        }

        private void cmdNext_Click(object sender, EventArgs e)
        {
            if (CurrentStep == StepChanged.Import)
            {
                using (OpenFileDialog dlg = new OpenFileDialog() { Filter = "Excel Workbook|*xlsx;*xls" })
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        backgroundWorker1.RunWorkerAsync();
                        ToStep(StepChanged.Preview);
                        lblFile.Text = dlg.FileName;
                            FillDataGridView(dlg.FileName);
                            pnStep.Visible = true;
                            CurrentStep = StepChanged.Preview;

                    }
                    
                }
            }
            else if (CurrentStep == StepChanged.Reload)
                FillDataGridView(lblFile.Text);
            else
            {
                backgroundWorker1.RunWorkerAsync();
                ImportDataToTable();
            }

            //this.WindowState = FormWindowState.Maximized;
        }

        private void FillDataGridView(string filename)
        {
            try
            {
                Thread.Sleep(1000);
            var extension = Path.GetExtension(filename).ToLower();
            FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read);
            IExcelDataReader reader = null/* TODO Change to default(_) if this is not a reference type */;
            CurrentStep = StepChanged.Preview;
            if (extension == ".xls")
                reader = ExcelReaderFactory.CreateBinaryReader(fs);
            else if (extension == ".xlsx")
                reader = ExcelReaderFactory.CreateOpenXmlReader(fs);

            if (reader == null)
                return;

            ExcelDataSetConfiguration x_config = new ExcelDataSetConfiguration();

            x_config.ConfigureDataTable = tableReader => new ExcelDataTableConfiguration()
            {
                UseHeaderRow = true
            };

            dsImport = reader.AsDataSet(x_config);

            MasterController ctlM = new MasterController();
            EmployeeController ctlE =new EmployeeController();

            dsImport.Tables[0].Columns.Add("PositionUID");
            dsImport.Tables[0].Columns.Add("DepartmentUID");
            dsImport.Tables[0].Columns.Add("DivisionUID");

            dsImport.Tables[0].Columns.Add("Error");

            foreach (DataRow row in dsImport.Tables[0].Rows)
            {
                if (row[0].ToString() != "")
                {
                    row["PositionUID"] = ctlM.Position_GetUID(row[4].ToString());
                    row["DepartmentUID"] = ctlM.Department_GetUID(row[5].ToString());
                    row["DivisionUID"] = ctlM.Division_GetUID(row[6].ToString());

                    if (row["PositionUID"].ToString() == "0")
                    {
                        row["Error"] = row["Error"] + "[ไม่พบตำแหน่ง]";
                    }
                    if (row["DepartmentUID"].ToString() == "0")
                    {
                        row["Error"] = row["Error"] + "[ไม่พบแผนก]";
                    }
                    if (row["DivisionUID"].ToString() == "0")
                    {
                        row["Error"] = row["Error"] + "[ไม่พบฝ่าย]";
                    }

                   if (ctlE.Employee_Duplicate(row[0].ToString()))
                    {
                       row["Error"] = row["Error"] + "[พนักงานซ้ำ]";
                    }


                }
                row.AcceptChanges();
            }

            grdData.DataSource = dsImport.Tables[0];

            if (dsImport.Tables[0].Columns.Count > 0)
            {
                grdViewData.Columns[dsImport.Tables[0].Columns.Count - 1].SortIndex = 0;
                grdViewData.Columns[dsImport.Tables[0].Columns.Count - 1].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                grdViewData.FocusedRowHandle = 0;

                grdViewData.Columns[0].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewData.Columns[1].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewData.Columns[2].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                grdViewData.Columns[3].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                grdViewData.Columns[4].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                grdViewData.Columns[5].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                grdViewData.Columns[6].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                grdViewData.Columns[7].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewData.Columns[8].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewData.Columns[9].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewData.Columns[10].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewData.Columns[11].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.BestFitColumns();
            }
            if (dsImport.Tables[0].Columns.Count <9)
            {
                ToStep(StepChanged.Error);
                MessageBox.Show("รูปแบบไฟล์ Import ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

                reader.Close();
            }
            catch (Exception ex)
            {
                if (ex.HResult.ToString() == "-2147024864")
                {
                    MessageBox.Show("ไฟล์ Excel ถูกเปิดใช้งานอยู่ กรุุณาปิดไฟล์แล้ว Reload ใหม่", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(ex.Message, "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ImportDataToTable()
        {
            int k;
            string sD,sM,sY;
            EmployeeController ctlE = new EmployeeController();
            dtE = dsImport.Tables[0];
            
            k = 0;
            foreach (DataRow row in dtE.Rows)
            {
                sD = "";
                sM = "";
                sY = "";

                if (row[0].ToString() !="" )
                {
                    if (ctlE.Employee_Duplicate(row[0].ToString())==false)
                    {

                        sD = row[7].ToString().Left(2);
                        sM = row[7].ToString().Mid(3,2);
                        sY = row[7].ToString().Right(4);


                        ctlE.Employee_SaveByImport(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(), row[5].ToString(), row[6].ToString(), BaseClass.ConvertStr2DBDateString(sD,sM,sY), row[8].ToString());
                        k = k + 1;
                    }
                    
                }
            }

            CurrentStep = StepChanged.Finish;

            if (k==0)
            {
                MessageBox.Show("ไม่สามารถ Import ข้อมูลได้ กรุณาตรวจสอบข้อมูล (อาจมีข้อมูลพนักงานในระบบแล้ว)", "Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(k==dtE.Rows.Count)
            {
                MessageBox.Show("Import เรียบร้อยแล้ว.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else MessageBox.Show("ข้อมูลทั้งหมด " + k + "เรคคอร์ด ถูก Import เรียบร้อยแล้ว.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //pnWait.Dock = DockStyle.Fill;
            pnWait.Visible = true;

            for (var i = 1; i <= 100; i++)
            {
                backgroundWorker1.ReportProgress(i);
                System.Threading.Thread.Sleep(10);
                lblProgress.Text = ProgressBar1.Value + "%";
            }
        }
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.ProgressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pnWait.Visible = false;
        }

        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                // Dim view As GridView = TryCast(sender, GridView)
                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[0]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[0]).ToString() == "")
                    { 
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[1]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[1]).ToString() == "")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[2]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[2]).ToString() == "")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[3]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[3]).ToString() == "")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[4]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[4]).ToString() == "")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[5]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[5]).ToString() == "")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[8]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[8]).ToString() == "0")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }

                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[9]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[9]).ToString() == "0")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }
                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[10]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[10]).ToString() == "0")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }
                }
                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[11]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[11]).ToString() == "0")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }

                }
                if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[12]) != null)
                {
                    if (grdViewData.GetRowCellValue(e.RowHandle, grdViewData.Columns[12]).ToString() != "")
                    {
                        e.Appearance.ForeColor = Color.Red;
                        CurrentStep = StepChanged.Reload;
                    }

                }
                ToStep(CurrentStep);
            }
  
        }

        private void grdViewData_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Handled == true)
            {
                GridView view = sender as GridView;
                string noData = "X";
                if (view.IsRowVisible(e.RowHandle) == RowVisibleState.Visible)
                {
                    if (e.Column.FieldName == "PrefixUID" & view.GetRowCellDisplayText(e.RowHandle, view.Columns[1]) == "")
                    {
                        e.Appearance.BackColor = Color.LightPink;
                        view.SetRowCellValue(e.RowHandle, view.Columns["Flag"], noData.ToString());
                        cmdNext.Text = "Brown";
                    }

                    if (e.Column.FieldName == "NameTH" & view.GetRowCellDisplayText(e.RowHandle, view.Columns[2]) == "")
                    {
                        e.Appearance.BackColor = Color.LightPink;
                        view.SetRowCellValue(e.RowHandle, view.Columns["Flag"], noData.ToString());
                        cmdNext.Text = "Brown";
                    }
                    if (e.Column.FieldName == "SurnameTH" & view.GetRowCellDisplayText(e.RowHandle, view.Columns[3]) == "")
                    {
                        e.Appearance.BackColor = Color.LightPink;
                        view.SetRowCellValue(e.RowHandle, view.Columns["Flag"], noData.ToString());
                        cmdNext.Text = "Brown";
                    }
                    if (e.Column.FieldName == "Gender" & view.GetRowCellDisplayText(e.RowHandle, view.Columns[8]) == "")
                    {
                        e.Appearance.BackColor = Color.LightPink;
                        view.SetRowCellValue(e.RowHandle, view.Columns["Flag"], noData.ToString());
                        cmdNext.Text = "Brown";
                    }
                    if (e.Column.FieldName == "PersonTypeCode" & view.GetRowCellDisplayText(e.RowHandle, view.Columns[9]) == "")
                    {
                        e.Appearance.BackColor = Color.LightPink;
                        view.SetRowCellValue(e.RowHandle, view.Columns["Flag"], noData.ToString());
                        cmdNext.Text = "Brown";
                    }
                }
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    }