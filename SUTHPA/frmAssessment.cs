﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using System.Collections;

namespace SUTHPA
{
    public partial class frmAssessment : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtPA = new DataTable();
        AssessmentController ctlPA = new AssessmentController();
        public string EmployeeID;
        public int EvaluationUID;
        public string EmployeeName;
        public bool flagEdit = false;
        public int row;
        public int  AssessorLevelUID;
        int RNDNO;
        public frmAssessment()
        {
            InitializeComponent();
        }

        private void frmAssessment_Load(object sender, EventArgs e)
        {
            try
            {
                ConfigurationController ctlCf = new ConfigurationController();
                RNDNO = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO));


                this.TopMost = false;
                if (flagEdit)
                    dtPA = ctlPA.Evaluation_GetByAssessee(GlobalVariables.BYear, this.EvaluationUID, EmployeeID, GlobalVariables.username, GlobalVariables.AssesseeLevelUID,this.AssessorLevelUID);
                else
                    dtPA = ctlPA.Evaluation_Get(GlobalVariables.BYear, this.EvaluationUID, GlobalVariables.AssesseeLevelUID);

                row = dtPA.Rows.Count;
                grdAssessment.DataSource = dtPA;
                grdViewAssessment.BestFitColumns();
                //viewPAAll.Columns[3].AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                //viewPAAll.Columns[3].Width = 500;
                grdViewAssessment.Columns[6].Width = 50;
                grdViewAssessment.Columns[12].Width = 50;
                grdViewAssessment.Columns[13].Width = 70;
                grdViewAssessment.Columns[14].Width = 50;
                grdViewAssessment.Columns[15].Width = 70;
                //

                if (RNDNO == 1)
                {
                    grdViewAssessment.Columns[14].OptionsColumn.AllowEdit = false;
                    grdViewAssessment.Columns[14].OptionsColumn.AllowFocus = false;
                    grdViewAssessment.Columns[14].AppearanceCell.BackColor = Color.White;

                    grdViewAssessment.Columns[14].Visible = false;
                    grdViewAssessment.Columns[15].Visible = false;
                }
                else
                {
                    grdViewAssessment.Columns[12].OptionsColumn.AllowEdit = false;
                    grdViewAssessment.Columns[12].OptionsColumn.AllowFocus = false;
                    grdViewAssessment.Columns[12].AppearanceCell.BackColor = Color.White;
                    grdViewAssessment.Columns[13].Visible = false;
                }



                if (dtPA.Rows.Count > 0)
                {
                    lblTitle.Text = dtPA.Rows[0]["EvaluationName"].ToString() + " ชื่อผู้ถูกประเมิน : " + EmployeeName;
                    txtRemark.Text = dtPA.Rows[0]["Remark"].ToString();
                    txtComment.Text = ctlPA.Assessment_GetComment(GlobalVariables.BYear, GlobalVariables.username, this.EmployeeID, this.EvaluationUID);
                }
            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool CheckZero()
        {
            bool HasZero = true;

            for (int i = 0; i < row; i++)
            {

                switch (RNDNO)
                {
                    case 1:
                        if (grdViewAssessment.GetRowCellValue(i, colScore1).ToString() == "0")
                        {
                            HasZero = true;
                            break;
                        }
                        else
                            HasZero = false;
                        break;
                    case 2:
                        if (grdViewAssessment.GetRowCellValue(i, colScore2).ToString() == "0")
                        {
                            HasZero = true;
                            break;
                        }
                        else
                            HasZero = false;
                        break;
                    default:
                        HasZero = false;
                        break;

                }
            }

            return HasZero;
        }
        private void btSave_Click(object sender, EventArgs e)
        {

            if (CheckZero() == true)
            {
                MessageBox.Show("กรุณาตรวจสอบคะแนน คะแนนต้องไม่มี 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int AssessmentUID = 0;
            //string CompetencyGroup = "";
            bool flagStatus = true;

            AssessmentUID = ctlPA.Assessment_Save(GlobalVariables.BYear, GlobalVariables.username, EmployeeID, this.EvaluationUID, Convert.ToDouble(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString()), Convert.ToDouble(grdViewAssessment.Columns[14].SummaryItem.SummaryValue.ToString()), txtComment.Text,RNDNO, GlobalVariables.AssessmentClass,this.AssessorLevelUID);

            double FinalWeight = ctlPA.LevelScore_Get(GlobalVariables.BYear, GlobalVariables.AssesseeLevelUID, this.EvaluationUID);
            double FinalScore,Score;

            if (RNDNO == 1)
            {
                FinalScore = (Convert.ToDouble(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString()) * FinalWeight) / 100;
                Score = Convert.ToDouble(grdViewAssessment.Columns[13].SummaryItem.SummaryValue.ToString());
            }
            else
            {
                FinalScore = (Convert.ToDouble(grdViewAssessment.Columns[15].SummaryItem.SummaryValue.ToString()) * FinalWeight) / 100;
                Score = Convert.ToDouble(grdViewAssessment.Columns[15].SummaryItem.SummaryValue.ToString());
            }
            ctlPA.AssessmentScoreFinal_Save(GlobalVariables.BYear, EmployeeID,  GlobalVariables.username, this.EvaluationUID, FinalWeight, Score, FinalScore,RNDNO, GlobalVariables.AssessmentClass,this.AssessorLevelUID, AssessmentUID);

            if (GlobalVariables.AssessmentClass=="U")
            {
                ctlPA.AssessmentGrade_Save(GlobalVariables.BYear, EmployeeID, this.EvaluationUID, FinalScore, GlobalVariables.username, RNDNO);
            }
            

            if (RNDNO == 1)
            {
                for (int i = 0; i < row; i++)
                {
                    if (grdViewAssessment.GetRowCellValue(i, colScore1).ToString().Length <= 0)
                    {
                        flagStatus = false;
                        break;
                    }
                    else
                        flagStatus = true;
                }
            }
            else
            {
                for (int i = 0; i < row; i++)
                {
                    if (grdViewAssessment.GetRowCellValue(i, colScore2).ToString().Length <= 0)
                    {
                        flagStatus = false;
                        break;
                    }
                    else
                        flagStatus = true;
                }

            }
            

            if (flagStatus)
            {
                for (int i = 0; i < row; i++)
                {


                    //ทำการบันทึก AssessmentScore
                    if (RNDNO == 1)
                        ctlPA.AssessmentScore_Save(GlobalVariables.BYear, AssessmentUID.ToString(), grdViewAssessment.GetRowCellValue(i, colCompetencyUID).ToString(), grdViewAssessment.GetRowCellValue(i, colScore1).ToString(), RNDNO);
                    else
                        ctlPA.AssessmentScore_Save(GlobalVariables.BYear, AssessmentUID.ToString(), grdViewAssessment.GetRowCellValue(i, colCompetencyUID).ToString(), grdViewAssessment.GetRowCellValue(i, colScore2).ToString(), RNDNO);


                }
                MessageBox.Show("บันทึกคะแนนประเมินเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);

                GlobalVariables.FlagRefresh = true;
                this.Close();


            }
            else
                MessageBox.Show("กรุณาประเมินให้ครบทุกข้อ", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtScore_Leave(object sender, EventArgs e)
        {
            try
            {
                
                double MaxValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
                double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
                double Score =  Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colScore1).ToString());
                double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

                if (Score < MinValue || Score > MaxValue)
                {                   
                        MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    grdViewAssessment.SetFocusedRowCellValue(colScore1, "");
                        grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;                                        
                }
                else
                {
                    grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle , colNetScore1, (Score * Weight) / MaxValue);
                    grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;                   
                }
            }
            catch { }
        }
        private void CellValidate(object sender,DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            try
            {

                double MaxValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMaxScore).ToString());
                double MinValue = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colMinScore).ToString());
                double Score = Convert.ToDouble(e.Value);
                double Weight = Convert.ToDouble(grdViewAssessment.GetFocusedRowCellValue(colWeight).ToString());

                if (Score < MinValue || Score > MaxValue)
                {
                    MessageBox.Show("กรุณากรอกตัวเลข " + MinValue.ToString() + " - " + MaxValue.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (RNDNO == 1)
                    {
                        grdViewAssessment.SetFocusedRowCellValue(colScore1, "");
                    }
                    else
                    {
                        grdViewAssessment.SetFocusedRowCellValue(colScore2, "");
                    }
                    grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle;
                }
                else
                {

                    if (RNDNO == 1)
                    {
                        grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colScore1, e.Value);
                        grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colNetScore1, (Score * Weight) / MaxValue);
                    }
                    else
                    {
                        grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colScore2, e.Value);
                        grdViewAssessment.SetRowCellValue(grdViewAssessment.FocusedRowHandle, colNetScore2, (Score * Weight) / MaxValue);
                    }
                  
                    grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
            catch { }
        }

        string[] strGroupName;
        //string[,] grpSum=new string[4,2];
        private void grdViewAssessment_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;

            info.GroupText = info.GroupValueText;
            if (info.Level == 0)
            {
                strGroupName = info.GroupValueText.Split(':');
                //info.GroupText = info.Column.Caption + "<color=#000066><b><size=12>" + strGroupName[0] + "</size></b></color><color=#333333> : " + strGroupName[1] + "</color>";
            }
        }
        double netScore;
        double grpScore;
        double sc, w;
        int k=0;

        private void grdViewAssessment_RowStyle(object sender, RowStyleEventArgs e)
        {
            //GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {

                if (grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[12]) != null)
                {
                    if (BaseClass.StrNull2Dbl(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[12]).ToString())!=0)
                    {                  

                        if (BaseClass.StrNull2Dbl(grdViewAssessment.GetRowCellValue(e.RowHandle, grdViewAssessment.Columns[12]).ToString()) < 3)
                            {
                                e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFCCCC");
                                e.Appearance.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                            }
                        //else
                        //{

                        //    e.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#fbc8c8");
                        //}                  
                    }
                }
            }

        }

        private void grdViewAssessment_KeyDown(object sender, KeyEventArgs e)
        {
            if (grdViewAssessment.FocusedColumn.FieldName=="Score1")
            {
                if (e.KeyCode==Keys.Enter)
                {
                    grdViewAssessment.MoveNext();
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
            else if(grdViewAssessment.FocusedColumn.FieldName == "Score2")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    grdViewAssessment.MoveNext();
                    //grdViewAssessment.FocusedRowHandle = grdViewAssessment.FocusedRowHandle + 1;
                }
            }
        }

        private void grdViewAssessment_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            
            if (e.FocusedRowHandle<0)
            {
                grdViewAssessment.MoveNext();
            }

        }

        private void grdViewAssessment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (grdViewAssessment.FocusedColumn.FieldName == "Score1")
            {
                int cInt = Convert.ToInt32(e.KeyChar);
                if ((cInt>=48 && cInt<=57) || cInt==8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
            else if (grdViewAssessment.FocusedColumn.FieldName == "Score2")
            {
                int cInt = Convert.ToInt32(e.KeyChar);
                if ((cInt >= 48 && cInt <= 57) || cInt == 8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void grdViewAssessment_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            GridView view = sender as GridView;
            int summaryID = Convert.ToInt32((e.Item as GridSummaryItem).Tag);
            
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {
                netScore = 0;
                grpScore = 0;
            }

            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                    switch (summaryID)
                    {
                        case 1:
                             sc = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score1").ToString());
                             w = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "WeightScore").ToString());
                            netScore += (sc * w)/4;
                            break;
                        case 2:
                           sc = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "Score2").ToString());
                             w = BaseClass.StrNull2Dbl(view.GetRowCellValue(e.RowHandle, "WeightScore").ToString());
                            grpScore += (sc * w) / 4;
                            break;
                    }               
            }

            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                switch (summaryID)
                {
                    case 1:
                        e.TotalValue = netScore;
                        break;
                    case 2:
                        e.TotalValue = grpScore;                       
                        //grpSum[k,0] =  grpScore.ToString();
                        //grpSum[k,1] = view.GetRowCellValue(e.RowHandle, "CompetencyGroupUID").ToString();
                        //k++;
                        break;
                }
            }
        }
    }
}