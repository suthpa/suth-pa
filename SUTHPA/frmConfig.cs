﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace SUTHPA
{
   
    public partial class frmConfig : DevExpress.XtraEditors.XtraForm
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        DataTable dtEmp = new DataTable(); 
        ConfigurationController ctlM = new ConfigurationController();

        private void frmConfig_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            LoadConfiguration(); 
           
        } 
        private void LoadConfiguration()
        {

            try
            {
                dtEmp = ctlM.Configuration_Get(); //GlobalVariables.BYear
                grdData.DataSource = dtEmp;
                //grdViewEmployee.BestFitColumns();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }

        
        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdEmployee_DoubleClick(object sender, EventArgs e)
        {
            //PatientVisitInfo.SendVisit(Convert.ToInt64(gridViewData.GetFocusedRowCellValue("VisitNumber")),Convert.ToInt64(gridViewData.GetFocusedRowCellValue("PatientUID")));
            //BaseGlobalClass.gPatientUID = Convert.ToInt32(gridViewData.GetFocusedRowCellValue("PatientUID"));
            //BaseGlobalClass.gPatientVisitUID = Convert.ToInt32(gridViewData.GetFocusedRowCellValue("VisitNumber"));
            //ShowPatientResult(BaseGlobalClass.gPatientUID, BaseGlobalClass.gPatientVisitUID);

            
            EditData(grdViewEmployee.GetFocusedRowCellValue("UID").ToString());
        }

        private void EditData(string pID)
        {
            DataTable dtE = new DataTable();
 
            dtE = ctlM.Configuration_GetByUID(BaseClass.StrNull2Zero(pID));
            if (dtE.Rows.Count > 0)
            {
                txtUID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<int>("UID"));                 
                txtCode.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Code"));
                txtValue.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("ConfigValue"));
                txtDesc.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Descriptions"));                 
            }
            dtE = null;
        }
        private void ClearData()
        {          
                txtUID.Text ="0";
                txtCode.Text ="";
                txtValue.Text ="";
                txtDesc.Text ="";
        }



        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void cmdSave_Click(object sender, EventArgs e)
        {
            ctlM.Configuration_Save(Convert.ToInt32(txtUID.Text), txtCode.Text, txtDesc.Text,txtValue.Text);
             
            MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
            ClearData();
            LoadConfiguration();

        }        
    }
}