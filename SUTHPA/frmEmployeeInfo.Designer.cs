﻿namespace SUTHPA
{
    partial class frmEmployeeInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmployeeInfo));
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label5 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grdEmployee = new DevExpress.XtraGrid.GridControl();
            this.grdViewEmployee = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtCurrentYear = new System.Windows.Forms.TextBox();
            this.cmdDisable = new DevExpress.XtraEditors.SimpleButton();
            this.cmdSave = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.ddlDirection = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ddlLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ddlPosition = new DevExpress.XtraEditors.LookUpEdit();
            this.ddlDivision = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.ddlDepartment = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkIsProbation = new System.Windows.Forms.CheckBox();
            this.chkAssessment = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkStatus = new System.Windows.Forms.CheckBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblJobUID = new System.Windows.Forms.Label();
            this.chkHR = new System.Windows.Forms.CheckBox();
            this.chkSuperAdmin = new System.Windows.Forms.CheckBox();
            this.chkReporter = new System.Windows.Forms.CheckBox();
            this.chkAdmin = new System.Windows.Forms.CheckBox();
            this.dtpHireDate = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.chkMedical = new System.Windows.Forms.CheckBox();
            this.txtCardID = new System.Windows.Forms.TextBox();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEmpUID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.grdJob = new DevExpress.XtraGrid.GridControl();
            this.grdViewJob = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ddlJobDirection = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.cmdCancelJob = new DevExpress.XtraEditors.SimpleButton();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.cmdDelJob = new DevExpress.XtraEditors.SimpleButton();
            this.label18 = new System.Windows.Forms.Label();
            this.cmdSaveJob = new DevExpress.XtraEditors.SimpleButton();
            this.label16 = new System.Windows.Forms.Label();
            this.ddlJobDepartment = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.ddlJobDivision = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.ddlJobPosition = new DevExpress.XtraEditors.LookUpEdit();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnMain.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDirection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDivision.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHireDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHireDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobDirection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobDivision.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobPosition.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1575, 35);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1575, 35);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(5, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Employee Info.";
            // 
            // lblExit
            // 
            this.lblExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1547, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 22);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 795);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1575, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1378, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 40);
            this.panel1.TabIndex = 33;
            // 
            // cmdClose
            // 
            this.cmdClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdClose.Appearance.Options.UseFont = true;
            this.cmdClose.Appearance.Options.UseForeColor = true;
            this.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.ImageOptions.Image")));
            this.cmdClose.Location = new System.Drawing.Point(93, 6);
            this.cmdClose.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdClose.Size = new System.Drawing.Size(95, 30);
            this.cmdClose.TabIndex = 30;
            this.cmdClose.Text = "Close";
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // pnMain
            // 
            this.pnMain.Controls.Add(this.panel5);
            this.pnMain.Controls.Add(this.panel4);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 35);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(1575, 760);
            this.pnMain.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.grdEmployee);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(736, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(839, 760);
            this.panel5.TabIndex = 3;
            // 
            // grdEmployee
            // 
            this.grdEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdEmployee.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.grdEmployee.Location = new System.Drawing.Point(0, 0);
            this.grdEmployee.LookAndFeel.SkinName = "Office 2010 Blue";
            this.grdEmployee.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdEmployee.MainView = this.grdViewEmployee;
            this.grdEmployee.Name = "grdEmployee";
            this.grdEmployee.Size = new System.Drawing.Size(839, 760);
            this.grdEmployee.TabIndex = 32;
            this.grdEmployee.TabStop = false;
            this.grdEmployee.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewEmployee,
            this.gridView1});
            this.grdEmployee.DoubleClick += new System.EventHandler(this.grdEmployee_DoubleClick);
            // 
            // grdViewEmployee
            // 
            this.grdViewEmployee.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grdViewEmployee.Appearance.GroupRow.Options.UseTextOptions = true;
            this.grdViewEmployee.Appearance.GroupRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewEmployee.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewEmployee.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewEmployee.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewEmployee.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewEmployee.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grdViewEmployee.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewEmployee.Appearance.Row.Options.UseFont = true;
            this.grdViewEmployee.Appearance.Row.Options.UseTextOptions = true;
            this.grdViewEmployee.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewEmployee.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grdViewEmployee.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn2,
            this.gridColumn15,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn6,
            this.gridColumn19,
            this.gridColumn22,
            this.gridColumn11,
            this.gridColumn28,
            this.colCardID,
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn20,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn21,
            this.gridColumn23});
            this.grdViewEmployee.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewEmployee.GridControl = this.grdEmployee;
            this.grdViewEmployee.Name = "grdViewEmployee";
            this.grdViewEmployee.OptionsBehavior.Editable = false;
            this.grdViewEmployee.OptionsFind.AlwaysVisible = true;
            this.grdViewEmployee.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewEmployee.OptionsView.ColumnAutoWidth = false;
            this.grdViewEmployee.OptionsView.ShowIndicator = false;
            this.grdViewEmployee.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewEmployee_RowStyle);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "UID";
            this.gridColumn8.FieldName = "UID";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "รหัสพนักงาน";
            this.gridColumn2.FieldName = "EmployeeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "คำนำหน้า";
            this.gridColumn15.FieldName = "Title";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 1;
            this.gridColumn15.Width = 120;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn4.Caption = "ชื่อ - สกุล";
            this.gridColumn4.FieldName = "Name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            this.gridColumn4.Width = 300;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn5.Caption = "ตำแหน่ง";
            this.gridColumn5.FieldName = "PositionName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            this.gridColumn5.Width = 300;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn7.Caption = "แผนก";
            this.gridColumn7.FieldName = "DepartmentName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 250;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn6.Caption = "ฝ่าย";
            this.gridColumn6.FieldName = "DivisionName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 300;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn19.Caption = "กลุ่มงาน";
            this.gridColumn19.FieldName = "DirectionName";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "ระดับ";
            this.gridColumn22.FieldName = "EmployeeLevelName";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 7;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "วันที่เริ่มงาน";
            this.gridColumn11.DisplayFormat.FormatString = "d";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn11.FieldName = "HireDate";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 8;
            this.gridColumn11.Width = 100;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "ทดลองงาน";
            this.gridColumn28.FieldName = "isProbation";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 9;
            // 
            // colCardID
            // 
            this.colCardID.Caption = "เลขที่บัตร ปชช.";
            this.colCardID.FieldName = "NationalID";
            this.colCardID.Name = "colCardID";
            this.colCardID.Visible = true;
            this.colCardID.VisibleIndex = 10;
            this.colCardID.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Password";
            this.gridColumn1.FieldName = "Password";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 11;
            this.gridColumn1.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Start Date";
            this.gridColumn3.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn3.FieldName = "StartDttm";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Width = 143;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "รหัสกลุ่ม";
            this.gridColumn20.FieldName = "DirectionUID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 13;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "รหัสฝ่าย";
            this.gridColumn16.FieldName = "DivisionUID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 12;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "รหัสแผนก";
            this.gridColumn17.FieldName = "DepartmentCode";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 14;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "สถานะ";
            this.gridColumn21.FieldName = "StatusFlag";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 15;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "รหัส Level";
            this.gridColumn23.FieldName = "EmployeeLevelID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 16;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdEmployee;
            this.gridView1.Name = "gridView1";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtCurrentYear);
            this.panel4.Controls.Add(this.cmdDisable);
            this.panel4.Controls.Add(this.cmdSave);
            this.panel4.Controls.Add(this.groupControl4);
            this.panel4.Controls.Add(this.groupControl1);
            this.panel4.Controls.Add(this.groupControl2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(736, 760);
            this.panel4.TabIndex = 2;
            // 
            // txtCurrentYear
            // 
            this.txtCurrentYear.Location = new System.Drawing.Point(408, 321);
            this.txtCurrentYear.Name = "txtCurrentYear";
            this.txtCurrentYear.Size = new System.Drawing.Size(44, 21);
            this.txtCurrentYear.TabIndex = 45;
            this.txtCurrentYear.Text = "2021";
            this.txtCurrentYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCurrentYear.Visible = false;
            // 
            // cmdDisable
            // 
            this.cmdDisable.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDisable.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdDisable.Appearance.Options.UseFont = true;
            this.cmdDisable.Appearance.Options.UseForeColor = true;
            this.cmdDisable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDisable.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdDisable.ImageOptions.Image")));
            this.cmdDisable.Location = new System.Drawing.Point(618, 321);
            this.cmdDisable.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.cmdDisable.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdDisable.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdDisable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdDisable.Name = "cmdDisable";
            this.cmdDisable.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdDisable.Size = new System.Drawing.Size(105, 30);
            this.cmdDisable.TabIndex = 35;
            this.cmdDisable.Text = "Delete";
            this.cmdDisable.Click += new System.EventHandler(this.cmdDisable_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdSave.Appearance.Options.UseFont = true;
            this.cmdSave.Appearance.Options.UseForeColor = true;
            this.cmdSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.ImageOptions.Image")));
            this.cmdSave.Location = new System.Drawing.Point(510, 321);
            this.cmdSave.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.HotTrack;
            this.cmdSave.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdSave.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdSave.Size = new System.Drawing.Size(100, 30);
            this.cmdSave.TabIndex = 34;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.ddlDirection);
            this.groupControl4.Controls.Add(this.label2);
            this.groupControl4.Controls.Add(this.label4);
            this.groupControl4.Controls.Add(this.ddlLevel);
            this.groupControl4.Controls.Add(this.label17);
            this.groupControl4.Controls.Add(this.label6);
            this.groupControl4.Controls.Add(this.ddlPosition);
            this.groupControl4.Controls.Add(this.ddlDivision);
            this.groupControl4.Controls.Add(this.label12);
            this.groupControl4.Controls.Add(this.ddlDepartment);
            this.groupControl4.Location = new System.Drawing.Point(4, 191);
            this.groupControl4.LookAndFeel.SkinName = "Office 2010 Blue";
            this.groupControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(719, 124);
            this.groupControl4.TabIndex = 2;
            this.groupControl4.Text = "ตำแหน่งงานปัจจุบัน";
            // 
            // ddlDirection
            // 
            this.ddlDirection.Location = new System.Drawing.Point(95, 30);
            this.ddlDirection.Name = "ddlDirection";
            this.ddlDirection.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlDirection.Properties.Appearance.Options.UseFont = true;
            this.ddlDirection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlDirection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlDirection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlDirection.Properties.NullText = "--- เลือก ---";
            this.ddlDirection.Size = new System.Drawing.Size(278, 24);
            this.ddlDirection.TabIndex = 42;
            this.ddlDirection.EditValueChanged += new System.EventHandler(this.ddlDirection_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(379, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "ตำแหน่ง";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = "แผนก";
            // 
            // ddlLevel
            // 
            this.ddlLevel.Location = new System.Drawing.Point(95, 90);
            this.ddlLevel.Name = "ddlLevel";
            this.ddlLevel.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlLevel.Properties.Appearance.Options.UseFont = true;
            this.ddlLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlLevel.Properties.NullText = "--- เลือก ---";
            this.ddlLevel.Size = new System.Drawing.Size(614, 24);
            this.ddlLevel.TabIndex = 44;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 91);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 19);
            this.label17.TabIndex = 43;
            this.label17.Text = "ระดับ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(379, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "ฝ่าย";
            // 
            // ddlPosition
            // 
            this.ddlPosition.Location = new System.Drawing.Point(436, 60);
            this.ddlPosition.Name = "ddlPosition";
            this.ddlPosition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlPosition.Properties.Appearance.Options.UseFont = true;
            this.ddlPosition.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPosition.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlPosition.Properties.NullText = "--- เลือก ---";
            this.ddlPosition.Size = new System.Drawing.Size(273, 24);
            this.ddlPosition.TabIndex = 11;
            // 
            // ddlDivision
            // 
            this.ddlDivision.Location = new System.Drawing.Point(436, 30);
            this.ddlDivision.Name = "ddlDivision";
            this.ddlDivision.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlDivision.Properties.Appearance.Options.UseFont = true;
            this.ddlDivision.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlDivision.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlDivision.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlDivision.Properties.NullText = "--- เลือก ---";
            this.ddlDivision.Size = new System.Drawing.Size(273, 24);
            this.ddlDivision.TabIndex = 12;
            this.ddlDivision.EditValueChanged += new System.EventHandler(this.ddlDivision_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 19);
            this.label12.TabIndex = 41;
            this.label12.Text = "กลุ่มงาน";
            // 
            // ddlDepartment
            // 
            this.ddlDepartment.Location = new System.Drawing.Point(95, 60);
            this.ddlDepartment.Name = "ddlDepartment";
            this.ddlDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlDepartment.Properties.Appearance.Options.UseFont = true;
            this.ddlDepartment.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlDepartment.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlDepartment.Properties.NullText = "--- เลือก ---";
            this.ddlDepartment.Size = new System.Drawing.Size(278, 24);
            this.ddlDepartment.TabIndex = 13;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.chkIsProbation);
            this.groupControl1.Controls.Add(this.chkAssessment);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.chkStatus);
            this.groupControl1.Controls.Add(this.txtPassword);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.lblJobUID);
            this.groupControl1.Controls.Add(this.chkHR);
            this.groupControl1.Controls.Add(this.chkSuperAdmin);
            this.groupControl1.Controls.Add(this.chkReporter);
            this.groupControl1.Controls.Add(this.chkAdmin);
            this.groupControl1.Controls.Add(this.dtpHireDate);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.chkMedical);
            this.groupControl1.Controls.Add(this.txtCardID);
            this.groupControl1.Controls.Add(this.txtPrefix);
            this.groupControl1.Controls.Add(this.txtName);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.txtEmployeeID);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.lblEmpUID);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Location = new System.Drawing.Point(4, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(719, 180);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "ข้อมูลพนักงาน";
            // 
            // chkIsProbation
            // 
            this.chkIsProbation.AutoSize = true;
            this.chkIsProbation.Location = new System.Drawing.Point(467, 148);
            this.chkIsProbation.Name = "chkIsProbation";
            this.chkIsProbation.Size = new System.Drawing.Size(126, 23);
            this.chkIsProbation.TabIndex = 45;
            this.chkIsProbation.Text = "ผ่านทดลองงานแล้ว";
            this.chkIsProbation.UseVisualStyleBackColor = true;
            // 
            // chkAssessment
            // 
            this.chkAssessment.AutoSize = true;
            this.chkAssessment.Location = new System.Drawing.Point(307, 148);
            this.chkAssessment.Name = "chkAssessment";
            this.chkAssessment.Size = new System.Drawing.Size(157, 23);
            this.chkAssessment.TabIndex = 40;
            this.chkAssessment.Text = "ถูกประเมินโดยหัวหน้างาน";
            this.chkAssessment.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 19);
            this.label11.TabIndex = 39;
            this.label11.Text = "สถานะปัจจุบัน";
            // 
            // chkStatus
            // 
            this.chkStatus.AutoSize = true;
            this.chkStatus.Location = new System.Drawing.Point(95, 148);
            this.chkStatus.Name = "chkStatus";
            this.chkStatus.Size = new System.Drawing.Size(160, 23);
            this.chkStatus.TabIndex = 38;
            this.chkStatus.Text = "ยังทำงาน/พนักงานปัจจุบัน";
            this.chkStatus.UseVisualStyleBackColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(467, 93);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(176, 25);
            this.txtPassword.TabIndex = 37;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(379, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 19);
            this.label10.TabIndex = 36;
            this.label10.Text = "Password";
            // 
            // lblJobUID
            // 
            this.lblJobUID.AutoSize = true;
            this.lblJobUID.Location = new System.Drawing.Point(657, 98);
            this.lblJobUID.Name = "lblJobUID";
            this.lblJobUID.Size = new System.Drawing.Size(17, 19);
            this.lblJobUID.TabIndex = 1;
            this.lblJobUID.Text = "0";
            this.lblJobUID.Visible = false;
            // 
            // chkHR
            // 
            this.chkHR.AutoSize = true;
            this.chkHR.Location = new System.Drawing.Point(559, 123);
            this.chkHR.Name = "chkHR";
            this.chkHR.Size = new System.Drawing.Size(76, 23);
            this.chkHR.TabIndex = 21;
            this.chkHR.Text = "จนท. HR";
            this.chkHR.UseVisualStyleBackColor = true;
            // 
            // chkSuperAdmin
            // 
            this.chkSuperAdmin.AutoSize = true;
            this.chkSuperAdmin.Location = new System.Drawing.Point(307, 123);
            this.chkSuperAdmin.Name = "chkSuperAdmin";
            this.chkSuperAdmin.Size = new System.Drawing.Size(121, 23);
            this.chkSuperAdmin.TabIndex = 20;
            this.chkSuperAdmin.Text = "Is Super Admin";
            this.chkSuperAdmin.UseVisualStyleBackColor = true;
            // 
            // chkReporter
            // 
            this.chkReporter.AutoSize = true;
            this.chkReporter.Location = new System.Drawing.Point(446, 123);
            this.chkReporter.Name = "chkReporter";
            this.chkReporter.Size = new System.Drawing.Size(95, 23);
            this.chkReporter.TabIndex = 19;
            this.chkReporter.Text = "Is Reporter";
            this.chkReporter.UseVisualStyleBackColor = true;
            // 
            // chkAdmin
            // 
            this.chkAdmin.AutoSize = true;
            this.chkAdmin.Location = new System.Drawing.Point(206, 123);
            this.chkAdmin.Name = "chkAdmin";
            this.chkAdmin.Size = new System.Drawing.Size(82, 23);
            this.chkAdmin.TabIndex = 18;
            this.chkAdmin.Text = "Is Admin";
            this.chkAdmin.UseVisualStyleBackColor = true;
            // 
            // dtpHireDate
            // 
            this.dtpHireDate.EditValue = null;
            this.dtpHireDate.Location = new System.Drawing.Point(95, 93);
            this.dtpHireDate.Name = "dtpHireDate";
            this.dtpHireDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.dtpHireDate.Properties.Appearance.Options.UseFont = true;
            this.dtpHireDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpHireDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpHireDate.Size = new System.Drawing.Size(168, 24);
            this.dtpHireDate.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "วันที่เริ่มงาน";
            // 
            // chkMedical
            // 
            this.chkMedical.AutoSize = true;
            this.chkMedical.Location = new System.Drawing.Point(95, 123);
            this.chkMedical.Name = "chkMedical";
            this.chkMedical.Size = new System.Drawing.Size(91, 23);
            this.chkMedical.TabIndex = 15;
            this.chkMedical.Text = "แพทย์ประจำ";
            this.chkMedical.UseVisualStyleBackColor = true;
            // 
            // txtCardID
            // 
            this.txtCardID.Location = new System.Drawing.Point(467, 62);
            this.txtCardID.MaxLength = 13;
            this.txtCardID.Name = "txtCardID";
            this.txtCardID.Size = new System.Drawing.Size(242, 25);
            this.txtCardID.TabIndex = 14;
            this.txtCardID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(277, 31);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(432, 25);
            this.txtPrefix.TabIndex = 10;
            this.txtPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(95, 62);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(278, 25);
            this.txtName.TabIndex = 9;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 19);
            this.label8.TabIndex = 8;
            this.label8.Text = "ชื่อ - นามสกุล";
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Location = new System.Drawing.Point(95, 31);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Size = new System.Drawing.Size(96, 25);
            this.txtEmployeeID.TabIndex = 7;
            this.txtEmployeeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(379, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 19);
            this.label7.TabIndex = 6;
            this.label7.Text = "เลขบัตร ปชช.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(197, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 19);
            this.label3.TabIndex = 3;
            this.label3.Text = "คำนำหน้าชื่อ";
            // 
            // lblEmpUID
            // 
            this.lblEmpUID.AutoSize = true;
            this.lblEmpUID.Location = new System.Drawing.Point(657, 125);
            this.lblEmpUID.Name = "lblEmpUID";
            this.lblEmpUID.Size = new System.Drawing.Size(17, 19);
            this.lblEmpUID.TabIndex = 1;
            this.lblEmpUID.Text = "0";
            this.lblEmpUID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "รหัสพนักงาน";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.panel3);
            this.groupControl2.Controls.Add(this.panel2);
            this.groupControl2.Location = new System.Drawing.Point(4, 357);
            this.groupControl2.LookAndFeel.SkinName = "Office 2010 Silver";
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(719, 383);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "ประวัติตำแหน่งงาน";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.grdJob);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(2, 150);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(715, 231);
            this.panel3.TabIndex = 34;
            // 
            // grdJob
            // 
            this.grdJob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdJob.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.grdJob.Location = new System.Drawing.Point(0, 0);
            this.grdJob.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdJob.MainView = this.grdViewJob;
            this.grdJob.Name = "grdJob";
            this.grdJob.Size = new System.Drawing.Size(715, 231);
            this.grdJob.TabIndex = 33;
            this.grdJob.TabStop = false;
            this.grdJob.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewJob,
            this.gridView3});
            this.grdJob.DoubleClick += new System.EventHandler(this.grdJob_DoubleClick);
            // 
            // grdViewJob
            // 
            this.grdViewJob.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewJob.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewJob.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewJob.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grdViewJob.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewJob.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewJob.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewJob.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewJob.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewJob.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.grdViewJob.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grdViewJob.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grdViewJob.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewJob.Appearance.Row.Options.UseFont = true;
            this.grdViewJob.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewJob.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewJob.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grdViewJob.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grdViewJob.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn18,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27});
            this.grdViewJob.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewJob.GridControl = this.grdJob;
            this.grdViewJob.Name = "grdViewJob";
            this.grdViewJob.OptionsBehavior.Editable = false;
            this.grdViewJob.OptionsFind.AlwaysVisible = true;
            this.grdViewJob.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewJob.OptionsView.ColumnAutoWidth = false;
            this.grdViewJob.OptionsView.ShowGroupPanel = false;
            this.grdViewJob.OptionsView.ShowIndicator = false;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "UID";
            this.gridColumn9.FieldName = "UID";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "ปี";
            this.gridColumn10.FieldName = "BYear";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "ตำแหน่ง";
            this.gridColumn12.FieldName = "PositionName";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 1;
            this.gridColumn12.Width = 250;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn13.Caption = "แผนก";
            this.gridColumn13.FieldName = "DepartmentName";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 2;
            this.gridColumn13.Width = 300;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "ฝ่าย";
            this.gridColumn14.FieldName = "DivisionName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            this.gridColumn14.Width = 300;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "กลุ่มงาน";
            this.gridColumn18.FieldName = "DirectionName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 4;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "DirectionUID";
            this.gridColumn24.FieldName = "DirectionUID";
            this.gridColumn24.Name = "gridColumn24";
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "DivisionUID";
            this.gridColumn25.FieldName = "DivisionUID";
            this.gridColumn25.Name = "gridColumn25";
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "DepartmentUID";
            this.gridColumn26.FieldName = "DepartmentUID";
            this.gridColumn26.Name = "gridColumn26";
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "PositionUID";
            this.gridColumn27.FieldName = "PositionUID";
            this.gridColumn27.Name = "gridColumn27";
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.grdJob;
            this.gridView3.Name = "gridView3";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ddlJobDirection);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.cmdCancelJob);
            this.panel2.Controls.Add(this.txtYear);
            this.panel2.Controls.Add(this.cmdDelJob);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.cmdSaveJob);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.ddlJobDepartment);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.ddlJobDivision);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.ddlJobPosition);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(2, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(715, 128);
            this.panel2.TabIndex = 33;
            // 
            // ddlJobDirection
            // 
            this.ddlJobDirection.Location = new System.Drawing.Point(93, 37);
            this.ddlJobDirection.Name = "ddlJobDirection";
            this.ddlJobDirection.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlJobDirection.Properties.Appearance.Options.UseFont = true;
            this.ddlJobDirection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlJobDirection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlJobDirection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlJobDirection.Properties.NullText = "--- เลือก ---";
            this.ddlJobDirection.Size = new System.Drawing.Size(278, 24);
            this.ddlJobDirection.TabIndex = 44;
            this.ddlJobDirection.EditValueChanged += new System.EventHandler(this.ddlJobDirection_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(37, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 19);
            this.label15.TabIndex = 43;
            this.label15.Text = "กลุ่มงาน";
            // 
            // cmdCancelJob
            // 
            this.cmdCancelJob.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancelJob.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdCancelJob.Appearance.Options.UseFont = true;
            this.cmdCancelJob.Appearance.Options.UseForeColor = true;
            this.cmdCancelJob.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCancelJob.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancelJob.ImageOptions.Image")));
            this.cmdCancelJob.Location = new System.Drawing.Point(589, 97);
            this.cmdCancelJob.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdCancelJob.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdCancelJob.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdCancelJob.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdCancelJob.Name = "cmdCancelJob";
            this.cmdCancelJob.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdCancelJob.Size = new System.Drawing.Size(53, 25);
            this.cmdCancelJob.TabIndex = 33;
            this.cmdCancelJob.Text = "ล้าง";
            this.cmdCancelJob.Click += new System.EventHandler(this.cmdCancelJob_Click);
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(93, 7);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(52, 25);
            this.txtYear.TabIndex = 7;
            this.txtYear.Text = "2021";
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmdDelJob
            // 
            this.cmdDelJob.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDelJob.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdDelJob.Appearance.Options.UseFont = true;
            this.cmdDelJob.Appearance.Options.UseForeColor = true;
            this.cmdDelJob.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDelJob.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdDelJob.ImageOptions.Image")));
            this.cmdDelJob.Location = new System.Drawing.Point(647, 97);
            this.cmdDelJob.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.cmdDelJob.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdDelJob.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdDelJob.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdDelJob.Name = "cmdDelJob";
            this.cmdDelJob.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdDelJob.Size = new System.Drawing.Size(60, 25);
            this.cmdDelJob.TabIndex = 32;
            this.cmdDelJob.Text = "ลบ";
            this.cmdDelJob.Click += new System.EventHandler(this.cmdDelJob_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(37, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 19);
            this.label18.TabIndex = 0;
            this.label18.Text = "ปี";
            // 
            // cmdSaveJob
            // 
            this.cmdSaveJob.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveJob.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmdSaveJob.Appearance.Options.UseFont = true;
            this.cmdSaveJob.Appearance.Options.UseForeColor = true;
            this.cmdSaveJob.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSaveJob.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveJob.ImageOptions.Image")));
            this.cmdSaveJob.Location = new System.Drawing.Point(524, 97);
            this.cmdSaveJob.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdSaveJob.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdSaveJob.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdSaveJob.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmdSaveJob.Name = "cmdSaveJob";
            this.cmdSaveJob.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.cmdSaveJob.Size = new System.Drawing.Size(60, 25);
            this.cmdSaveJob.TabIndex = 31;
            this.cmdSaveJob.Text = "เพิ่ม";
            this.cmdSaveJob.Click += new System.EventHandler(this.cmdSaveJob_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(377, 70);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 19);
            this.label16.TabIndex = 2;
            this.label16.Text = "ตำแหน่ง";
            // 
            // ddlJobDepartment
            // 
            this.ddlJobDepartment.Location = new System.Drawing.Point(93, 67);
            this.ddlJobDepartment.Name = "ddlJobDepartment";
            this.ddlJobDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlJobDepartment.Properties.Appearance.Options.UseFont = true;
            this.ddlJobDepartment.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlJobDepartment.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlJobDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlJobDepartment.Properties.NullText = "--- เลือก ---";
            this.ddlJobDepartment.Size = new System.Drawing.Size(278, 24);
            this.ddlJobDepartment.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(37, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 19);
            this.label14.TabIndex = 4;
            this.label14.Text = "แผนก";
            // 
            // ddlJobDivision
            // 
            this.ddlJobDivision.Location = new System.Drawing.Point(434, 37);
            this.ddlJobDivision.Name = "ddlJobDivision";
            this.ddlJobDivision.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlJobDivision.Properties.Appearance.Options.UseFont = true;
            this.ddlJobDivision.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlJobDivision.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlJobDivision.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlJobDivision.Properties.NullText = "--- เลือก ---";
            this.ddlJobDivision.Size = new System.Drawing.Size(273, 24);
            this.ddlJobDivision.TabIndex = 12;
            this.ddlJobDivision.EditValueChanged += new System.EventHandler(this.ddlJobDivision_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(398, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 19);
            this.label13.TabIndex = 5;
            this.label13.Text = "ฝ่าย";
            // 
            // ddlJobPosition
            // 
            this.ddlJobPosition.Location = new System.Drawing.Point(434, 67);
            this.ddlJobPosition.Name = "ddlJobPosition";
            this.ddlJobPosition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ddlJobPosition.Properties.Appearance.Options.UseFont = true;
            this.ddlJobPosition.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlJobPosition.Properties.AppearanceDropDown.Options.UseFont = true;
            this.ddlJobPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlJobPosition.Properties.NullText = "--- เลือก ---";
            this.ddlJobPosition.Size = new System.Drawing.Size(273, 24);
            this.ddlJobPosition.TabIndex = 11;
            // 
            // frmEmployeeInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1575, 835);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "frmEmployeeInfo";
            this.Text = "Employee Info.";
            this.Load += new System.EventHandler(this.frmEmployeeInfo_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnMain.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDirection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDivision.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHireDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHireDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobDirection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobDivision.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJobPosition.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton cmdClose;
        private System.Windows.Forms.Panel pnMain;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton cmdSaveJob;
        private DevExpress.XtraEditors.LookUpEdit ddlJobDepartment;
        private DevExpress.XtraEditors.LookUpEdit ddlJobDivision;
        private DevExpress.XtraEditors.LookUpEdit ddlJobPosition;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblJobUID;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkHR;
        private System.Windows.Forms.CheckBox chkSuperAdmin;
        private System.Windows.Forms.CheckBox chkReporter;
        private System.Windows.Forms.CheckBox chkAdmin;
        private DevExpress.XtraEditors.DateEdit dtpHireDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkMedical;
        private System.Windows.Forms.TextBox txtCardID;
        private DevExpress.XtraEditors.LookUpEdit ddlDepartment;
        private DevExpress.XtraEditors.LookUpEdit ddlDivision;
        private DevExpress.XtraEditors.LookUpEdit ddlPosition;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEmpUID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.GridControl grdEmployee;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewEmployee;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.SimpleButton cmdDisable;
        private DevExpress.XtraEditors.SimpleButton cmdSave;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl grdJob;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewJob;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraGrid.Columns.GridColumn colCardID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.SimpleButton cmdDelJob;
        private DevExpress.XtraEditors.SimpleButton cmdCancelJob;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private System.Windows.Forms.CheckBox chkAssessment;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkStatus;
        private DevExpress.XtraEditors.LookUpEdit ddlDirection;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraEditors.LookUpEdit ddlJobDirection;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.LookUpEdit ddlLevel;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private System.Windows.Forms.CheckBox chkIsProbation;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.TextBox txtCurrentYear;
    }
}