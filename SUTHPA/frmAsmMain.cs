﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
    public partial class frmAsmMain : DevExpress.XtraEditors.XtraForm
    {
        public frmAsmMain()
        {
            InitializeComponent();
        }

        int RNDNO;
        private void frmTemplate_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;    
            lblAssessor.Text = "ผู้ประเมิน  " + GlobalVariables.UserEmployeeName;

            if (OpenAssesseeListForm()==false)
            {
                tileControl1.Visible = false;
                lblNotic.Visible = true;
                lblAssessor.Text = "ระบบประเมินพนักงาน ประจำปี 2564";
                return;
            }
            else
            {
                lblNotic.Visible = false;
            }




            ConfigurationController ctlCf = new ConfigurationController();
            RNDNO = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_RNDNO));
            if (GlobalVariables.AssessorLevelUID>3)
            {
                tileItem3.Visible = false;
            }

            if (GlobalVariables.AssessorLevelUID < 2)
            {
                tileItem1.Visible = false;
                tileItem2.Visible = false;
            }


           // AssessmentController ctlA = new AssessmentController();

           //if (ctlA.AssessmentStatusByEmployee(GlobalVariables.BYear, GlobalVariables.username, GlobalVariables.AssessorLevelUID)=="Y")
           // {
           //     tileItem1.AppearanceItem.Normal.BackColor = Color.Green;
           //     tileItem1.AppearanceItem.Normal.BorderColor = Color.Green;
           // }


        }

        private void tileItem1_ItemClick(object sender, TileItemEventArgs e)
        {
            GlobalVariables.AssessmentClass = "S";
            LoadFrom();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tileItem2_ItemClick(object sender, TileItemEventArgs e)
        {
            GlobalVariables.AssessmentClass = "C";
            frmAssesseeList frmEmp = new frmAssesseeList();
            frmEmp.MdiParent = this.MdiParent;
            frmEmp.Show();
        }

        private void tileItem3_ItemClick(object sender, TileItemEventArgs e)
        {
            GlobalVariables.AssessmentClass = "U";
            frmAssesseeList frmEmp = new frmAssesseeList();
            frmEmp.MdiParent = this.MdiParent;
            frmEmp.Show();
        }
        private bool OpenAssesseeListForm()
        {

            bool AppAlive;
            AppAlive = true;

            int Bdate, Edate, sToday;
            ConfigurationController ctlCf = new ConfigurationController();

            Bdate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_STARTDATE));
            Edate = Convert.ToInt32(ctlCf.Configuration_GetByCode(GlobalVariables.CFG_ENDDATE));
            sToday = Convert.ToInt32(BaseClass.ConvertStrDate2YYYYMMDDString(ctlCf.GET_DATE_SERVER().ToString()));
            if (sToday >= Bdate && sToday <= Edate)
            {
                AppAlive = true;
            }
            else
            {
                EmployeeController ctlU = new EmployeeController();

                if (ctlU.User_GetAllowPermission(GlobalVariables.username))
                {
                    AppAlive = true;
                }
                else
                {

                    if (sToday < Bdate)
                    {
                        //MessageBox.Show("ยังไม่ถึงกำหนดเวลาประเมิน ท่านสามารถเริ่มทำแบบประเมินได้ตั้งแต่วันที่ 12 - 23 กรกฎาคม 2564", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lblNotic.Text = "ยังไม่ถึงกำหนดเวลาประเมิน ท่านสามารถเริ่มทำแบบประเมินได้ตั้งแต่วันที่ 12 - 23 กรกฎาคม 2564";
                        AppAlive = false;
                    }

                    if (sToday > Edate)
                    {
                        if (!GlobalVariables.IsAdmin)
                        {
                            //MessageBox.Show("หมดเขตระยะเวลาที่ให้ประเมินแล้ว", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            lblNotic.Text = "หมดเขตระยะเวลาที่ให้ประเมินแล้ว";
                        }
                        AppAlive = false;
                    }
                }

            }

            //if (GlobalVariables.IsSuperAdmin)
            //{
            //    AppAlive = true;
            //}

            return AppAlive;
        }

        private void LoadFrom()
        {            
                try
                {                   
                            DataTable dtC = new DataTable();
                            CompetencyController ctlCom = new CompetencyController();
                
                            dtC = ctlCom.Evaluation_GetByEmployee(GlobalVariables.BYear, GlobalVariables.LoginUser.ToString(), GlobalVariables.AssessorLevelUID.ToString(), GlobalVariables.username, GlobalVariables.AssessmentClass,RNDNO, GlobalVariables.AssessorLevelUID);

                            if (dtC.Rows.Count > 1)
                            {
                                frmCompetencyEmployee frmComp = new frmCompetencyEmployee();
                                frmComp.EmployeeID = GlobalVariables.LoginUser.ToString();
                                frmComp.EmployeeLevelUID = Convert.ToInt32(GlobalVariables.AssessorLevelUID);
                                GlobalVariables.AssesseeLevelUID = Convert.ToInt32(GlobalVariables.AssessorLevelUID);

                                frmComp.EmployeeName = GlobalVariables.UserEmployeeName.ToString();
                                frmComp.ShowDialog(this);

                            }
                            else
                            {
                                //เปิดฟอร์ม PA
                                frmAssessment fPA = new frmAssessment();
                                //ส่งพารามิเตอร์ไปให้ฟอร์ม PA
                                fPA.EmployeeID = GlobalVariables.LoginUser.ToString();

                    fPA.EvaluationUID = dtC.Rows[0].Field<int>("EvaluationUID");
                                if (dtC.Rows[0]["isAssessment"].ToString() == "Y")
                                    fPA.flagEdit = true;

                                fPA.EmployeeName = GlobalVariables.UserEmployeeName.ToString();

                    ////this.Close();
                    ////fPA.MdiParent = this.MdiParent;
                    //fPA.TopMost = true;
                    fPA.ShowDialog();

                            }

                            //LoadAssessees();                        
                }
                catch { }
            
        }
    }
}