﻿namespace SUTHPA
{
    partial class frmHODRelation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHODRelation));
            this.pnTop = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblAssessor = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new System.Windows.Forms.Panel();
            this.grdEmployee = new DevExpress.XtraGrid.GridControl();
            this.grdViewEmployee = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkAssessment = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAssessment)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.groupControl3);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1482, 35);
            this.pnTop.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblAssessor);
            this.groupControl3.Controls.Add(this.lblExit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.groupControl3.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1482, 35);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "groupControl3";
            // 
            // lblAssessor
            // 
            this.lblAssessor.AutoSize = true;
            this.lblAssessor.BackColor = System.Drawing.Color.Transparent;
            this.lblAssessor.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssessor.ForeColor = System.Drawing.Color.White;
            this.lblAssessor.Location = new System.Drawing.Point(5, 8);
            this.lblAssessor.Name = "lblAssessor";
            this.lblAssessor.Size = new System.Drawing.Size(91, 20);
            this.lblAssessor.TabIndex = 3;
            this.lblAssessor.Text = "รายชื่อ HOD";
            // 
            // lblExit
            // 
            this.lblExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1455, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 31);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 595);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1482, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1285, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 40);
            this.panel1.TabIndex = 33;
            // 
            // btClose
            // 
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.Location = new System.Drawing.Point(90, 3);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.Color.DodgerBlue;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(97, 32);
            this.btClose.TabIndex = 30;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // pnMain
            // 
            this.pnMain.BackColor = System.Drawing.Color.White;
            this.pnMain.Controls.Add(this.grdEmployee);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 35);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(1482, 560);
            this.pnMain.TabIndex = 2;
            // 
            // grdEmployee
            // 
            this.grdEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdEmployee.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.grdEmployee.Location = new System.Drawing.Point(0, 0);
            this.grdEmployee.LookAndFeel.SkinName = "Office 2010 Blue";
            this.grdEmployee.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdEmployee.MainView = this.grdViewEmployee;
            this.grdEmployee.Name = "grdEmployee";
            this.grdEmployee.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkAssessment});
            this.grdEmployee.Size = new System.Drawing.Size(1482, 560);
            this.grdEmployee.TabIndex = 5;
            this.grdEmployee.TabStop = false;
            this.grdEmployee.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewEmployee});
            // 
            // grdViewEmployee
            // 
            this.grdViewEmployee.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grdViewEmployee.Appearance.GroupRow.BackColor = System.Drawing.SystemColors.Info;
            this.grdViewEmployee.Appearance.GroupRow.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.grdViewEmployee.Appearance.GroupRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewEmployee.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewEmployee.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewEmployee.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewEmployee.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewEmployee.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.grdViewEmployee.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewEmployee.Appearance.Row.Options.UseFont = true;
            this.grdViewEmployee.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.grdViewEmployee.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grdViewEmployee.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grdViewEmployee.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grdViewEmployee.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.colLevel,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn6,
            this.gridColumn1});
            this.grdViewEmployee.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grdViewEmployee.GridControl = this.grdEmployee;
            this.grdViewEmployee.GroupFormat = "{0} [#image]{1} {2}";
            this.grdViewEmployee.Name = "grdViewEmployee";
            this.grdViewEmployee.OptionsBehavior.AutoExpandAllGroups = true;
            this.grdViewEmployee.OptionsBehavior.Editable = false;
            this.grdViewEmployee.OptionsFind.AlwaysVisible = true;
            this.grdViewEmployee.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewEmployee.OptionsView.ColumnAutoWidth = false;
            this.grdViewEmployee.OptionsView.EnableAppearanceEvenRow = true;
            this.grdViewEmployee.OptionsView.EnableAppearanceOddRow = true;
            this.grdViewEmployee.OptionsView.ShowGroupPanel = false;
            this.grdViewEmployee.OptionsView.ShowIndicator = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Level No.";
            this.gridColumn3.FieldName = "EmployeeLevelID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // colLevel
            // 
            this.colLevel.Caption = "ระดับ";
            this.colLevel.FieldName = "LevelName";
            this.colLevel.FieldNameSortGroup = "LevelName";
            this.colLevel.Name = "colLevel";
            this.colLevel.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colLevel.Visible = true;
            this.colLevel.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "รหัสพนักงาน";
            this.gridColumn2.FieldName = "EmployeeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 90;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ชื่อ - สกุล";
            this.gridColumn4.FieldName = "EmployeeName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 250;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "ตำแหน่ง";
            this.gridColumn5.FieldName = "PositionName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn7.Caption = "แผนก";
            this.gridColumn7.FieldName = "DepartmentName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ฝ่าย";
            this.gridColumn6.FieldName = "DivisionName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 175;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remark";
            this.gridColumn1.FieldName = "AsmRemark";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            // 
            // chkAssessment
            // 
            this.chkAssessment.AutoHeight = false;
            this.chkAssessment.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkAssessment.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("chkAssessment.ImageOptions.ImageChecked")));
            this.chkAssessment.Name = "chkAssessment";
            this.chkAssessment.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkAssessment.ValueChecked = "Y";
            this.chkAssessment.ValueUnchecked = "N";
            // 
            // frmHODRelation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1482, 635);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.Name = "frmHODRelation";
            this.Text = "รายชื่อ HOD";
            this.Load += new System.EventHandler(this.frmHODRelation_Load);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAssessment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label lblAssessor;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private System.Windows.Forms.Panel pnMain;
        private DevExpress.XtraGrid.GridControl grdEmployee;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewEmployee;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkAssessment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}