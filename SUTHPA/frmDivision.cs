﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SUTHPA
{
   
    public partial class frmDivision : DevExpress.XtraEditors.XtraForm
    {
        public frmDivision()
        {
            InitializeComponent();
        }

        DataTable dtM = new DataTable();
        MasterController ctlM = new MasterController();
        private void frmDivision_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            LoadDirection();               
            LoadData(); 
           
            if (!GlobalVariables.IsAdmin && !GlobalVariables.IsSuperAdmin && !GlobalVariables.IsHR) 
            {
                cmdSave.Enabled = false;
                cmdDel.Enabled = false;
                cmdCancel.Enabled = false;
            }
            
        }
        private void LoadDirection()
        {

            ddlDirection.Properties.Columns.Clear();
            ddlDirection.Properties.DataSource = ctlM.Direction_Get();
            ddlDirection.Properties.DisplayMember = "Name";
            ddlDirection.Properties.ValueMember = "UID";

            ddlDirection.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            ddlDirection.Properties.DropDownRows = 10;
            ddlDirection.Properties.ShowFooter = false;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
            ddlDirection.Properties.Columns.Add(col3);

            ddlDirection.Width = 250;

        }
        //private void LoadDivision()
        //{
        //    //DataTable dt = new DataTable();

        //    //if (GlobalVariables.IsAdmin)
        //    //{
        //    ddlDirection.Properties.Columns.Clear();
        //    ddlDirection.Properties.DataSource = ctlEmp.Employee_GetDivision();
        //    ddlDirection.Properties.DisplayMember = "DivisionName";
        //    ddlDirection.Properties.ValueMember = "DivisionUID";

        //    ddlDirection.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //    ddlDirection.Properties.DropDownRows = 10;
        //    ddlDirection.Properties.ShowFooter = false;
        //    DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DivisionName", 300, "");
        //    ddlDirection.Properties.Columns.Add(col3);
        //    ddlDirection.EditValue = 0;

        //    ddlDirection.Width = 250;
        //    //}
        //    //else
        //    //{

        //    //    dt = ctlEmp.Employee_GetEmployeeDivision(GlobalVariables.username);
        //    //    lblDivisionName.Text = dt.Rows[0].Field<string>("DivisionName");
        //    //    lblUID.Text = dt.Rows[0]["DivisionUID"].ToString();
        //    //    lblLevelLabel.Text = "Division";
        //    //}




        //    //dt = null;                      
        //}

        //private void LoadDepartment()
        //{
        //    if (ddlDirection.EditValue.ToString() != "")
        //    {

        //        ddlDepartment.Properties.Columns.Clear();
        //        ddlDepartment.Properties.DataSource = ctlEmp.Employee_GetDepartmentByDivision(Convert.ToInt32(ddlDirection.EditValue));
        //        ddlDepartment.Properties.DisplayMember = "DepartmentName";
        //        ddlDepartment.Properties.ValueMember = "DepartmentUID";

        //        ddlDepartment.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //        ddlDepartment.Properties.DropDownRows = 10;
        //        ddlDepartment.Properties.ShowFooter = false;
        //        DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", 300, "");
        //        ddlDepartment.Properties.Columns.Add(col3);
        //        ddlDepartment.EditValue = 0;
        //        ddlDepartment.Width = 250;
        //    }
        //}
        //private void LoadPosition()
        //{
        //    ddlPosition.Properties.Columns.Clear();
        //    ddlPosition.Properties.DataSource = ctlEmp.Employee_GetPosition();
        //    ddlPosition.Properties.DisplayMember = "Name";
        //    ddlPosition.Properties.ValueMember = "UID";

        //    ddlPosition.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //    ddlPosition.Properties.DropDownRows = 10;
        //    ddlPosition.Properties.ShowFooter = false;
        //    DevExpress.XtraEditors.Controls.LookUpColumnInfo col3 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 300, "");
        //    ddlPosition.Properties.Columns.Add(col3);
        //    ddlPosition.EditValue = 0;
        //    ddlPosition.Width = 250;

        //}

        //private void LoadLevel()
        //{
        //    ddlLevel.Properties.Columns.Clear();
        //    ddlLevel.Properties.DataSource = ctlEmp.Employee_GetLevel();
        //    ddlLevel.Properties.DisplayMember = "LevelName";
        //    ddlLevel.Properties.ValueMember = "LevelID";
        //    lblLevelLabel.Text = "ระดับ";

        //    ddlLevel.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
        //    ddlLevel.Properties.DropDownRows = 10;
        //    ddlLevel.Properties.ShowFooter = false;
        //    DevExpress.XtraEditors.Controls.LookUpColumnInfo col2 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LevelName", 200, "");
        //    ddlLevel.Properties.Columns.Add(col2);
        //    ddlLevel.EditValue = 0;
        //}

      
        private void LoadData()
        {

            try
            {
                dtM = ctlM.Division_Get(); //GlobalVariables.BYear
                grdData.DataSource = dtM;
                //grdViewEmployee.BestFitColumns();

            }
            finally
            {
                this.Invoke(new MethodInvoker(delegate
                {

                }));
            }
        }
              

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {        
            EditData(grdViewData.GetFocusedRowCellValue("UID").ToString());
        }

        private void EditData(string pID)
        {
            DataTable dtE = new DataTable();
 
            dtE = ctlM.Division_GetByUID(Convert.ToInt32(pID));
            if (dtE.Rows.Count > 0)
            {

                lblUID.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<int>("UID"));                 
                txtCode.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Code"));
                txtNameTH.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("Name"));
                txtNameEN.Text = BaseClass.DBNull2Str(dtE.Rows[0].Field<string>("NameEN"));      
                ddlDirection.EditValue = BaseClass.DBNull2Zero(dtE.Rows[0].Field<int>("DirectionUID"));
                chkStatus.Checked = GlobalFunctions.ConvertStatusActive2CheckBox(dtE.Rows[0].Field<string>("StatusFlag"));
            }
            dtE = null;
        }
        private void ClearData()
        {          
                lblUID.Text ="0";
                txtCode.Text ="";
                txtNameEN.Text ="";
                txtNameTH.Text ="";             
                ddlDirection.Text =""; 
            chkStatus.Checked = true;

        }                   
       
        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
               
              

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                ctlM.Division_Delete(Convert.ToInt32(lblUID.Text));
                MessageBox.Show("ลบข้อมูลเรียบร้อย");
                ClearData();
            }

            
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            ctlM.Division_Save(Convert.ToInt32(lblUID.Text), txtCode.Text,txtNameTH.Text,txtNameEN.Text , Convert.ToInt32(ddlDirection.EditValue), GlobalFunctions.ConvertCheckBox2StatusActive(chkStatus.Checked));
            MessageBox.Show("บันทึกข้อมูลเรียบร้อย");
            ClearData();
            LoadData();

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }
    }
}