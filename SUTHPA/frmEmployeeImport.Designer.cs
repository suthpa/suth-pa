﻿namespace SUTHPA
{
    partial class frmEmployeeImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnTop = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new System.Windows.Forms.Panel();
            this.pnWait = new System.Windows.Forms.Panel();
            this.lblProgress = new System.Windows.Forms.Label();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pnStep = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.cmdNext = new DevExpress.XtraEditors.SimpleButton();
            this.btnStepFinish = new System.Windows.Forms.Button();
            this.btnStepCompare = new System.Windows.Forms.Button();
            this.btnStepImport = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dlg = new System.Windows.Forms.OpenFileDialog();
            this.lblFile = new System.Windows.Forms.Label();
            this.pnTop.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnMain.SuspendLayout();
            this.pnWait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            this.pnStep.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.pnTop.Controls.Add(this.label5);
            this.pnTop.Controls.Add(this.lblExit);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1527, 35);
            this.pnTop.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Employee Import";
            // 
            // lblExit
            // 
            this.lblExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(1502, 0);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 35);
            this.lblExit.TabIndex = 2;
            this.lblExit.Text = "X";
            this.lblExit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.panel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 715);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1527, 40);
            this.pnBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1330, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 40);
            this.panel1.TabIndex = 33;
            // 
            // btClose
            // 
            this.btClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btClose.Appearance.Options.UseFont = true;
            this.btClose.Appearance.Options.UseForeColor = true;
            this.btClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btClose.Location = new System.Drawing.Point(90, 3);
            this.btClose.LookAndFeel.SkinMaskColor = System.Drawing.SystemColors.ActiveCaption;
            this.btClose.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btClose.Name = "btClose";
            this.btClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btClose.Size = new System.Drawing.Size(97, 32);
            this.btClose.TabIndex = 30;
            this.btClose.Text = "Close";
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // pnMain
            // 
            this.pnMain.BackColor = System.Drawing.Color.White;
            this.pnMain.Controls.Add(this.pnWait);
            this.pnMain.Controls.Add(this.grdData);
            this.pnMain.Controls.Add(this.pnStep);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 35);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(1527, 680);
            this.pnMain.TabIndex = 2;
            // 
            // pnWait
            // 
            this.pnWait.BackColor = System.Drawing.Color.White;
            this.pnWait.Controls.Add(this.lblProgress);
            this.pnWait.Controls.Add(this.ProgressBar1);
            this.pnWait.Controls.Add(this.label2);
            this.pnWait.Controls.Add(this.label1);
            this.pnWait.Location = new System.Drawing.Point(518, 207);
            this.pnWait.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnWait.Name = "pnWait";
            this.pnWait.Size = new System.Drawing.Size(442, 146);
            this.pnWait.TabIndex = 100;
            // 
            // lblProgress
            // 
            this.lblProgress.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProgress.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblProgress.Location = new System.Drawing.Point(0, 73);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(442, 17);
            this.lblProgress.TabIndex = 6;
            this.lblProgress.Text = "0%";
            this.lblProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ProgressBar1.Location = new System.Drawing.Point(0, 50);
            this.ProgressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(442, 23);
            this.ProgressBar1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(442, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Loading data...";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(442, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please wait";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdData.Location = new System.Drawing.Point(0, 32);
            this.grdData.LookAndFeel.SkinName = "Office 2010 Blue";
            this.grdData.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.Size = new System.Drawing.Size(1527, 648);
            this.grdData.TabIndex = 2;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.Empty.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.grdViewData.Appearance.Empty.Options.UseBackColor = true;
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Appearance.SelectedRow.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.SelectedRow.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsBehavior.Editable = false;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.OptionsView.ShowIndicator = false;
            this.grdViewData.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grdViewData_CustomDrawCell);
            this.grdViewData.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewData_RowStyle);
            // 
            // pnStep
            // 
            this.pnStep.Controls.Add(this.lblFile);
            this.pnStep.Controls.Add(this.Panel2);
            this.pnStep.Controls.Add(this.btnStepFinish);
            this.pnStep.Controls.Add(this.btnStepCompare);
            this.pnStep.Controls.Add(this.btnStepImport);
            this.pnStep.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnStep.Location = new System.Drawing.Point(0, 0);
            this.pnStep.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnStep.Name = "pnStep";
            this.pnStep.Size = new System.Drawing.Size(1527, 32);
            this.pnStep.TabIndex = 1;
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.cmdNext);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel2.Location = new System.Drawing.Point(1277, 0);
            this.Panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(250, 32);
            this.Panel2.TabIndex = 9;
            // 
            // cmdNext
            // 
            this.cmdNext.Appearance.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.cmdNext.Appearance.Options.UseFont = true;
            this.cmdNext.Location = new System.Drawing.Point(4, 4);
            this.cmdNext.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(145)))));
            this.cmdNext.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.cmdNext.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdNext.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmdNext.Name = "cmdNext";
            this.cmdNext.Size = new System.Drawing.Size(86, 24);
            this.cmdNext.TabIndex = 0;
            this.cmdNext.Text = "Brown";
            this.cmdNext.Click += new System.EventHandler(this.cmdNext_Click);
            // 
            // btnStepFinish
            // 
            this.btnStepFinish.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnStepFinish.FlatAppearance.BorderSize = 0;
            this.btnStepFinish.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(208)))), ((int)(((byte)(213)))));
            this.btnStepFinish.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(208)))), ((int)(((byte)(213)))));
            this.btnStepFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStepFinish.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepFinish.ForeColor = System.Drawing.Color.DarkGray;
            this.btnStepFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStepFinish.Location = new System.Drawing.Point(233, 0);
            this.btnStepFinish.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStepFinish.Name = "btnStepFinish";
            this.btnStepFinish.Size = new System.Drawing.Size(85, 32);
            this.btnStepFinish.TabIndex = 8;
            this.btnStepFinish.Text = "Finish";
            this.btnStepFinish.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnStepFinish.UseVisualStyleBackColor = true;
            // 
            // btnStepCompare
            // 
            this.btnStepCompare.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnStepCompare.FlatAppearance.BorderSize = 0;
            this.btnStepCompare.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(208)))), ((int)(((byte)(213)))));
            this.btnStepCompare.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(208)))), ((int)(((byte)(213)))));
            this.btnStepCompare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStepCompare.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepCompare.ForeColor = System.Drawing.Color.DarkGray;
            this.btnStepCompare.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStepCompare.Location = new System.Drawing.Point(114, 0);
            this.btnStepCompare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStepCompare.Name = "btnStepCompare";
            this.btnStepCompare.Size = new System.Drawing.Size(119, 32);
            this.btnStepCompare.TabIndex = 7;
            this.btnStepCompare.Text = "Preview";
            this.btnStepCompare.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnStepCompare.UseVisualStyleBackColor = true;
            // 
            // btnStepImport
            // 
            this.btnStepImport.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnStepImport.FlatAppearance.BorderSize = 0;
            this.btnStepImport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(208)))), ((int)(((byte)(213)))));
            this.btnStepImport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(208)))), ((int)(((byte)(213)))));
            this.btnStepImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStepImport.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepImport.ForeColor = System.Drawing.Color.DarkGray;
            this.btnStepImport.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStepImport.Location = new System.Drawing.Point(0, 0);
            this.btnStepImport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStepImport.Name = "btnStepImport";
            this.btnStepImport.Size = new System.Drawing.Size(114, 32);
            this.btnStepImport.TabIndex = 6;
            this.btnStepImport.Text = "Import";
            this.btnStepImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnStepImport.UseVisualStyleBackColor = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // dlg
            // 
            this.dlg.FileName = "openFileDialog1";
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(403, 10);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(35, 13);
            this.lblFile.TabIndex = 10;
            this.lblFile.Text = "label3";
            this.lblFile.Visible = false;
            // 
            // frmEmployeeImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1527, 755);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.Name = "frmEmployeeImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import พนักงานใหม่";
            this.Load += new System.EventHandler(this.frmEmployeeImport_Load);
            this.pnTop.ResumeLayout(false);
            this.pnTop.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnMain.ResumeLayout(false);
            this.pnWait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            this.pnStep.ResumeLayout(false);
            this.pnStep.PerformLayout();
            this.Panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btClose;
        private System.Windows.Forms.Panel pnMain;
        private System.Windows.Forms.Panel pnWait;
        internal System.Windows.Forms.Label lblProgress;
        internal System.Windows.Forms.ProgressBar ProgressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraGrid.GridControl grdData;
        internal DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        internal System.Windows.Forms.Panel pnStep;
        internal System.Windows.Forms.Panel Panel2;
        internal DevExpress.XtraEditors.SimpleButton cmdNext;
        private System.Windows.Forms.Button btnStepFinish;
        public System.Windows.Forms.Button btnStepCompare;
        private System.Windows.Forms.Button btnStepImport;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog dlg;
        private System.Windows.Forms.Label lblFile;
    }
}